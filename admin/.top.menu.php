<?
$aMenuLinks = Array(
	Array(
		"Главная", 
		"/admin/", 
		Array(), 
		Array("class"=>"fa fa-desktop"), 
		"" 
	),
	Array(
		"Задачи", 
		"/admin/tasks/", 
		Array(), 
		Array("class"=>"fa fa-tasks"), 
		"" 
	),
	Array(
		"Продажи", 
		"/admin/sales/", 
		Array(), 
		Array("class"=>"glyphicon glyphicon-usd"), 
		"" 
	),	
	Array(
		"Доходы и расходы", 
		"/admin/balance/", 
		Array(), 
		Array("class"=>"fa fa-dollar"), 
		"" 
	),	
	Array(
		"Пользователи", 
		"/admin/users/", 
		Array(), 
		Array("class"=>"glyphicon glyphicon-user"), 
		"" 
	),	
	Array(
		"Персонал", 
		"/admin/personal/", 
		Array(), 
		Array("class"=>"fa fa-users"), 
		"" 
	),	
	Array(
		"Инвентарь", 
		"/admin/inventory/", 
		Array(), 
		Array("class"=>"glyphicon glyphicon-tag"), 
		"" 
	),	
	Array(
		"Товары", 
		"/admin/stock/", 
		Array(), 
		Array("class"=>"glyphicon glyphicon-shopping-cart"), 
		"" 
	),		
	Array(
		"Занятия", 
		"/admin/services/", 
		Array(), 
		Array("class"=>"glyphicon glyphicon-tasks"), 
		"" 
	),	
	Array(
		"Клубы и залы", 
		"/admin/clubs/", 
		Array(), 
		Array("class"=>"fa fa-building-o"), 
		"" 
	),
	Array(
		"Клиенты", 
		"/admin/clients/", 
		Array(), 
		Array("class"=>"fa fa-user"), 
		"" 
	),	
	Array(
		"Справочники", 
		"/admin/reference/", 
		Array(), 
		Array("class"=>"glyphicon glyphicon-book"), 
		"" 
	),
	Array(
		"Настройки", 
		"/admin/settings/", 
		Array(), 
		Array("class"=>"fa fa-gear"), 
		"" 
	)
);
?>