<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<? 
CModule::IncludeModule("iblock");
$res = CIBlockElement::GetList(Array(), Array("ID"=>IntVal($_POST['id'])),false,false,array('ID','NAME','PROPERTY_VALUE'));
while($ob = $res->GetNextElement())
{
 $arFields = $ob->GetFields();
?>
<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<h3>РЕДАКТИРОВАНИЕ НАСТРОЙКИ</h3>
				<form role="form" id="form_save" class="form-horizontal" method="post">
					<div class="row">
						<input type="hidden" value="<?=$arFields['ID']?>" name="id">
						<div class="col-lg-12">
							<div class="form-group" style="margin-top: 15px;">
								<label class="col-md-4 control-label">Название настройки:</label>
								<div class="col-md-8">
									<label class="col-md-12 control-label"><?=$arFields['NAME']?></label>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="form-group" style="margin-top: 15px;">
								<label class="col-md-4 control-label">Значение</label>
								<div class="col-md-8">
									<input type="text" name="value" id="value_settings" class="form-control" value="<?=$arFields['PROPERTY_VALUE_VALUE']?>">
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
				<button type="button" onclick="savesettings(<?=$arFields['ID']?>);" class="btn btn-primary">Сохранить</button>
			</div>
		</div>
</div>
<? } ?>