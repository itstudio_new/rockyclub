<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
$val_text = array();
?>
<? if (  $_POST['id'] != '' ) { ?>
	<?
		CModule::IncludeModule("main");	
		global $USER;
		$rsUser = CUser::GetByID($_POST['id']);
		$arUser = $rsUser->Fetch();
	?>
<? } ?>
<div class="modal-dialog">
        <div class="modal-content">
			<form role="form" id="add_edit_user_form" class="form-horizontal" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <h3><? if (  $_POST['id'] == '' ) { ?>Новый пользователь<? } else { ?>Редактирование пользователя<? } ?></h3>
                        <input type="hidden" name="user" value="<?=$_POST['id']?>">
                        <div class="row">
                            <div class="col-lg-12">
									<div class="form-group">
										<label class="col-md-3 control-label"><b style="color:red">*</b> Ф.И.О.</label>
										<div class="col-md-9">
											<input type="text" name="NAME" value="<?=$arUser['NAME']?>" class="validate[required] form-control required_field">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label"><b style="color:red">*</b> Роль</label>
										<div class="col-md-9">
											<select class="validate[required] form-control required_field" name="ROLE" id="data_role">
												<option value="">Не выбрано</option>
												<? $arGroups = CUser::GetUserGroup($arUser['ID']); ?>
<?
		$rsGroups = CGroup::GetList(($by="ID"), ($order="ASC"), array());
		while ($Groups = $rsGroups->Fetch()) 
		{
				if ( $Groups['ID'] != 2 and $Groups['ID'] != 3 and $Groups['ID'] != 4 ) {
?>												
												<option <? if ( in_array($Groups['ID'],$arGroups)) { ?>selected="selected"<? } ?> value="<?=$Groups['ID']?>"><?=$Groups['NAME']?></option>
				<? } ?>
		<? } ?>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label"><b style="color:red">*</b> Почта</label>
										<div class="col-md-9">
											<input type="text" name="EMAIL" value="<?=$arUser['EMAIL']?>" class="validate[required] form-control required_field">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label"><b style="color:red">*</b> Пароль</label>
										<div class="col-md-9">
											<div class="input-group" role="group">
												<input type="password" id="data_password" name="PASSWORD" value="" class="validate[required] form-control hiddenPasswordRepeat ">
												<span class="input-group-addon"><a style="color:white;" onclick="showpass('data_password'); return false;" href="#" class="seePassword"><i class="fa fa-eye"></i></a></span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label"><b style="color:red">*</b> Подтв. пароля</label>
										<div class="col-md-9">
											<div class="input-group" role="group">
												<input type="password" id="data_password_repeat" name="PASSWORD2" value="" class="validate[required] form-control hiddenPasswordRepeat ">
												<span class="input-group-addon"><a onclick="showpass('data_password_repeat'); return false;" style="color:white;" href="#" class="seePasswordRepeat"><i class="fa fa-eye"></i></a></span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label"><b style="color:red">*</b> Статус</label>
										<div class="col-md-9">
											<select class="validate[required] form-control required_field" name="ACTIVE" id="data_active">
												<option value="">Не выбрано</option>
												<option value="N" <? if ( $arUser['ACTIVE'] == 'N' ) { ?>selected="selected"<? } ?>>Заблокирован</option>
												<option value="Y" <? if ( $arUser['ACTIVE'] == 'Y' ) { ?>selected="selected"<? } ?>>Активен</option>
											</select>
										</div>
									</div>							
                                <div class="form-group">
                                    <label class="col-md-12 control-label"><b style="color:red">*</b> - Обязательные поля</label>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="button" onclick="saveform_user(); return false;" name="test" id="createButton" class="btn btn-primary"><? if (  $_POST['id'] == '' ) { ?>Создать<? } else { ?>Обновить<? } ?></button>
                </div>
			</form>            
        </div>
    </div>