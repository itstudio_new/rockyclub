<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	global $APPLICATION;
	$HIDE_MENU = $APPLICATION->get_cookie("HIDE_MENU");
	if ( $HIDE_MENU == 'Y' ) {
		$APPLICATION->set_cookie("HIDE_MENU", 'N', time()+60*60*24*30*12*2, "/");
	} else {
		$APPLICATION->set_cookie("HIDE_MENU", 'Y', time()+60*60*24*30*12*2, "/");
	}
?>