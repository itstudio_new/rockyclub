<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
$val_text = array();
?>
<? if (  $_POST['id'] != '' ) { ?>
	<?
		CModule::IncludeModule("iblock");	
		$res = CIBlockElement::GetList(Array(), Array("ID"=>$_POST['id']));
		while($ob = $res->GetNextElement()){ 
			$arFields = $ob->GetFields();  
			$arProps = $ob->GetProperties();
			$val_text = $arFields;
			$val_text['PROP'] = $arProps;
			
		}	
	?>
<? } ?>
<? require_once($_SERVER["DOCUMENT_ROOT"]."/admin/config/".$_POST['module'].".php");?>
<? $formname = explode("/",$config['NAME']); ?>
<div class="modal-dialog">
        <div class="modal-content">
			<form role="form" id="add_edit_form" class="form-horizontal" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <h3><? if (  $_POST['id'] == '' ) { ?>Новый <?=$formname[0]?><? } else { ?>Редактирование <?=$formname[1]?><? } ?></h3>
                        <input type="hidden" name="id_element" value="<?=$_POST['id']?>">

                        <div class="row">
                            <div class="col-lg-12">
<? foreach ( $config['FIELD'] as $value ) { ?>	
	<? if ( $value['TYPE'] != 'count' and $value['TYPE'] != 'hidden') { ?>
                                <div class="form-group">
								<? if ( !$value['VALUE'] or $value['VALUE'] == '' ) { ?>
                                    <label class="col-md-3 control-label"><? if ( $value['REQUIRED'] == 'Y' ) { ?><b style="color:red">*</b><? } ?> <?=$value['NAME']?></label>
								<? } ?>
                                    <div class="col-md-9">
									   <? if ( $value['TYPE'] == 'link' ) { ?>
											<select <? if ( $value['FIELD'] == 'CLUB' ) { ?>onchange="changeclub(this); return false;"<? }?> class="form-control filter <? if ( $value['REQUIRED'] == 'Y' ) { ?>required_field<? } ?><? if ( $value['FIELD'] != 'CLUB' ) { ?> linked<?  }?>" name="<?=$value['FIELD_TYPE']?>[<?=$value['FIELD']?>]">
													<option value=""></option>
												<?
												$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>$value['LINK']['IBLOCK'], "ACTIVE"=>"Y"),false,false,array('ID','NAME','PROPERTY_CLUB'));
												while($ob = $res->GetNextElement()){ 
												 $arFields = $ob->GetFields();  
												?>					
													<option <? if(  $arFields['PROPERTY_CLUB_VALUE'] != '' ) { ?>club="<?=$arFields['PROPERTY_CLUB_VALUE']?>"<? } ?> <? if ( $val_text['PROP'][$value['FIELD']]['VALUE'] == $arFields['ID'] ) { ?>selected<? } ?> value="<?=$arFields['ID']?>"><?=$arFields[$value['LINK']['FIELD']]?></option>
												<? } ?>						
											</select>
									   <? } elseif ( $value['TYPE'] == 'text' or $value['TYPE'] == 'entercount' ) { ?>
												 <? $text = ''; ?>
												 <? if (  $value['FIELD_TYPE'] == 'main') { ?>
													<? $text = $val_text[$value['FIELD']]; ?>
												 <? } elseif (  $value['FIELD_TYPE'] == 'prop') { ?>
													<? $text = $val_text['PROP'][$value['FIELD']]['VALUE']; ?>
												 <? } ?>
												<input type="text"  name="<?=$value['FIELD_TYPE']?>[<?=$value['FIELD']?>]" value="<?=$text?>" class="form-control <? if ( $value['REQUIRED'] == 'Y' ) { ?>required_field<? } ?>">
									   <?  } elseif ( $value['TYPE'] == 'textarea' ) { ?>
												<? $text = ''; ?>
												 <? if (  $value['FIELD_TYPE'] == 'main') { ?>
													<? $text = $val_text[$value['FIELD']]; ?>
												 <? } elseif (  $value['FIELD_TYPE'] == 'prop') { ?>
													<? $text = $val_text['PROP'][$value['FIELD']]['VALUE']; ?>
												 <? } ?>
												 <textarea type="text" name="<?=$value['FIELD_TYPE']?>[<?=$value['FIELD']?>]" class="form-control <? if ( $value['REQUIRED'] == 'Y' ) { ?>required_field<? } ?>"><?=$text?></textarea>
									   <?  } elseif ( $value['TYPE'] == 'img' ) { ?>
												 <input class="fileUpload" value="" name="<?=$value['FIELD_TYPE']?>[<?=$value['FIELD']?>]"  type="file">
									   <?  } elseif ( $value['TYPE'] == 'date' ) { ?>
												<? $text = ''; ?>
												 <? if (  $value['FIELD_TYPE'] == 'main') { ?>
													<? $text = $val_text[$value['FIELD']]; ?>
												 <? } elseif (  $value['FIELD_TYPE'] == 'prop') { ?>
													<? $text = $val_text['PROP'][$value['FIELD']]['VALUE']; ?>
												 <? } ?>
												 <input class="validate[required] datetimepicker form-control" value="<?=$text?>" name="<?=$value['FIELD_TYPE']?>[<?=$value['FIELD']?>]" id="data_date" type="text">
									   <?  } elseif ( $value['TYPE'] == 'users' ) { ?>
									     <? if ( $value['VALUE'] and $value['VALUE'] != '' ) { ?>
												<input type="hidden" name="<?=$value['FIELD_TYPE']?>[<?=$value['FIELD']?>]" value="<?=$value['VALUE']?>">
										 <? } else { ?>
											<select class="form-control filter <? if ( $value['REQUIRED'] == 'Y' ) { ?>required_field<? } ?>" name="<?=$value['FIELD_TYPE']?>[<?=$value['FIELD']?>]">
													<option value=""></option>
												<?
													$cUser = new CUser; 
													$sort_by = "ID";
													$sort_ord = "ASC";
													$arFilter = array();
													$dbUsers = $cUser->GetList($sort_by, $sort_ord, $arFilter);
													while ($arUser = $dbUsers->Fetch()) 
													{
												?>					
													<option <? if ( $val_text['PROP'][$value['FIELD']]['VALUE'] == $arUser['ID'] ) { ?>selected<? } ?> value="<?=$arUser['ID']?>"><?=$arUser['NAME']?></option>
												<? } ?>						
											</select>
										 <? } ?>
									   <?  } elseif ( $value['TYPE'] == 'select' ) { ?>
											<select class="form-control filter <? if ( $value['REQUIRED'] == 'Y' ) { ?>required_field<? } ?>" name="<?=$value['FIELD_TYPE']?>[<?=$value['FIELD']?>]">
												<option value=""></option>
												<?
													$i = 1;
													foreach ( $value['SELECT_VALUE'] as $index=>$val ) {
												?>					
													<option <? if ( $val_text['PROP'][$value['FIELD']]['VALUE'] == $index and $val_text['PROP'][$value['FIELD']]['VALUE'] != ''  ) { ?>selected<? } ?> value="<?=$index?>"><?=$val?></option>
													<? $i = $i + 1; ?>
												<? } ?>						
											</select>									   
									   <? } ?>
                                    </div>
                                </div>
	<? } elseif($value['TYPE'] == 'hidden') { ?>								
			<input type="hidden" name="<?=$value['FIELD_TYPE']?>[<?=$value['FIELD']?>]" id="<?=$value['FIELD_TYPE']?>_<?=$value['FIELD']?>" value="<?=$value['DEFAULT']?>" >
	<? } ?>
<?  } ?>								
                                <div class="form-group">
                                    <label class="col-md-12 control-label"><b style="color:red">*</b> - Обязательные поля</label>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="button" onclick="saveform(this,'<?=$_POST['module']?>'); return false;" name="test" id="createButton" class="btn btn-primary"><? if (  $_POST['id'] == '' ) { ?>Создать<? } else { ?>Обновить<? } ?></button>
                </div>
            </form>
        </div>
    </div>