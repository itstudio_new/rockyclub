<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	CModule::IncludeModule("iblock");
	$res = CIBlockElement::GetByID($_POST['id']);
	$ar_res = $res->GetNext();
?>
<div class="mb-container">
	<div class="mb-middle">
		<div class="mb-title"><span class="fa fa-times"></span> Внимание!</div>
		<div class="mb-content">
			<p>Вы действительно хотите удалить <?=$ar_res['NAME']?> ?</p>
		</div>
		<div class="mb-footer">
			<button class="btn btn-default btn-lg pull-right mb-control-close" onclick="$('#dellbox').modal('hide');" style="margin-left: 10px;">Нет</button>
			<form role="form" method="POST" action="#" enctype="multipart/form-data">
				<button type="button" onclick="delitem(<?=$ar_res['ID']?>);" class="btn btn-default btn-lg pull-right">Да</button>
			</form>
		</div>
	</div>
</div>