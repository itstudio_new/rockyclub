<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("ВХОД в Rocky Club");
?>
<? global $USER;
if ($USER->IsAuthorized()) { ?>
	<?
		LocalRedirect("/admin/main/"); 
		exit;
	?>
<? } else { ?>
	<form class="form-horizontal" method="post">
		<div class="form-group">
			<div class="col-md-12">
				<input class="form-control" placeholder="Электронная почта" id="auth_login_small" name="email" style="color: #000000" type="text">
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<input class="form-control" placeholder="Пароль" name="password" id="auth_password_small" style="color: #000000" type="password">
			</div>
		</div>
		<div class="form-group" id="auth_info" style="color: red;">
		
		</div>
		<div class="form-group">
			<div class="col-md-4">
				<a href="/" class="btn btn-link btn-block">
					<span class="fa fa-angle-left"></span>На главную
				</a>
			</div>
			<div class="col-md-4"  >
			</div>
			<div class="col-md-4">
				<button class="btn btn-info btn-block" onclick="authusersmall(); return false();" type="button">Войти</button>
			</div>
		</div>
		<div class="clearfix"></div>
	</form>
<?}?>
<script>
	function authusersmall() {
			if ( $('#auth_login_small').val() != '' && $('#auth_password_small').val() != '' ) {
				$.post( "/admin/includes/userauth.php", { user: $('#auth_login_small').val(), pass: $('#auth_password_small').val()},
				function( data ) {
					if ( data == 0 ) {
						$('#auth_info').html('Имя пользователя введено не верно');
					} else if ( data == 2 ) {
						$('#auth_info').html('Пароль введён не верно');
					} else {
						location.reload(true);
					}				
				});				
			} else {
				$('#auth_info').html('Имя пользователя или пароль введен не верно');
			}
	}
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>