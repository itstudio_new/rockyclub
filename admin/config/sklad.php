<?
$config = array(
	'IBLOCK_ID'=> 26,
	'IBLOCK_TYPE'=> 'references',
	'NAME'=> '/',
	'COLOR' => 'sklad',
	'SAVE_PAGE'=> 'Y',
	'NEW_BUTTON' => array(
		'BALANS'=> array('ICON'=> 'fa fa-book','URL'=>'/admin/stock/sklad/historysklad/?sklad=#ID#','NAME'=>'Изменения по товару'),
	),
	'MODULE'=> pathinfo(__FILE__),
	'FIELD' =>	array(
					0 => array(
						'NAME'=> 'Название',
						'TYPE'=> 'hidden',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'main',  //prop //main
						'FIELD'=> 'NAME',
						'DEFAULT'=> 'товар на складе',
						'SHOW_TIBLE'=>'N',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
					1 => array(
						'NAME'=> 'Клуб ',
						'TYPE'=> 'link',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'CLUB',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'Y',		//Y , N
						'LINK' => array(
							'IBLOCK'=> 2,
							'FIELD_TYPE'=>'main',   //prop //main
							'FIELD'=>'NAME',
						),
					),
					2 => array(
						'NAME'=> 'Продукт ',
						'TYPE'=> 'link',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'PRODUCT',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'Y',		//Y , N
						'LINK' => array(
							'IBLOCK'=> 14,
							'FIELD_TYPE'=>'main',   //prop //main
							'FIELD'=>'NAME',
						),
					),	
					3 => array(
						'NAME'=> 'Цена',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'PRICE',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),	
					4 => array(
						'NAME'=> 'Количество',
						'TYPE'=> 'entercount',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'COUNT',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),					
					5 => array(
						'NAME'=> 'Оптимальное количество',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'COUNT_OPTIMAL',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
	)
);	
?>