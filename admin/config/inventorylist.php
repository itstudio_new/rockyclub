<?
$config = array(
	'IBLOCK_ID'=> 24,
	'IBLOCK_TYPE'=> 'references',
	'NAME'=> '/',
	'MODULE'=> pathinfo(__FILE__),
	'FIELD' =>	array(
					0 => array(
						'NAME'=> 'Название',
						'TYPE'=> 'hidden',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'main',  //prop //main
						'FIELD'=> 'NAME',
						'DEFAULT'=> 'Инвентарь',
						'SHOW_TIBLE'=>'N',		//Y , N
						'REQUIRED'=> 'N',		//Y , N
						'ALL_FILTER'=>'Y',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
					1 => array(
						'NAME'=> 'Клуб ',
						'TYPE'=> 'link',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'CLUB',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'Y',		//Y , N
						'LINK' => array(
							'IBLOCK'=> 2,
							'FIELD_TYPE'=>'main',   //prop //main
							'FIELD'=>'NAME',
						),
					),
					2 => array(
						'NAME'=> 'Инвентарь',
						'TYPE'=> 'link',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'INVENTAR',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> 12,
							'FIELD_TYPE'=>'main',   //prop //main
							'FIELD'=>'NAME',
						),
					),	
					3 => array(
						'NAME'=> 'Состояние',
						'TYPE'=> 'link',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'SOSTOYANIE',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'Y',		//Y , N
						'LINK' => array(
							'IBLOCK'=> 10,
							'FIELD_TYPE'=>'main',   //prop //main
							'FIELD'=>'NAME',
						),
					),
					4 => array(
						'NAME'=> 'Ответственный',
						'TYPE'=> 'link',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'USER_ID',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'Y',		//Y , N
						'LINK' => array(
							'IBLOCK'=> 17,
							'FIELD_TYPE'=>'main',   //prop //main
							'FIELD'=>'NAME',
						),
					),
					5 => array(
						'NAME'=> 'Инвентарный номер',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'NOMER',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'Y',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
					6 => array(
						'NAME'=> 'Цена',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'COST',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'Y',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),	
					7 => array(
						'NAME'=> 'Количество',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'COUNT',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),	
					8 => array(
						'NAME'=> 'Изображение',
						'TYPE'=> 'img',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'main',  //prop //main
						'FIELD'=> 'PREVIEW_PICTURE',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'N',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),					
/*					
					2 => array(
						'NAME'=> 'Тип ',
						'TYPE'=> 'select',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'SELECT_VALUE' => array(0 =>'Ограниченный',1=>'Безлимитный'),
						'FIELD'=> 'TYPE',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'Y',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
					3 => array(
						'NAME'=> 'Количество дней',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'DAY',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
					4 => array(
						'NAME'=> 'Количество посещений',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'VISIT',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'N',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
					5 => array(
						'NAME'=> 'Цена',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'COST',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
*/					
	)
);	
?>