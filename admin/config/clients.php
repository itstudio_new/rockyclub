<?
$config = array(
	'IBLOCK_ID'=> 16,
	'IBLOCK_TYPE'=> 'references',
	'NAME'=> 'клиент/клиента',
	'NEW_BUTTON' => array(
		'BALANS'=> array('ICON'=> 'fa fa-ruble','URL'=>'/admin/clients/balans/?user=#ID#','NAME'=>'Баланс'),
		'ABONEMENT'=> array('ICON'=> 'fa fa-list-alt','URL'=>'/admin/clients/abonement/?user=#ID#','NAME'=>'Абонементы'),
	),
	'MODULE'=> pathinfo(__FILE__),
	'FIELD' =>	array(
					0 => array(
						'NAME'=> 'Название',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'main',  //prop //main
						'FIELD'=> 'NAME',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'Y',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
					1 => array(
						'NAME'=> 'Пол ',
						'TYPE'=> 'select',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'SELECT_VALUE' => array('Муж' =>'Муж','Жен'=>'Жен'),
						'FIELD'=> 'SEX',
						'SHOW_TIBLE'=>'N',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),					
					2 => array(
						'NAME'=> 'Клуб ',
						'TYPE'=> 'link',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'CLUB',
						'SHOW_TIBLE'=>'N',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'Y',		//Y , N
						'LINK' => array(
							'IBLOCK'=> 2,
							'FIELD_TYPE'=>'main',   //prop //main
							'FIELD'=>'NAME',
						),
					),
					3 => array(
						'NAME'=> 'Адрес',
						'TYPE'=> 'textarea',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'main',  //prop //main
						'FIELD'=> 'PREVIEW_TEXT',
						'SHOW_TIBLE'=>'N',		//Y , N
						'REQUIRED'=> 'N',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),	
					4 => array(
						'NAME'=> 'Фото',
						'TYPE'=> 'img',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'main',  //prop //main
						'FIELD'=> 'PREVIEW_PICTURE',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'N',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
					5 => array(
						'NAME'=> 'Дата рождения',
						'TYPE'=> 'date',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'BIRTHDATE',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
					6 => array(
						'NAME'=> 'Телефон',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'PHONE',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),	
					7 => array(
						'NAME'=> 'Статус ',
						'TYPE'=> 'link',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'STATUS',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'Y',		//Y , N
						'LINK' => array(
							'IBLOCK'=> 8,
							'FIELD_TYPE'=>'main',   //prop //main
							'FIELD'=>'NAME',
						),
					),	
					8 => array(
						'NAME'=> 'E-mail',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'EMAIL',
						'SHOW_TIBLE'=>'N',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),	
					9 => array(
						'NAME'=> 'Реферал ',
						'TYPE'=> 'link',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'REFERAL',
						'SHOW_TIBLE'=>'N',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> 6,
							'FIELD_TYPE'=>'main',   //prop //main
							'FIELD'=>'NAME',
						),
					),
					10 => array(
						'NAME'=> 'Скидка',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'DISCONT',
						'SHOW_TIBLE'=>'N',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),	
					11 => array(
						'NAME'=> 'Штрихкод карты',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'CARD_ID',
						'SHOW_TIBLE'=>'N',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
					12 => array(
						'NAME'=> 'Номер карты',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'CARD_NUM',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),	
					13 => array(
						'NAME'=> 'Описание',
						'TYPE'=> 'textarea',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'main',  //prop //main
						'FIELD'=> 'DETAIL_TEXT',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'N',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),	
					14 => array(
						'NAME'=> 'Привязать к другому клиенту',
						'TYPE'=> 'link',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'PARENT',
						'SHOW_TIBLE'=>'N',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> 16,
							'FIELD_TYPE'=>'main',   //prop //main
							'FIELD'=>'NAME',
						),
					),
					15 => array(
						'NAME'=> 'Договор подписан ',
						'TYPE'=> 'select',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'SELECT_VALUE' => array('0' =>'Да','1'=>'Нет'),
						'FIELD'=> 'SEX',
						'SHOW_TIBLE'=>'N',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
	)
);	
?>