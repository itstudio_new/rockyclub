<?
$config = array(
	'IBLOCK_ID'=> 3,
	'IBLOCK_TYPE'=> 'references',
	'NAME'=> 'город/города',
	'MODULE'=> pathinfo(__FILE__),
	'FIELD' =>	array(
					0 => array(
						'NAME'=> 'Название',
						'TYPE'=> 'hidden',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'main',  //prop //main
						'FIELD'=> 'NAME',
						'DEFAULT'=> 'доход/расход'
						'SHOW_TIBLE'=>'N',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'Y',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
					1 => array(
						'NAME'=> 'Клуб',
						'TYPE'=> 'link',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'CLUB',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'Y',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> 2,
							'FIELD_TYPE'=>'main',   //prop //main
							'FIELD'=>'NAME',
						),
					),					
					1 => array(
						'NAME'=> 'Сумма',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'SUMMA',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'Y',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
					2 => array(
						'NAME'=> 'Дата',
						'TYPE'=> 'date',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'DATA',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'Y',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
										
				)
);	
?>