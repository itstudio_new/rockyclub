<?
$config = array(
	'IBLOCK_ID'=> 27,
	'IBLOCK_TYPE'=> 'references',
	'NAME'=> '/',
	'COLOR' => '',
	'NO_BUTTON' => 'Y',
	'FILTER' => 'off',
	'BACK_HISTORYSKLAD' => 'N',
	'SAVE_PAGE'=> 'N',
	'MODULE'=> pathinfo(__FILE__),
	'FIELD' =>	array(
					0 => array(
						'NAME'=> 'Дата',
						'TYPE'=> 'date',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'DATA',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),	
					1 => array(
						'NAME'=> 'Название',
						'TYPE'=> 'hidden',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'main',  //prop //main
						'FIELD'=> 'NAME',
						'DEFAULT'=> 'товар на складе',
						'SHOW_TIBLE'=>'N',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
					2 => array(
						'NAME'=> 'Клуб ',
						'TYPE'=> 'link',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'CLUB',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'Y',		//Y , N
						'LINK' => array(
							'IBLOCK'=> 2,
							'FIELD_TYPE'=>'main',   //prop //main
							'FIELD'=>'NAME',
						),
					),
					3 => array(
						'NAME'=> 'Продукт ',
						'TYPE'=> 'link',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'PRODUCT',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'Y',		//Y , N
						'LINK' => array(
							'IBLOCK'=> 14,
							'FIELD_TYPE'=>'main',   //prop //main
							'FIELD'=>'NAME',
						),
					),	
					4 => array(
						'NAME'=> 'Цена',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'PRICE',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),	
					5 => array(
						'NAME'=> 'Количество',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'COUNT',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),					
					6 => array(
						'NAME'=> 'Оптимальное количество',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'COUNT_OPTIMAL',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),
					7 => array(
						'NAME'=> 'Товар добавлен',
						'TYPE'=> 'select',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'ADD',
						'SELECT_VALUE' => array('0'=>'Нет','1'=>'Да'),
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'Y',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),	
					8 => array(
						'NAME'=> 'Товар удалён',
						'TYPE'=> 'select',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'DELETED',
						'SELECT_VALUE' => array('0'=>'Нет','1'=>'Да'),
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'Y',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),					
					9 => array(
						'NAME'=> 'Пользователь',
						'TYPE'=> 'users',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'prop',  //prop //main
						'FIELD'=> 'USER_ID',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'Y',		//Y , N
						'ALL_FILTER'=>'N',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'VALUE'=> $USER->GetID(),
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),	
					10 => array(
						'NAME'=> 'Комментарий',
						'TYPE'=> 'text',		//link  //count //img  //files //text
						'FIELD_TYPE'=> 'main',  //prop //main
						'FIELD'=> 'PREVIEW_TEXT',
						'SHOW_TIBLE'=>'Y',		//Y , N
						'REQUIRED'=> 'N',		//Y , N
						'ALL_FILTER'=>'Y',		//Y , N
						'FILTER'=> 'N',		//Y , N
						'LINK' => array(
							'IBLOCK'=> '',
							'FIELD_TYPE'=>'',   //prop //main
							'FIELD'=>'',
						),
					),					
	)
);	
?>