<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Пользователи");
?>
    <div class="row-content">

        <div class="panel-body">
			<div class="form-group">
				<a class="btn btn-primary" href="#" onclick="add_edit_user(); return false;" style="margin-left: 15px;">
					<span class="fa fa-plus-square"></span>ДОБАВИТЬ
				</a>
			</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
<?
global $APPLICATION;
$dir = $APPLICATION->GetCurDir();
?>		
<form method="get" action="" id="filter" >
                            <div class="row">
                                    <div class="col-sm-2">
                                        <input class="form-control filter" placeholder="Ф.И.О." name="filter[name]" value="<?=$_GET['filter']['name']?>" type="text">                                    
									</div>
                                    <div class="col-sm-2">
											<select class="form-control required_field" name="filter[role]" id="data_role">
												<option value="">Не выбрано</option>
												<? $arGroups = CUser::GetUserGroup($arUser['ID']); ?>
												<?
														$rsGroups = CGroup::GetList(($by="ID"), ($order="ASC"), array());
														while ($Groups = $rsGroups->Fetch()) 
														{
																if ( $Groups['ID'] != 2 and $Groups['ID'] != 3 and $Groups['ID'] != 4 ) {
												?>												
																		<option <? if ( $_GET['filter']['role'] == $Groups['ID']) { ?>selected="selected"<? } ?> value="<?=$Groups['ID']?>"><?=$Groups['NAME']?></option>
																<? } ?>
														<? } ?>
											</select>                                 
									</div>
                                    <div class="col-sm-2">
                                        <select class="form-control required_field" name="filter[active]" id="data_active">
												<option value="">Не выбрано</option>
												<option value="N" <? if ( $_GET['filter']['active'] == 'N' ) { ?>selected="selected"<? } ?>>Заблокирован</option>
												<option value="Y" <? if ( $_GET['filter']['active'] == 'Y' ) { ?>selected="selected"<? } ?>>Активен</option>
										</select>
									</div>
                      				<a class="btn btn-default" href="#" onclick='$("#filter").submit(); return false;' style="margin-left: 15px;">
											Фильтровать
									</a>
									<a href="<?=$dir?>" title="Сброс фильтра" class="btn btn-danger btn-condensed"><i class="fa fa-times"></i></a>
                            </div>
                        </form>   
<? if ( $_GET['filter']['CLUB'] != '' ) { ?>
		<script>
			changeclubfilter($('#club_select'));
		</script>
<? } ?>						
                    </div>
                    <div class="panel-body panel-body-table">
                        <div class="table-responsive">
                            <div class="dataTables_wrapper no-footer" id="table_wrap">
                            <table class="table table-bordered table-striped table-actions">
									<thead>
									<tr>
											<th>
												Ф.И.О.
											</th>
											<th>
												Роль
											</th>
											<th>
												Почта
											</th>
											<th>
												Статус
											</th>
											
										<th width="120">Действия</th>
									</tr>
									</thead>
									<tbody>
<?
	$filter = array();
	if ( $_GET['filter']['name'] != '' ) {
		$filter['NAME'] = $_GET['filter']['name'];
	}
	if ( $_GET['filter']['role'] != '' ) {
		$filter['GROUPS_ID'] = $_GET['filter']['role'];
	}
	if ( $_GET['filter']['active'] != '' ) {
		$filter['ACTIVE'] = $_GET['filter']['active'];
	}
	$rsUsers = CUser::GetList(($by="id"), ($order="desc"), $filter);
	$rsUsers->NavStart(10);
	
	while ($arUser = $rsUsers->Fetch()) 
    {
?>								
										<tr>
											<td><strong><?=$arUser['NAME']?></strong></td>
											<td>
												<? $arGroups = CUser::GetUserGroup($arUser['ID']); ?>
												<? if ( in_array('1',$arGroups)) { ?>
														Администратор
												<? }elseif ( in_array('5',$arGroups)) { ?>
														Тренер
												<? }elseif ( in_array('6',$arGroups)) { ?>
														Управляющий
												<? }elseif ( in_array('7',$arGroups)) { ?>
														Администратор сайта
												<? }elseif ( in_array('8',$arGroups)) { ?>
														Администратор зала
												<? } ?>
											</td>
											<td><?=$arUser['EMAIL']?></td>
											<td><?if ($arUser['ACTIVE'] == 'Y' ) { echo 'активен';} else { echo 'заблокирован';} ?></td>
											<td>
												<a href="#" class="btn btn-default btn-condensed" title="Редактировать" onclick="add_edit_user(<?=$arUser['ID']?>); return false;" >
															<span class="fa fa-pencil"></span>
												</a>
												<button type="button" class="mb-control btn btn-danger btn-condensed" onclick="dellboxuser(<?=$arUser['ID']?>); return false;" title="Удалить">
													<i class="fa fa-trash-o"></i>
												</button>

											</td>
										</tr>
	<? } ?>										
									</tbody>
								</table>
								<?=$rsUsers->GetPageNavStringEx($navComponentObject,'','round','Y');?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
<style>
	.errors {
		border: 1px solid red;
		background: #fde4e4 none repeat scroll 0 0;
	}
</style>	
	<div class="modal" id="add_edit_club" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" style="display: none;"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>