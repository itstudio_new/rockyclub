<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
$val_text = array();
?>
<? if (  $_POST['id'] != '' ) { ?>
	<?
		CModule::IncludeModule("iblock");	
		$res = CIBlockElement::GetList(Array(), Array("ID"=>$_POST['id']));
		while($ob = $res->GetNextElement()){ 
			$arFields = $ob->GetFields();  
			$arProps = $ob->GetProperties();
			$val_text = $arFields;
			$val_text['PROP'] = $arProps;
			
		}	
	?>
<? } ?>
<div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="add_edit_club_form" class="form-horizontal" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <h3><? if (  $_POST['id'] == '' ) { ?>Новый клуб<? } else { ?>Редактирование клуба<? } ?></h3>
                        <input type="hidden" name="club_id" value="<?=$_POST['id']?>">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><b style="color:red">*</b> Название</label>
                                    <div class="col-md-9">
                                        <input type="text" name="name" value="<?=$val_text['NAME']?>" class="validate[required] form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><b style="color:red">*</b> Город</label>
                                    <div class="col-md-9">
										<select data-name="city_id" class="form-control filter" name="city_id" id="filter_city_id">
											<option value="">Город</option>
												<?
												$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y"),false,false,array('ID','NAME'));
												while($ob = $res->GetNextElement()){ 
												 $arFields = $ob->GetFields();  
												?>					
													<option <? if ( $val_text['PROP']['CITY']['VALUE'] == $arFields['ID'] ) { ?>selected<? } ?> value="<?=$arFields['ID']?>"><?=$arFields['NAME']?></option>
												<? } ?>						
										</select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Описание</label>
                                    <div class="col-md-9">
                                        <textarea type="text" name="description" class="form-control"><?=str_replace("<br />","",htmlspecialchars_decode($val_text['PREVIEW_TEXT']))?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Адрес</label>
                                    <div class="col-md-9">
                                        <textarea type="text" name="address" class="form-control"><?=$val_text['PROP']['ADRESS']['VALUE']?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">График работы</label>
                                    <div class="col-md-9">
                                        <textarea type="text" name="schedule" class="form-control"><?=$val_text['PROP']['RASPISANIE']['VALUE']['TEXT']?></textarea>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="col-md-3 control-label">Телефоны</label>
                                    <div class="col-md-9">
                                        <textarea type="text" name="phones" class="form-control"><?=$val_text['PROP']['PHONE']['VALUE']?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><b style="color:red">*</b> E-mail</label>
                                    <div class="col-md-9">
                                        <input type="text" name="email" value="<?=$val_text['PROP']['EMAIL']['VALUE']?>" class="validate[required] form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12 control-label"><b style="color:red">*</b> - Обязательные поля</label>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="button" onclick="saveform_club('<?=$_POST['id']?>'); return false;" name="test" id="createButton" class="btn btn-primary"><? if (  $_POST['id'] == '' ) { ?>Создать<? } else { ?>Обновить<? } ?></button>
                </div>
            </form>
        </div>
    </div>