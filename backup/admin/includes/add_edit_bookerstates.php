<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
$val_text = array();
?>
<? if (  $_POST['id'] != '' ) { ?>
	<?
		CModule::IncludeModule("iblock");	
		$res = CIBlockElement::GetList(Array(), Array("ID"=>$_POST['id']));
		while($ob = $res->GetNextElement()){ 
			$arFields = $ob->GetFields();  
			$arProps = $ob->GetProperties();
			$val_text = $arFields;
			$val_text['PROP'] = $arProps;
			
		}	
	?>
<? } ?>
<div class="modal-dialog">
        <div class="modal-content">
			<form role="form" id="add_edit_bookerstates_form" class="form-horizontal" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <h3><? if (  $_POST['id'] == '' ) { ?>Новая статья<? } else { ?>Редактирование статьи<? } ?></h3>
                        <input type="hidden" name="bookerstates_id" value="<?=$_POST['id']?>">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><b style="color:red">*</b> Название</label>
                                    <div class="col-md-9">
                                        <input type="text" name="name" value="<?=$val_text['NAME']?>" class="validate[required] form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><b style="color:red">*</b> Клуб</label>
                                    <div class="col-md-9">
                                        <select data-name="club_id" class="form-control filter" name="club_id" id="filter_city_id">
											<option value=""></option>
												<?
												$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>2, "ACTIVE"=>"Y"),false,false,array('ID','NAME'));
												while($ob = $res->GetNextElement()){ 
												 $arFields = $ob->GetFields();  
												?>					
													<option <? if ( $val_text['PROP']['CLUB']['VALUE'] == $arFields['ID'] ) { ?>selected<? } ?> value="<?=$arFields['ID']?>"><?=$arFields['NAME']?></option>
												<? } ?>						
										</select>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="col-md-12 control-label"><b style="color:red">*</b> - Обязательные поля</label>
                                </div>
                            </div>
                        </div>
                </div>
			</form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="button" onclick="saveform_bookerstates('<?=$_POST['id']?>'); return false;" name="test" id="createButton" class="btn btn-primary"><? if (  $_POST['id'] == '' ) { ?>Создать<? } else { ?>Обновить<? } ?></button>
                </div>
            
        </div>
    </div>