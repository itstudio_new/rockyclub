<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
$val_text = array();
?>
<? if (  $_POST['id'] != '' ) { ?>
	<?
		CModule::IncludeModule("iblock");	
		$res = CIBlockElement::GetList(Array(), Array("ID"=>$_POST['id']));
		while($ob = $res->GetNextElement()){ 
			$arFields = $ob->GetFields();  
			$arProps = $ob->GetProperties();
			$val_text = $arFields;
			$val_text['PROP'] = $arProps;
			
		}	
	?>
<? } ?>
<div class="modal-dialog">
        <div class="modal-content">
			<form role="form" id="add_edit_city_form" class="form-horizontal" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <h3><? if (  $_POST['id'] == '' ) { ?>Новый город<? } else { ?>Редактирование города<? } ?></h3>
                        <input type="hidden" name="city_id" value="<?=$_POST['id']?>">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><b style="color:red">*</b> Название</label>
                                    <div class="col-md-9">
                                        <input type="text" name="name" value="<?=$val_text['NAME']?>" class="validate[required] form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><b style="color:red">*</b> Контактное лицо</label>
                                    <div class="col-md-9">
                                        <input type="text" name="contact" value="<?=$val_text['PROP']['NAME']['VALUE']?>" class="validate[required] form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">E-mail</label>
                                    <div class="col-md-9">
                                        <input type="text" name="email" value="<?=$val_text['PROP']['EMAIL']['VALUE']?>" class="validate[required] form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Телефон</label>
                                    <div class="col-md-9">
                                        <input type="text" name="phone" value="<?=$val_text['PROP']['PHONE']['VALUE']?>" class="validate[required] form-control">
                                    </div>
                                </div>
								 <div class="form-group">
                                    <label class="col-md-3 control-label">Должность</label>
                                    <div class="col-md-9">
                                        <input type="text" name="dolznost" value="<?=$val_text['PROP']['DOLZNOST']['VALUE']?>" class="validate[required] form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12 control-label"><b style="color:red">*</b> - Обязательные поля</label>
                                </div>
                            </div>
                        </div>
                </div>
			</form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="button" onclick="saveform_city('<?=$_POST['id']?>'); return false;" name="test" id="createButton" class="btn btn-primary"><? if (  $_POST['id'] == '' ) { ?>Создать<? } else { ?>Обновить<? } ?></button>
                </div>
            
        </div>
    </div>