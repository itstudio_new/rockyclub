<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="form-group">
	<a class="btn btn-primary" href="#" onclick="add_edit_bookerstates(); return false;" style="margin-left: 15px;">
		<span class="fa fa-plus-square"></span>ДОБАВИТЬ
	</a>
</div>
<div class="row">
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="row">
				<div class="col-sm-2">
					<input data-name="task" class="form-control filter" placeholder="ПОИСК" value="" name="filter[task]" id="filter_task" type="text">
				</div>
				<a class="btn btn-default " href="#" data-toggle="modal" data-target="#newEntryModal" style="margin-left: 15px;">
						Фильтровать
				</a>
				<a href="#" title="Сброс фильтра" class="btn btn-danger btn-condensed"><i class="fa fa-times"></i></a>
			</div>
		</div>
		<div class="panel-body panel-body-table">
			<div class="table-responsive">
				<div class="dataTables_wrapper no-footer" id="table_wrap">
								<table class="table table-bordered table-striped table-actions">
									<thead>
										<tr>
											<th>
												Название                
											</th>
											<th>
												Клуб
											</th>
											<th width="120">Действия</th>
										</tr>
									</thead>
									<tbody>					
										<?foreach($arResult["ITEMS"] as $arItem):?>
											<?
											$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
											$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
											if ( $arItem['PROPERTIES']['CLUB']['VALUE'] != '' ) {
												$res = CIBlockElement::GetByID($arItem['PROPERTIES']['CLUB']['VALUE']);
												$ar_res = $res->GetNext();
											} else {
												$ar_res['NAME'] = '-';
											}
											?>
												<tr id="<?=$this->GetEditAreaId($arItem['ID']);?>" >
													<td><strong id="club_name_<?=$arItem['ID']?>" ><?=$arItem['NAME']?></strong></td>
													<td id="club_name1_<?=$arItem['ID']?>" ><?=$ar_res['NAME']?></td>
													<td>
														<a href="#" class="btn btn-default btn-condensed" title="Редактировать" onclick="add_edit_bookerstates(<?=$arItem['ID']?>); return false;" >
															<span class="fa fa-pencil"></span>
														</a>
														<button type="button" class="mb-control btn btn-danger btn-condensed" title="Удалить" onclick="dellbox(<?=$arItem['ID']?>); return false;">
															<i class="fa fa-trash-o"></i>
														</button>
													</td>
												</tr>
										<?endforeach;?>
									</tbody>
								</table>	
								<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
									<?=$arResult["NAV_STRING"]?>
								<?endif;?>									
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
<div class="modal" id="add_edit_club" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" style="display: none;"></div>