<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
$csvFile = new CCSVData('R', true);
$csvFile->LoadFile($_SERVER["DOCUMENT_ROOT"].'/parser/files/tbl_inventory_list.csv');
$csvFile->SetFirstHeader();
$csvFile->SetDelimiter('~');
while ($arRes = $csvFile->Fetch()) {
	$el = new CIBlockElement;
	$PROP = array();
	$PROP['OLD_ID'] = $arRes[0];
	$res = CIBlockElement::GetList(Array(), array('IBLOCK_ID'=>12,'PROPERTY_OLD_ID'=>$arRes[1]), false, false, array('NAME','ID'));
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$PROP['INVENTAR'] = $arFields['ID'];
	}
	$res = CIBlockElement::GetList(Array(), array('IBLOCK_ID'=>10,'PROPERTY_OLD_ID'=>$arRes[2]), false, false, array('NAME','ID'));
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$PROP['SOSTOYANIE'] = $arFields['ID'];
	}
	$res = CIBlockElement::GetList(Array(), array('IBLOCK_ID'=>17,'PROPERTY_OLD_ID'=>$arRes[3]), false, false, array('NAME','ID'));
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$PROP['USER_ID'] = $arFields['ID'];
	}	
	$PROP['NOMER'] = $arRes[4];
	$PROP['COST'] = $arRes[5];
	$PROP['COUNT'] = $arRes[8];
	$PROP['IMAGE_ID'] = $arRes[7];
	
	$res = CIBlockElement::GetList(Array(), array('IBLOCK_ID'=>2,'PROPERTY_OLD_ID'=>$arRes[6]), false, false, array('NAME','ID'));
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$PROP['CLUB'] = $arFields['ID'];
	}	
	$arLoadProductArray = Array(
	  "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
	  "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
	  "IBLOCK_ID"      => 24,
	  "PROPERTY_VALUES"=> $PROP,
	  "NAME"           => 'Инвентарь',
	  "ACTIVE"         => "Y",            // активен
	);	
	if($PRODUCT_ID = $el->Add($arLoadProductArray))
	  echo "New ID: ".$PRODUCT_ID."<br/>";
	else
	  echo "Error: ".$el->LAST_ERROR."<br/>";	
}
?>