<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
$csvFile = new CCSVData('R', true);
$csvFile->LoadFile($_SERVER["DOCUMENT_ROOT"].'/parser/files/tbl_balance.csv');
$csvFile->SetFirstHeader();
$csvFile->SetDelimiter('~');
while ($arRes = $csvFile->Fetch()) {
		$el = new CIBlockElement;
		$PROP = array();
		$PROP['OLD_ID'] = $arRes[0];
		$res = CIBlockElement::GetList(Array(), array('IBLOCK_ID'=>2,'PROPERTY_OLD_ID'=>$arRes[1]), false, false, array('NAME','ID'));
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$PROP['CLUB'] = $arFields['ID'];
		}	
		$res = CIBlockElement::GetList(Array(), array('IBLOCK_ID'=>5,'PROPERTY_OLD_ID'=>$arRes[2]), false, false, array('NAME','ID'));
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$PROP['STAT'] = $arFields['ID'];
		}		
		$dat = explode("-",$arRes[3]);
		$PROP['DATA'] = mktime(0,0,0,$dat[1],$dat[2],$dat[0]);
		if ( $arRes[5] < 0 ) {
			$PROP['SUMMA'] = str_replace("-",'',$arRes[5]);
			$PROP['OPERATION'] = '-';
		} else {
			$PROP['SUMMA'] = $arRes[5];
			$PROP['OPERATION'] = '+';
		}
		$filter = Array
		(
			"PERSONAL_NOTES"     => $arRes[6],
		);
		$rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $filter); // выбираем пользователей
		while($rsUsers->NavNext(true, "f_")) :
			$PROP['USER_ID'] = $f_ID;
		endwhile;
		if ($arRes[8] == 'cash' ) {
			$PROP['TYPE'] = 1;	
		} else {
			$PROP['TYPE'] = 0;	
		}
	$arLoadProductArray = Array(
	  "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
	  "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
	  "IBLOCK_ID"      => 19,
	  "PROPERTY_VALUES"=> $PROP,
	  "NAME"           => 'доход/расход',
	  "ACTIVE"         => "Y",            // активен
	  "PREVIEW_TEXT"   => $arRes[4],
	  "DETAIL_TEXT"   => $arRes[9],
	);
	if($PRODUCT_ID = $el->Add($arLoadProductArray))
	  echo "New ID: ".$PRODUCT_ID."<br/>";
	else
	  echo "Error: ".$el->LAST_ERROR."<br/>";
}
?>