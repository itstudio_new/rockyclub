<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
$csvFile = new CCSVData('R', true);
$csvFile->LoadFile($_SERVER["DOCUMENT_ROOT"].'/parser/files/tbl_clients_balance.csv');
$csvFile->SetFirstHeader();
$csvFile->SetDelimiter('~');
while ($arRes = $csvFile->Fetch()) {
    $el = new CIBlockElement;
	$PROP = array();	
	$PROP['OLD_ID'] = $arRes[0];
	$filter = Array
	(
		"PERSONAL_NOTES"     => $arRes[6],
	);
	$rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $filter); // выбираем пользователей
	while($rsUsers->NavNext(true, "f_")) :
		$PROP['USER_ID'] = $f_ID;
	endwhile;		
	$dat = explode("-",$arRes[2]);
	$PROP['DATA'] = mktime(0,0,0,$dat[1],$dat[2],$dat[0]);		
		if ( $arRes[3] < 0 ) {
			$PROP['SUMMA'] = str_replace("-",'',$arRes[3]);
			$PROP['OPERATION'] = '-';
		} else {
			$PROP['SUMMA'] = $arRes[3];
			$PROP['OPERATION'] = '+';
		}	
	$res = CIBlockElement::GetList(Array(), array('IBLOCK_ID'=>5,'PROPERTY_OLD_ID'=>$arRes[4]), false, false, array('NAME','ID'));
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$PROP['STAT'] = $arFields['ID'];
	}	
	$res = CIBlockElement::GetList(Array(), array('IBLOCK_ID'=>16,'PROPERTY_OLD_ID'=>$arRes[1]), false, false, array('NAME','ID'));
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$PROP['CLIENT'] = $arFields['ID'];
	}		
	$PROP['DELETED'] = $arRes[7];
	$arLoadProductArray = Array(
	  "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
	  "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
	  "IBLOCK_ID"      => 23,
	  "PROPERTY_VALUES"=> $PROP,
	  "NAME"           => 'Изменение баланса',
	  "ACTIVE"         => "Y",            // активен
	  "PREVIEW_TEXT"   => $arRes[5],
	);
	if($PRODUCT_ID = $el->Add($arLoadProductArray))
	  echo "New ID: ".$PRODUCT_ID."<br/>";
	else
	  echo "Error: ".$el->LAST_ERROR."<br/>";
}
?>