<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
$csvFile = new CCSVData('R', true);
$csvFile->LoadFile($_SERVER["DOCUMENT_ROOT"].'/parser/files/tbl_personal.csv');
$csvFile->SetFirstHeader();
$csvFile->SetDelimiter('~');
while ($arRes = $csvFile->Fetch()) {
		$el = new CIBlockElement;
		$PROP = array();
		$PROP['OLD_ID'] = $arRes[0];
		$res = CIBlockElement::GetList(Array(), array('IBLOCK_ID'=>2,'PROPERTY_OLD_ID'=>$arRes[8]), false, false, array('NAME','ID'));
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$PROP['CLUB'] = $arFields['ID'];
		}
		$PROP['PHOTO_ID'] = $arRes[2];
		$res = CIBlockElement::GetList(Array(), array('IBLOCK_ID'=>7, 'PROPERTY_OLD_ID'=>$arRes[3]), false, false, array('NAME','ID'));
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$PROP['DOLZNOST'] = $arFields['ID'];
		}		
		$res = CIBlockElement::GetList(Array(), array('IBLOCK_ID'=>15, 'PROPERTY_OLD_ID'=>$arRes[4]), false, false, array('NAME','ID'));
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$PROP['DOGOVOR'] = $arFields['ID'];
		}				
		
		//$PROP['USER_ID'] = $arRes[5];
		$PROP['OKLAD'] = $arRes[5];
		if ( $arRes[11] == 1 ) {
			$PROP['STATUS'] = Array("VALUE" => 3 );
		} else {
			$PROP['STATUS'] = Array("VALUE" => 4 );
		}
		$PROP['ACT_SITE'] = $arRes[12];
		$filter = Array
		(
			"PERSONAL_NOTES"     => $arRes[9],
		);
		$rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $filter); // выбираем пользователей
		while($rsUsers->NavNext(true, "f_")) :
			$PROP['USER_ID'] = $f_ID;
		endwhile;		
		
		$arLoadProductArray = Array(
		  "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
		  "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
		  "IBLOCK_ID"      => 17,
		  "PROPERTY_VALUES"=> $PROP,
		  "NAME"           => $arRes[1],
		  "ACTIVE"         => "Y",            // активен
		  "PREVIEW_TEXT"   => $arRes[6],
		  "DETAIL_TEXT"   => $arRes[7],
		);
	if($PRODUCT_ID = $el->Add($arLoadProductArray))
	  echo "New ID: ".$PRODUCT_ID."<br/>";
	else
	  echo "Error: ".$el->LAST_ERROR."<br/>";
}
?>