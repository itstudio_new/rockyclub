<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
$csvFile = new CCSVData('R', true);
$csvFile->LoadFile($_SERVER["DOCUMENT_ROOT"].'/parser/files/tbl_images.csv');
$csvFile->SetFirstHeader();
$csvFile->SetDelimiter('~');
$images = array();
while ($arRes = $csvFile->Fetch()) {
	$images[$arRes[0]] = $arRes[1];
}
$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>14,"!PROPERTY_ID_IMG"=>"0"), false, false, array('ID','PROPERTY_ID_IMG'));
while($ob = $res->GetNextElement())
{
 $arFields = $ob->GetFields();
	$el = new CIBlockElement;
	$arLoadProductArray = Array(
		"PREVIEW_PICTURE" => CFile::MakeFileArray("http://rockyclub.ru".$images[$arFields['PROPERTY_ID_IMG_VALUE']])
	);
	$el->Update($arFields['ID'], $arLoadProductArray);
}
?>