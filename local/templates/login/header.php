<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>
    <title><?$APPLICATION->ShowTitle()?></title>
	<?$APPLICATION->ShowHead()?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="icon" href="/local/templates/admin_panel/images/favicon.png" type="image/x-icon" />
	<?
		$APPLICATION->SetAdditionalCSS("/local/templates/admin_panel/stylesheet/styles.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/stylesheet/styles.css");
	?>
	<?
		$APPLICATION->AddHeadScript("/local/templates/admin_panel/js/plugins/jquery/jquery.min.js");
		$APPLICATION->AddHeadScript("/local/templates/admin_panel/js/plugins/lesscss/less.min.js");
	?>
</head>
<body>
<div class="login-container">
    <div class="login-box animated fadeInDown text-center">
        <img class="push-down-10" src="/local/templates/admin_panel/img/header_logo.png">
        <div class="login-body">
        <div class="login-title">
				<strong>ВХОД</strong> в Rocky Club    
		</div>