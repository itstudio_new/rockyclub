<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?$APPLICATION->ShowTitle()?></title>
	<?$APPLICATION->ShowHead()?>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="stylesheet" href="/local/templates/admin_panel/stylesheet/site/app.css">
    <link rel="icon" href="/local/templates/admin_panel/img/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/local/templates/admin_panel/img/favicon.ico" type="image/x-icon">
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
	<? CJSCore::Init(array('ajax', 'window')); ?>
	<? 
	global $USER;
	global $APPLICATION;
	$dir = $APPLICATION->GetCurDir();
	$dirs = explode('/',$dir);
		  function ShowCanonical(){ 
			 global $APPLICATION; 
				if ($APPLICATION->GetProperty("canonical")!="" && $APPLICATION->GetProperty("canonical")!=$APPLICATION->sDirPath){
					  return '<link rel="canonical" href="'.$APPLICATION->GetProperty("canonical").'" />'; 
				} 
				else {
					  return false;
				} 
		  } 
		  $APPLICATION->AddBufferContent('ShowCanonical');
			if ($_GET) {
			   $APPLICATION->SetPageProperty('canonical', $dir);
			}
	?>			
</head>
<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>	
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter37380520 = new Ya.Metrika({
                    id:37380520,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/37380520" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<header class="header-top">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="header-region">
                    <div class="dropdown">
					 <? if ( $dirs[1] != 'franshiza') { ?>
                        <a id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expended="true" class="link dotted region-link dropdown-toggle">
							<? if ( $APPLICATION->get_cookie("CITY") != '' ) {?>
									<? echo $APPLICATION->get_cookie("CITY");?>
							<? } else { ?>
									Выберите город
							<? } ?>
                            <span class="caret"></span>
                        </a>
					  
                        <ul aria-labelledby="dropdownMenu" class="dropdown-menu">
							
<?
 /*						
                            <li><a href="/?changeCity=default" class="link underline black">Основной сайт</a></li>
 */
?> 
                            <?php
							/*
                            $cities = Cities::model()->findAll();
                            foreach($cities as $city){
                            ?>
                                <li><a href="/?changeCity=<?= $city->id; ?>" class="link underline black"><?= $city->name; ?></a></li>
                            <?php } */?>
                        </ul>
					 <? } ?>
                    </div>
                    <p class="region-phone">
							+7 (4932) 342-333
                        <?php /*
                        if(Yii::app()->controller->id != 'franchasing'){
                            if(!Yii::app()->session['curCity']){
                                echo "+7 (4932) 342-333";
                            }else{
                                echo Yii::app()->session['curCity']->phone;
                            }
                        }else{
                            echo "+7 (962) 162-11-77";
                        } */
                        ?>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <a href="/">
                    <div class="header-logo"><img src="/local/templates/admin_panel/img/site/main/header_logo.png">
                        <div class="header-logo-glow hidden-xs"></div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="header-auth">
                    <?php /*
                    if(!isset(Yii::app()->session['client'])){ */
                        ?>
                        <div class="header-auth-item mod-login">
                            <div class="header-auth-icon mod-login"></div><a class="link dotted">Личный кабинет</a>
                        </div>
                        <?php
                    /*}else{ 
                        ?>
                        <div class="header-auth-item">
                            <div class="header-auth-icon mod-login"></div><a href="/lk" class="link underline"><? Yii::app()->session['client']->name; ?></a>
                        </div>
                        <div class="header-auth-item mod-logout">
                            <div class="header-auth-icon mod-logout"></div><a href="form/logout" class="link underline">Выход</a>
                        </div>
                        <?php
                    }*/
                    ?>


                </div>
                <p class="header-title text-left text-uppercase">мы сделали бокс<br>доступным для каждого</p>
            </div>
        </div>
        <div class="navbar">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target="#menu" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div id="menu" class="collapse navbar-collapse">
				<?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
					"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
						"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
						"DELAY" => "N",	// Откладывать выполнение шаблона меню
						"MAX_LEVEL" => "1",	// Уровень вложенности меню
						"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
						"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
						"MENU_CACHE_TYPE" => "N",	// Тип кеширования
						"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
						"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
						"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
						"COMPONENT_TEMPLATE" => ".default"
					),
					false
				);?>
            </div>
        </div>
    </div>
</header>
<div class="form-subscribe">
    <div class="col-md-4 hidden-sm">
        <div class="form-container mod-record">
            <div class="form-chain"></div>
            <form action="/form/reserv" id="validate" method="POST" class="form mod-record">
                <h2 class="form-title">Запишись на занятие</h2>
				<select class="form-control" name="data[club]" id="data_club">
<option value="">Выберите клуб *</option>
<option value="5">RockyBoxingClub в Иваново</option>
</select>                <select class="form-control" name="data[service]" id="data_service">
<option value="">Выберите занятие *</option>
<option value="ROCKY BOX">ROCKY BOX</option>
<option value="ROCKY COMBAT">ROCKY COMBAT</option>
<option value="ROCKY COMBAT LADY">ROCKY COMBAT LADY</option>
<option value="ROCKY KIDS">ROCKY KIDS</option>
<option value="ROCKY ЛАНЧ">ROCKY ЛАНЧ</option>
<option value="ROCKY TRAINING">ROCKY TRAINING</option>
<option value="ROCKY FIT">ROCKY FIT</option>
<option value="ДЕТСКИЙ ЛАГЕРЬ ROCKY">ДЕТСКИЙ ЛАГЕРЬ ROCKY</option>
</select>                <input class="validate[required] form-control" placeholder="Ваше имя *" type="text" value="" name="data[name]" id="data_name" />                <input class="validate[required] form-control" placeholder="Телефон *" type="text" value="" name="data[phone]" id="data_phone" />                <input class="form-control" placeholder="E-mail *" type="text" value="" name="data[email]" id="data_email" />                <button class="btn mod-record">записаться</button><a style="font-size: 12px" class="link underline white form-link link-cond">Политика конфиденциальность</a>
            </form>
        </div>
    </div>
</div>
<div class="partion">
    <div class="container">
        <div class="row">