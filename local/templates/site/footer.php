</div></div></div>
<footer class="footer-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6">
                <h2 class="footer-list-title">информация</h2>
                <ul class="footer-list list-unstyled">
                    <li class="footer-list-item"><a  href=""class="link underline">Новости</a></li>
                    <li class="footer-list-item"><a href="" class="link underline">Наши услуги</a></li>
                    <li class="footer-list-item"><a href="" class="link underline">Почему Rocky</a></li>
                    <li class="footer-list-item"><a href="" class="link underline">Ваш клуб</a></li>
                    <li class="footer-list-item"><a href="" class="link underline">Франчайзинг</a></li>
                    <li class="footer-list-item"><a href="" class="link underline">Контакты</a></li>
                    <li class="footer-list-item"><a class="link underline">Карта сайта</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <h2 class="footer-list-title">программы</h2>
                <ul class="footer-list list-unstyled">
                    <?php
					/*
                    $uslugi = NashiUslugi::model()->findAll();
                    foreach($uslugi as $one){
						*/
                        ?>
                        <li class="footer-list-item"><a  href="<?/*= $this->createUrl("nashiUslugi/one",array("id"=>$one->id)); */?>" class="link underline"><?/*= $one->title; */?></a></li>
                        <?/*php
                    }
                    */?>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <h2 class="footer-list-title">филиалы</h2>
                <ul class="footer-list list-unstyled">
                    <?php
					/*
                    $criteria = new CDbCriteria();
                    $criteria->order = '`order` ASC';

                    ?>
                    <?php
                    $cities = Cities::model()->findAll($criteria);
                    foreach($cities as $one){
						*/
                    ?>
                    <li class="footer-list-item"><a class="link underline"><?= $one->name; ?></a></li>
                    <?php/* } */?>
                    <li>
                        <div class="social-icon mod-vk"></div>
                        <div class="social-icon mod-fb"></div>
                        <div class="social-icon mod-tw"></div>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="footer-logo"><img src="/local/templates/admin_panel/img/site/main/footer_logo.png" class="img-responsive"></div>
                <p class="footer-meta-info">Спортивная школа для детей</p>
                <p class="footer-meta-info">Секции для мужчин и женщин</p>
                <p class="footer-meta-info">Индивидуальные тренировки (занятия)</p>
            </div>
        </div>
        <div class="row">
            <?php/*
            if(isset(Yii::app()->session['curCity'])){
                $club = Clubs::model()->find("city_id = '".Yii::app()->session['curCity']->id."'");
				
                ?>
                <div class="col-md-4">
                    <p class="footer-text">Режим работы:<b> <?= (isset($club->schedule))?$club->schedule:"пн-пт 8:00-22:00, сб-вс 11:00-20:00"; ?></b></p>
                </div>
                <?php
            }else{
            ?>
            <div class="col-md-4">
                <p class="footer-text">Режим работы:<b> пн-пт 8:00-22:00, сб-вс 11:00-20:00</b></p>
            </div>
            <?php } */?>
            <div class="col-md-offset-1 col-md-3">
                <p class="footer-text">Email:<b> <a class="link underline grey">info@rockyclub.ru</a></b></p>
            </div>
            <div class="col-md-offset-1 col-md-3">
                <p class="footer-text">Разработка сайта:<b> <a class="link underline grey" href="https://it-studio.ru/" target="_blank">it-studio.ru</a></b></p>
            </div>
        </div>
    </div>
</footer>
<div id="modalLogIn" class="modal">
    <div class="modal-overlay"></div>
    <div class="modal-content modal-login">
        <div class="modal-close"></div>
        <h2 class="modal-title">вход в личный кабинет</h2>
        <div class="modal-inner-content clearfix"><img src="<?= Yii::app()->homeUrl; ?>img/site/main/card_1.png" class="card-num hidden-xs">
            <form class="form pull-right" action="/form/loginAsClient" method="POST">
                <div class="form-group">
                    <?php echo CHtml::textField("data[card_num]",'',array('class' => "form-control", 'placeholder' => "Номер карты *")); ?>
                    <?php echo CHtml::textField("data[card_id]",'',array('class' => "form-control", 'placeholder' => "Штрихкод *")); ?>
                    <button class="btn mod-record pull-right">Войти</button>
                </div>
            </form><img src="<?= Yii::app()->homeUrl; ?>img/site/main/card_2.png" class="s-code hidden-xs">
        </div>
    </div>
</div>
<div id="modalOrder" class="modal">
    <div class="modal-overlay"></div>
    <div class="modal-content mod-small mod-black">
        <div class="modal-close"></div>
        <h4 class="modal-title">заказать обратный звонок</h4>
        <form class="form" id="validate" method="POST" action="/form/backCall">
            <?php echo CHtml::dropDownList("data[city]",'',CHtml::listData(Cities::model()->findAll(),'id','name'),array("class" => "form-control", "empty" => "Выберите город *")); ?>
            <?php echo CHtml::textField("data[name]",'',array('class' => "validate[required] form-control", 'placeholder' => "Ваше имя *")); ?>
            <?php echo CHtml::textField("data[phone]",'',array('class' => "validate[required] form-control", 'placeholder' => "Телефон *")); ?>
            <?php echo CHtml::textField("data[email]",'',array('class' => "form-control", 'placeholder' => "E-mail")); ?>
            <div class="text-center">
                <button class="btn mod-record">отправить</button>
            </div><a style="font-size: 12px" class="link underline white form-link link-cond">Политика конфиденциальность</a>
        </form>
    </div>
</div>

<div id="modalOrderFranch" class="modal">
    <div class="modal-overlay"></div>
    <div class="modal-content mod-small mod-black">
        <div class="modal-close"></div>
        <h4 class="modal-title">заказать обратный звонок</h4>
        <form class="form" id="validate" method="POST" action="/form/backCall">
            <?php echo CHtml::textField("data[city]",'',array('class' => "validate[required] form-control", 'placeholder' => "Город *")); ?>
            <?php echo CHtml::textField("data[name]",'',array('class' => "validate[required] form-control", 'placeholder' => "Ваше имя *")); ?>
            <?php echo CHtml::textField("data[phone]",'',array('class' => "validate[required] form-control", 'placeholder' => "Телефон *")); ?>
            <?php echo CHtml::textField("data[email]",'',array('class' => "form-control", 'placeholder' => "E-mail")); ?>
            <div class="text-center">
                <button class="btn mod-record">отправить</button>
            </div><a style="font-size: 12px" class="link underline white form-link link-cond">Политика конфиденциальность</a>
        </form>
    </div>
</div>

<div id="modalReserv" class="modal">
    <div class="modal-overlay"></div>
    <div class="modal-content mod-small mod-black">
        <div class="modal-close"></div>
        <h4 class="modal-title">запись на занятие</h4>
        <form class="form" id="validate" method="POST" action="/form/reserv">
            <?php
            $criteria = new CDbCriteria();
            if($city = Yii::app()->session['curCity']){
                $criteria->compare("city_id",$city->id);
            }
            ?>
            <?php echo CHtml::dropDownList("data[club]",'',CHtml::listData(Clubs::model()->findAll($criteria),'id','name'),array("class" => "form-control", "empty" => "Выберите клуб *")); ?>
            <?php echo CHtml::dropDownList("data[service]",'',CHtml::listData(NashiUslugi::model()->findAll(),'title','title'),array("class" => "form-control", "empty" => "Выберите занятие *")); ?>
            <?php echo CHtml::textField("data[name]",'',array('class' => "validate[required] form-control", 'placeholder' => "Ваше имя *")); ?>
            <?php echo CHtml::textField("data[phone]",'',array('class' => "validate[required] form-control", 'placeholder' => "Телефон *")); ?>
            <?php echo CHtml::textField("data[email]",'',array('class' => "form-control", 'placeholder' => "E-mail *")); ?>
            <div class="text-center">
                <button class="btn mod-record">отправить</button>
            </div><a style="font-size: 12px" class="link underline white form-link link-cond">Политика конфиденциальность</a>
        </form>
    </div>
</div>
<?php
if(Yii::app()->user->hasFlash("success")){
    $success = Yii::app()->user->getFlash("success");
    ?>
    <div id="modalSuccess" class="modal">
        <div class="modal-overlay"></div>
        <div class="modal-content mod-black">
            <div class="modal-close"></div>
            <h4 class="modal-title" style="margin-bottom: 0px;"><?= $success; ?></h4>
        </div>
    </div>
    <?php
}
?>
<div id="modalCondit" class="modal">
    <div class="modal-overlay"></div>
    <div class="modal-content">
        <div class="modal-close"></div>
        <h2 class="modal-title">политика конфиденциальности</h2>
        <p class="modal-text">Политика конфиденциальности содержит пояснения о том, как используется информация, которую Вы нам предоставляете, и как связаться с нами, если у Вас возникнут вопросы и проблемы.</p>
        <h3 class="modal-header">1. Какая информация нам необходима? </h3>
        <p class="modal-text">Если вы решаете оставить заявку на вступление в клуб, мы запросим у Вас такую информацию, как имя, телефон и адрес электронной почты. Это объясняется следующими соображениями. Чтобы Вы могли вступить в наш клуб, нам необходимо с Вами связаться для того чтобы проинформировать Вас о предоставляемых услугах и стоимости. Кроме того, мы можем проводить опрос для выяснения Вашего мнения о нашем клубе и предоставляемых услугах, а также Ваших пожеланий. Перед тем как предоставить нам чужие персональные данные, обязательно получите разрешение соответствующих лиц. Убедитесь, что вы предоставляете правильные и точные сведения.</p>
        <h3 class="modal-header">2. Что мы делаем с Вашей персональной информацией?</h3>
        <p class="modal-text">Ваш телефон и адрес Вашей электронной почты и Ваше имя мы используем в следующих целях:</p>
        <ul>
            <li class="modal-text">для предоставления Вам информации о нашем клубе и услугах;</li>
            <li class="modal-text">для рассылки новостей и рекламной информации о продуктах, услугах, специальных предложениях, продуктах и услугах избранных партнеров в пределах, разрешенных действующим законодательством, или в соответствии с Вашими пожеланиями (см. ниже раздел «Отказ от подписки»);</li>
            <li class="modal-text">для рассылки служебных сообщений (например, если Вы забыли свой пароль, если Вы станете членом клуба и у Вас будет Личный кабинет на сайте);</li>
        </ul>
        <h3 class="modal-header">3. Кто еще получает доступ к персональной информации?</h3>
        <p class="modal-text">Для осуществления рассылки мы используем сервис почтовых рассылок mailchimp.com и unisender.ru. Эти сервисы обрабатывает Вашу персональную информацию (т.е. e-mail и указанное Вами имя) для того, чтобы мы могли отправить Вам электронное письмо с доступом в Личный кабинет или провести опрос с целью выявления информации, в которой Вы нуждаетесь. В исключительных обстоятельствах мы можем раскрыть Вашу персональную информацию, если этого требует закон или в целях защиты себя и других от противоправных действий или других опасностей.</p>
        <h3 class="modal-header">4. Идентификационные файлы (cookies) </h3>
        <p class="modal-text">Наш сайт содержит идентификационные файлы, так называемые, cookies. Cookies – представляют собой небольшие текстовые файлы, отправляемые на компьютер посетителя сайта для учета его действий. Cookies используются на нашем сайте для персонализации учета посещений, изучения поведения посетителей на сайте и регистрации их действий. Вы можете отключить использование cookies в параметрах настройки браузера. Следует учитывать, однако, что в этом случае некоторые функции будут недоступны или могут работать некорректно.</p>
        <h3 class="modal-header">5. Безопасность</h3>
        <p class="modal-text">Мы предпринимаем разумные меры для сведения к минимуму возможности утраты, кражи, недобросовестного использования, несанкционированного доступа к Вашей персональной информации, уничтожения, изменения или ее раскрытию. В то же время мы не можем гарантировать абсолютного устранения риска несанкционированного злоупотребления персональной информацией. Убедительно просим Вас самым внимательным образом подойти к хранению паролей к учетным записям и не сообщать их кому-либо еще (в случае, если речь идет о продуктах, содержащих пароли доступа). Немедленно свяжитесь с нами, если станет известно о каком-либо нарушении информационной безопасности (например, несанкционированном использовании Вашего пароля).</p>
        <h3 class="modal-header">6. Дети</h3>
        <p class="modal-text">Мы всецело разделяем настороженность родителей в вопросах использования персональной информации об их детях. Настоятельно прошу всех посетителей младше 18 лет, получить разрешение родителей или опекунов, прежде чем предоставлять какую-либо персональную информацию. Мы не собираем информацию о детях намеренно. Если мне станет известно о том, что я получил персональную информацию о ребенке, не достигшем 14-летнего возраста, мы предпримем меры для удаления такой информации в возможно короткие сроки.</p>
        <h3 class="modal-header">7. Отказ от подписки</h3>
        <p class="modal-text">Если Вы больше не хотите получать от нас сообщения, перейдите по ссылке, указанной в нижней части любого нашего письма.</p>
        <h3 class="modal-header">8. Контакты</h3>
        <p class="modal-text">Если у Вас возникнут вопросы и/или проблемы, связанные с политикой безопасности и сохранностью Ваших персональных данных, обращайтесь, пожалуйста, на адрес: <a class="link underline red">info@rockyclub.ru</a></p>
        <h4 style="margin-top: 30px;" class="text-center"><a data-dismiss="modal" class="link underline red">Принять и заполнить форму</a></h4>
    </div>
</div>
<script src="/local/templates/admin_panel/js/site/bundle.js"></script>
<script src="/local/templates/admin_panel/js/site/scripts.js"></script>
<script type="text/javascript" src="/local/templates/admin_panel/js/plugins/validationengine/jquery.validationEngine.js"></script>
<script type="text/javascript" src="/local/templates/admin_panel/js/plugins/validationengine/languages/jquery.validationEngine-ru.js"></script>
</body>
</html>