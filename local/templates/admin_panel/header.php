<? 
global $USER;
if (!$USER->IsAuthorized()) { 
		LocalRedirect("/admin/"); 
		exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?$APPLICATION->ShowTitle()?></title>
	<?$APPLICATION->ShowHead()?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/img/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/img/favicon.ico" type="image/x-icon">
	<?
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/stylesheet/styles.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/plugins/tinymce/skins/lightgray/content.min.css");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/jquery/jquery.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/jquery/jquery-ui.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/bootstrap/bootstrap.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/summernote/summernote.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/tinymce/tinymce.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/tinymce/langs/ru.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/icheck/icheck.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/scrolltotop/scrolltopcontrol.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/morris/raphael-min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/morris/morris.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/datepicker/js/bootstrap-datepicker.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/fileinput/fileinput.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/datepicker/locales/bootstrap-datepicker.ru.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/bootstrap/bootstrap-timepicker.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/owl/owl.carousel.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/moment.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/maskedinput/jquery.maskedinput.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/select2/select2.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/fullcalendar/fullcalendar.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/fullcalendar/lang/ru.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/daterangepicker/daterangepicker.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/validationengine/jquery.validationEngine.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/validationengine/languages/jquery.validationEngine-ru.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/blueimp/jquery.blueimp-gallery.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/datetimepicker/locales/bootstrap-datetimepicker.ru.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/bootstrap/bootstrap-select.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins/bootstrap/bootstrap-file-input.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/imgLiquid.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/plugins.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/actions.js");	
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/function.js");
		
	?>
	<? CJSCore::Init(array('ajax', 'window')); ?>
<? 
global $USER;
$dir = $APPLICATION->GetCurDir();
      function ShowCanonical(){ 
         global $APPLICATION; 
            if ($APPLICATION->GetProperty("canonical")!="" && $APPLICATION->GetProperty("canonical")!=$APPLICATION->sDirPath){
                  return '<link rel="canonical" href="'.$APPLICATION->GetProperty("canonical").'" />'; 
            } 
            else {
                  return false;
            } 
      } 
      $APPLICATION->AddBufferContent('ShowCanonical');
        if ($_GET) {
           $APPLICATION->SetPageProperty('canonical', $dir);
        }
?>			
</head>
<body>
<?
	global $USER;
	global $APPLICATION;
	$dir = $APPLICATION->GetCurDir();
	$dirs = explode('/',$dir);
	CModule::IncludeModule("iblock");
	global $APPLICATION;
	$HIDE_MENU = $APPLICATION->get_cookie("HIDE_MENU");
?>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>	
<div class="page-container <? if ( $HIDE_MENU == 'Y' ) { ?>page-navigation-toggled page-container-wide<? } ?>">
    <div class="page-sidebar">
        <ul class="x-navigation ">
			<ul class="x-navigation ">
				<li class="xn-logo">
					<a href="/admin" style="background: url() center no-repeat #22242a;"></a>
					<a href="#" class="x-navigation-control"></a>
				</li>
				<li class="xn-profile">
					<div class="profile">
						<div class="profile-data">
							<div class="profile-data-name"><?=$USER->GetFullName()?></div>
							<div class="profile-data-title"><?=$USER->GetEmail()?></div>
						</div>
					</div>
				</li>
				<?$APPLICATION->IncludeComponent(
					"bitrix:menu", 
					"leftmenu", 
					array(
						"ALLOW_MULTI_SELECT" => "N",
						"CHILD_MENU_TYPE" => "left",
						"DELAY" => "N",
						"MAX_LEVEL" => "2",
						"MENU_CACHE_GET_VARS" => array(
						),
						"MENU_CACHE_TIME" => "3600",
						"MENU_CACHE_TYPE" => "N",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"ROOT_MENU_TYPE" => "top",
						"USE_EXT" => "N",
						"COMPONENT_TEMPLATE" => "leftmenu"
					),
					false
				);?>				
<?
 /*			
                        <li class="">
                <a href="/admin/tasks">
                    <span class="fa fa-tasks"></span>
                    <span class="xn-text">Задачи</span>
                </a>
            </li>
                                    <li class="">
                <a href="/admin/sales">
                    <span class="glyphicon glyphicon-usd"></span>
                    <span class="xn-text">Продажи</span>
                </a>
            </li>
                                    <li class="">
                <a href="/admin/balance">
                    <span class="fa fa-dollar"></span>
                    <span class="xn-text">Доходы и расходы</span>
                </a>
            </li>
                                    <li class="">
                <a href="/admin/users">
                    <span class="glyphicon glyphicon-user"></span>
                    <span class="xn-text">Пользователи</span>
                </a>
            </li>
                                    <li class="">
                <a href="/admin/personal">
                    <span class="fa fa-users"></span>
                    <span class="xn-text">Персонал</span>
                </a>
            </li>
                                    <li class="xn-openable ">
                <a href="/admin">
                    <span class="glyphicon glyphicon-tag"></span>
                    <span class="xn-text">Инвентарь</span>
                </a>
                <ul>
                    <li class="">
                        <a href="/admin/inventoryList">
                            <span class="fa fa-bars"></span>
                            Список инвентаря
                        </a>
                    </li>
                    <li class="">
                        <a href="/admin/inventarisation">
                            <span class="fa fa-book"></span>
                            Инвентаризация
                        </a>
                    </li>
                </ul>
            </li>
                                    <li class="xn-openable ">
                <a href="/admin">
                    <span class="glyphicon glyphicon-shopping-cart"></span>
                    <span class="xn-text">Товары</span>
                </a>
                <ul>
                    <li class="">
                        <a href="/admin/stock">
                            <span class="fa fa-archive"></span>
                            Склад
                        </a>
                    </li>
                    <li class="">
                        <a href="/admin/stock/movings">
                            <span class="fa fa-exchange"></span>
                            Движение товаров
                        </a>
                    </li>
                    <li class="">
                        <a href="/admin/stockInventarisation">
                            <span class="fa fa-book"></span>
                            Инвентаризация
                        </a>
                    </li>
                </ul>
            </li>
                                    <li class="">
                <a href="/admin/shedule">
                    <span class="glyphicon glyphicon-calendar"></span>
                    <span class="xn-text">Расписание</span>
                </a>
            </li>
                                    <li class="xn-openable ">
                <a href="/admin">
                    <span class="glyphicon glyphicon-tasks"></span>
                    <span class="xn-text">Занятия</span>
                </a>
                <ul>
                    <li class="">
                        <a href="/admin/services">
                            <span class="fa fa-user"></span>
                            Индивидуальные
                        </a>
                    </li>
                    <li class="">
                        <a href="/admin/groupPrograms">
                            <span class="fa fa-users"></span>
                            Групповые
                        </a>
                    </li>
                    <li class="">
                        <a href="/admin/servicesCat">
                            <span class="fa fa-sitemap"></span>
                            Категории
                        </a>
                    </li>
                </ul>
            </li>
                                    <li class="xn-openable ">
                <a href="/admin">
                    <span class="fa fa-building-o"></span>
                    <span class="xn-text">Клубы и залы</span>
                </a>
                <ul>
                                        <li class="">
                        <a href="/admin/clubs">
                            <span class="fa fa-building-o"></span>Клубы</a>
                    </li>
                                        <li class="">
                        <a href="/admin/halls">
                            <span class="fa fa-cubes"></span>Залы</a>
                    </li>
                </ul>
            </li>
                                    <li class="">
                <a href="/admin/clients">
                    <span class="fa fa-user"></span>
                    Клиенты
                </a>
            </li>
                                    <li class="xn-openable ">
                <a href="/admin">
                    <span class="glyphicon glyphicon-book"></span>
                    <span class="xn-text">Справочники</span>
                </a>
                <ul>
                    <li class="">
                        <a href="/admin/bookerStates">
                            <span class="fa fa-wrench"></span>Статьи</a>
                    </li>
                    <li class="">
                        <a href="/admin/referals">
                            <span class="fa fa-wrench"></span>Рефералы</a>
                    </li>
                    <li class="">
                        <a href="/admin/abonements">
                            <span class="fa fa-list-alt"></span>
                            <span class="xn-text">Абонементы</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="/admin/inventory">
                            <span class="fa fa-wrench"></span>Инвентарь</a>
                    </li>
                    <li class="">
                        <a href="/admin/inventoryCat">
                            <span class="fa fa-wrench"></span>Категории инвентаря</a>
                    </li>
                    <li class="">
                        <a href="/admin/inventoryStates">
                            <span class="fa fa-wrench"></span>Состояние инвентаря</a>
                    </li>

                    <li class="">
                        <a href="/admin/products">
                            <span class="fa fa-wrench"></span>Товары</a>
                    </li>
                    <li class="">
                        <a href="/admin/productsCat">
                            <span class="fa fa-wrench"></span>Категории товаров</a>
                    </li>
                    <li class="">
                        <a href="/admin/clientsStatus">
                            <span class="fa fa-gear"></span>
                            Статусы клиентов
                        </a>
                    </li>
                    <li class="">
                        <a href="/admin/positions">
                            <span class="fa fa-wrench"></span>Должности</a>
                    </li>
                    <li class="">
                        <a href="/admin/dogovorType">
                            <span class="fa fa-wrench"></span>Типы договоров</a>
                    </li>
                                        <li class="">
                        <a href="/admin/cities">
                            <span class="fa fa-wrench"></span>Города</a>
                    </li>
                                    </ul>
            </li>
                                    <li class="xn-openable ">
                <a href="/admin">
                    <span class="fa fa-book"></span>
                    <span class="xn-text">Отчёты</span>
                </a>
                <ul>
                                        <li class="">
                        <a href="/admin/reports/activeClients">
                            <span class="fa fa-tasks"></span>
                            Активные клиенты
                        </a>
                    </li>
                                                            <li class="">
                        <a href="/admin/reports/notActiveClients">
                            <span class="fa fa-tasks"></span>
                            Не активные клиенты
                        </a>
                    </li>
                                                            <li class="">
                        <a href="/admin/reports/newClients">
                            <span class="fa fa-tasks"></span>
                            Новые клиенты
                        </a>
                    </li>
                                                            <li class="">
                        <a href="/admin/reports/sales">
                            <span class="fa fa-tasks"></span>
                            Продажи
                        </a>
                    </li>
                                                            <li class="">
                        <a href="/admin/reports/trainers">
                            <span class="fa fa-tasks"></span>
                            Тренеры
                        </a>
                    </li>
                                                            <li class="">
                        <a href="/admin/reports/costs">
                            <span class="fa fa-tasks"></span>
                            Расходы и доходы
                        </a>
                    </li>
                                                            <li class="">
                        <a href="/admin/reports/consolidated">
                            <span class="fa fa-tasks"></span>
                            Сводный
                        </a>
                    </li>
                                    </ul>
            </li>
*/			
?>			
                <li>
                <a href="/">
                    <span class="glyphicon glyphicon-arrow-left"></span>
                    <span class="xn-text">Вернуться на сайт</span>
                </a>
            </li>

        </ul>
        </ul>
    </div>

    <div class="page-content">
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <div>
                <li class="xn-icon-button">
                    <a href="#" onclick="leftblock(); return false;" class="x-navigation-minimize x-nav_my" id="nav_left">
                        <span class="fa fa-dedent"></span>
                    </a>
                </li>
                <li class="xn-icon-button pull-right last">
                    <a href="#" class="mb-control x-nav_my" data-box="#mb-signout">
                        <span class="fa fa-power-off"></span>
                    </a>
                </li>
            </div>
        </ul>
		<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumb", Array(
			"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
				"SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
				"START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
			),
			false
		);?>		
		<div class="content-frame" style="overflow: hidden">
			<div id="t_rf">
				<div class="content-frame-top" id="top_title">
					<div class="page-title">
						<h2><?$APPLICATION->ShowTitle(false)?></h2>
					</div>
					<div class="pull-right">
						<button class="btn btn-default content-frame-right-toggle">
							<span class="fa fa-bars"></span>
						</button>
					</div>
				</div>
<div class="page-content-wrap page-tabs-item active" id="info">
    <div class="row-content">   