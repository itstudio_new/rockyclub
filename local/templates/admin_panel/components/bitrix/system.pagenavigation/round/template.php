<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */
$this->setFrameMode(true);
if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>
<? if ( $arResult["NavPageNomer"] <= 3 ) { ?>
		<? $start = 1 ?>
		<? if ( $arResult['NavPageCount'] >= 5 ) { ?>
			<? $finish = 5;?>
		<? } else { ?>
			<? $finish = $arResult['NavPageCount']; ?>
		<? } ?>
<? } else { ?>
		<? if ( $arResult["NavPageNomer"] + 2 > $arResult['NavPageCount'] ) { ?>
			<? if ( $arResult["NavPageNomer"] + 1 > $arResult['NavPageCount']  ) { ?>
				<? $start = $arResult["NavPageNomer"] - 4;?> 
			<? } else { ?>
				<? $start = $arResult["NavPageNomer"] - 3;?> 
			<? } ?>
		<? }	 else { ?>
			<? $start = $arResult["NavPageNomer"] - 2;?> 
		<? } ?>
		<? if ( $arResult["NavPageNomer"] + 2 <= $arResult['NavPageCount'] ) { ?>
				<? $finish = $arResult["NavPageNomer"] + 2;?>
		<? } elseif ( $arResult["NavPageNomer"] + 1 <= $arResult['NavPageCount'] ) {?>
				<? $finish = $arResult["NavPageNomer"] + 1;?>
		<? } else { ?>
				<? $finish = $arResult["NavPageNomer"]; ?>
		<? } ?>
<? } ?>
<div class="dataTables_info" role="status" aria-live="polite">Показано с <?=$arResult["NavFirstRecordShow"]?> по <?=$arResult["NavLastRecordShow"]?> из <?=$arResult["NavRecordCount"]?> записей</div>
<div class="dataTables_paginate paging_simple_numbers">
	<ul class="pagination" id="yw0">
	<?if ($arResult["NavPageNomer"] == 1 ){?>
				<li class=" disabled"><a data-page="1" class="" href="#"><i class="fa fa-angle-double-left"></i></a></li>
				<li class="page disabled"><a data-page="1" class="" href="#">&lt; Предыдущая</a></li>
	<? } else { ?>
				<li class=""><a data-page="1" class="" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1"><i class="fa fa-angle-double-left"></i></a></li>
				<li class="page"><a data-page="1" class="" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">&lt; Предыдущая</a></li>
	<? } ?>
		<? for ($x=$start; $x<=$finish; $x++) { ?>
		    <? if ( $arResult['NavPageNomer'] == $x ) { ?>
				<li class="page page active"><a data-page="<?=$x?>" class="" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$x?>"><?=$x?></a></li>
			<? } else { ?>
				<li class="page page"><a data-page="<?=$x?>" class="" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$x?>"><?=$x?></a></li>
			<?  }?>
		<? } ?>
		<? if ( $arResult["NavPageNomer"] != $arResult['NavPageCount'] ) { ?>
				<li class="page"><a data-page="<?=$arResult["NavPageNomer"]+1?>" class="" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">Следующая &gt;</a></li>
				<li class="last"><a data-page="<?=$arResult["NavPageCount"]?>" class="" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageCount"])?>"><i class="fa fa-angle-double-right"></i></a></li>
		<? } else { ?>
				<li class="page disabled"><a data-page="<?=$arResult["NavPageCount"]?>" class="" href="#">Следующая &gt;</a></li>		
				<li class="last disabled"><a data-page="<?=$arResult["NavPageCount"]?>" class="" href="#"><i class="fa fa-angle-double-right"></i></a></li>
		<? } ?>
	</ul>
</div>
