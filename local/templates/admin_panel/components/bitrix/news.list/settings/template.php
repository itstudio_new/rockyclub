<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<a style="display: none;" id="block_edit_user"  href="#" data-toggle="modal" data-target="#edit_user" ></a>
<table class="table table-bordered table-striped table-actions">
	<thead>
		<tr>
			<th>Название настройки</th>
			<th>Значение</th>
			<th width="120">Действия</th>
		</tr>
	</thead>
	<tbody>
<?foreach($arResult["ITEMS"] as $arItem) {?>
		<tr>
			<td><strong><?=$arItem['NAME']?></strong></td>
			<td id="settings_<?=$arItem['ID']?>_value" ><?=$arItem['PROPERTIES']['VALUE']['VALUE']?></td>
			<td>
				<a href="#" onclick="editconfig(<?=$arItem['ID']?>); return false;" class="btn btn-default btn-condensed">
					<span class="fa fa-pencil"></span>
				</a>
			</td>
		</tr>
<? } ?>
	</tbody>
</table>	
<div class="modal fade" id="edit_user" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" style="display: none;"></div>