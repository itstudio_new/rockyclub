<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ( $arParams['CONFIG']['BACK_HISTORYSKLAD'] == 'Y' ) { ?>
			<div class="form-group">
				<a href="/admin/stock/sklad/" class="btn btn-default"><i class="fa fa-arrow-left"></i>Обратно на склад</a>
			</div>
<? } elseif ( $arParams['CONFIG']['BACK_HISTORYSKLAD'] != 'N' ) { ?>
<div class="form-group">
	<a class="btn btn-primary" href="#" onclick="add_edit('<?=$arParams['CONFIG']['MODULE']['filename']?>'); return false;" style="margin-left: 15px;">
		<span class="fa fa-plus-square"></span>ДОБАВИТЬ
	</a>
</div>
<? } ?>
<div class="row">
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">
<?
global $APPLICATION;
$dir = $APPLICATION->GetCurDir();
?>		
<form method="get" action="" id="filter" <? if ( $arParams['CONFIG']['FILTER'] and $arParams['CONFIG']['FILTER'] == 'off' ) { ?>style="display: none;"<? } ?> >		
			<div class="row">
				<div class="col-sm-2">
					<input class="form-control filter" placeholder="ПОИСК" value="<?=$_GET['allfilter']?>" name="allfilter" type="text">
				</div>			
			 <? foreach ( $arParams['CONFIG']['FIELD'] as $name ) { ?>
				<? if ( $name['FILTER'] == 'Y' ) { ?>
				<div class="col-sm-2">
					<? if ( $name['TYPE'] == 'select' ) { ?>
						<select class="form-control filter" name="filter[<?=$name['FIELD']?>]">
							<option value="">-<?=$name['NAME']?>-</option>
							<? foreach ( $name['SELECT_VALUE'] as $index=>$selectname ) { ?>
								<option <? if ( $index == $_GET['filter'][$name['FIELD']] and $_GET['filter'][$name['FIELD']] != '' ) { ?>selected<? } ?> value="<?=$index?>"><?=$selectname?></option>
							<? } ?>
						</select>
					<? } elseif ( $name['TYPE'] == 'link' ) { ?>
						<select <? if ( $name['FIELD'] == 'CLUB' ) { ?> id="club_select" onchange="changeclub(this); return false;"<? }?> class="form-control filter <? if ( $name['FIELD'] != 'CLUB' ) { ?> linked<?  }?>" name="filter[<?=$name['FIELD']?>]">
							<option value="">-<?=$name['NAME']?>-</option>
							<?
							$res = CIBlockElement::GetList(Array('NAME'=>'ASC'), Array("IBLOCK_ID"=>$name['LINK']['IBLOCK'], "ACTIVE"=>"Y"),false,false,array('ID','NAME','PROPERTY_CLUB'));
							while($ob = $res->GetNextElement()){ 
							 $arFields = $ob->GetFields();  
							?>					
								<option <? if(  $arFields['PROPERTY_CLUB_VALUE'] != '' ) { ?>club="<?=$arFields['PROPERTY_CLUB_VALUE']?>"<? } ?> <? if ( $arFields['ID'] == $_GET['filter'][$name['FIELD']] ) { ?>selected<? } ?> value="<?=$arFields['ID']?>"><?=$arFields[$name['LINK']['FIELD']]?></option>
							<? } ?>
						</select>					
					<? } ?>
				</div>
				<?}?>
			 <? } ?>
				<a class="btn btn-default" href="#" onclick='$("#filter").submit(); return false;' style="margin-left: 15px;">
						Фильтровать
				</a>
				<a href="<?=$dir?>" title="Сброс фильтра" class="btn btn-danger btn-condensed"><i class="fa fa-times"></i></a>
			</div>
</form>
<? if ( $arParams['CONFIG']['USER_ID_BALANS'] != '' ) { ?>
							<input type="hidden" id="useredit_hidden" value="<?=$arParams['CONFIG']['USER_ID_BALANS']?>" >
<?
$res = CIBlockElement::GetByID($arParams['CONFIG']['USER_ID_BALANS']);
if($ar_res = $res->GetNext())
?>
<div class="panel-body panel-default">
	Текущий баланс клиента <b class="text-info"><i class="fa fa-user"></i>&nbsp;<?=$ar_res['NAME']?></b> составляет 
	<b class="text-success">
<?
	$summa = 0;
	$res = CIBlockElement::GetList(Array(), array('IBLOCK_ID'=>23,'PROPERTY_CLIENT'=>$arParams['CONFIG']['USER_ID_BALANS']),false,false,array('PROPERTY_SUMMA','PROPERTY_OPERATION'));
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		if ( $arFields['PROPERTY_OPERATION_VALUE'] == '-' ) {
			$summa = $summa - $arFields['PROPERTY_SUMMA_VALUE'];
		} else {
			$summa = $summa + $arFields['PROPERTY_SUMMA_VALUE'];
		}
	}
?>	
	<?=$summa?> рублей</b>
</div>	
<? } ?>
<? if ( $_GET['filter']['CLUB'] != '' ) { ?>
		<script>
			changeclubfilter($('#club_select'));
		</script>
<? } ?>
		</div>
		<div class="panel-body panel-body-table">
			<div class="table-responsive">
				<div class="dataTables_wrapper no-footer" id="table_wrap">
<form method="get" action="" id="tablelistall">		
								<table class="table table-bordered <? if ( !$arParams['CONFIG']['COLOR']) { ?>table-striped<? } ?> table-actions">
									<thead>
										<tr>
	<?
		foreach ( $arParams['CONFIG']['FIELD'] as $name ) {
			if ( $name['SHOW_TIBLE'] == 'Y' ) {
	?>																			
											<th>
												<?=$name['NAME']?>
											</th>
			<? } ?>
	<? } ?>								
<? if ( !$arParams['CONFIG']['NO_BUTTON'] or $arParams['CONFIG']['NO_BUTTON'] != 'Y' ) { ?>														
											<th>Действия</th>
<? } ?>											
										</tr>
									</thead>
									<tbody>					
										<?foreach($arResult["ITEMS"] as $arItem):?>
											<?
											$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
											$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
											?>
												<tr id="<?=$this->GetEditAreaId($arItem['ID']);?>" <? if ( $arParams['CONFIG']['COLOR'] == 'sklad' and  $arItem['PROPERTIES'][$arParams['CONFIG']['FIELD'][5]['FIELD']]['VALUE'] < $arItem['PROPERTIES'][$arParams['CONFIG']['FIELD'][4]['FIELD']]['VALUE'] ) { ?>class="success"<? } elseif( $arParams['CONFIG']['COLOR'] == 'sklad' and  $arItem['PROPERTIES'][$arParams['CONFIG']['FIELD'][5]['FIELD']]['VALUE'] >= $arItem['PROPERTIES'][$arParams['CONFIG']['FIELD'][4]['FIELD']]['VALUE'] ) { ?>class="danger"<? } ?> >
												<? foreach ( $arParams['CONFIG']['FIELD'] as $value ) { ?>												
													<? if ( $value['TYPE'] == 'link' and $value['SHOW_TIBLE'] == 'Y' ) { ?>
																<?
																	if ( $arItem['PROPERTIES'][$value['FIELD']]['VALUE'] != '' ) {
																		$res = CIBlockElement::GetByID($arItem['PROPERTIES'][$value['FIELD']]['VALUE']);
																		$ar_res = $res->GetNext();
																	} else {
																		$ar_res['NAME'] = '-';
																	}
																?>														
																<td><?=$ar_res[$value['LINK']['FIELD']]?></td>
													<? } elseif ( $value['TYPE'] == 'count' and $value['SHOW_TIBLE'] == 'Y' ) { ?>
																		<td>
																			<?
																			$fiter['IBLOCK_ID'] = $value['LINK']['IBLOCK'];
																			if ( $value['LINK']['FIELD_TYPE'] == 'prop' ) {
																				$fiter['PROPERTY_'.$value['LINK']['FIELD']] = $arItem['ID'];
																			}
																			$cnt = CIBlockElement::GetList(
																				array(),
																				array($fiter),
																				array(),
																				false,
																				array('ID', 'NAME')
																			);
																			echo $cnt; 
																			?>					
																		</td>													
													<? } elseif ( $value['TYPE'] == 'img' and $value['SHOW_TIBLE'] == 'Y'  ) { ?>
																	<? if ($value['FIELD_TYPE'] == 'main') {?>
																		<td>
																		  <? if ( $arItem[$value['FIELD']]['SRC'] != '' ) { ?>
																			<div class="gallery">
																				<a class="gallery-item" data-gallery href="<?=$arItem[$value['FIELD']]['SRC']?>">
																					<img src="<?$img=CFile::ResizeImageGet($arItem[$value['FIELD']],array("width" => 50, "height" => 50),BX_RESIZE_IMAGE_EXACT); echo $img['src']?>">
																				</a>
																			</div>
																		  <? } ?>
																		</td>													
																	<? } elseif ($value['FIELD_TYPE'] == 'prop') {?>
																		<td>
																		   <? if ( is_array($arItem['PROPERTIES'][$value['FIELD']]['VALUE']) ) { ?>
																				<? foreach ( $arItem['PROPERTIES'][$value['FIELD']]['VALUE'] as $img ) { ?>
																						<a data-gallery class="gallery-item" href="<?=CFile::GetPath($img)?>"><img src="<?$img=CFile::ResizeImageGet($img,array("width" => 50, "height" => 50),BX_RESIZE_IMAGE_EXACT); echo $img['src']?>"></a>
																				<? } ?>
																		   <? } else { ?>
																				 <? if ( $arItem['PROPERTIES'][$value['FIELD']]['VALUE'] != '' ) { ?>
																					<a data-gallery class="gallery-item" href="<?=CFile::GetPath($arItem['PROPERTIES'][$value['FIELD']]['VALUE'])?>"><img src="<?$img=CFile::ResizeImageGet($arItem['PROPERTIES'][$value['FIELD']]['VALUE'],array("width" => 50, "height" => 50),BX_RESIZE_IMAGE_EXACT); echo $img['src']?>"></a>
																				 <? } ?>
																		   <? } ?>
																		</td>
																	<? } ?>
													<? } elseif ( $value['TYPE'] == 'files' and $value['SHOW_TIBLE'] == 'Y'  ) { ?>
																	<td>
																	   <? if ( $arItem['PROPERTIES'][$value['FIELD']]['VALUE'] != '' ) { ?>
																		<a href="<?=CFile::GetPath($arItem['PROPERTIES'][$value['FIELD']]['VALUE'])?>">файл</a>
																	   <? } else { ?>
																				-
																	   <? } ?>
																	</td>
													<? } elseif ( $value['TYPE'] == 'users' and $value['SHOW_TIBLE'] == 'Y'  ) { ?>
																	<td>
																		<?
																				$rsUser = CUser::GetByID($arItem['PROPERTIES'][$value['FIELD']]['VALUE']);
																				$arUser = $rsUser->Fetch();
																				echo $arUser['NAME'];
																		?>
																	</td>
													<? } elseif ( $value['TYPE'] == 'select' and $value['SHOW_TIBLE'] == 'Y'  ) { ?>
															<td>
																<?=$value['SELECT_VALUE'][$arItem['PROPERTIES'][$value['FIELD']]['VALUE']]?>
															</td>
													<? } elseif ( $value['TYPE'] == 'entercount' and $value['SHOW_TIBLE'] == 'Y'  ) { ?>
															<td>
																<div class="form-inline">
																	<a class="btn btn-default btn-condensed changeCount" data-sign="+" data-id="<?=$arItem['ID']?>">+</a>
																	<span style="padding: 0px 10px;" id="view_count_<?=$arItem['ID']?>"><?=$arItem['PROPERTIES'][$value['FIELD']]['VALUE']?></span>
																	<input id="count_<?=$arItem['ID']?>" value="<?=$arItem['PROPERTIES'][$value['FIELD']]['VALUE']?>" name="data[<?=$arItem['ID']?>]" type="hidden">                    
																	<a class="btn btn-default btn-condensed changeCount" data-sign="-" data-id="<?=$arItem['ID']?>">-</a>
																</div>
															</td>
													<? } elseif ( $value['TYPE'] == 'date' and $value['SHOW_TIBLE'] == 'Y'  ) { ?>		
															<td>
																<?=str_replace("00:00:00",'',date("d.m.Y H:i:s",$arItem['PROPERTIES'][$value['FIELD']]['VALUE']))?>
															</td>															
													<? } elseif(  $value['SHOW_TIBLE'] == 'Y'  ) { ?>
																<? if ($value['FIELD_TYPE'] == 'main') {?>
																			<td><? if ($i == 1) { ?><strong><? } ?><?=$arItem[$value['FIELD']]?><? if ($i == 1) { ?></strong><? } ?></td>
																<?  }elseif ($value['FIELD_TYPE'] == 'prop') {?>
																			<? if ( is_array($arItem['PROPERTIES'][$value['FIELD']]['VALUE']) ) { ?>
																					<td><?=$arItem['PROPERTIES'][$value['FIELD']]['VALUE']['TEXT']?></td>
																			<? } else { ?>
																				<td><?=$arItem['PROPERTIES'][$value['FIELD']]['VALUE']?></td>
																			<? } ?>
																<? } ?>
															<?}?>											
													<?}?>
<? if ( !$arParams['CONFIG']['NO_BUTTON'] or $arParams['CONFIG']['NO_BUTTON'] != 'Y' ) { ?>													
													<td>
														<a href="#" class="btn btn-default btn-condensed" title="Редактировать" onclick="add_edit('<?=$arParams['CONFIG']['MODULE']['filename']?>',<?=$arItem['ID']?>); return false;" >
															<span class="fa fa-pencil"></span>
														</a>
														<button type="button" class="mb-control btn btn-danger btn-condensed" title="Удалить" onclick="dellbox(<?=$arItem['ID']?>); return false;">
															<i class="fa fa-trash-o"></i>
														</button>
														<? foreach ( $arParams['CONFIG']['NEW_BUTTON'] as $button ) { ?>
															<a href="<?=str_replace("#ID#",$arItem['ID'],$button['URL'])?>" title="<?=$button['NAME']?>" class="btn btn-default btn-primary btn-condensed">
																<i class="<?=$button['ICON']?>"></i>
															</a>
														<? } ?>
													</td>
<? } ?>													
												</tr>
										<?endforeach;?>
									</tbody>
<? if ( $arParams['CONFIG']['SAVE_PAGE']  == 'Y' ) { ?>									
									<tfoot>
									  <tr>
										<td colspan="6">
												<button type="button" onclick="savetablelist();" id="submitChangeCount" class=" btn btn-default right">Сохранить</button>
										</td>
									  </tr>
									</tfoot>		
<? } ?>
								</table>	
                
</form>								
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
								<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
									<?=$arResult["NAV_STRING"]?>
								<?endif;?>									
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
<style>
	.errors {
		border: 1px solid red;
		background: #fde4e4 none repeat scroll 0 0;
	}
</style>
<div class="modal" id="add_edit_club" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" style="display: none;"></div>