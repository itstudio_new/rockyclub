function editconfig(id) {
	$.post('/admin/includes/config.php', { id: id },
		function(data) {
			$('#edit_user').html(data);
			$('#edit_user').modal();
		}
	)
}
function dellbox(id) {
	$.post('/admin/includes/delbox.php', { id: id },
		function(data) {
			$('#dellbox').html(data);
			$('#dellbox').modal();
		}
	)
}
function dellboxuser(id) {	
	$.post('/admin/includes/delboxuser.php', { id: id },
		function(data) {
			$('#dellbox').html(data);
			$('#dellbox').modal();
		}
	)
}
function delitemuser(id) {
	$.post('/admin/includes/ajax/delluser.php', { id: id },
		function(data) {
			location.reload();
		}
	)	
}
function add_edit_club(id) {
	$.post('/admin/includes/add_edit_club.php', { id: id },
		function(data) {
			$('#add_edit_club').html(data);
			$('#add_edit_club').modal();
		}
	)
}
function savesettings(id) {
	$.post('/admin/includes/ajax/saveconfig.php', $('#form_save').serialize(),
		function(data) {
			$('#settings_'+id+'_value').html($('#value_settings').val());
			$('#edit_user').modal('hide');
		}
	);
}
function delitem(id) {
	$.post('/admin/includes/ajax/dell.php', { id: id },
		function(data) {
			location.reload();
		}
	)	
}
function saveform_club(id) {
	$.post('/admin/includes/ajax/add_edit_club.php', $('#add_edit_club_form').serialize(),
		function(data) {
			location.reload();
		}
	)	
}
function add_edit_hall(id) {
	$.post('/admin/includes/add_edit_hall.php', { id: id },
		function(data) {
			$('#add_edit_club').html(data);
			$('#add_edit_club').modal();
		}
	)
}
function saveform_hall(id) {
	$.post('/admin/includes/ajax/add_edit_hall.php', $('#add_edit_hall_form').serialize(),
		function(data) {
			location.reload();
		}
	)	
}
function add_edit_city(id) {
	$.post('/admin/includes/add_edit_city.php', { id: id },
		function(data) {
			$('#add_edit_club').html(data);
			$('#add_edit_club').modal();
		}
	)
}
function saveform_city(id) {
	$.post('/admin/includes/ajax/add_edit_city.php', $('#add_edit_city_form').serialize(),
		function(data) {
			location.reload();
		}
	)	
}
function add_edit_bookerstates(id) {
	$.post('/admin/includes/add_edit_bookerstates.php', { id: id },
		function(data) {
			$('#add_edit_club').html(data);
			$('#add_edit_club').modal();
		}
	)
}
function saveform_bookerstates(id) {
	$.post('/admin/includes/ajax/add_edit_bookerstates.php', $('#add_edit_bookerstates_form').serialize(),
		function(data) {
			location.reload();
		}
	)	
}
function add_edit_referals(id) {
	$.post('/admin/includes/add_edit_referals.php', { id: id },
		function(data) {
			$('#add_edit_club').html(data);
			$('#add_edit_club').modal();
		}
	)
}
function saveform_referals(id) {
	$.post('/admin/includes/ajax/add_edit_referals.php', $('#add_edit_referals_form').serialize(),
		function(data) {
			location.reload();
		}
	)	
}

function saveform_user(){
	  $('.required_field').each(function(i,elem) {
		  $(this).removeClass('errors');
		  if ( $(this).val() == '' ) {
			$(this).addClass('errors');  
		  }
	  });
	  if ( $('#data_password').val() != $('#data_password_repeat').val() ) {
		  $('#data_password').addClass('errors');
		  $('#data_password_repeat').addClass('errors');
	  }
	  if ($('#add_edit_user_form .errors').length == 0 ) {
			$.post('/admin/includes/ajax/add_edit_user.php', $('#add_edit_user_form').serialize(),
				function(data) {
					location.reload();
				}
			)	
	  }
}
function add_edit(module,id) {
	$.post('/admin/includes/add_edit.php', {module:module, id: id },
		function(data) {
			$('#add_edit_club').html(data);
				var feFileInput = function(){
					if($("input.fileUpload").length > 0){
						$("input.fileUpload").fileinput({
							//browseClass: "btn btn-primary btn-block",
							//showCaption: false,
							uploadAsync: false,
							showUploadedThumbs: false,
							showPreview: false,
							language: 'ru',
							browseLabel: "Выбрать",
							showRemove: false,
							showUpload: false
						});
					}

				}
				var feBsFileInput = function(){
					if($("input.fileinput").length > 0){
						$("input.fileinput").bootstrapFileInput();
					}
					
				}
				var faDateTimePicker = function(){
					if($("input.datetimepicker").length > 0){
						$("input.datetimepicker").datetimepicker({
							format: 'dd-mm-yyyy hh:ii',
							autoclose: true,
							language: 'ru',
						})
					}
					if($("input.datetimepickerEn").length > 0){
						$("input.datetimepickerEn").datetimepicker({
							format: 'yyyy-mm-dd hh:ii',
							autoclose: true,
							language: 'ru',
						})
					}
				}
			feFileInput();
			feBsFileInput();
			faDateTimePicker();
			$('#add_edit_club').modal();
			if($("#useredit_hidden").length>0) {
			  $('#prop_CLIENT').val($('#useredit_hidden').val());
			}
		}
	)
}
function showpass(id) {
	if ( $('#'+id).attr('type') == 'text' ) {
		$('#'+id).attr('type','password');
	} else {
		$('#'+id).attr('type','text');
	}
}
function add_edit_user(id) {
	$.post('/admin/includes/add_edit_user.php', {id: id },
		function(data) {
			$('#add_edit_club').html(data);
				
				var feFileInput = function(){
					if($("input.fileUpload").length > 0){
						$("input.fileUpload").fileinput({
							//browseClass: "btn btn-primary btn-block",
							//showCaption: false,
							uploadAsync: false,
							showUploadedThumbs: false,
							showPreview: false,
							language: 'ru',
							browseLabel: "Выбрать",
							showRemove: false,
							showUpload: false
						});
					}

				}
				var feBsFileInput = function(){
					if($("input.fileinput").length > 0){
						$("input.fileinput").bootstrapFileInput();
					}
					
				}
			feFileInput();
			feBsFileInput();
			$('#add_edit_club').modal();
		}
	)
}
function saveform(ckickbutton,module) {
	  $('.required_field').each(function(i,elem) {
		  $(this).removeClass('errors');
		  if ( $(this).val() == '' ) {
			$(this).addClass('errors');  
		  }
	  });
	  if ($('#add_edit_form .errors').length == 0 ) {
		$.post('/admin/includes/ajax/add_edit.php?module='+module, $('#add_edit_form').serialize(),
			function(data) {
				location.reload();
			}
		)	
	  }
}
function changeclub(el) {
	if ($(el).val() == '') {
		$('.linked option').show();
	} else {
		$('.linked option').hide();
		$('.linked option[value=""]').show();
		$('.linked option[club='+$(el).val()+']').show();
		$('.linked option:visible:first').attr('selected','selected');
	}
}
function changeclubfilter(el) {
	if ($(el).val() == '') {
		$('.linked option').show();
	} else {
		$('.linked option').hide();
		$('.linked option[value=""]').show();
		$('.linked option[club='+$(el).val()+']').show();
	}
}
function leftblock() {
		$.post('/admin/includes/ajax/leftblock.php',
			function(data) {
				
			}
		)	
}
function savetablelist() {
		$.post('/admin/includes/ajax/update_table.php', $('#tablelistall').serialize(),
			function(data) {
				//location.reload();
			}
		)
}
$( document ).ready(function() {
	$('.changeCount').click(function(){
		var viewChange = "#view_count_"+$(this).attr('data-id');
		var countChange = "#count_"+$(this).attr('data-id');
		var count = $(countChange).val();
		if(!$("#submitChangeCount").hasClass('btn-success')){
			$("#submitChangeCount").addClass('btn-success');
		}
		switch ($(this).attr('data-sign')){
			case "+":{
				count++;
				break;
			}
			case "-":{
				count--;
				break;
			}
		}
		if(count>=0){
			$(viewChange).html(count);
			$(countChange).val(count);
		}
	});
});