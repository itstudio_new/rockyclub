"use strict";
if (!function (e, t) {
        "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function (e) {
            if (!e.document)throw new Error("jQuery requires a window with a document");
            return t(e)
        } : t(e)
    }("undefined" != typeof window ? window : void 0, function (e, t) {
        function n(e) {
            var t = "length" in e && e.length, n = oe.type(e);
            return "function" === n || oe.isWindow(e) ? !1 : 1 === e.nodeType && t ? !0 : "array" === n || 0 === t || "number" == typeof t && t > 0 && t - 1 in e
        }

        function i(e, t, n) {
            if (oe.isFunction(t))return oe.grep(e, function (e, i) {
                return !!t.call(e, i, e) !== n
            });
            if (t.nodeType)return oe.grep(e, function (e) {
                return e === t !== n
            });
            if ("string" == typeof t) {
                if (ue.test(t))return oe.filter(t, e, n);
                t = oe.filter(t, e)
            }
            return oe.grep(e, function (e) {
                return oe.inArray(e, t) >= 0 !== n
            })
        }

        function o(e, t) {
            do e = e[t]; while (e && 1 !== e.nodeType);
            return e
        }

        function a(e) {
            var t = be[e] = {};
            return oe.each(e.match(we) || [], function (e, n) {
                t[n] = !0
            }), t
        }

        function r() {
            fe.addEventListener ? (fe.removeEventListener("DOMContentLoaded", s, !1), e.removeEventListener("load", s, !1)) : (fe.detachEvent("onreadystatechange", s), e.detachEvent("onload", s))
        }

        function s() {
            (fe.addEventListener || "load" === event.type || "complete" === fe.readyState) && (r(), oe.ready())
        }

        function l(e, t, n) {
            if (void 0 === n && 1 === e.nodeType) {
                var i = "data-" + t.replace(ke, "-$1").toLowerCase();
                if (n = e.getAttribute(i), "string" == typeof n) {
                    try {
                        n = "true" === n ? !0 : "false" === n ? !1 : "null" === n ? null : +n + "" === n ? +n : Se.test(n) ? oe.parseJSON(n) : n
                    } catch (o) {
                    }
                    oe.data(e, t, n)
                } else n = void 0
            }
            return n
        }

        function d(e) {
            var t;
            for (t in e)if (("data" !== t || !oe.isEmptyObject(e[t])) && "toJSON" !== t)return !1;
            return !0
        }

        function p(e, t, n, i) {
            if (oe.acceptData(e)) {
                var o, a, r = oe.expando, s = e.nodeType, l = s ? oe.cache : e, d = s ? e[r] : e[r] && r;
                if (d && l[d] && (i || l[d].data) || void 0 !== n || "string" != typeof t)return d || (d = s ? e[r] = U.pop() || oe.guid++ : r), l[d] || (l[d] = s ? {} : {toJSON: oe.noop}), ("object" == typeof t || "function" == typeof t) && (i ? l[d] = oe.extend(l[d], t) : l[d].data = oe.extend(l[d].data, t)), a = l[d], i || (a.data || (a.data = {}), a = a.data), void 0 !== n && (a[oe.camelCase(t)] = n), "string" == typeof t ? (o = a[t], null == o && (o = a[oe.camelCase(t)])) : o = a, o
            }
        }

        function c(e, t, n) {
            if (oe.acceptData(e)) {
                var i, o, a = e.nodeType, r = a ? oe.cache : e, s = a ? e[oe.expando] : oe.expando;
                if (r[s]) {
                    if (t && (i = n ? r[s] : r[s].data)) {
                        oe.isArray(t) ? t = t.concat(oe.map(t, oe.camelCase)) : t in i ? t = [t] : (t = oe.camelCase(t), t = t in i ? [t] : t.split(" ")), o = t.length;
                        for (; o--;)delete i[t[o]];
                        if (n ? !d(i) : !oe.isEmptyObject(i))return
                    }
                    (n || (delete r[s].data, d(r[s]))) && (a ? oe.cleanData([e], !0) : ne.deleteExpando || r != r.window ? delete r[s] : r[s] = null)
                }
            }
        }

        function u() {
            return !0
        }

        function h() {
            return !1
        }

        function f() {
            try {
                return fe.activeElement
            } catch (e) {
            }
        }

        function m(e) {
            var t = ze.split("|"), n = e.createDocumentFragment();
            if (n.createElement)for (; t.length;)n.createElement(t.pop());
            return n
        }

        function g(e, t) {
            var n, i, o = 0, a = typeof e.getElementsByTagName !== Ce ? e.getElementsByTagName(t || "*") : typeof e.querySelectorAll !== Ce ? e.querySelectorAll(t || "*") : void 0;
            if (!a)for (a = [], n = e.childNodes || e; null != (i = n[o]); o++)!t || oe.nodeName(i, t) ? a.push(i) : oe.merge(a, g(i, t));
            return void 0 === t || t && oe.nodeName(e, t) ? oe.merge([e], a) : a
        }

        function v(e) {
            Ne.test(e.type) && (e.defaultChecked = e.checked)
        }

        function y(e, t) {
            return oe.nodeName(e, "table") && oe.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e
        }

        function w(e) {
            return e.type = (null !== oe.find.attr(e, "type")) + "/" + e.type, e
        }

        function b(e) {
            var t = Xe.exec(e.type);
            return t ? e.type = t[1] : e.removeAttribute("type"), e
        }

        function x(e, t) {
            for (var n, i = 0; null != (n = e[i]); i++)oe._data(n, "globalEval", !t || oe._data(t[i], "globalEval"))
        }

        function T(e, t) {
            if (1 === t.nodeType && oe.hasData(e)) {
                var n, i, o, a = oe._data(e), r = oe._data(t, a), s = a.events;
                if (s) {
                    delete r.handle, r.events = {};
                    for (n in s)for (i = 0, o = s[n].length; o > i; i++)oe.event.add(t, n, s[n][i])
                }
                r.data && (r.data = oe.extend({}, r.data))
            }
        }

        function C(e, t) {
            var n, i, o;
            if (1 === t.nodeType) {
                if (n = t.nodeName.toLowerCase(), !ne.noCloneEvent && t[oe.expando]) {
                    o = oe._data(t);
                    for (i in o.events)oe.removeEvent(t, i, o.handle);
                    t.removeAttribute(oe.expando)
                }
                "script" === n && t.text !== e.text ? (w(t).text = e.text, b(t)) : "object" === n ? (t.parentNode && (t.outerHTML = e.outerHTML), ne.html5Clone && e.innerHTML && !oe.trim(t.innerHTML) && (t.innerHTML = e.innerHTML)) : "input" === n && Ne.test(e.type) ? (t.defaultChecked = t.checked = e.checked, t.value !== e.value && (t.value = e.value)) : "option" === n ? t.defaultSelected = t.selected = e.defaultSelected : ("input" === n || "textarea" === n) && (t.defaultValue = e.defaultValue)
            }
        }

        function S(t, n) {
            var i, o = oe(n.createElement(t)).appendTo(n.body), a = e.getDefaultComputedStyle && (i = e.getDefaultComputedStyle(o[0])) ? i.display : oe.css(o[0], "display");
            return o.detach(), a
        }

        function k(e) {
            var t = fe, n = Je[e];
            return n || (n = S(e, t), "none" !== n && n || (Ze = (Ze || oe("<iframe frameborder='0' width='0' height='0'/>")).appendTo(t.documentElement), t = (Ze[0].contentWindow || Ze[0].contentDocument).document, t.write(), t.close(), n = S(e, t), Ze.detach()), Je[e] = n), n
        }

        function E(e, t) {
            return {
                get: function () {
                    var n = e();
                    return null != n ? n ? void delete this.get : (this.get = t).apply(this, arguments) : void 0
                }
            }
        }

        function P(e, t) {
            if (t in e)return t;
            for (var n = t.charAt(0).toUpperCase() + t.slice(1), i = t, o = ut.length; o--;)if (t = ut[o] + n, t in e)return t;
            return i
        }

        function I(e, t) {
            for (var n, i, o, a = [], r = 0, s = e.length; s > r; r++)i = e[r], i.style && (a[r] = oe._data(i, "olddisplay"), n = i.style.display, t ? (a[r] || "none" !== n || (i.style.display = ""), "" === i.style.display && Ie(i) && (a[r] = oe._data(i, "olddisplay", k(i.nodeName)))) : (o = Ie(i), (n && "none" !== n || !o) && oe._data(i, "olddisplay", o ? n : oe.css(i, "display"))));
            for (r = 0; s > r; r++)i = e[r], i.style && (t && "none" !== i.style.display && "" !== i.style.display || (i.style.display = t ? a[r] || "" : "none"));
            return e
        }

        function D(e, t, n) {
            var i = lt.exec(t);
            return i ? Math.max(0, i[1] - (n || 0)) + (i[2] || "px") : t
        }

        function N(e, t, n, i, o) {
            for (var a = n === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0, r = 0; 4 > a; a += 2)"margin" === n && (r += oe.css(e, n + Pe[a], !0, o)), i ? ("content" === n && (r -= oe.css(e, "padding" + Pe[a], !0, o)), "margin" !== n && (r -= oe.css(e, "border" + Pe[a] + "Width", !0, o))) : (r += oe.css(e, "padding" + Pe[a], !0, o), "padding" !== n && (r += oe.css(e, "border" + Pe[a] + "Width", !0, o)));
            return r
        }

        function $(e, t, n) {
            var i = !0, o = "width" === t ? e.offsetWidth : e.offsetHeight, a = et(e), r = ne.boxSizing && "border-box" === oe.css(e, "boxSizing", !1, a);
            if (0 >= o || null == o) {
                if (o = tt(e, t, a), (0 > o || null == o) && (o = e.style[t]), it.test(o))return o;
                i = r && (ne.boxSizingReliable() || o === e.style[t]), o = parseFloat(o) || 0
            }
            return o + N(e, t, n || (r ? "border" : "content"), i, a) + "px"
        }

        function A(e, t, n, i, o) {
            return new A.prototype.init(e, t, n, i, o)
        }

        function L() {
            return setTimeout(function () {
                ht = void 0
            }), ht = oe.now()
        }

        function M(e, t) {
            var n, i = {height: e}, o = 0;
            for (t = t ? 1 : 0; 4 > o; o += 2 - t)n = Pe[o], i["margin" + n] = i["padding" + n] = e;
            return t && (i.opacity = i.width = e), i
        }

        function O(e, t, n) {
            for (var i, o = (wt[t] || []).concat(wt["*"]), a = 0, r = o.length; r > a; a++)if (i = o[a].call(n, t, e))return i
        }

        function z(e, t, n) {
            var i, o, a, r, s, l, d, p, c = this, u = {}, h = e.style, f = e.nodeType && Ie(e), m = oe._data(e, "fxshow");
            n.queue || (s = oe._queueHooks(e, "fx"), null == s.unqueued && (s.unqueued = 0, l = s.empty.fire, s.empty.fire = function () {
                s.unqueued || l()
            }), s.unqueued++, c.always(function () {
                c.always(function () {
                    s.unqueued--, oe.queue(e, "fx").length || s.empty.fire()
                })
            })), 1 === e.nodeType && ("height" in t || "width" in t) && (n.overflow = [h.overflow, h.overflowX, h.overflowY], d = oe.css(e, "display"), p = "none" === d ? oe._data(e, "olddisplay") || k(e.nodeName) : d, "inline" === p && "none" === oe.css(e, "float") && (ne.inlineBlockNeedsLayout && "inline" !== k(e.nodeName) ? h.zoom = 1 : h.display = "inline-block")), n.overflow && (h.overflow = "hidden", ne.shrinkWrapBlocks() || c.always(function () {
                h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2]
            }));
            for (i in t)if (o = t[i], mt.exec(o)) {
                if (delete t[i], a = a || "toggle" === o, o === (f ? "hide" : "show")) {
                    if ("show" !== o || !m || void 0 === m[i])continue;
                    f = !0
                }
                u[i] = m && m[i] || oe.style(e, i)
            } else d = void 0;
            if (oe.isEmptyObject(u))"inline" === ("none" === d ? k(e.nodeName) : d) && (h.display = d); else {
                m ? "hidden" in m && (f = m.hidden) : m = oe._data(e, "fxshow", {}), a && (m.hidden = !f), f ? oe(e).show() : c.done(function () {
                    oe(e).hide()
                }), c.done(function () {
                    var t;
                    oe._removeData(e, "fxshow");
                    for (t in u)oe.style(e, t, u[t])
                });
                for (i in u)r = O(f ? m[i] : 0, i, c), i in m || (m[i] = r.start, f && (r.end = r.start, r.start = "width" === i || "height" === i ? 1 : 0))
            }
        }

        function _(e, t) {
            var n, i, o, a, r;
            for (n in e)if (i = oe.camelCase(n), o = t[i], a = e[n], oe.isArray(a) && (o = a[1], a = e[n] = a[0]), n !== i && (e[i] = a, delete e[n]), r = oe.cssHooks[i], r && "expand" in r) {
                a = r.expand(a), delete e[i];
                for (n in a)n in e || (e[n] = a[n], t[n] = o)
            } else t[i] = o
        }

        function j(e, t, n) {
            var i, o, a = 0, r = yt.length, s = oe.Deferred().always(function () {
                delete l.elem
            }), l = function c() {
                if (o)return !1;
                for (var t = ht || L(), n = Math.max(0, d.startTime + d.duration - t), i = n / d.duration || 0, a = 1 - i, r = 0, c = d.tweens.length; c > r; r++)d.tweens[r].run(a);
                return s.notifyWith(e, [d, a, n]), 1 > a && c ? n : (s.resolveWith(e, [d]), !1)
            }, d = s.promise({
                elem: e,
                props: oe.extend({}, t),
                opts: oe.extend(!0, {specialEasing: {}}, n),
                originalProperties: t,
                originalOptions: n,
                startTime: ht || L(),
                duration: n.duration,
                tweens: [],
                createTween: function (t, n) {
                    var i = oe.Tween(e, d.opts, t, n, d.opts.specialEasing[t] || d.opts.easing);
                    return d.tweens.push(i), i
                },
                stop: function (t) {
                    var n = 0, i = t ? d.tweens.length : 0;
                    if (o)return this;
                    for (o = !0; i > n; n++)d.tweens[n].run(1);
                    return t ? s.resolveWith(e, [d, t]) : s.rejectWith(e, [d, t]), this
                }
            }), p = d.props;
            for (_(p, d.opts.specialEasing); r > a; a++)if (i = yt[a].call(d, e, p, d.opts))return i;
            return oe.map(p, O, d), oe.isFunction(d.opts.start) && d.opts.start.call(e, d), oe.fx.timer(oe.extend(l, {
                elem: e,
                anim: d,
                queue: d.opts.queue
            })), d.progress(d.opts.progress).done(d.opts.done, d.opts.complete).fail(d.opts.fail).always(d.opts.always)
        }

        function R(e) {
            return function (t, n) {
                "string" != typeof t && (n = t, t = "*");
                var i, o = 0, a = t.toLowerCase().match(we) || [];
                if (oe.isFunction(n))for (; i = a[o++];)"+" === i.charAt(0) ? (i = i.slice(1) || "*", (e[i] = e[i] || []).unshift(n)) : (e[i] = e[i] || []).push(n)
            }
        }

        function H(e, t, n, i) {
            function o(s) {
                var l;
                return a[s] = !0, oe.each(e[s] || [], function (e, s) {
                    var d = s(t, n, i);
                    return "string" != typeof d || r || a[d] ? r ? !(l = d) : void 0 : (t.dataTypes.unshift(d), o(d), !1)
                }), l
            }

            var a = {}, r = e === Wt;
            return o(t.dataTypes[0]) || !a["*"] && o("*")
        }

        function F(e, t) {
            var n, i, o = oe.ajaxSettings.flatOptions || {};
            for (i in t)void 0 !== t[i] && ((o[i] ? e : n || (n = {}))[i] = t[i]);
            return n && oe.extend(!0, e, n), e
        }

        function B(e, t, n) {
            for (var i, o, a, r, s = e.contents, l = e.dataTypes; "*" === l[0];)l.shift(), void 0 === o && (o = e.mimeType || t.getResponseHeader("Content-Type"));
            if (o)for (r in s)if (s[r] && s[r].test(o)) {
                l.unshift(r);
                break
            }
            if (l[0] in n)a = l[0]; else {
                for (r in n) {
                    if (!l[0] || e.converters[r + " " + l[0]]) {
                        a = r;
                        break
                    }
                    i || (i = r)
                }
                a = a || i
            }
            return a ? (a !== l[0] && l.unshift(a), n[a]) : void 0
        }

        function W(e, t, n, i) {
            var o, a, r, s, l, d = {}, p = e.dataTypes.slice();
            if (p[1])for (r in e.converters)d[r.toLowerCase()] = e.converters[r];
            for (a = p.shift(); a;)if (e.responseFields[a] && (n[e.responseFields[a]] = t), !l && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = a, a = p.shift())if ("*" === a)a = l; else if ("*" !== l && l !== a) {
                if (r = d[l + " " + a] || d["* " + a], !r)for (o in d)if (s = o.split(" "), s[1] === a && (r = d[l + " " + s[0]] || d["* " + s[0]])) {
                    r === !0 ? r = d[o] : d[o] !== !0 && (a = s[0], p.unshift(s[1]));
                    break
                }
                if (r !== !0)if (r && e["throws"])t = r(t); else try {
                    t = r(t)
                } catch (c) {
                    return {state: "parsererror", error: r ? c : "No conversion from " + l + " to " + a}
                }
            }
            return {state: "success", data: t}
        }

        function q(e, t, n, i) {
            var o;
            if (oe.isArray(t))oe.each(t, function (t, o) {
                n || Xt.test(e) ? i(e, o) : q(e + "[" + ("object" == typeof o ? t : "") + "]", o, n, i)
            }); else if (n || "object" !== oe.type(t))i(e, t); else for (o in t)q(e + "[" + o + "]", t[o], n, i)
        }

        function V() {
            try {
                return new e.XMLHttpRequest
            } catch (t) {
            }
        }

        function G() {
            try {
                return new e.ActiveXObject("Microsoft.XMLHTTP")
            } catch (t) {
            }
        }

        function X(e) {
            return oe.isWindow(e) ? e : 9 === e.nodeType ? e.defaultView || e.parentWindow : !1
        }

        var U = [], Y = U.slice, K = U.concat, Q = U.push, Z = U.indexOf, J = {}, ee = J.toString, te = J.hasOwnProperty, ne = {}, ie = "1.11.3", oe = function sn(e, t) {
            return new sn.fn.init(e, t)
        }, ae = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, re = /^-ms-/, se = /-([\da-z])/gi, le = function (e, t) {
            return t.toUpperCase()
        };
        oe.fn = oe.prototype = {
            jquery: ie, constructor: oe, selector: "", length: 0, toArray: function () {
                return Y.call(this)
            }, get: function (e) {
                return null != e ? 0 > e ? this[e + this.length] : this[e] : Y.call(this)
            }, pushStack: function (e) {
                var t = oe.merge(this.constructor(), e);
                return t.prevObject = this, t.context = this.context, t
            }, each: function (e, t) {
                return oe.each(this, e, t)
            }, map: function (e) {
                return this.pushStack(oe.map(this, function (t, n) {
                    return e.call(t, n, t)
                }))
            }, slice: function () {
                return this.pushStack(Y.apply(this, arguments))
            }, first: function () {
                return this.eq(0)
            }, last: function () {
                return this.eq(-1)
            }, eq: function (e) {
                var t = this.length, n = +e + (0 > e ? t : 0);
                return this.pushStack(n >= 0 && t > n ? [this[n]] : [])
            }, end: function () {
                return this.prevObject || this.constructor(null)
            }, push: Q, sort: U.sort, splice: U.splice
        }, oe.extend = oe.fn.extend = function () {
            var e, t, n, i, o, a, r = arguments[0] || {}, s = 1, l = arguments.length, d = !1;
            for ("boolean" == typeof r && (d = r, r = arguments[s] || {}, s++), "object" == typeof r || oe.isFunction(r) || (r = {}), s === l && (r = this, s--); l > s; s++)if (null != (o = arguments[s]))for (i in o)e = r[i], n = o[i], r !== n && (d && n && (oe.isPlainObject(n) || (t = oe.isArray(n))) ? (t ? (t = !1, a = e && oe.isArray(e) ? e : []) : a = e && oe.isPlainObject(e) ? e : {}, r[i] = oe.extend(d, a, n)) : void 0 !== n && (r[i] = n));
            return r
        }, oe.extend({
            expando: "jQuery" + (ie + Math.random()).replace(/\D/g, ""), isReady: !0, error: function (e) {
                throw new Error(e)
            }, noop: function () {
            }, isFunction: function (e) {
                return "function" === oe.type(e)
            }, isArray: Array.isArray || function (e) {
                return "array" === oe.type(e)
            }, isWindow: function (e) {
                return null != e && e == e.window
            }, isNumeric: function (e) {
                return !oe.isArray(e) && e - parseFloat(e) + 1 >= 0
            }, isEmptyObject: function (e) {
                var t;
                for (t in e)return !1;
                return !0
            }, isPlainObject: function (e) {
                var t;
                if (!e || "object" !== oe.type(e) || e.nodeType || oe.isWindow(e))return !1;
                try {
                    if (e.constructor && !te.call(e, "constructor") && !te.call(e.constructor.prototype, "isPrototypeOf"))return !1
                } catch (n) {
                    return !1
                }
                if (ne.ownLast)for (t in e)return te.call(e, t);
                for (t in e);
                return void 0 === t || te.call(e, t)
            }, type: function (e) {
                return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? J[ee.call(e)] || "object" : typeof e
            }, globalEval: function (t) {
                t && oe.trim(t) && (e.execScript || function (t) {
                    e.eval.call(e, t)
                })(t)
            }, camelCase: function (e) {
                return e.replace(re, "ms-").replace(se, le)
            }, nodeName: function (e, t) {
                return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
            }, each: function (e, t, i) {
                var o, a = 0, r = e.length, s = n(e);
                if (i) {
                    if (s)for (; r > a && (o = t.apply(e[a], i), o !== !1); a++); else for (a in e)if (o = t.apply(e[a], i), o === !1)break
                } else if (s)for (; r > a && (o = t.call(e[a], a, e[a]), o !== !1); a++); else for (a in e)if (o = t.call(e[a], a, e[a]), o === !1)break;
                return e
            }, trim: function (e) {
                return null == e ? "" : (e + "").replace(ae, "")
            }, makeArray: function (e, t) {
                var i = t || [];
                return null != e && (n(Object(e)) ? oe.merge(i, "string" == typeof e ? [e] : e) : Q.call(i, e)), i
            }, inArray: function (e, t, n) {
                var i;
                if (t) {
                    if (Z)return Z.call(t, e, n);
                    for (i = t.length, n = n ? 0 > n ? Math.max(0, i + n) : n : 0; i > n; n++)if (n in t && t[n] === e)return n
                }
                return -1
            }, merge: function (e, t) {
                for (var n = +t.length, i = 0, o = e.length; n > i;)e[o++] = t[i++];
                if (n !== n)for (; void 0 !== t[i];)e[o++] = t[i++];
                return e.length = o, e
            }, grep: function (e, t, n) {
                for (var i, o = [], a = 0, r = e.length, s = !n; r > a; a++)i = !t(e[a], a), i !== s && o.push(e[a]);
                return o
            }, map: function (e, t, i) {
                var o, a = 0, r = e.length, s = n(e), l = [];
                if (s)for (; r > a; a++)o = t(e[a], a, i), null != o && l.push(o); else for (a in e)o = t(e[a], a, i), null != o && l.push(o);
                return K.apply([], l)
            }, guid: 1, proxy: function (e, t) {
                var n, i, o;
                return "string" == typeof t && (o = e[t], t = e, e = o), oe.isFunction(e) ? (n = Y.call(arguments, 2), i = function () {
                    return e.apply(t || this, n.concat(Y.call(arguments)))
                }, i.guid = e.guid = e.guid || oe.guid++, i) : void 0
            }, now: function () {
                return +new Date
            }, support: ne
        }), oe.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function (e, t) {
            J["[object " + t + "]"] = t.toLowerCase()
        });
        var de = function (e) {
            function t(e, t, n, i) {
                var o, a, r, s, l, d, c, h, f, m;
                if ((t ? t.ownerDocument || t : H) !== A && $(t), t = t || A, n = n || [], s = t.nodeType, "string" != typeof e || !e || 1 !== s && 9 !== s && 11 !== s)return n;
                if (!i && M) {
                    if (11 !== s && (o = ye.exec(e)))if (r = o[1]) {
                        if (9 === s) {
                            if (a = t.getElementById(r), !a || !a.parentNode)return n;
                            if (a.id === r)return n.push(a), n
                        } else if (t.ownerDocument && (a = t.ownerDocument.getElementById(r)) && j(t, a) && a.id === r)return n.push(a), n
                    } else {
                        if (o[2])return Z.apply(n, t.getElementsByTagName(e)), n;
                        if ((r = o[3]) && x.getElementsByClassName)return Z.apply(n, t.getElementsByClassName(r)), n
                    }
                    if (x.qsa && (!O || !O.test(e))) {
                        if (h = c = R, f = t, m = 1 !== s && e, 1 === s && "object" !== t.nodeName.toLowerCase()) {
                            for (d = k(e), (c = t.getAttribute("id")) ? h = c.replace(be, "\\$&") : t.setAttribute("id", h), h = "[id='" + h + "'] ", l = d.length; l--;)d[l] = h + u(d[l]);
                            f = we.test(e) && p(t.parentNode) || t, m = d.join(",")
                        }
                        if (m)try {
                            return Z.apply(n, f.querySelectorAll(m)), n
                        } catch (g) {
                        } finally {
                            c || t.removeAttribute("id")
                        }
                    }
                }
                return P(e.replace(le, "$1"), t, n, i)
            }

            function n() {
                function e(n, i) {
                    return t.push(n + " ") > T.cacheLength && delete e[t.shift()], e[n + " "] = i
                }

                var t = [];
                return e
            }

            function i(e) {
                return e[R] = !0, e
            }

            function o(e) {
                var t = A.createElement("div");
                try {
                    return !!e(t)
                } catch (n) {
                    return !1
                } finally {
                    t.parentNode && t.parentNode.removeChild(t), t = null
                }
            }

            function a(e, t) {
                for (var n = e.split("|"), i = e.length; i--;)T.attrHandle[n[i]] = t
            }

            function r(e, t) {
                var n = t && e, i = n && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || X) - (~e.sourceIndex || X);
                if (i)return i;
                if (n)for (; n = n.nextSibling;)if (n === t)return -1;
                return e ? 1 : -1
            }

            function s(e) {
                return function (t) {
                    var n = t.nodeName.toLowerCase();
                    return "input" === n && t.type === e
                }
            }

            function l(e) {
                return function (t) {
                    var n = t.nodeName.toLowerCase();
                    return ("input" === n || "button" === n) && t.type === e
                }
            }

            function d(e) {
                return i(function (t) {
                    return t = +t, i(function (n, i) {
                        for (var o, a = e([], n.length, t), r = a.length; r--;)n[o = a[r]] && (n[o] = !(i[o] = n[o]))
                    })
                })
            }

            function p(e) {
                return e && "undefined" != typeof e.getElementsByTagName && e
            }

            function c() {
            }

            function u(e) {
                for (var t = 0, n = e.length, i = ""; n > t; t++)i += e[t].value;
                return i
            }

            function h(e, t, n) {
                var i = t.dir, o = n && "parentNode" === i, a = B++;
                return t.first ? function (t, n, a) {
                    for (; t = t[i];)if (1 === t.nodeType || o)return e(t, n, a)
                } : function (t, n, r) {
                    var s, l, d = [F, a];
                    if (r) {
                        for (; t = t[i];)if ((1 === t.nodeType || o) && e(t, n, r))return !0
                    } else for (; t = t[i];)if (1 === t.nodeType || o) {
                        if (l = t[R] || (t[R] = {}), (s = l[i]) && s[0] === F && s[1] === a)return d[2] = s[2];
                        if (l[i] = d, d[2] = e(t, n, r))return !0
                    }
                }
            }

            function f(e) {
                return e.length > 1 ? function (t, n, i) {
                    for (var o = e.length; o--;)if (!e[o](t, n, i))return !1;
                    return !0
                } : e[0]
            }

            function m(e, n, i) {
                for (var o = 0, a = n.length; a > o; o++)t(e, n[o], i);
                return i
            }

            function g(e, t, n, i, o) {
                for (var a, r = [], s = 0, l = e.length, d = null != t; l > s; s++)(a = e[s]) && (!n || n(a, i, o)) && (r.push(a), d && t.push(s));
                return r
            }

            function v(e, t, n, o, a, r) {
                return o && !o[R] && (o = v(o)), a && !a[R] && (a = v(a, r)), i(function (i, r, s, l) {
                    var d, p, c, u = [], h = [], f = r.length, v = i || m(t || "*", s.nodeType ? [s] : s, []), y = !e || !i && t ? v : g(v, u, e, s, l), w = n ? a || (i ? e : f || o) ? [] : r : y;
                    if (n && n(y, w, s, l), o)for (d = g(w, h), o(d, [], s, l), p = d.length; p--;)(c = d[p]) && (w[h[p]] = !(y[h[p]] = c));
                    if (i) {
                        if (a || e) {
                            if (a) {
                                for (d = [], p = w.length; p--;)(c = w[p]) && d.push(y[p] = c);
                                a(null, w = [], d, l)
                            }
                            for (p = w.length; p--;)(c = w[p]) && (d = a ? ee(i, c) : u[p]) > -1 && (i[d] = !(r[d] = c))
                        }
                    } else w = g(w === r ? w.splice(f, w.length) : w), a ? a(null, r, w, l) : Z.apply(r, w)
                })
            }

            function y(e) {
                for (var t, n, i, o = e.length, a = T.relative[e[0].type], r = a || T.relative[" "], s = a ? 1 : 0, l = h(function (e) {
                    return e === t
                }, r, !0), d = h(function (e) {
                    return ee(t, e) > -1
                }, r, !0), p = [function (e, n, i) {
                    var o = !a && (i || n !== I) || ((t = n).nodeType ? l(e, n, i) : d(e, n, i));
                    return t = null, o
                }]; o > s; s++)if (n = T.relative[e[s].type])p = [h(f(p), n)]; else {
                    if (n = T.filter[e[s].type].apply(null, e[s].matches), n[R]) {
                        for (i = ++s; o > i && !T.relative[e[i].type]; i++);
                        return v(s > 1 && f(p), s > 1 && u(e.slice(0, s - 1).concat({value: " " === e[s - 2].type ? "*" : ""})).replace(le, "$1"), n, i > s && y(e.slice(s, i)), o > i && y(e = e.slice(i)), o > i && u(e))
                    }
                    p.push(n)
                }
                return f(p)
            }

            function w(e, n) {
                var o = n.length > 0, a = e.length > 0, r = function (i, r, s, l, d) {
                    var p, c, u, h = 0, f = "0", m = i && [], v = [], y = I, w = i || a && T.find.TAG("*", d), b = F += null == y ? 1 : Math.random() || .1, x = w.length;
                    for (d && (I = r !== A && r); f !== x && null != (p = w[f]); f++) {
                        if (a && p) {
                            for (c = 0; u = e[c++];)if (u(p, r, s)) {
                                l.push(p);
                                break
                            }
                            d && (F = b)
                        }
                        o && ((p = !u && p) && h--, i && m.push(p))
                    }
                    if (h += f, o && f !== h) {
                        for (c = 0; u = n[c++];)u(m, v, r, s);
                        if (i) {
                            if (h > 0)for (; f--;)m[f] || v[f] || (v[f] = K.call(l));
                            v = g(v)
                        }
                        Z.apply(l, v), d && !i && v.length > 0 && h + n.length > 1 && t.uniqueSort(l)
                    }
                    return d && (F = b, I = y), m
                };
                return o ? i(r) : r
            }

            var b, x, T, C, S, k, E, P, I, D, N, $, A, L, M, O, z, _, j, R = "sizzle" + 1 * new Date, H = e.document, F = 0, B = 0, W = n(), q = n(), V = n(), G = function (e, t) {
                return e === t && (N = !0), 0
            }, X = 1 << 31, U = {}.hasOwnProperty, Y = [], K = Y.pop, Q = Y.push, Z = Y.push, J = Y.slice, ee = function (e, t) {
                for (var n = 0, i = e.length; i > n; n++)if (e[n] === t)return n;
                return -1
            }, te = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", ne = "[\\x20\\t\\r\\n\\f]", ie = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", oe = ie.replace("w", "w#"), ae = "\\[" + ne + "*(" + ie + ")(?:" + ne + "*([*^$|!~]?=)" + ne + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + oe + "))|)" + ne + "*\\]", re = ":(" + ie + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + ae + ")*)|.*)\\)|)", se = new RegExp(ne + "+", "g"), le = new RegExp("^" + ne + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ne + "+$", "g"), de = new RegExp("^" + ne + "*," + ne + "*"), pe = new RegExp("^" + ne + "*([>+~]|" + ne + ")" + ne + "*"), ce = new RegExp("=" + ne + "*([^\\]'\"]*?)" + ne + "*\\]", "g"), ue = new RegExp(re), he = new RegExp("^" + oe + "$"), fe = {
                ID: new RegExp("^#(" + ie + ")"),
                CLASS: new RegExp("^\\.(" + ie + ")"),
                TAG: new RegExp("^(" + ie.replace("w", "w*") + ")"),
                ATTR: new RegExp("^" + ae),
                PSEUDO: new RegExp("^" + re),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ne + "*(even|odd|(([+-]|)(\\d*)n|)" + ne + "*(?:([+-]|)" + ne + "*(\\d+)|))" + ne + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + te + ")$", "i"),
                needsContext: new RegExp("^" + ne + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ne + "*((?:-\\d)?\\d*)" + ne + "*\\)|)(?=[^-]|$)", "i")
            }, me = /^(?:input|select|textarea|button)$/i, ge = /^h\d$/i, ve = /^[^{]+\{\s*\[native \w/, ye = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, we = /[+~]/, be = /'|\\/g, xe = new RegExp("\\\\([\\da-f]{1,6}" + ne + "?|(" + ne + ")|.)", "ig"), Te = function (e, t, n) {
                var i = "0x" + t - 65536;
                return i !== i || n ? t : 0 > i ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
            }, Ce = function () {
                $()
            };
            try {
                Z.apply(Y = J.call(H.childNodes), H.childNodes), Y[H.childNodes.length].nodeType
            } catch (Se) {
                Z = {
                    apply: Y.length ? function (e, t) {
                        Q.apply(e, J.call(t))
                    } : function (e, t) {
                        for (var n = e.length, i = 0; e[n++] = t[i++];);
                        e.length = n - 1
                    }
                }
            }
            x = t.support = {}, S = t.isXML = function (e) {
                var t = e && (e.ownerDocument || e).documentElement;
                return t ? "HTML" !== t.nodeName : !1
            }, $ = t.setDocument = function (e) {
                var t, n, i = e ? e.ownerDocument || e : H;
                return i !== A && 9 === i.nodeType && i.documentElement ? (A = i, L = i.documentElement, n = i.defaultView, n && n !== n.top && (n.addEventListener ? n.addEventListener("unload", Ce, !1) : n.attachEvent && n.attachEvent("onunload", Ce)), M = !S(i), x.attributes = o(function (e) {
                    return e.className = "i", !e.getAttribute("className")
                }), x.getElementsByTagName = o(function (e) {
                    return e.appendChild(i.createComment("")), !e.getElementsByTagName("*").length
                }), x.getElementsByClassName = ve.test(i.getElementsByClassName), x.getById = o(function (e) {
                    return L.appendChild(e).id = R, !i.getElementsByName || !i.getElementsByName(R).length
                }), x.getById ? (T.find.ID = function (e, t) {
                    if ("undefined" != typeof t.getElementById && M) {
                        var n = t.getElementById(e);
                        return n && n.parentNode ? [n] : []
                    }
                }, T.filter.ID = function (e) {
                    var t = e.replace(xe, Te);
                    return function (e) {
                        return e.getAttribute("id") === t
                    }
                }) : (delete T.find.ID, T.filter.ID = function (e) {
                    var t = e.replace(xe, Te);
                    return function (e) {
                        var n = "undefined" != typeof e.getAttributeNode && e.getAttributeNode("id");
                        return n && n.value === t
                    }
                }), T.find.TAG = x.getElementsByTagName ? function (e, t) {
                    return "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e) : x.qsa ? t.querySelectorAll(e) : void 0
                } : function (e, t) {
                    var n, i = [], o = 0, a = t.getElementsByTagName(e);
                    if ("*" === e) {
                        for (; n = a[o++];)1 === n.nodeType && i.push(n);
                        return i
                    }
                    return a
                }, T.find.CLASS = x.getElementsByClassName && function (e, t) {
                        return M ? t.getElementsByClassName(e) : void 0
                    }, z = [], O = [], (x.qsa = ve.test(i.querySelectorAll)) && (o(function (e) {
                    L.appendChild(e).innerHTML = "<a id='" + R + "'></a><select id='" + R + "-\f]' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && O.push("[*^$]=" + ne + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || O.push("\\[" + ne + "*(?:value|" + te + ")"), e.querySelectorAll("[id~=" + R + "-]").length || O.push("~="), e.querySelectorAll(":checked").length || O.push(":checked"), e.querySelectorAll("a#" + R + "+*").length || O.push(".#.+[+~]")
                }), o(function (e) {
                    var t = i.createElement("input");
                    t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && O.push("name" + ne + "*[*^$|!~]?="), e.querySelectorAll(":enabled").length || O.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), O.push(",.*:")
                })), (x.matchesSelector = ve.test(_ = L.matches || L.webkitMatchesSelector || L.mozMatchesSelector || L.oMatchesSelector || L.msMatchesSelector)) && o(function (e) {
                    x.disconnectedMatch = _.call(e, "div"), _.call(e, "[s!='']:x"), z.push("!=", re)
                }), O = O.length && new RegExp(O.join("|")), z = z.length && new RegExp(z.join("|")), t = ve.test(L.compareDocumentPosition), j = t || ve.test(L.contains) ? function (e, t) {
                    var n = 9 === e.nodeType ? e.documentElement : e, i = t && t.parentNode;
                    return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i)))
                } : function (e, t) {
                    if (t)for (; t = t.parentNode;)if (t === e)return !0;
                    return !1
                }, G = t ? function (e, t) {
                    if (e === t)return N = !0, 0;
                    var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                    return n ? n : (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1, 1 & n || !x.sortDetached && t.compareDocumentPosition(e) === n ? e === i || e.ownerDocument === H && j(H, e) ? -1 : t === i || t.ownerDocument === H && j(H, t) ? 1 : D ? ee(D, e) - ee(D, t) : 0 : 4 & n ? -1 : 1)
                } : function (e, t) {
                    if (e === t)return N = !0, 0;
                    var n, o = 0, a = e.parentNode, s = t.parentNode, l = [e], d = [t];
                    if (!a || !s)return e === i ? -1 : t === i ? 1 : a ? -1 : s ? 1 : D ? ee(D, e) - ee(D, t) : 0;
                    if (a === s)return r(e, t);
                    for (n = e; n = n.parentNode;)l.unshift(n);
                    for (n = t; n = n.parentNode;)d.unshift(n);
                    for (; l[o] === d[o];)o++;
                    return o ? r(l[o], d[o]) : l[o] === H ? -1 : d[o] === H ? 1 : 0
                }, i) : A
            }, t.matches = function (e, n) {
                return t(e, null, null, n)
            }, t.matchesSelector = function (e, n) {
                if ((e.ownerDocument || e) !== A && $(e), n = n.replace(ce, "='$1']"), !(!x.matchesSelector || !M || z && z.test(n) || O && O.test(n)))try {
                    var i = _.call(e, n);
                    if (i || x.disconnectedMatch || e.document && 11 !== e.document.nodeType)return i
                } catch (o) {
                }
                return t(n, A, null, [e]).length > 0
            }, t.contains = function (e, t) {
                return (e.ownerDocument || e) !== A && $(e), j(e, t)
            }, t.attr = function (e, t) {
                (e.ownerDocument || e) !== A && $(e);
                var n = T.attrHandle[t.toLowerCase()], i = n && U.call(T.attrHandle, t.toLowerCase()) ? n(e, t, !M) : void 0;
                return void 0 !== i ? i : x.attributes || !M ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
            }, t.error = function (e) {
                throw new Error("Syntax error, unrecognized expression: " + e)
            }, t.uniqueSort = function (e) {
                var t, n = [], i = 0, o = 0;
                if (N = !x.detectDuplicates, D = !x.sortStable && e.slice(0), e.sort(G), N) {
                    for (; t = e[o++];)t === e[o] && (i = n.push(o));
                    for (; i--;)e.splice(n[i], 1)
                }
                return D = null, e
            }, C = t.getText = function (e) {
                var t, n = "", i = 0, o = e.nodeType;
                if (o) {
                    if (1 === o || 9 === o || 11 === o) {
                        if ("string" == typeof e.textContent)return e.textContent;
                        for (e = e.firstChild; e; e = e.nextSibling)n += C(e)
                    } else if (3 === o || 4 === o)return e.nodeValue
                } else for (; t = e[i++];)n += C(t);
                return n
            }, T = t.selectors = {
                cacheLength: 50,
                createPseudo: i,
                match: fe,
                attrHandle: {},
                find: {},
                relative: {
                    ">": {dir: "parentNode", first: !0},
                    " ": {dir: "parentNode"},
                    "+": {dir: "previousSibling", first: !0},
                    "~": {dir: "previousSibling"}
                },
                preFilter: {
                    ATTR: function (e) {
                        return e[1] = e[1].replace(xe, Te), e[3] = (e[3] || e[4] || e[5] || "").replace(xe, Te), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                    }, CHILD: function (e) {
                        return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || t.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && t.error(e[0]), e
                    }, PSEUDO: function (e) {
                        var t, n = !e[6] && e[2];
                        return fe.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && ue.test(n) && (t = k(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                    }
                },
                filter: {
                    TAG: function (e) {
                        var t = e.replace(xe, Te).toLowerCase();
                        return "*" === e ? function () {
                            return !0
                        } : function (e) {
                            return e.nodeName && e.nodeName.toLowerCase() === t
                        }
                    }, CLASS: function (e) {
                        var t = W[e + " "];
                        return t || (t = new RegExp("(^|" + ne + ")" + e + "(" + ne + "|$)")) && W(e, function (e) {
                                return t.test("string" == typeof e.className && e.className || "undefined" != typeof e.getAttribute && e.getAttribute("class") || "")
                            })
                    }, ATTR: function (e, n, i) {
                        return function (o) {
                            var a = t.attr(o, e);
                            return null == a ? "!=" === n : n ? (a += "", "=" === n ? a === i : "!=" === n ? a !== i : "^=" === n ? i && 0 === a.indexOf(i) : "*=" === n ? i && a.indexOf(i) > -1 : "$=" === n ? i && a.slice(-i.length) === i : "~=" === n ? (" " + a.replace(se, " ") + " ").indexOf(i) > -1 : "|=" === n ? a === i || a.slice(0, i.length + 1) === i + "-" : !1) : !0
                        }
                    }, CHILD: function (e, t, n, i, o) {
                        var a = "nth" !== e.slice(0, 3), r = "last" !== e.slice(-4), s = "of-type" === t;
                        return 1 === i && 0 === o ? function (e) {
                            return !!e.parentNode
                        } : function (t, n, l) {
                            var d, p, c, u, h, f, m = a !== r ? "nextSibling" : "previousSibling", g = t.parentNode, v = s && t.nodeName.toLowerCase(), y = !l && !s;
                            if (g) {
                                if (a) {
                                    for (; m;) {
                                        for (c = t; c = c[m];)if (s ? c.nodeName.toLowerCase() === v : 1 === c.nodeType)return !1;
                                        f = m = "only" === e && !f && "nextSibling"
                                    }
                                    return !0
                                }
                                if (f = [r ? g.firstChild : g.lastChild], r && y) {
                                    for (p = g[R] || (g[R] = {}), d = p[e] || [], h = d[0] === F && d[1], u = d[0] === F && d[2], c = h && g.childNodes[h]; c = ++h && c && c[m] || (u = h = 0) || f.pop();)if (1 === c.nodeType && ++u && c === t) {
                                        p[e] = [F, h, u];
                                        break
                                    }
                                } else if (y && (d = (t[R] || (t[R] = {}))[e]) && d[0] === F)u = d[1]; else for (; (c = ++h && c && c[m] || (u = h = 0) || f.pop()) && ((s ? c.nodeName.toLowerCase() !== v : 1 !== c.nodeType) || !++u || (y && ((c[R] || (c[R] = {}))[e] = [F, u]), c !== t)););
                                return u -= o, u === i || u % i === 0 && u / i >= 0
                            }
                        }
                    }, PSEUDO: function (e, n) {
                        var o, a = T.pseudos[e] || T.setFilters[e.toLowerCase()] || t.error("unsupported pseudo: " + e);
                        return a[R] ? a(n) : a.length > 1 ? (o = [e, e, "", n], T.setFilters.hasOwnProperty(e.toLowerCase()) ? i(function (e, t) {
                            for (var i, o = a(e, n), r = o.length; r--;)i = ee(e, o[r]), e[i] = !(t[i] = o[r])
                        }) : function (e) {
                            return a(e, 0, o)
                        }) : a
                    }
                },
                pseudos: {
                    not: i(function (e) {
                        var t = [], n = [], o = E(e.replace(le, "$1"));
                        return o[R] ? i(function (e, t, n, i) {
                            for (var a, r = o(e, null, i, []), s = e.length; s--;)(a = r[s]) && (e[s] = !(t[s] = a))
                        }) : function (e, i, a) {
                            return t[0] = e, o(t, null, a, n), t[0] = null, !n.pop()
                        }
                    }), has: i(function (e) {
                        return function (n) {
                            return t(e, n).length > 0
                        }
                    }), contains: i(function (e) {
                        return e = e.replace(xe, Te), function (t) {
                            return (t.textContent || t.innerText || C(t)).indexOf(e) > -1
                        }
                    }), lang: i(function (e) {
                        return he.test(e || "") || t.error("unsupported lang: " + e), e = e.replace(xe, Te).toLowerCase(), function (t) {
                            var n;
                            do if (n = M ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang"))return n = n.toLowerCase(), n === e || 0 === n.indexOf(e + "-"); while ((t = t.parentNode) && 1 === t.nodeType);
                            return !1
                        }
                    }), target: function (t) {
                        var n = e.location && e.location.hash;
                        return n && n.slice(1) === t.id
                    }, root: function (e) {
                        return e === L
                    }, focus: function (e) {
                        return e === A.activeElement && (!A.hasFocus || A.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                    }, enabled: function (e) {
                        return e.disabled === !1
                    }, disabled: function (e) {
                        return e.disabled === !0
                    }, checked: function (e) {
                        var t = e.nodeName.toLowerCase();
                        return "input" === t && !!e.checked || "option" === t && !!e.selected
                    }, selected: function (e) {
                        return e.parentNode && e.parentNode.selectedIndex, e.selected === !0
                    }, empty: function (e) {
                        for (e = e.firstChild; e; e = e.nextSibling)if (e.nodeType < 6)return !1;
                        return !0
                    }, parent: function (e) {
                        return !T.pseudos.empty(e)
                    }, header: function (e) {
                        return ge.test(e.nodeName)
                    }, input: function (e) {
                        return me.test(e.nodeName)
                    }, button: function (e) {
                        var t = e.nodeName.toLowerCase();
                        return "input" === t && "button" === e.type || "button" === t
                    }, text: function (e) {
                        var t;
                        return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                    }, first: d(function () {
                        return [0]
                    }), last: d(function (e, t) {
                        return [t - 1]
                    }), eq: d(function (e, t, n) {
                        return [0 > n ? n + t : n]
                    }), even: d(function (e, t) {
                        for (var n = 0; t > n; n += 2)e.push(n);
                        return e
                    }), odd: d(function (e, t) {
                        for (var n = 1; t > n; n += 2)e.push(n);
                        return e
                    }), lt: d(function (e, t, n) {
                        for (var i = 0 > n ? n + t : n; --i >= 0;)e.push(i);
                        return e
                    }), gt: d(function (e, t, n) {
                        for (var i = 0 > n ? n + t : n; ++i < t;)e.push(i);
                        return e
                    })
                }
            }, T.pseudos.nth = T.pseudos.eq;
            for (b in{radio: !0, checkbox: !0, file: !0, password: !0, image: !0})T.pseudos[b] = s(b);
            for (b in{submit: !0, reset: !0})T.pseudos[b] = l(b);
            return c.prototype = T.filters = T.pseudos, T.setFilters = new c, k = t.tokenize = function (e, n) {
                var i, o, a, r, s, l, d, p = q[e + " "];
                if (p)return n ? 0 : p.slice(0);
                for (s = e, l = [], d = T.preFilter; s;) {
                    (!i || (o = de.exec(s))) && (o && (s = s.slice(o[0].length) || s), l.push(a = [])), i = !1, (o = pe.exec(s)) && (i = o.shift(), a.push({
                        value: i,
                        type: o[0].replace(le, " ")
                    }), s = s.slice(i.length));
                    for (r in T.filter)!(o = fe[r].exec(s)) || d[r] && !(o = d[r](o)) || (i = o.shift(),
                        a.push({value: i, type: r, matches: o}), s = s.slice(i.length));
                    if (!i)break
                }
                return n ? s.length : s ? t.error(e) : q(e, l).slice(0)
            }, E = t.compile = function (e, t) {
                var n, i = [], o = [], a = V[e + " "];
                if (!a) {
                    for (t || (t = k(e)), n = t.length; n--;)a = y(t[n]), a[R] ? i.push(a) : o.push(a);
                    a = V(e, w(o, i)), a.selector = e
                }
                return a
            }, P = t.select = function (e, t, n, i) {
                var o, a, r, s, l, d = "function" == typeof e && e, c = !i && k(e = d.selector || e);
                if (n = n || [], 1 === c.length) {
                    if (a = c[0] = c[0].slice(0), a.length > 2 && "ID" === (r = a[0]).type && x.getById && 9 === t.nodeType && M && T.relative[a[1].type]) {
                        if (t = (T.find.ID(r.matches[0].replace(xe, Te), t) || [])[0], !t)return n;
                        d && (t = t.parentNode), e = e.slice(a.shift().value.length)
                    }
                    for (o = fe.needsContext.test(e) ? 0 : a.length; o-- && (r = a[o], !T.relative[s = r.type]);)if ((l = T.find[s]) && (i = l(r.matches[0].replace(xe, Te), we.test(a[0].type) && p(t.parentNode) || t))) {
                        if (a.splice(o, 1), e = i.length && u(a), !e)return Z.apply(n, i), n;
                        break
                    }
                }
                return (d || E(e, c))(i, t, !M, n, we.test(e) && p(t.parentNode) || t), n
            }, x.sortStable = R.split("").sort(G).join("") === R, x.detectDuplicates = !!N, $(), x.sortDetached = o(function (e) {
                return 1 & e.compareDocumentPosition(A.createElement("div"))
            }), o(function (e) {
                return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
            }) || a("type|href|height|width", function (e, t, n) {
                return n ? void 0 : e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
            }), x.attributes && o(function (e) {
                return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
            }) || a("value", function (e, t, n) {
                return n || "input" !== e.nodeName.toLowerCase() ? void 0 : e.defaultValue
            }), o(function (e) {
                return null == e.getAttribute("disabled")
            }) || a(te, function (e, t, n) {
                var i;
                return n ? void 0 : e[t] === !0 ? t.toLowerCase() : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
            }), t
        }(e);
        oe.find = de, oe.expr = de.selectors, oe.expr[":"] = oe.expr.pseudos, oe.unique = de.uniqueSort, oe.text = de.getText, oe.isXMLDoc = de.isXML, oe.contains = de.contains;
        var pe = oe.expr.match.needsContext, ce = /^<(\w+)\s*\/?>(?:<\/\1>|)$/, ue = /^.[^:#\[\.,]*$/;
        oe.filter = function (e, t, n) {
            var i = t[0];
            return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === i.nodeType ? oe.find.matchesSelector(i, e) ? [i] : [] : oe.find.matches(e, oe.grep(t, function (e) {
                return 1 === e.nodeType
            }))
        }, oe.fn.extend({
            find: function (e) {
                var t, n = [], i = this, o = i.length;
                if ("string" != typeof e)return this.pushStack(oe(e).filter(function () {
                    for (t = 0; o > t; t++)if (oe.contains(i[t], this))return !0
                }));
                for (t = 0; o > t; t++)oe.find(e, i[t], n);
                return n = this.pushStack(o > 1 ? oe.unique(n) : n), n.selector = this.selector ? this.selector + " " + e : e, n
            }, filter: function (e) {
                return this.pushStack(i(this, e || [], !1))
            }, not: function (e) {
                return this.pushStack(i(this, e || [], !0))
            }, is: function (e) {
                return !!i(this, "string" == typeof e && pe.test(e) ? oe(e) : e || [], !1).length
            }
        });
        var he, fe = e.document, me = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, ge = oe.fn.init = function (e, t) {
            var n, i;
            if (!e)return this;
            if ("string" == typeof e) {
                if (n = "<" === e.charAt(0) && ">" === e.charAt(e.length - 1) && e.length >= 3 ? [null, e, null] : me.exec(e), !n || !n[1] && t)return !t || t.jquery ? (t || he).find(e) : this.constructor(t).find(e);
                if (n[1]) {
                    if (t = t instanceof oe ? t[0] : t, oe.merge(this, oe.parseHTML(n[1], t && t.nodeType ? t.ownerDocument || t : fe, !0)), ce.test(n[1]) && oe.isPlainObject(t))for (n in t)oe.isFunction(this[n]) ? this[n](t[n]) : this.attr(n, t[n]);
                    return this
                }
                if (i = fe.getElementById(n[2]), i && i.parentNode) {
                    if (i.id !== n[2])return he.find(e);
                    this.length = 1, this[0] = i
                }
                return this.context = fe, this.selector = e, this
            }
            return e.nodeType ? (this.context = this[0] = e, this.length = 1, this) : oe.isFunction(e) ? "undefined" != typeof he.ready ? he.ready(e) : e(oe) : (void 0 !== e.selector && (this.selector = e.selector, this.context = e.context), oe.makeArray(e, this))
        };
        ge.prototype = oe.fn, he = oe(fe);
        var ve = /^(?:parents|prev(?:Until|All))/, ye = {children: !0, contents: !0, next: !0, prev: !0};
        oe.extend({
            dir: function (e, t, n) {
                for (var i = [], o = e[t]; o && 9 !== o.nodeType && (void 0 === n || 1 !== o.nodeType || !oe(o).is(n));)1 === o.nodeType && i.push(o), o = o[t];
                return i
            }, sibling: function (e, t) {
                for (var n = []; e; e = e.nextSibling)1 === e.nodeType && e !== t && n.push(e);
                return n
            }
        }), oe.fn.extend({
            has: function (e) {
                var t, n = oe(e, this), i = n.length;
                return this.filter(function () {
                    for (t = 0; i > t; t++)if (oe.contains(this, n[t]))return !0
                })
            }, closest: function (e, t) {
                for (var n, i = 0, o = this.length, a = [], r = pe.test(e) || "string" != typeof e ? oe(e, t || this.context) : 0; o > i; i++)for (n = this[i]; n && n !== t; n = n.parentNode)if (n.nodeType < 11 && (r ? r.index(n) > -1 : 1 === n.nodeType && oe.find.matchesSelector(n, e))) {
                    a.push(n);
                    break
                }
                return this.pushStack(a.length > 1 ? oe.unique(a) : a)
            }, index: function (e) {
                return e ? "string" == typeof e ? oe.inArray(this[0], oe(e)) : oe.inArray(e.jquery ? e[0] : e, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
            }, add: function (e, t) {
                return this.pushStack(oe.unique(oe.merge(this.get(), oe(e, t))))
            }, addBack: function (e) {
                return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
            }
        }), oe.each({
            parent: function (e) {
                var t = e.parentNode;
                return t && 11 !== t.nodeType ? t : null
            }, parents: function (e) {
                return oe.dir(e, "parentNode")
            }, parentsUntil: function (e, t, n) {
                return oe.dir(e, "parentNode", n)
            }, next: function (e) {
                return o(e, "nextSibling")
            }, prev: function (e) {
                return o(e, "previousSibling")
            }, nextAll: function (e) {
                return oe.dir(e, "nextSibling")
            }, prevAll: function (e) {
                return oe.dir(e, "previousSibling")
            }, nextUntil: function (e, t, n) {
                return oe.dir(e, "nextSibling", n)
            }, prevUntil: function (e, t, n) {
                return oe.dir(e, "previousSibling", n)
            }, siblings: function (e) {
                return oe.sibling((e.parentNode || {}).firstChild, e)
            }, children: function (e) {
                return oe.sibling(e.firstChild)
            }, contents: function (e) {
                return oe.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : oe.merge([], e.childNodes)
            }
        }, function (e, t) {
            oe.fn[e] = function (n, i) {
                var o = oe.map(this, t, n);
                return "Until" !== e.slice(-5) && (i = n), i && "string" == typeof i && (o = oe.filter(i, o)), this.length > 1 && (ye[e] || (o = oe.unique(o)), ve.test(e) && (o = o.reverse())), this.pushStack(o)
            }
        });
        var we = /\S+/g, be = {};
        oe.Callbacks = function (e) {
            e = "string" == typeof e ? be[e] || a(e) : oe.extend({}, e);
            var t, n, i, o, r, s, l = [], d = !e.once && [], p = function u(a) {
                for (n = e.memory && a, i = !0, r = s || 0, s = 0, o = l.length, t = !0; l && o > r; r++)if (l[r].apply(a[0], a[1]) === !1 && e.stopOnFalse) {
                    n = !1;
                    break
                }
                t = !1, l && (d ? d.length && u(d.shift()) : n ? l = [] : c.disable())
            }, c = {
                add: function () {
                    if (l) {
                        var i = l.length;
                        !function a(t) {
                            oe.each(t, function (t, n) {
                                var i = oe.type(n);
                                "function" === i ? e.unique && c.has(n) || l.push(n) : n && n.length && "string" !== i && a(n)
                            })
                        }(arguments), t ? o = l.length : n && (s = i, p(n))
                    }
                    return this
                }, remove: function () {
                    return l && oe.each(arguments, function (e, n) {
                        for (var i; (i = oe.inArray(n, l, i)) > -1;)l.splice(i, 1), t && (o >= i && o--, r >= i && r--)
                    }), this
                }, has: function (e) {
                    return e ? oe.inArray(e, l) > -1 : !(!l || !l.length)
                }, empty: function () {
                    return l = [], o = 0, this
                }, disable: function () {
                    return l = d = n = void 0, this
                }, disabled: function () {
                    return !l
                }, lock: function () {
                    return d = void 0, n || c.disable(), this
                }, locked: function () {
                    return !d
                }, fireWith: function (e, n) {
                    return !l || i && !d || (n = n || [], n = [e, n.slice ? n.slice() : n], t ? d.push(n) : p(n)), this
                }, fire: function () {
                    return c.fireWith(this, arguments), this
                }, fired: function () {
                    return !!i
                }
            };
            return c
        }, oe.extend({
            Deferred: function (e) {
                var t = [["resolve", "done", oe.Callbacks("once memory"), "resolved"], ["reject", "fail", oe.Callbacks("once memory"), "rejected"], ["notify", "progress", oe.Callbacks("memory")]], n = "pending", i = {
                    state: function () {
                        return n
                    }, always: function () {
                        return o.done(arguments).fail(arguments), this
                    }, then: function () {
                        var e = arguments;
                        return oe.Deferred(function (n) {
                            oe.each(t, function (t, a) {
                                var r = oe.isFunction(e[t]) && e[t];
                                o[a[1]](function () {
                                    var e = r && r.apply(this, arguments);
                                    e && oe.isFunction(e.promise) ? e.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[a[0] + "With"](this === i ? n.promise() : this, r ? [e] : arguments)
                                })
                            }), e = null
                        }).promise()
                    }, promise: function (e) {
                        return null != e ? oe.extend(e, i) : i
                    }
                }, o = {};
                return i.pipe = i.then, oe.each(t, function (e, a) {
                    var r = a[2], s = a[3];
                    i[a[1]] = r.add, s && r.add(function () {
                        n = s
                    }, t[1 ^ e][2].disable, t[2][2].lock), o[a[0]] = function () {
                        return o[a[0] + "With"](this === o ? i : this, arguments), this
                    }, o[a[0] + "With"] = r.fireWith
                }), i.promise(o), e && e.call(o, o), o
            }, when: function (e) {
                var t, n, i, o = 0, a = Y.call(arguments), r = a.length, s = 1 !== r || e && oe.isFunction(e.promise) ? r : 0, l = 1 === s ? e : oe.Deferred(), d = function (e, n, i) {
                    return function (o) {
                        n[e] = this, i[e] = arguments.length > 1 ? Y.call(arguments) : o, i === t ? l.notifyWith(n, i) : --s || l.resolveWith(n, i)
                    }
                };
                if (r > 1)for (t = new Array(r), n = new Array(r), i = new Array(r); r > o; o++)a[o] && oe.isFunction(a[o].promise) ? a[o].promise().done(d(o, i, a)).fail(l.reject).progress(d(o, n, t)) : --s;
                return s || l.resolveWith(i, a), l.promise()
            }
        });
        var xe;
        oe.fn.ready = function (e) {
            return oe.ready.promise().done(e), this
        }, oe.extend({
            isReady: !1, readyWait: 1, holdReady: function (e) {
                e ? oe.readyWait++ : oe.ready(!0)
            }, ready: function (e) {
                if (e === !0 ? !--oe.readyWait : !oe.isReady) {
                    if (!fe.body)return setTimeout(oe.ready);
                    oe.isReady = !0, e !== !0 && --oe.readyWait > 0 || (xe.resolveWith(fe, [oe]), oe.fn.triggerHandler && (oe(fe).triggerHandler("ready"), oe(fe).off("ready")))
                }
            }
        }), oe.ready.promise = function (t) {
            if (!xe)if (xe = oe.Deferred(), "complete" === fe.readyState)setTimeout(oe.ready); else if (fe.addEventListener)fe.addEventListener("DOMContentLoaded", s, !1), e.addEventListener("load", s, !1); else {
                fe.attachEvent("onreadystatechange", s), e.attachEvent("onload", s);
                var n = !1;
                try {
                    n = null == e.frameElement && fe.documentElement
                } catch (i) {
                }
                n && n.doScroll && !function o() {
                    if (!oe.isReady) {
                        try {
                            n.doScroll("left")
                        } catch (e) {
                            return setTimeout(o, 50)
                        }
                        r(), oe.ready()
                    }
                }()
            }
            return xe.promise(t)
        };
        var Te, Ce = "undefined";
        for (Te in oe(ne))break;
        ne.ownLast = "0" !== Te, ne.inlineBlockNeedsLayout = !1, oe(function () {
            var e, t, n, i;
            n = fe.getElementsByTagName("body")[0], n && n.style && (t = fe.createElement("div"), i = fe.createElement("div"), i.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(i).appendChild(t), typeof t.style.zoom !== Ce && (t.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1", ne.inlineBlockNeedsLayout = e = 3 === t.offsetWidth, e && (n.style.zoom = 1)), n.removeChild(i))
        }), function () {
            var e = fe.createElement("div");
            if (null == ne.deleteExpando) {
                ne.deleteExpando = !0;
                try {
                    delete e.test
                } catch (t) {
                    ne.deleteExpando = !1
                }
            }
            e = null
        }(), oe.acceptData = function (e) {
            var t = oe.noData[(e.nodeName + " ").toLowerCase()], n = +e.nodeType || 1;
            return 1 !== n && 9 !== n ? !1 : !t || t !== !0 && e.getAttribute("classid") === t
        };
        var Se = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, ke = /([A-Z])/g;
        oe.extend({
            cache: {},
            noData: {"applet ": !0, "embed ": !0, "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"},
            hasData: function (e) {
                return e = e.nodeType ? oe.cache[e[oe.expando]] : e[oe.expando], !!e && !d(e)
            },
            data: function (e, t, n) {
                return p(e, t, n)
            },
            removeData: function (e, t) {
                return c(e, t)
            },
            _data: function (e, t, n) {
                return p(e, t, n, !0)
            },
            _removeData: function (e, t) {
                return c(e, t, !0)
            }
        }), oe.fn.extend({
            data: function (e, t) {
                var n, i, o, a = this[0], r = a && a.attributes;
                if (void 0 === e) {
                    if (this.length && (o = oe.data(a), 1 === a.nodeType && !oe._data(a, "parsedAttrs"))) {
                        for (n = r.length; n--;)r[n] && (i = r[n].name, 0 === i.indexOf("data-") && (i = oe.camelCase(i.slice(5)), l(a, i, o[i])));
                        oe._data(a, "parsedAttrs", !0)
                    }
                    return o
                }
                return "object" == typeof e ? this.each(function () {
                    oe.data(this, e)
                }) : arguments.length > 1 ? this.each(function () {
                    oe.data(this, e, t)
                }) : a ? l(a, e, oe.data(a, e)) : void 0
            }, removeData: function (e) {
                return this.each(function () {
                    oe.removeData(this, e)
                })
            }
        }), oe.extend({
            queue: function (e, t, n) {
                var i;
                return e ? (t = (t || "fx") + "queue", i = oe._data(e, t), n && (!i || oe.isArray(n) ? i = oe._data(e, t, oe.makeArray(n)) : i.push(n)), i || []) : void 0
            }, dequeue: function (e, t) {
                t = t || "fx";
                var n = oe.queue(e, t), i = n.length, o = n.shift(), a = oe._queueHooks(e, t), r = function () {
                    oe.dequeue(e, t)
                };
                "inprogress" === o && (o = n.shift(), i--), o && ("fx" === t && n.unshift("inprogress"), delete a.stop, o.call(e, r, a)), !i && a && a.empty.fire()
            }, _queueHooks: function (e, t) {
                var n = t + "queueHooks";
                return oe._data(e, n) || oe._data(e, n, {
                        empty: oe.Callbacks("once memory").add(function () {
                            oe._removeData(e, t + "queue"), oe._removeData(e, n)
                        })
                    })
            }
        }), oe.fn.extend({
            queue: function (e, t) {
                var n = 2;
                return "string" != typeof e && (t = e, e = "fx", n--), arguments.length < n ? oe.queue(this[0], e) : void 0 === t ? this : this.each(function () {
                    var n = oe.queue(this, e, t);
                    oe._queueHooks(this, e), "fx" === e && "inprogress" !== n[0] && oe.dequeue(this, e)
                })
            }, dequeue: function (e) {
                return this.each(function () {
                    oe.dequeue(this, e)
                })
            }, clearQueue: function (e) {
                return this.queue(e || "fx", [])
            }, promise: function (e, t) {
                var n, i = 1, o = oe.Deferred(), a = this, r = this.length, s = function () {
                    --i || o.resolveWith(a, [a])
                };
                for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; r--;)n = oe._data(a[r], e + "queueHooks"), n && n.empty && (i++, n.empty.add(s));
                return s(), o.promise(t)
            }
        });
        var Ee = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, Pe = ["Top", "Right", "Bottom", "Left"], Ie = function (e, t) {
            return e = t || e, "none" === oe.css(e, "display") || !oe.contains(e.ownerDocument, e)
        }, De = oe.access = function (e, t, n, i, o, a, r) {
            var s = 0, l = e.length, d = null == n;
            if ("object" === oe.type(n)) {
                o = !0;
                for (s in n)oe.access(e, t, s, n[s], !0, a, r)
            } else if (void 0 !== i && (o = !0, oe.isFunction(i) || (r = !0), d && (r ? (t.call(e, i), t = null) : (d = t, t = function (e, t, n) {
                    return d.call(oe(e), n)
                })), t))for (; l > s; s++)t(e[s], n, r ? i : i.call(e[s], s, t(e[s], n)));
            return o ? e : d ? t.call(e) : l ? t(e[0], n) : a
        }, Ne = /^(?:checkbox|radio)$/i;
        !function () {
            var e = fe.createElement("input"), t = fe.createElement("div"), n = fe.createDocumentFragment();
            if (t.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", ne.leadingWhitespace = 3 === t.firstChild.nodeType, ne.tbody = !t.getElementsByTagName("tbody").length, ne.htmlSerialize = !!t.getElementsByTagName("link").length, ne.html5Clone = "<:nav></:nav>" !== fe.createElement("nav").cloneNode(!0).outerHTML, e.type = "checkbox", e.checked = !0, n.appendChild(e), ne.appendChecked = e.checked, t.innerHTML = "<textarea>x</textarea>", ne.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue, n.appendChild(t), t.innerHTML = "<input type='radio' checked='checked' name='t'/>", ne.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, ne.noCloneEvent = !0, t.attachEvent && (t.attachEvent("onclick", function () {
                    ne.noCloneEvent = !1
                }), t.cloneNode(!0).click()), null == ne.deleteExpando) {
                ne.deleteExpando = !0;
                try {
                    delete t.test
                } catch (i) {
                    ne.deleteExpando = !1
                }
            }
        }(), function () {
            var t, n, i = fe.createElement("div");
            for (t in{
                submit: !0,
                change: !0,
                focusin: !0
            })n = "on" + t, (ne[t + "Bubbles"] = n in e) || (i.setAttribute(n, "t"), ne[t + "Bubbles"] = i.attributes[n].expando === !1);
            i = null
        }();
        var $e = /^(?:input|select|textarea)$/i, Ae = /^key/, Le = /^(?:mouse|pointer|contextmenu)|click/, Me = /^(?:focusinfocus|focusoutblur)$/, Oe = /^([^.]*)(?:\.(.+)|)$/;
        oe.event = {
            global: {},
            add: function (e, t, n, i, o) {
                var a, r, s, l, d, p, c, u, h, f, m, g = oe._data(e);
                if (g) {
                    for (n.handler && (l = n, n = l.handler, o = l.selector), n.guid || (n.guid = oe.guid++), (r = g.events) || (r = g.events = {}), (p = g.handle) || (p = g.handle = function (e) {
                        return typeof oe === Ce || e && oe.event.triggered === e.type ? void 0 : oe.event.dispatch.apply(p.elem, arguments)
                    }, p.elem = e), t = (t || "").match(we) || [""], s = t.length; s--;)a = Oe.exec(t[s]) || [], h = m = a[1], f = (a[2] || "").split(".").sort(), h && (d = oe.event.special[h] || {}, h = (o ? d.delegateType : d.bindType) || h, d = oe.event.special[h] || {}, c = oe.extend({
                        type: h,
                        origType: m,
                        data: i,
                        handler: n,
                        guid: n.guid,
                        selector: o,
                        needsContext: o && oe.expr.match.needsContext.test(o),
                        namespace: f.join(".")
                    }, l), (u = r[h]) || (u = r[h] = [], u.delegateCount = 0, d.setup && d.setup.call(e, i, f, p) !== !1 || (e.addEventListener ? e.addEventListener(h, p, !1) : e.attachEvent && e.attachEvent("on" + h, p))), d.add && (d.add.call(e, c), c.handler.guid || (c.handler.guid = n.guid)), o ? u.splice(u.delegateCount++, 0, c) : u.push(c), oe.event.global[h] = !0);
                    e = null
                }
            },
            remove: function (e, t, n, i, o) {
                var a, r, s, l, d, p, c, u, h, f, m, g = oe.hasData(e) && oe._data(e);
                if (g && (p = g.events)) {
                    for (t = (t || "").match(we) || [""], d = t.length; d--;)if (s = Oe.exec(t[d]) || [], h = m = s[1], f = (s[2] || "").split(".").sort(), h) {
                        for (c = oe.event.special[h] || {}, h = (i ? c.delegateType : c.bindType) || h, u = p[h] || [], s = s[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), l = a = u.length; a--;)r = u[a], !o && m !== r.origType || n && n.guid !== r.guid || s && !s.test(r.namespace) || i && i !== r.selector && ("**" !== i || !r.selector) || (u.splice(a, 1), r.selector && u.delegateCount--, c.remove && c.remove.call(e, r));
                        l && !u.length && (c.teardown && c.teardown.call(e, f, g.handle) !== !1 || oe.removeEvent(e, h, g.handle), delete p[h])
                    } else for (h in p)oe.event.remove(e, h + t[d], n, i, !0);
                    oe.isEmptyObject(p) && (delete g.handle, oe._removeData(e, "events"))
                }
            },
            trigger: function (t, n, i, o) {
                var a, r, s, l, d, p, c, u = [i || fe], h = te.call(t, "type") ? t.type : t, f = te.call(t, "namespace") ? t.namespace.split(".") : [];
                if (s = p = i = i || fe, 3 !== i.nodeType && 8 !== i.nodeType && !Me.test(h + oe.event.triggered) && (h.indexOf(".") >= 0 && (f = h.split("."), h = f.shift(), f.sort()), r = h.indexOf(":") < 0 && "on" + h, t = t[oe.expando] ? t : new oe.Event(h, "object" == typeof t && t), t.isTrigger = o ? 2 : 3, t.namespace = f.join("."), t.namespace_re = t.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = i), n = null == n ? [t] : oe.makeArray(n, [t]), d = oe.event.special[h] || {}, o || !d.trigger || d.trigger.apply(i, n) !== !1)) {
                    if (!o && !d.noBubble && !oe.isWindow(i)) {
                        for (l = d.delegateType || h, Me.test(l + h) || (s = s.parentNode); s; s = s.parentNode)u.push(s), p = s;
                        p === (i.ownerDocument || fe) && u.push(p.defaultView || p.parentWindow || e)
                    }
                    for (c = 0; (s = u[c++]) && !t.isPropagationStopped();)t.type = c > 1 ? l : d.bindType || h, a = (oe._data(s, "events") || {})[t.type] && oe._data(s, "handle"), a && a.apply(s, n), a = r && s[r], a && a.apply && oe.acceptData(s) && (t.result = a.apply(s, n), t.result === !1 && t.preventDefault());
                    if (t.type = h, !o && !t.isDefaultPrevented() && (!d._default || d._default.apply(u.pop(), n) === !1) && oe.acceptData(i) && r && i[h] && !oe.isWindow(i)) {
                        p = i[r], p && (i[r] = null), oe.event.triggered = h;
                        try {
                            i[h]()
                        } catch (m) {
                        }
                        oe.event.triggered = void 0, p && (i[r] = p)
                    }
                    return t.result
                }
            },
            dispatch: function (e) {
                e = oe.event.fix(e);
                var t, n, i, o, a, r = [], s = Y.call(arguments), l = (oe._data(this, "events") || {})[e.type] || [], d = oe.event.special[e.type] || {};
                if (s[0] = e, e.delegateTarget = this, !d.preDispatch || d.preDispatch.call(this, e) !== !1) {
                    for (r = oe.event.handlers.call(this, e, l), t = 0; (o = r[t++]) && !e.isPropagationStopped();)for (e.currentTarget = o.elem, a = 0; (i = o.handlers[a++]) && !e.isImmediatePropagationStopped();)(!e.namespace_re || e.namespace_re.test(i.namespace)) && (e.handleObj = i, e.data = i.data, n = ((oe.event.special[i.origType] || {}).handle || i.handler).apply(o.elem, s), void 0 !== n && (e.result = n) === !1 && (e.preventDefault(), e.stopPropagation()));
                    return d.postDispatch && d.postDispatch.call(this, e), e.result
                }
            },
            handlers: function (e, t) {
                var n, i, o, a, r = [], s = t.delegateCount, l = e.target;
                if (s && l.nodeType && (!e.button || "click" !== e.type))for (; l != this; l = l.parentNode || this)if (1 === l.nodeType && (l.disabled !== !0 || "click" !== e.type)) {
                    for (o = [], a = 0; s > a; a++)i = t[a], n = i.selector + " ", void 0 === o[n] && (o[n] = i.needsContext ? oe(n, this).index(l) >= 0 : oe.find(n, this, null, [l]).length), o[n] && o.push(i);
                    o.length && r.push({elem: l, handlers: o})
                }
                return s < t.length && r.push({elem: this, handlers: t.slice(s)}), r
            },
            fix: function (e) {
                if (e[oe.expando])return e;
                var t, n, i, o = e.type, a = e, r = this.fixHooks[o];
                for (r || (this.fixHooks[o] = r = Le.test(o) ? this.mouseHooks : Ae.test(o) ? this.keyHooks : {}), i = r.props ? this.props.concat(r.props) : this.props, e = new oe.Event(a), t = i.length; t--;)n = i[t], e[n] = a[n];
                return e.target || (e.target = a.srcElement || fe), 3 === e.target.nodeType && (e.target = e.target.parentNode), e.metaKey = !!e.metaKey, r.filter ? r.filter(e, a) : e
            },
            props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
            fixHooks: {},
            keyHooks: {
                props: "char charCode key keyCode".split(" "), filter: function (e, t) {
                    return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e
                }
            },
            mouseHooks: {
                props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
                filter: function (e, t) {
                    var n, i, o, a = t.button, r = t.fromElement;
                    return null == e.pageX && null != t.clientX && (i = e.target.ownerDocument || fe, o = i.documentElement, n = i.body, e.pageX = t.clientX + (o && o.scrollLeft || n && n.scrollLeft || 0) - (o && o.clientLeft || n && n.clientLeft || 0), e.pageY = t.clientY + (o && o.scrollTop || n && n.scrollTop || 0) - (o && o.clientTop || n && n.clientTop || 0)), !e.relatedTarget && r && (e.relatedTarget = r === e.target ? t.toElement : r), e.which || void 0 === a || (e.which = 1 & a ? 1 : 2 & a ? 3 : 4 & a ? 2 : 0), e
                }
            },
            special: {
                load: {noBubble: !0}, focus: {
                    trigger: function () {
                        if (this !== f() && this.focus)try {
                            return this.focus(), !1
                        } catch (e) {
                        }
                    }, delegateType: "focusin"
                }, blur: {
                    trigger: function () {
                        return this === f() && this.blur ? (this.blur(), !1) : void 0
                    }, delegateType: "focusout"
                }, click: {
                    trigger: function () {
                        return oe.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : void 0
                    }, _default: function (e) {
                        return oe.nodeName(e.target, "a")
                    }
                }, beforeunload: {
                    postDispatch: function (e) {
                        void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                    }
                }
            },
            simulate: function (e, t, n, i) {
                var o = oe.extend(new oe.Event, n, {type: e, isSimulated: !0, originalEvent: {}});
                i ? oe.event.trigger(o, null, t) : oe.event.dispatch.call(t, o), o.isDefaultPrevented() && n.preventDefault()
            }
        }, oe.removeEvent = fe.removeEventListener ? function (e, t, n) {
            e.removeEventListener && e.removeEventListener(t, n, !1)
        } : function (e, t, n) {
            var i = "on" + t;
            e.detachEvent && (typeof e[i] === Ce && (e[i] = null), e.detachEvent(i, n))
        }, oe.Event = function (e, t) {
            return this instanceof oe.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && e.returnValue === !1 ? u : h) : this.type = e, t && oe.extend(this, t), this.timeStamp = e && e.timeStamp || oe.now(), void(this[oe.expando] = !0)) : new oe.Event(e, t)
        }, oe.Event.prototype = {
            isDefaultPrevented: h,
            isPropagationStopped: h,
            isImmediatePropagationStopped: h,
            preventDefault: function () {
                var e = this.originalEvent;
                this.isDefaultPrevented = u, e && (e.preventDefault ? e.preventDefault() : e.returnValue = !1)
            },
            stopPropagation: function () {
                var e = this.originalEvent;
                this.isPropagationStopped = u, e && (e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0)
            },
            stopImmediatePropagation: function () {
                var e = this.originalEvent;
                this.isImmediatePropagationStopped = u, e && e.stopImmediatePropagation && e.stopImmediatePropagation(), this.stopPropagation()
            }
        }, oe.each({
            mouseenter: "mouseover",
            mouseleave: "mouseout",
            pointerenter: "pointerover",
            pointerleave: "pointerout"
        }, function (e, t) {
            oe.event.special[e] = {
                delegateType: t, bindType: t, handle: function (e) {
                    var n, i = this, o = e.relatedTarget, a = e.handleObj;
                    return (!o || o !== i && !oe.contains(i, o)) && (e.type = a.origType, n = a.handler.apply(this, arguments), e.type = t), n
                }
            }
        }), ne.submitBubbles || (oe.event.special.submit = {
            setup: function () {
                return oe.nodeName(this, "form") ? !1 : void oe.event.add(this, "click._submit keypress._submit", function (e) {
                    var t = e.target, n = oe.nodeName(t, "input") || oe.nodeName(t, "button") ? t.form : void 0;
                    n && !oe._data(n, "submitBubbles") && (oe.event.add(n, "submit._submit", function (e) {
                        e._submit_bubble = !0
                    }), oe._data(n, "submitBubbles", !0))
                })
            }, postDispatch: function (e) {
                e._submit_bubble && (delete e._submit_bubble, this.parentNode && !e.isTrigger && oe.event.simulate("submit", this.parentNode, e, !0))
            }, teardown: function () {
                return oe.nodeName(this, "form") ? !1 : void oe.event.remove(this, "._submit")
            }
        }), ne.changeBubbles || (oe.event.special.change = {
            setup: function () {
                return $e.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (oe.event.add(this, "propertychange._change", function (e) {
                    "checked" === e.originalEvent.propertyName && (this._just_changed = !0)
                }), oe.event.add(this, "click._change", function (e) {
                    this._just_changed && !e.isTrigger && (this._just_changed = !1), oe.event.simulate("change", this, e, !0)
                })), !1) : void oe.event.add(this, "beforeactivate._change", function (e) {
                    var t = e.target;
                    $e.test(t.nodeName) && !oe._data(t, "changeBubbles") && (oe.event.add(t, "change._change", function (e) {
                        !this.parentNode || e.isSimulated || e.isTrigger || oe.event.simulate("change", this.parentNode, e, !0)
                    }), oe._data(t, "changeBubbles", !0))
                })
            }, handle: function (e) {
                var t = e.target;
                return this !== t || e.isSimulated || e.isTrigger || "radio" !== t.type && "checkbox" !== t.type ? e.handleObj.handler.apply(this, arguments) : void 0
            }, teardown: function () {
                return oe.event.remove(this, "._change"), !$e.test(this.nodeName)
            }
        }), ne.focusinBubbles || oe.each({focus: "focusin", blur: "focusout"}, function (e, t) {
            var n = function (e) {
                oe.event.simulate(t, e.target, oe.event.fix(e), !0)
            };
            oe.event.special[t] = {
                setup: function () {
                    var i = this.ownerDocument || this, o = oe._data(i, t);
                    o || i.addEventListener(e, n, !0), oe._data(i, t, (o || 0) + 1)
                }, teardown: function () {
                    var i = this.ownerDocument || this, o = oe._data(i, t) - 1;
                    o ? oe._data(i, t, o) : (i.removeEventListener(e, n, !0), oe._removeData(i, t))
                }
            }
        }), oe.fn.extend({
            on: function (e, t, n, i, o) {
                var a, r;
                if ("object" == typeof e) {
                    "string" != typeof t && (n = n || t, t = void 0);
                    for (a in e)this.on(a, t, n, e[a], o);
                    return this
                }
                if (null == n && null == i ? (i = t, n = t = void 0) : null == i && ("string" == typeof t ? (i = n, n = void 0) : (i = n, n = t, t = void 0)), i === !1)i = h; else if (!i)return this;
                return 1 === o && (r = i, i = function (e) {
                    return oe().off(e), r.apply(this, arguments)
                }, i.guid = r.guid || (r.guid = oe.guid++)), this.each(function () {
                    oe.event.add(this, e, i, n, t)
                })
            }, one: function (e, t, n, i) {
                return this.on(e, t, n, i, 1)
            }, off: function (e, t, n) {
                var i, o;
                if (e && e.preventDefault && e.handleObj)return i = e.handleObj, oe(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
                if ("object" == typeof e) {
                    for (o in e)this.off(o, t, e[o]);
                    return this
                }
                return (t === !1 || "function" == typeof t) && (n = t, t = void 0), n === !1 && (n = h), this.each(function () {
                    oe.event.remove(this, e, n, t)
                })
            }, trigger: function (e, t) {
                return this.each(function () {
                    oe.event.trigger(e, t, this)
                })
            }, triggerHandler: function (e, t) {
                var n = this[0];
                return n ? oe.event.trigger(e, t, n, !0) : void 0
            }
        });
        var ze = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video", _e = / jQuery\d+="(?:null|\d+)"/g, je = new RegExp("<(?:" + ze + ")[\\s/>]", "i"), Re = /^\s+/, He = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, Fe = /<([\w:]+)/, Be = /<tbody/i, We = /<|&#?\w+;/, qe = /<(?:script|style|link)/i, Ve = /checked\s*(?:[^=]|=\s*.checked.)/i, Ge = /^$|\/(?:java|ecma)script/i, Xe = /^true\/(.*)/, Ue = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, Ye = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            legend: [1, "<fieldset>", "</fieldset>"],
            area: [1, "<map>", "</map>"],
            param: [1, "<object>", "</object>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: ne.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
        }, Ke = m(fe), Qe = Ke.appendChild(fe.createElement("div"));
        Ye.optgroup = Ye.option, Ye.tbody = Ye.tfoot = Ye.colgroup = Ye.caption = Ye.thead, Ye.th = Ye.td, oe.extend({
            clone: function (e, t, n) {
                var i, o, a, r, s, l = oe.contains(e.ownerDocument, e);
                if (ne.html5Clone || oe.isXMLDoc(e) || !je.test("<" + e.nodeName + ">") ? a = e.cloneNode(!0) : (Qe.innerHTML = e.outerHTML, Qe.removeChild(a = Qe.firstChild)), !(ne.noCloneEvent && ne.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || oe.isXMLDoc(e)))for (i = g(a), s = g(e), r = 0; null != (o = s[r]); ++r)i[r] && C(o, i[r]);
                if (t)if (n)for (s = s || g(e), i = i || g(a), r = 0; null != (o = s[r]); r++)T(o, i[r]); else T(e, a);
                return i = g(a, "script"), i.length > 0 && x(i, !l && g(e, "script")), i = s = o = null, a
            }, buildFragment: function (e, t, n, i) {
                for (var o, a, r, s, l, d, p, c = e.length, u = m(t), h = [], f = 0; c > f; f++)if (a = e[f], a || 0 === a)if ("object" === oe.type(a))oe.merge(h, a.nodeType ? [a] : a); else if (We.test(a)) {
                    for (s = s || u.appendChild(t.createElement("div")), l = (Fe.exec(a) || ["", ""])[1].toLowerCase(), p = Ye[l] || Ye._default, s.innerHTML = p[1] + a.replace(He, "<$1></$2>") + p[2], o = p[0]; o--;)s = s.lastChild;
                    if (!ne.leadingWhitespace && Re.test(a) && h.push(t.createTextNode(Re.exec(a)[0])), !ne.tbody)for (a = "table" !== l || Be.test(a) ? "<table>" !== p[1] || Be.test(a) ? 0 : s : s.firstChild, o = a && a.childNodes.length; o--;)oe.nodeName(d = a.childNodes[o], "tbody") && !d.childNodes.length && a.removeChild(d);
                    for (oe.merge(h, s.childNodes), s.textContent = ""; s.firstChild;)s.removeChild(s.firstChild);
                    s = u.lastChild
                } else h.push(t.createTextNode(a));
                for (s && u.removeChild(s), ne.appendChecked || oe.grep(g(h, "input"), v), f = 0; a = h[f++];)if ((!i || -1 === oe.inArray(a, i)) && (r = oe.contains(a.ownerDocument, a), s = g(u.appendChild(a), "script"), r && x(s), n))for (o = 0; a = s[o++];)Ge.test(a.type || "") && n.push(a);
                return s = null, u
            }, cleanData: function (e, t) {
                for (var n, i, o, a, r = 0, s = oe.expando, l = oe.cache, d = ne.deleteExpando, p = oe.event.special; null != (n = e[r]); r++)if ((t || oe.acceptData(n)) && (o = n[s], a = o && l[o])) {
                    if (a.events)for (i in a.events)p[i] ? oe.event.remove(n, i) : oe.removeEvent(n, i, a.handle);
                    l[o] && (delete l[o], d ? delete n[s] : typeof n.removeAttribute !== Ce ? n.removeAttribute(s) : n[s] = null, U.push(o))
                }
            }
        }), oe.fn.extend({
            text: function (e) {
                return De(this, function (e) {
                    return void 0 === e ? oe.text(this) : this.empty().append((this[0] && this[0].ownerDocument || fe).createTextNode(e))
                }, null, e, arguments.length)
            }, append: function () {
                return this.domManip(arguments, function (e) {
                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                        var t = y(this, e);
                        t.appendChild(e)
                    }
                })
            }, prepend: function () {
                return this.domManip(arguments, function (e) {
                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                        var t = y(this, e);
                        t.insertBefore(e, t.firstChild)
                    }
                })
            }, before: function () {
                return this.domManip(arguments, function (e) {
                    this.parentNode && this.parentNode.insertBefore(e, this)
                })
            }, after: function () {
                return this.domManip(arguments, function (e) {
                    this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
                })
            }, remove: function (e, t) {
                for (var n, i = e ? oe.filter(e, this) : this, o = 0; null != (n = i[o]); o++)t || 1 !== n.nodeType || oe.cleanData(g(n)), n.parentNode && (t && oe.contains(n.ownerDocument, n) && x(g(n, "script")), n.parentNode.removeChild(n));
                return this
            }, empty: function () {
                for (var e, t = 0; null != (e = this[t]); t++) {
                    for (1 === e.nodeType && oe.cleanData(g(e, !1)); e.firstChild;)e.removeChild(e.firstChild);
                    e.options && oe.nodeName(e, "select") && (e.options.length = 0)
                }
                return this
            }, clone: function (e, t) {
                return e = null == e ? !1 : e, t = null == t ? e : t, this.map(function () {
                    return oe.clone(this, e, t)
                })
            }, html: function (e) {
                return De(this, function (e) {
                    var t = this[0] || {}, n = 0, i = this.length;
                    if (void 0 === e)return 1 === t.nodeType ? t.innerHTML.replace(_e, "") : void 0;
                    if (!("string" != typeof e || qe.test(e) || !ne.htmlSerialize && je.test(e) || !ne.leadingWhitespace && Re.test(e) || Ye[(Fe.exec(e) || ["", ""])[1].toLowerCase()])) {
                        e = e.replace(He, "<$1></$2>");
                        try {
                            for (; i > n; n++)t = this[n] || {}, 1 === t.nodeType && (oe.cleanData(g(t, !1)), t.innerHTML = e);
                            t = 0
                        } catch (o) {
                        }
                    }
                    t && this.empty().append(e)
                }, null, e, arguments.length)
            }, replaceWith: function () {
                var e = arguments[0];
                return this.domManip(arguments, function (t) {
                    e = this.parentNode, oe.cleanData(g(this)), e && e.replaceChild(t, this)
                }), e && (e.length || e.nodeType) ? this : this.remove()
            }, detach: function (e) {
                return this.remove(e, !0)
            }, domManip: function (e, t) {
                e = K.apply([], e);
                var n, i, o, a, r, s, l = 0, d = this.length, p = this, c = d - 1, u = e[0], h = oe.isFunction(u);
                if (h || d > 1 && "string" == typeof u && !ne.checkClone && Ve.test(u))return this.each(function (n) {
                    var i = p.eq(n);
                    h && (e[0] = u.call(this, n, i.html())), i.domManip(e, t)
                });
                if (d && (s = oe.buildFragment(e, this[0].ownerDocument, !1, this), n = s.firstChild, 1 === s.childNodes.length && (s = n), n)) {
                    for (a = oe.map(g(s, "script"), w), o = a.length; d > l; l++)i = s, l !== c && (i = oe.clone(i, !0, !0), o && oe.merge(a, g(i, "script"))), t.call(this[l], i, l);
                    if (o)for (r = a[a.length - 1].ownerDocument, oe.map(a, b), l = 0; o > l; l++)i = a[l], Ge.test(i.type || "") && !oe._data(i, "globalEval") && oe.contains(r, i) && (i.src ? oe._evalUrl && oe._evalUrl(i.src) : oe.globalEval((i.text || i.textContent || i.innerHTML || "").replace(Ue, "")));
                    s = n = null
                }
                return this
            }
        }), oe.each({
            appendTo: "append",
            prependTo: "prepend",
            insertBefore: "before",
            insertAfter: "after",
            replaceAll: "replaceWith"
        }, function (e, t) {
            oe.fn[e] = function (e) {
                for (var n, i = 0, o = [], a = oe(e), r = a.length - 1; r >= i; i++)n = i === r ? this : this.clone(!0), oe(a[i])[t](n), Q.apply(o, n.get());
                return this.pushStack(o)
            }
        });
        var Ze, Je = {};
        !function () {
            var e;
            ne.shrinkWrapBlocks = function () {
                if (null != e)return e;
                e = !1;
                var t, n, i;
                return n = fe.getElementsByTagName("body")[0], n && n.style ? (t = fe.createElement("div"), i = fe.createElement("div"), i.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(i).appendChild(t), typeof t.style.zoom !== Ce && (t.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1", t.appendChild(fe.createElement("div")).style.width = "5px", e = 3 !== t.offsetWidth), n.removeChild(i), e) : void 0
            }
        }();
        var et, tt, nt = /^margin/, it = new RegExp("^(" + Ee + ")(?!px)[a-z%]+$", "i"), ot = /^(top|right|bottom|left)$/;
        e.getComputedStyle ? (et = function (t) {
            return t.ownerDocument.defaultView.opener ? t.ownerDocument.defaultView.getComputedStyle(t, null) : e.getComputedStyle(t, null)
        }, tt = function (e, t, n) {
            var i, o, a, r, s = e.style;
            return n = n || et(e), r = n ? n.getPropertyValue(t) || n[t] : void 0, n && ("" !== r || oe.contains(e.ownerDocument, e) || (r = oe.style(e, t)), it.test(r) && nt.test(t) && (i = s.width, o = s.minWidth,
                a = s.maxWidth, s.minWidth = s.maxWidth = s.width = r, r = n.width, s.width = i, s.minWidth = o, s.maxWidth = a)), void 0 === r ? r : r + ""
        }) : fe.documentElement.currentStyle && (et = function (e) {
            return e.currentStyle
        }, tt = function (e, t, n) {
            var i, o, a, r, s = e.style;
            return n = n || et(e), r = n ? n[t] : void 0, null == r && s && s[t] && (r = s[t]), it.test(r) && !ot.test(t) && (i = s.left, o = e.runtimeStyle, a = o && o.left, a && (o.left = e.currentStyle.left), s.left = "fontSize" === t ? "1em" : r, r = s.pixelLeft + "px", s.left = i, a && (o.left = a)), void 0 === r ? r : r + "" || "auto"
        }), !function () {
            var t, n, i, o, a, r, s;
            t = fe.createElement("div"), t.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", i = t.getElementsByTagName("a")[0], (n = i && i.style) && !function () {
                var i = function l() {
                    var t, n, i, l;
                    n = fe.getElementsByTagName("body")[0], n && n.style && (t = fe.createElement("div"), i = fe.createElement("div"), i.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(i).appendChild(t), t.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", o = a = !1, s = !0, e.getComputedStyle && (o = "1%" !== (e.getComputedStyle(t, null) || {}).top, a = "4px" === (e.getComputedStyle(t, null) || {width: "4px"}).width, l = t.appendChild(fe.createElement("div")), l.style.cssText = t.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", l.style.marginRight = l.style.width = "0", t.style.width = "1px", s = !parseFloat((e.getComputedStyle(l, null) || {}).marginRight), t.removeChild(l)), t.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", l = t.getElementsByTagName("td"), l[0].style.cssText = "margin:0;border:0;padding:0;display:none", r = 0 === l[0].offsetHeight, r && (l[0].style.display = "", l[1].style.display = "none", r = 0 === l[0].offsetHeight), n.removeChild(i))
                };
                n.cssText = "float:left;opacity:.5", ne.opacity = "0.5" === n.opacity, ne.cssFloat = !!n.cssFloat, t.style.backgroundClip = "content-box", t.cloneNode(!0).style.backgroundClip = "", ne.clearCloneStyle = "content-box" === t.style.backgroundClip, ne.boxSizing = "" === n.boxSizing || "" === n.MozBoxSizing || "" === n.WebkitBoxSizing, oe.extend(ne, {
                    reliableHiddenOffsets: function () {
                        return null == r && i(), r
                    }, boxSizingReliable: function () {
                        return null == a && i(), a
                    }, pixelPosition: function () {
                        return null == o && i(), o
                    }, reliableMarginRight: function () {
                        return null == s && i(), s
                    }
                })
            }()
        }(), oe.swap = function (e, t, n, i) {
            var o, a, r = {};
            for (a in t)r[a] = e.style[a], e.style[a] = t[a];
            o = n.apply(e, i || []);
            for (a in t)e.style[a] = r[a];
            return o
        };
        var at = /alpha\([^)]*\)/i, rt = /opacity\s*=\s*([^)]*)/, st = /^(none|table(?!-c[ea]).+)/, lt = new RegExp("^(" + Ee + ")(.*)$", "i"), dt = new RegExp("^([+-])=(" + Ee + ")", "i"), pt = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        }, ct = {letterSpacing: "0", fontWeight: "400"}, ut = ["Webkit", "O", "Moz", "ms"];
        oe.extend({
            cssHooks: {
                opacity: {
                    get: function (e, t) {
                        if (t) {
                            var n = tt(e, "opacity");
                            return "" === n ? "1" : n
                        }
                    }
                }
            },
            cssNumber: {
                columnCount: !0,
                fillOpacity: !0,
                flexGrow: !0,
                flexShrink: !0,
                fontWeight: !0,
                lineHeight: !0,
                opacity: !0,
                order: !0,
                orphans: !0,
                widows: !0,
                zIndex: !0,
                zoom: !0
            },
            cssProps: {"float": ne.cssFloat ? "cssFloat" : "styleFloat"},
            style: function (e, t, n, i) {
                if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                    var o, a, r, s = oe.camelCase(t), l = e.style;
                    if (t = oe.cssProps[s] || (oe.cssProps[s] = P(l, s)), r = oe.cssHooks[t] || oe.cssHooks[s], void 0 === n)return r && "get" in r && void 0 !== (o = r.get(e, !1, i)) ? o : l[t];
                    if (a = typeof n, "string" === a && (o = dt.exec(n)) && (n = (o[1] + 1) * o[2] + parseFloat(oe.css(e, t)), a = "number"), null != n && n === n && ("number" !== a || oe.cssNumber[s] || (n += "px"), ne.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"), !(r && "set" in r && void 0 === (n = r.set(e, n, i)))))try {
                        l[t] = n
                    } catch (d) {
                    }
                }
            },
            css: function (e, t, n, i) {
                var o, a, r, s = oe.camelCase(t);
                return t = oe.cssProps[s] || (oe.cssProps[s] = P(e.style, s)), r = oe.cssHooks[t] || oe.cssHooks[s], r && "get" in r && (a = r.get(e, !0, n)), void 0 === a && (a = tt(e, t, i)), "normal" === a && t in ct && (a = ct[t]), "" === n || n ? (o = parseFloat(a), n === !0 || oe.isNumeric(o) ? o || 0 : a) : a
            }
        }), oe.each(["height", "width"], function (e, t) {
            oe.cssHooks[t] = {
                get: function (e, n, i) {
                    return n ? st.test(oe.css(e, "display")) && 0 === e.offsetWidth ? oe.swap(e, pt, function () {
                        return $(e, t, i)
                    }) : $(e, t, i) : void 0
                }, set: function (e, n, i) {
                    var o = i && et(e);
                    return D(e, n, i ? N(e, t, i, ne.boxSizing && "border-box" === oe.css(e, "boxSizing", !1, o), o) : 0)
                }
            }
        }), ne.opacity || (oe.cssHooks.opacity = {
            get: function (e, t) {
                return rt.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : t ? "1" : ""
            }, set: function (e, t) {
                var n = e.style, i = e.currentStyle, o = oe.isNumeric(t) ? "alpha(opacity=" + 100 * t + ")" : "", a = i && i.filter || n.filter || "";
                n.zoom = 1, (t >= 1 || "" === t) && "" === oe.trim(a.replace(at, "")) && n.removeAttribute && (n.removeAttribute("filter"), "" === t || i && !i.filter) || (n.filter = at.test(a) ? a.replace(at, o) : a + " " + o)
            }
        }), oe.cssHooks.marginRight = E(ne.reliableMarginRight, function (e, t) {
            return t ? oe.swap(e, {display: "inline-block"}, tt, [e, "marginRight"]) : void 0
        }), oe.each({margin: "", padding: "", border: "Width"}, function (e, t) {
            oe.cssHooks[e + t] = {
                expand: function (n) {
                    for (var i = 0, o = {}, a = "string" == typeof n ? n.split(" ") : [n]; 4 > i; i++)o[e + Pe[i] + t] = a[i] || a[i - 2] || a[0];
                    return o
                }
            }, nt.test(e) || (oe.cssHooks[e + t].set = D)
        }), oe.fn.extend({
            css: function (e, t) {
                return De(this, function (e, t, n) {
                    var i, o, a = {}, r = 0;
                    if (oe.isArray(t)) {
                        for (i = et(e), o = t.length; o > r; r++)a[t[r]] = oe.css(e, t[r], !1, i);
                        return a
                    }
                    return void 0 !== n ? oe.style(e, t, n) : oe.css(e, t)
                }, e, t, arguments.length > 1)
            }, show: function () {
                return I(this, !0)
            }, hide: function () {
                return I(this)
            }, toggle: function (e) {
                return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function () {
                    Ie(this) ? oe(this).show() : oe(this).hide()
                })
            }
        }), oe.Tween = A, A.prototype = {
            constructor: A, init: function (e, t, n, i, o, a) {
                this.elem = e, this.prop = n, this.easing = o || "swing", this.options = t, this.start = this.now = this.cur(), this.end = i, this.unit = a || (oe.cssNumber[n] ? "" : "px")
            }, cur: function () {
                var e = A.propHooks[this.prop];
                return e && e.get ? e.get(this) : A.propHooks._default.get(this)
            }, run: function (e) {
                var t, n = A.propHooks[this.prop];
                return this.options.duration ? this.pos = t = oe.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : A.propHooks._default.set(this), this
            }
        }, A.prototype.init.prototype = A.prototype, A.propHooks = {
            _default: {
                get: function (e) {
                    var t;
                    return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = oe.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0) : e.elem[e.prop]
                }, set: function (e) {
                    oe.fx.step[e.prop] ? oe.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[oe.cssProps[e.prop]] || oe.cssHooks[e.prop]) ? oe.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now
                }
            }
        }, A.propHooks.scrollTop = A.propHooks.scrollLeft = {
            set: function (e) {
                e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
            }
        }, oe.easing = {
            linear: function (e) {
                return e
            }, swing: function (e) {
                return .5 - Math.cos(e * Math.PI) / 2
            }
        }, oe.fx = A.prototype.init, oe.fx.step = {};
        var ht, ft, mt = /^(?:toggle|show|hide)$/, gt = new RegExp("^(?:([+-])=|)(" + Ee + ")([a-z%]*)$", "i"), vt = /queueHooks$/, yt = [z], wt = {
            "*": [function (e, t) {
                var n = this.createTween(e, t), i = n.cur(), o = gt.exec(t), a = o && o[3] || (oe.cssNumber[e] ? "" : "px"), r = (oe.cssNumber[e] || "px" !== a && +i) && gt.exec(oe.css(n.elem, e)), s = 1, l = 20;
                if (r && r[3] !== a) {
                    a = a || r[3], o = o || [], r = +i || 1;
                    do s = s || ".5", r /= s, oe.style(n.elem, e, r + a); while (s !== (s = n.cur() / i) && 1 !== s && --l)
                }
                return o && (r = n.start = +r || +i || 0, n.unit = a, n.end = o[1] ? r + (o[1] + 1) * o[2] : +o[2]), n
            }]
        };
        oe.Animation = oe.extend(j, {
            tweener: function (e, t) {
                oe.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" ");
                for (var n, i = 0, o = e.length; o > i; i++)n = e[i], wt[n] = wt[n] || [], wt[n].unshift(t)
            }, prefilter: function (e, t) {
                t ? yt.unshift(e) : yt.push(e)
            }
        }), oe.speed = function (e, t, n) {
            var i = e && "object" == typeof e ? oe.extend({}, e) : {
                complete: n || !n && t || oe.isFunction(e) && e,
                duration: e,
                easing: n && t || t && !oe.isFunction(t) && t
            };
            return i.duration = oe.fx.off ? 0 : "number" == typeof i.duration ? i.duration : i.duration in oe.fx.speeds ? oe.fx.speeds[i.duration] : oe.fx.speeds._default, (null == i.queue || i.queue === !0) && (i.queue = "fx"), i.old = i.complete, i.complete = function () {
                oe.isFunction(i.old) && i.old.call(this), i.queue && oe.dequeue(this, i.queue)
            }, i
        }, oe.fn.extend({
            fadeTo: function (e, t, n, i) {
                return this.filter(Ie).css("opacity", 0).show().end().animate({opacity: t}, e, n, i)
            }, animate: function (e, t, n, i) {
                var o = oe.isEmptyObject(e), a = oe.speed(t, n, i), r = function () {
                    var t = j(this, oe.extend({}, e), a);
                    (o || oe._data(this, "finish")) && t.stop(!0)
                };
                return r.finish = r, o || a.queue === !1 ? this.each(r) : this.queue(a.queue, r)
            }, stop: function (e, t, n) {
                var i = function (e) {
                    var t = e.stop;
                    delete e.stop, t(n)
                };
                return "string" != typeof e && (n = t, t = e, e = void 0), t && e !== !1 && this.queue(e || "fx", []), this.each(function () {
                    var t = !0, o = null != e && e + "queueHooks", a = oe.timers, r = oe._data(this);
                    if (o)r[o] && r[o].stop && i(r[o]); else for (o in r)r[o] && r[o].stop && vt.test(o) && i(r[o]);
                    for (o = a.length; o--;)a[o].elem !== this || null != e && a[o].queue !== e || (a[o].anim.stop(n), t = !1, a.splice(o, 1));
                    (t || !n) && oe.dequeue(this, e)
                })
            }, finish: function (e) {
                return e !== !1 && (e = e || "fx"), this.each(function () {
                    var t, n = oe._data(this), i = n[e + "queue"], o = n[e + "queueHooks"], a = oe.timers, r = i ? i.length : 0;
                    for (n.finish = !0, oe.queue(this, e, []), o && o.stop && o.stop.call(this, !0), t = a.length; t--;)a[t].elem === this && a[t].queue === e && (a[t].anim.stop(!0), a.splice(t, 1));
                    for (t = 0; r > t; t++)i[t] && i[t].finish && i[t].finish.call(this);
                    delete n.finish
                })
            }
        }), oe.each(["toggle", "show", "hide"], function (e, t) {
            var n = oe.fn[t];
            oe.fn[t] = function (e, i, o) {
                return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(M(t, !0), e, i, o)
            }
        }), oe.each({
            slideDown: M("show"),
            slideUp: M("hide"),
            slideToggle: M("toggle"),
            fadeIn: {opacity: "show"},
            fadeOut: {opacity: "hide"},
            fadeToggle: {opacity: "toggle"}
        }, function (e, t) {
            oe.fn[e] = function (e, n, i) {
                return this.animate(t, e, n, i)
            }
        }), oe.timers = [], oe.fx.tick = function () {
            var e, t = oe.timers, n = 0;
            for (ht = oe.now(); n < t.length; n++)e = t[n], e() || t[n] !== e || t.splice(n--, 1);
            t.length || oe.fx.stop(), ht = void 0
        }, oe.fx.timer = function (e) {
            oe.timers.push(e), e() ? oe.fx.start() : oe.timers.pop()
        }, oe.fx.interval = 13, oe.fx.start = function () {
            ft || (ft = setInterval(oe.fx.tick, oe.fx.interval))
        }, oe.fx.stop = function () {
            clearInterval(ft), ft = null
        }, oe.fx.speeds = {slow: 600, fast: 200, _default: 400}, oe.fn.delay = function (e, t) {
            return e = oe.fx ? oe.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function (t, n) {
                var i = setTimeout(t, e);
                n.stop = function () {
                    clearTimeout(i)
                }
            })
        }, function () {
            var e, t, n, i, o;
            t = fe.createElement("div"), t.setAttribute("className", "t"), t.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", i = t.getElementsByTagName("a")[0], n = fe.createElement("select"), o = n.appendChild(fe.createElement("option")), e = t.getElementsByTagName("input")[0], i.style.cssText = "top:1px", ne.getSetAttribute = "t" !== t.className, ne.style = /top/.test(i.getAttribute("style")), ne.hrefNormalized = "/a" === i.getAttribute("href"), ne.checkOn = !!e.value, ne.optSelected = o.selected, ne.enctype = !!fe.createElement("form").enctype, n.disabled = !0, ne.optDisabled = !o.disabled, e = fe.createElement("input"), e.setAttribute("value", ""), ne.input = "" === e.getAttribute("value"), e.value = "t", e.setAttribute("type", "radio"), ne.radioValue = "t" === e.value
        }();
        var bt = /\r/g;
        oe.fn.extend({
            val: function (e) {
                var t, n, i, o = this[0];
                return arguments.length ? (i = oe.isFunction(e), this.each(function (n) {
                    var o;
                    1 === this.nodeType && (o = i ? e.call(this, n, oe(this).val()) : e, null == o ? o = "" : "number" == typeof o ? o += "" : oe.isArray(o) && (o = oe.map(o, function (e) {
                        return null == e ? "" : e + ""
                    })), t = oe.valHooks[this.type] || oe.valHooks[this.nodeName.toLowerCase()], t && "set" in t && void 0 !== t.set(this, o, "value") || (this.value = o))
                })) : o ? (t = oe.valHooks[o.type] || oe.valHooks[o.nodeName.toLowerCase()], t && "get" in t && void 0 !== (n = t.get(o, "value")) ? n : (n = o.value, "string" == typeof n ? n.replace(bt, "") : null == n ? "" : n)) : void 0
            }
        }), oe.extend({
            valHooks: {
                option: {
                    get: function (e) {
                        var t = oe.find.attr(e, "value");
                        return null != t ? t : oe.trim(oe.text(e))
                    }
                }, select: {
                    get: function (e) {
                        for (var t, n, i = e.options, o = e.selectedIndex, a = "select-one" === e.type || 0 > o, r = a ? null : [], s = a ? o + 1 : i.length, l = 0 > o ? s : a ? o : 0; s > l; l++)if (n = i[l], !(!n.selected && l !== o || (ne.optDisabled ? n.disabled : null !== n.getAttribute("disabled")) || n.parentNode.disabled && oe.nodeName(n.parentNode, "optgroup"))) {
                            if (t = oe(n).val(), a)return t;
                            r.push(t)
                        }
                        return r
                    }, set: function (e, t) {
                        for (var n, i, o = e.options, a = oe.makeArray(t), r = o.length; r--;)if (i = o[r], oe.inArray(oe.valHooks.option.get(i), a) >= 0)try {
                            i.selected = n = !0
                        } catch (s) {
                            i.scrollHeight
                        } else i.selected = !1;
                        return n || (e.selectedIndex = -1), o
                    }
                }
            }
        }), oe.each(["radio", "checkbox"], function () {
            oe.valHooks[this] = {
                set: function (e, t) {
                    return oe.isArray(t) ? e.checked = oe.inArray(oe(e).val(), t) >= 0 : void 0
                }
            }, ne.checkOn || (oe.valHooks[this].get = function (e) {
                return null === e.getAttribute("value") ? "on" : e.value
            })
        });
        var xt, Tt, Ct = oe.expr.attrHandle, St = /^(?:checked|selected)$/i, kt = ne.getSetAttribute, Et = ne.input;
        oe.fn.extend({
            attr: function (e, t) {
                return De(this, oe.attr, e, t, arguments.length > 1)
            }, removeAttr: function (e) {
                return this.each(function () {
                    oe.removeAttr(this, e)
                })
            }
        }), oe.extend({
            attr: function (e, t, n) {
                var i, o, a = e.nodeType;
                return e && 3 !== a && 8 !== a && 2 !== a ? typeof e.getAttribute === Ce ? oe.prop(e, t, n) : (1 === a && oe.isXMLDoc(e) || (t = t.toLowerCase(), i = oe.attrHooks[t] || (oe.expr.match.bool.test(t) ? Tt : xt)), void 0 === n ? i && "get" in i && null !== (o = i.get(e, t)) ? o : (o = oe.find.attr(e, t), null == o ? void 0 : o) : null !== n ? i && "set" in i && void 0 !== (o = i.set(e, n, t)) ? o : (e.setAttribute(t, n + ""), n) : void oe.removeAttr(e, t)) : void 0
            }, removeAttr: function (e, t) {
                var n, i, o = 0, a = t && t.match(we);
                if (a && 1 === e.nodeType)for (; n = a[o++];)i = oe.propFix[n] || n, oe.expr.match.bool.test(n) ? Et && kt || !St.test(n) ? e[i] = !1 : e[oe.camelCase("default-" + n)] = e[i] = !1 : oe.attr(e, n, ""), e.removeAttribute(kt ? n : i)
            }, attrHooks: {
                type: {
                    set: function (e, t) {
                        if (!ne.radioValue && "radio" === t && oe.nodeName(e, "input")) {
                            var n = e.value;
                            return e.setAttribute("type", t), n && (e.value = n), t
                        }
                    }
                }
            }
        }), Tt = {
            set: function (e, t, n) {
                return t === !1 ? oe.removeAttr(e, n) : Et && kt || !St.test(n) ? e.setAttribute(!kt && oe.propFix[n] || n, n) : e[oe.camelCase("default-" + n)] = e[n] = !0, n
            }
        }, oe.each(oe.expr.match.bool.source.match(/\w+/g), function (e, t) {
            var n = Ct[t] || oe.find.attr;
            Ct[t] = Et && kt || !St.test(t) ? function (e, t, i) {
                var o, a;
                return i || (a = Ct[t], Ct[t] = o, o = null != n(e, t, i) ? t.toLowerCase() : null, Ct[t] = a), o
            } : function (e, t, n) {
                return n ? void 0 : e[oe.camelCase("default-" + t)] ? t.toLowerCase() : null
            }
        }), Et && kt || (oe.attrHooks.value = {
            set: function (e, t, n) {
                return oe.nodeName(e, "input") ? void(e.defaultValue = t) : xt && xt.set(e, t, n)
            }
        }), kt || (xt = {
            set: function (e, t, n) {
                var i = e.getAttributeNode(n);
                return i || e.setAttributeNode(i = e.ownerDocument.createAttribute(n)), i.value = t += "", "value" === n || t === e.getAttribute(n) ? t : void 0
            }
        }, Ct.id = Ct.name = Ct.coords = function (e, t, n) {
            var i;
            return n ? void 0 : (i = e.getAttributeNode(t)) && "" !== i.value ? i.value : null
        }, oe.valHooks.button = {
            get: function (e, t) {
                var n = e.getAttributeNode(t);
                return n && n.specified ? n.value : void 0
            }, set: xt.set
        }, oe.attrHooks.contenteditable = {
            set: function (e, t, n) {
                xt.set(e, "" === t ? !1 : t, n)
            }
        }, oe.each(["width", "height"], function (e, t) {
            oe.attrHooks[t] = {
                set: function (e, n) {
                    return "" === n ? (e.setAttribute(t, "auto"), n) : void 0
                }
            }
        })), ne.style || (oe.attrHooks.style = {
            get: function (e) {
                return e.style.cssText || void 0
            }, set: function (e, t) {
                return e.style.cssText = t + ""
            }
        });
        var Pt = /^(?:input|select|textarea|button|object)$/i, It = /^(?:a|area)$/i;
        oe.fn.extend({
            prop: function (e, t) {
                return De(this, oe.prop, e, t, arguments.length > 1)
            }, removeProp: function (e) {
                return e = oe.propFix[e] || e, this.each(function () {
                    try {
                        this[e] = void 0, delete this[e]
                    } catch (t) {
                    }
                })
            }
        }), oe.extend({
            propFix: {"for": "htmlFor", "class": "className"}, prop: function (e, t, n) {
                var i, o, a, r = e.nodeType;
                return e && 3 !== r && 8 !== r && 2 !== r ? (a = 1 !== r || !oe.isXMLDoc(e), a && (t = oe.propFix[t] || t, o = oe.propHooks[t]), void 0 !== n ? o && "set" in o && void 0 !== (i = o.set(e, n, t)) ? i : e[t] = n : o && "get" in o && null !== (i = o.get(e, t)) ? i : e[t]) : void 0
            }, propHooks: {
                tabIndex: {
                    get: function (e) {
                        var t = oe.find.attr(e, "tabindex");
                        return t ? parseInt(t, 10) : Pt.test(e.nodeName) || It.test(e.nodeName) && e.href ? 0 : -1
                    }
                }
            }
        }), ne.hrefNormalized || oe.each(["href", "src"], function (e, t) {
            oe.propHooks[t] = {
                get: function (e) {
                    return e.getAttribute(t, 4)
                }
            }
        }), ne.optSelected || (oe.propHooks.selected = {
            get: function (e) {
                var t = e.parentNode;
                return t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex), null
            }
        }), oe.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
            oe.propFix[this.toLowerCase()] = this
        }), ne.enctype || (oe.propFix.enctype = "encoding");
        var Dt = /[\t\r\n\f]/g;
        oe.fn.extend({
            addClass: function (e) {
                var t, n, i, o, a, r, s = 0, l = this.length, d = "string" == typeof e && e;
                if (oe.isFunction(e))return this.each(function (t) {
                    oe(this).addClass(e.call(this, t, this.className))
                });
                if (d)for (t = (e || "").match(we) || []; l > s; s++)if (n = this[s], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Dt, " ") : " ")) {
                    for (a = 0; o = t[a++];)i.indexOf(" " + o + " ") < 0 && (i += o + " ");
                    r = oe.trim(i), n.className !== r && (n.className = r)
                }
                return this
            }, removeClass: function (e) {
                var t, n, i, o, a, r, s = 0, l = this.length, d = 0 === arguments.length || "string" == typeof e && e;
                if (oe.isFunction(e))return this.each(function (t) {
                    oe(this).removeClass(e.call(this, t, this.className))
                });
                if (d)for (t = (e || "").match(we) || []; l > s; s++)if (n = this[s], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Dt, " ") : "")) {
                    for (a = 0; o = t[a++];)for (; i.indexOf(" " + o + " ") >= 0;)i = i.replace(" " + o + " ", " ");
                    r = e ? oe.trim(i) : "", n.className !== r && (n.className = r)
                }
                return this
            }, toggleClass: function (e, t) {
                var n = typeof e;
                return "boolean" == typeof t && "string" === n ? t ? this.addClass(e) : this.removeClass(e) : this.each(oe.isFunction(e) ? function (n) {
                    oe(this).toggleClass(e.call(this, n, this.className, t), t)
                } : function () {
                    if ("string" === n)for (var t, i = 0, o = oe(this), a = e.match(we) || []; t = a[i++];)o.hasClass(t) ? o.removeClass(t) : o.addClass(t); else(n === Ce || "boolean" === n) && (this.className && oe._data(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : oe._data(this, "__className__") || "")
                })
            }, hasClass: function (e) {
                for (var t = " " + e + " ", n = 0, i = this.length; i > n; n++)if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(Dt, " ").indexOf(t) >= 0)return !0;
                return !1
            }
        }), oe.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (e, t) {
            oe.fn[t] = function (e, n) {
                return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
            }
        }), oe.fn.extend({
            hover: function (e, t) {
                return this.mouseenter(e).mouseleave(t || e)
            }, bind: function (e, t, n) {
                return this.on(e, null, t, n)
            }, unbind: function (e, t) {
                return this.off(e, null, t)
            }, delegate: function (e, t, n, i) {
                return this.on(t, e, n, i)
            }, undelegate: function (e, t, n) {
                return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
            }
        });
        var Nt = oe.now(), $t = /\?/, At = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
        oe.parseJSON = function (t) {
            if (e.JSON && e.JSON.parse)return e.JSON.parse(t + "");
            var n, i = null, o = oe.trim(t + "");
            return o && !oe.trim(o.replace(At, function (e, t, o, a) {
                return n && t && (i = 0), 0 === i ? e : (n = o || t, i += !a - !o, "")
            })) ? Function("return " + o)() : oe.error("Invalid JSON: " + t)
        }, oe.parseXML = function (t) {
            var n, i;
            if (!t || "string" != typeof t)return null;
            try {
                e.DOMParser ? (i = new DOMParser, n = i.parseFromString(t, "text/xml")) : (n = new ActiveXObject("Microsoft.XMLDOM"), n.async = "false", n.loadXML(t))
            } catch (o) {
                n = void 0
            }
            return n && n.documentElement && !n.getElementsByTagName("parsererror").length || oe.error("Invalid XML: " + t), n
        };
        var Lt, Mt, Ot = /#.*$/, zt = /([?&])_=[^&]*/, _t = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm, jt = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, Rt = /^(?:GET|HEAD)$/, Ht = /^\/\//, Ft = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, Bt = {}, Wt = {}, qt = "*/".concat("*");
        try {
            Mt = location.href
        } catch (Vt) {
            Mt = fe.createElement("a"), Mt.href = "", Mt = Mt.href
        }
        Lt = Ft.exec(Mt.toLowerCase()) || [], oe.extend({
            active: 0,
            lastModified: {},
            etag: {},
            ajaxSettings: {
                url: Mt,
                type: "GET",
                isLocal: jt.test(Lt[1]),
                global: !0,
                processData: !0,
                async: !0,
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                accepts: {
                    "*": qt,
                    text: "text/plain",
                    html: "text/html",
                    xml: "application/xml, text/xml",
                    json: "application/json, text/javascript"
                },
                contents: {xml: /xml/, html: /html/, json: /json/},
                responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"},
                converters: {"* text": String, "text html": !0, "text json": oe.parseJSON, "text xml": oe.parseXML},
                flatOptions: {url: !0, context: !0}
            },
            ajaxSetup: function (e, t) {
                return t ? F(F(e, oe.ajaxSettings), t) : F(oe.ajaxSettings, e)
            },
            ajaxPrefilter: R(Bt),
            ajaxTransport: R(Wt),
            ajax: function (e, t) {
                function n(e, t, n, i) {
                    var o, p, v, y, b, T = t;
                    2 !== w && (w = 2, s && clearTimeout(s), d = void 0, r = i || "", x.readyState = e > 0 ? 4 : 0, o = e >= 200 && 300 > e || 304 === e, n && (y = B(c, x, n)), y = W(c, y, x, o), o ? (c.ifModified && (b = x.getResponseHeader("Last-Modified"), b && (oe.lastModified[a] = b), b = x.getResponseHeader("etag"), b && (oe.etag[a] = b)), 204 === e || "HEAD" === c.type ? T = "nocontent" : 304 === e ? T = "notmodified" : (T = y.state, p = y.data, v = y.error, o = !v)) : (v = T, (e || !T) && (T = "error", 0 > e && (e = 0))), x.status = e, x.statusText = (t || T) + "", o ? f.resolveWith(u, [p, T, x]) : f.rejectWith(u, [x, T, v]), x.statusCode(g), g = void 0, l && h.trigger(o ? "ajaxSuccess" : "ajaxError", [x, c, o ? p : v]), m.fireWith(u, [x, T]), l && (h.trigger("ajaxComplete", [x, c]), --oe.active || oe.event.trigger("ajaxStop")))
                }

                "object" == typeof e && (t = e, e = void 0), t = t || {};
                var i, o, a, r, s, l, d, p, c = oe.ajaxSetup({}, t), u = c.context || c, h = c.context && (u.nodeType || u.jquery) ? oe(u) : oe.event, f = oe.Deferred(), m = oe.Callbacks("once memory"), g = c.statusCode || {}, v = {}, y = {}, w = 0, b = "canceled", x = {
                    readyState: 0,
                    getResponseHeader: function (e) {
                        var t;
                        if (2 === w) {
                            if (!p)for (p = {}; t = _t.exec(r);)p[t[1].toLowerCase()] = t[2];
                            t = p[e.toLowerCase()]
                        }
                        return null == t ? null : t
                    },
                    getAllResponseHeaders: function () {
                        return 2 === w ? r : null
                    },
                    setRequestHeader: function (e, t) {
                        var n = e.toLowerCase();
                        return w || (e = y[n] = y[n] || e, v[e] = t), this
                    },
                    overrideMimeType: function (e) {
                        return w || (c.mimeType = e), this
                    },
                    statusCode: function (e) {
                        var t;
                        if (e)if (2 > w)for (t in e)g[t] = [g[t], e[t]]; else x.always(e[x.status]);
                        return this
                    },
                    abort: function (e) {
                        var t = e || b;
                        return d && d.abort(t), n(0, t), this
                    }
                };
                if (f.promise(x).complete = m.add, x.success = x.done, x.error = x.fail, c.url = ((e || c.url || Mt) + "").replace(Ot, "").replace(Ht, Lt[1] + "//"), c.type = t.method || t.type || c.method || c.type, c.dataTypes = oe.trim(c.dataType || "*").toLowerCase().match(we) || [""], null == c.crossDomain && (i = Ft.exec(c.url.toLowerCase()), c.crossDomain = !(!i || i[1] === Lt[1] && i[2] === Lt[2] && (i[3] || ("http:" === i[1] ? "80" : "443")) === (Lt[3] || ("http:" === Lt[1] ? "80" : "443")))), c.data && c.processData && "string" != typeof c.data && (c.data = oe.param(c.data, c.traditional)), H(Bt, c, t, x), 2 === w)return x;
                l = oe.event && c.global, l && 0 === oe.active++ && oe.event.trigger("ajaxStart"), c.type = c.type.toUpperCase(), c.hasContent = !Rt.test(c.type), a = c.url, c.hasContent || (c.data && (a = c.url += ($t.test(a) ? "&" : "?") + c.data, delete c.data), c.cache === !1 && (c.url = zt.test(a) ? a.replace(zt, "$1_=" + Nt++) : a + ($t.test(a) ? "&" : "?") + "_=" + Nt++)), c.ifModified && (oe.lastModified[a] && x.setRequestHeader("If-Modified-Since", oe.lastModified[a]), oe.etag[a] && x.setRequestHeader("If-None-Match", oe.etag[a])), (c.data && c.hasContent && c.contentType !== !1 || t.contentType) && x.setRequestHeader("Content-Type", c.contentType), x.setRequestHeader("Accept", c.dataTypes[0] && c.accepts[c.dataTypes[0]] ? c.accepts[c.dataTypes[0]] + ("*" !== c.dataTypes[0] ? ", " + qt + "; q=0.01" : "") : c.accepts["*"]);
                for (o in c.headers)x.setRequestHeader(o, c.headers[o]);
                if (c.beforeSend && (c.beforeSend.call(u, x, c) === !1 || 2 === w))return x.abort();
                b = "abort";
                for (o in{success: 1, error: 1, complete: 1})x[o](c[o]);
                if (d = H(Wt, c, t, x)) {
                    x.readyState = 1, l && h.trigger("ajaxSend", [x, c]), c.async && c.timeout > 0 && (s = setTimeout(function () {
                        x.abort("timeout")
                    }, c.timeout));
                    try {
                        w = 1, d.send(v, n)
                    } catch (T) {
                        if (!(2 > w))throw T;
                        n(-1, T)
                    }
                } else n(-1, "No Transport");
                return x
            },
            getJSON: function (e, t, n) {
                return oe.get(e, t, n, "json")
            },
            getScript: function (e, t) {
                return oe.get(e, void 0, t, "script")
            }
        }), oe.each(["get", "post"], function (e, t) {
            oe[t] = function (e, n, i, o) {
                return oe.isFunction(n) && (o = o || i, i = n, n = void 0), oe.ajax({
                    url: e,
                    type: t,
                    dataType: o,
                    data: n,
                    success: i
                })
            }
        }), oe._evalUrl = function (e) {
            return oe.ajax({url: e, type: "GET", dataType: "script", async: !1, global: !1, "throws": !0})
        }, oe.fn.extend({
            wrapAll: function (e) {
                if (oe.isFunction(e))return this.each(function (t) {
                    oe(this).wrapAll(e.call(this, t))
                });
                if (this[0]) {
                    var t = oe(e, this[0].ownerDocument).eq(0).clone(!0);
                    this[0].parentNode && t.insertBefore(this[0]), t.map(function () {
                        for (var e = this; e.firstChild && 1 === e.firstChild.nodeType;)e = e.firstChild;
                        return e
                    }).append(this)
                }
                return this
            }, wrapInner: function (e) {
                return this.each(oe.isFunction(e) ? function (t) {
                    oe(this).wrapInner(e.call(this, t))
                } : function () {
                    var t = oe(this), n = t.contents();
                    n.length ? n.wrapAll(e) : t.append(e)
                })
            }, wrap: function (e) {
                var t = oe.isFunction(e);
                return this.each(function (n) {
                    oe(this).wrapAll(t ? e.call(this, n) : e)
                })
            }, unwrap: function () {
                return this.parent().each(function () {
                    oe.nodeName(this, "body") || oe(this).replaceWith(this.childNodes)
                }).end()
            }
        }), oe.expr.filters.hidden = function (e) {
            return e.offsetWidth <= 0 && e.offsetHeight <= 0 || !ne.reliableHiddenOffsets() && "none" === (e.style && e.style.display || oe.css(e, "display"))
        }, oe.expr.filters.visible = function (e) {
            return !oe.expr.filters.hidden(e)
        };
        var Gt = /%20/g, Xt = /\[\]$/, Ut = /\r?\n/g, Yt = /^(?:submit|button|image|reset|file)$/i, Kt = /^(?:input|select|textarea|keygen)/i;
        oe.param = function (e, t) {
            var n, i = [], o = function (e, t) {
                t = oe.isFunction(t) ? t() : null == t ? "" : t, i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
            };
            if (void 0 === t && (t = oe.ajaxSettings && oe.ajaxSettings.traditional), oe.isArray(e) || e.jquery && !oe.isPlainObject(e))oe.each(e, function () {
                o(this.name, this.value)
            }); else for (n in e)q(n, e[n], t, o);
            return i.join("&").replace(Gt, "+")
        }, oe.fn.extend({
            serialize: function () {
                return oe.param(this.serializeArray())
            }, serializeArray: function () {
                return this.map(function () {
                    var e = oe.prop(this, "elements");
                    return e ? oe.makeArray(e) : this
                }).filter(function () {
                    var e = this.type;
                    return this.name && !oe(this).is(":disabled") && Kt.test(this.nodeName) && !Yt.test(e) && (this.checked || !Ne.test(e))
                }).map(function (e, t) {
                    var n = oe(this).val();
                    return null == n ? null : oe.isArray(n) ? oe.map(n, function (e) {
                        return {name: t.name, value: e.replace(Ut, "\r\n")}
                    }) : {name: t.name, value: n.replace(Ut, "\r\n")}
                }).get()
            }
        }), oe.ajaxSettings.xhr = void 0 !== e.ActiveXObject ? function () {
            return !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && V() || G()
        } : V;
        var Qt = 0, Zt = {}, Jt = oe.ajaxSettings.xhr();
        e.attachEvent && e.attachEvent("onunload", function () {
            for (var e in Zt)Zt[e](void 0, !0)
        }), ne.cors = !!Jt && "withCredentials" in Jt, Jt = ne.ajax = !!Jt, Jt && oe.ajaxTransport(function (e) {
            if (!e.crossDomain || ne.cors) {
                var t;
                return {
                    send: function (n, i) {
                        var o, a = e.xhr(), r = ++Qt;
                        if (a.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields)for (o in e.xhrFields)a[o] = e.xhrFields[o];
                        e.mimeType && a.overrideMimeType && a.overrideMimeType(e.mimeType), e.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest");
                        for (o in n)void 0 !== n[o] && a.setRequestHeader(o, n[o] + "");
                        a.send(e.hasContent && e.data || null), t = function (n, o) {
                            var s, l, d;
                            if (t && (o || 4 === a.readyState))if (delete Zt[r], t = void 0, a.onreadystatechange = oe.noop, o)4 !== a.readyState && a.abort(); else {
                                d = {}, s = a.status, "string" == typeof a.responseText && (d.text = a.responseText);
                                try {
                                    l = a.statusText
                                } catch (p) {
                                    l = ""
                                }
                                s || !e.isLocal || e.crossDomain ? 1223 === s && (s = 204) : s = d.text ? 200 : 404
                            }
                            d && i(s, l, d, a.getAllResponseHeaders())
                        }, e.async ? 4 === a.readyState ? setTimeout(t) : a.onreadystatechange = Zt[r] = t : t()
                    }, abort: function () {
                        t && t(void 0, !0)
                    }
                }
            }
        }), oe.ajaxSetup({
            accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},
            contents: {script: /(?:java|ecma)script/},
            converters: {
                "text script": function (e) {
                    return oe.globalEval(e), e
                }
            }
        }), oe.ajaxPrefilter("script", function (e) {
            void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET", e.global = !1)
        }), oe.ajaxTransport("script", function (e) {
            if (e.crossDomain) {
                var t, n = fe.head || oe("head")[0] || fe.documentElement;
                return {
                    send: function (i, o) {
                        t = fe.createElement("script"), t.async = !0, e.scriptCharset && (t.charset = e.scriptCharset), t.src = e.url, t.onload = t.onreadystatechange = function (e, n) {
                            (n || !t.readyState || /loaded|complete/.test(t.readyState)) && (t.onload = t.onreadystatechange = null, t.parentNode && t.parentNode.removeChild(t), t = null, n || o(200, "success"))
                        }, n.insertBefore(t, n.firstChild)
                    }, abort: function () {
                        t && t.onload(void 0, !0)
                    }
                }
            }
        });
        var en = [], tn = /(=)\?(?=&|$)|\?\?/;
        oe.ajaxSetup({
            jsonp: "callback", jsonpCallback: function () {
                var e = en.pop() || oe.expando + "_" + Nt++;
                return this[e] = !0, e
            }
        }), oe.ajaxPrefilter("json jsonp", function (t, n, i) {
            var o, a, r, s = t.jsonp !== !1 && (tn.test(t.url) ? "url" : "string" == typeof t.data && !(t.contentType || "").indexOf("application/x-www-form-urlencoded") && tn.test(t.data) && "data");
            return s || "jsonp" === t.dataTypes[0] ? (o = t.jsonpCallback = oe.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, s ? t[s] = t[s].replace(tn, "$1" + o) : t.jsonp !== !1 && (t.url += ($t.test(t.url) ? "&" : "?") + t.jsonp + "=" + o), t.converters["script json"] = function () {
                return r || oe.error(o + " was not called"), r[0]
            }, t.dataTypes[0] = "json", a = e[o], e[o] = function () {
                r = arguments
            }, i.always(function () {
                e[o] = a, t[o] && (t.jsonpCallback = n.jsonpCallback, en.push(o)), r && oe.isFunction(a) && a(r[0]), r = a = void 0
            }), "script") : void 0
        }), oe.parseHTML = function (e, t, n) {
            if (!e || "string" != typeof e)return null;
            "boolean" == typeof t && (n = t, t = !1), t = t || fe;
            var i = ce.exec(e), o = !n && [];
            return i ? [t.createElement(i[1])] : (i = oe.buildFragment([e], t, o), o && o.length && oe(o).remove(), oe.merge([], i.childNodes))
        };
        var nn = oe.fn.load;
        oe.fn.load = function (e, t, n) {
            if ("string" != typeof e && nn)return nn.apply(this, arguments);
            var i, o, a, r = this, s = e.indexOf(" ");
            return s >= 0 && (i = oe.trim(e.slice(s, e.length)), e = e.slice(0, s)), oe.isFunction(t) ? (n = t, t = void 0) : t && "object" == typeof t && (a = "POST"), r.length > 0 && oe.ajax({
                url: e,
                type: a,
                dataType: "html",
                data: t
            }).done(function (e) {
                o = arguments, r.html(i ? oe("<div>").append(oe.parseHTML(e)).find(i) : e)
            }).complete(n && function (e, t) {
                    r.each(n, o || [e.responseText, t, e])
                }), this
        }, oe.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (e, t) {
            oe.fn[t] = function (e) {
                return this.on(t, e)
            }
        }), oe.expr.filters.animated = function (e) {
            return oe.grep(oe.timers, function (t) {
                return e === t.elem
            }).length
        };
        var on = e.document.documentElement;
        oe.offset = {
            setOffset: function (e, t, n) {
                var i, o, a, r, s, l, d, p = oe.css(e, "position"), c = oe(e), u = {};
                "static" === p && (e.style.position = "relative"), s = c.offset(), a = oe.css(e, "top"), l = oe.css(e, "left"), d = ("absolute" === p || "fixed" === p) && oe.inArray("auto", [a, l]) > -1, d ? (i = c.position(), r = i.top, o = i.left) : (r = parseFloat(a) || 0, o = parseFloat(l) || 0), oe.isFunction(t) && (t = t.call(e, n, s)), null != t.top && (u.top = t.top - s.top + r), null != t.left && (u.left = t.left - s.left + o), "using" in t ? t.using.call(e, u) : c.css(u)
            }
        }, oe.fn.extend({
            offset: function (e) {
                if (arguments.length)return void 0 === e ? this : this.each(function (t) {
                    oe.offset.setOffset(this, e, t)
                });
                var t, n, i = {top: 0, left: 0}, o = this[0], a = o && o.ownerDocument;
                return a ? (t = a.documentElement, oe.contains(t, o) ? (typeof o.getBoundingClientRect !== Ce && (i = o.getBoundingClientRect()), n = X(a), {
                    top: i.top + (n.pageYOffset || t.scrollTop) - (t.clientTop || 0),
                    left: i.left + (n.pageXOffset || t.scrollLeft) - (t.clientLeft || 0)
                }) : i) : void 0
            }, position: function () {
                if (this[0]) {
                    var e, t, n = {top: 0, left: 0}, i = this[0];
                    return "fixed" === oe.css(i, "position") ? t = i.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), oe.nodeName(e[0], "html") || (n = e.offset()), n.top += oe.css(e[0], "borderTopWidth", !0), n.left += oe.css(e[0], "borderLeftWidth", !0)), {
                        top: t.top - n.top - oe.css(i, "marginTop", !0),
                        left: t.left - n.left - oe.css(i, "marginLeft", !0)
                    }
                }
            }, offsetParent: function () {
                return this.map(function () {
                    for (var e = this.offsetParent || on; e && !oe.nodeName(e, "html") && "static" === oe.css(e, "position");)e = e.offsetParent;
                    return e || on
                })
            }
        }), oe.each({scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function (e, t) {
            var n = /Y/.test(t);
            oe.fn[e] = function (i) {
                return De(this, function (e, i, o) {
                    var a = X(e);
                    return void 0 === o ? a ? t in a ? a[t] : a.document.documentElement[i] : e[i] : void(a ? a.scrollTo(n ? oe(a).scrollLeft() : o, n ? o : oe(a).scrollTop()) : e[i] = o)
                }, e, i, arguments.length, null)
            }
        }), oe.each(["top", "left"], function (e, t) {
            oe.cssHooks[t] = E(ne.pixelPosition, function (e, n) {
                return n ? (n = tt(e, t), it.test(n) ? oe(e).position()[t] + "px" : n) : void 0
            })
        }), oe.each({Height: "height", Width: "width"}, function (e, t) {
            oe.each({padding: "inner" + e, content: t, "": "outer" + e}, function (n, i) {
                oe.fn[i] = function (i, o) {
                    var a = arguments.length && (n || "boolean" != typeof i), r = n || (i === !0 || o === !0 ? "margin" : "border");
                    return De(this, function (t, n, i) {
                        var o;
                        return oe.isWindow(t) ? t.document.documentElement["client" + e] : 9 === t.nodeType ? (o = t.documentElement, Math.max(t.body["scroll" + e], o["scroll" + e], t.body["offset" + e], o["offset" + e], o["client" + e])) : void 0 === i ? oe.css(t, n, r) : oe.style(t, n, i, r)
                    }, t, a ? i : void 0, a, null)
                }
            })
        }), oe.fn.size = function () {
            return this.length
        }, oe.fn.andSelf = oe.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function () {
            return oe
        });
        var an = e.jQuery, rn = e.$;
        return oe.noConflict = function (t) {
            return e.$ === oe && (e.$ = rn), t && e.jQuery === oe && (e.jQuery = an), oe
        }, typeof t === Ce && (e.jQuery = e.$ = oe), oe
    }), !function (e) {
        "function" == typeof define && define.amd ? define(["./blueimp-helper"], e) : (window.blueimp = window.blueimp || {}, window.blueimp.Gallery = e(window.blueimp.helper || window.jQuery))
    }(function (e) {
        function t(e, n) {
            return void 0 === document.body.style.maxHeight ? null : this && this.options === t.prototype.options ? e && e.length ? (this.list = e, this.num = e.length, this.initOptions(n), void this.initialize()) : void this.console.log("blueimp Gallery: No or empty list provided as first argument.", e) : new t(e, n)
        }

        return e.extend(t.prototype, {
            options: {
                container: "#blueimp-gallery",
                slidesContainer: "div",
                titleElement: "h3",
                displayClass: "blueimp-gallery-display",
                controlsClass: "blueimp-gallery-controls",
                singleClass: "blueimp-gallery-single",
                leftEdgeClass: "blueimp-gallery-left",
                rightEdgeClass: "blueimp-gallery-right",
                playingClass: "blueimp-gallery-playing",
                slideClass: "slide",
                slideLoadingClass: "slide-loading",
                slideErrorClass: "slide-error",
                slideContentClass: "slide-content",
                toggleClass: "toggle",
                prevClass: "prev",
                nextClass: "next",
                closeClass: "close",
                playPauseClass: "play-pause",
                typeProperty: "type",
                titleProperty: "title",
                urlProperty: "href",
                displayTransition: !0,
                clearSlides: !0,
                stretchImages: !1,
                toggleControlsOnReturn: !0,
                toggleSlideshowOnSpace: !0,
                enableKeyboardNavigation: !0,
                closeOnEscape: !0,
                closeOnSlideClick: !0,
                closeOnSwipeUpOrDown: !0,
                emulateTouchEvents: !0,
                stopTouchEventsPropagation: !1,
                hidePageScrollbars: !0,
                disableScroll: !0,
                carousel: !1,
                continuous: !0,
                unloadElements: !0,
                startSlideshow: !1,
                slideshowInterval: 5e3,
                index: 0,
                preloadRange: 2,
                transitionSpeed: 400,
                slideshowTransitionSpeed: void 0,
                event: void 0,
                onopen: void 0,
                onopened: void 0,
                onslide: void 0,
                onslideend: void 0,
                onslidecomplete: void 0,
                onclose: void 0,
                onclosed: void 0
            },
            carouselOptions: {
                hidePageScrollbars: !1,
                toggleControlsOnReturn: !1,
                toggleSlideshowOnSpace: !1,
                enableKeyboardNavigation: !1,
                closeOnEscape: !1,
                closeOnSlideClick: !1,
                closeOnSwipeUpOrDown: !1,
                disableScroll: !1,
                startSlideshow: !0
            },
            console: window.console && "function" == typeof window.console.log ? window.console : {
                log: function () {
                }
            },
            support: function (t) {
                var n = {touch: void 0 !== window.ontouchstart || window.DocumentTouch && document instanceof DocumentTouch}, i = {
                    webkitTransition: {
                        end: "webkitTransitionEnd",
                        prefix: "-webkit-"
                    },
                    MozTransition: {end: "transitionend", prefix: "-moz-"},
                    OTransition: {end: "otransitionend", prefix: "-o-"},
                    transition: {end: "transitionend", prefix: ""}
                }, o = function a() {
                    var e, i, a = n.transition;
                    document.body.appendChild(t), a && (e = a.name.slice(0, -9) + "ransform", void 0 !== t.style[e] && (t.style[e] = "translateZ(0)", i = window.getComputedStyle(t).getPropertyValue(a.prefix + "transform"), n.transform = {
                        prefix: a.prefix,
                        name: e,
                        translate: !0,
                        translateZ: !!i && "none" !== i
                    })), void 0 !== t.style.backgroundSize && (n.backgroundSize = {}, t.style.backgroundSize = "contain", n.backgroundSize.contain = "contain" === window.getComputedStyle(t).getPropertyValue("background-size"), t.style.backgroundSize = "cover", n.backgroundSize.cover = "cover" === window.getComputedStyle(t).getPropertyValue("background-size")), document.body.removeChild(t)
                };
                return function (e, n) {
                    var i;
                    for (i in n)if (n.hasOwnProperty(i) && void 0 !== t.style[i]) {
                        e.transition = n[i], e.transition.name = i;
                        break
                    }
                }(n, i), document.body ? o() : e(document).on("DOMContentLoaded", o), n
            }(document.createElement("div")),
            requestAnimationFrame: window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame,
            initialize: function () {
                return this.initStartIndex(), this.initWidget() === !1 ? !1 : (this.initEventListeners(), this.onslide(this.index), this.ontransitionend(), void(this.options.startSlideshow && this.play()))
            },
            slide: function (e, t) {
                window.clearTimeout(this.timeout);
                var n, i, o, a = this.index;
                if (a !== e && 1 !== this.num) {
                    if (t || (t = this.options.transitionSpeed), this.support.transform) {
                        for (this.options.continuous || (e = this.circle(e)), n = Math.abs(a - e) / (a - e), this.options.continuous && (i = n, n = -this.positions[this.circle(e)] / this.slideWidth, n !== i && (e = -n * this.num + e)), o = Math.abs(a - e) - 1; o;)o -= 1, this.move(this.circle((e > a ? e : a) - o - 1), this.slideWidth * n, 0);
                        e = this.circle(e), this.move(a, this.slideWidth * n, t), this.move(e, 0, t), this.options.continuous && this.move(this.circle(e - n), -(this.slideWidth * n), 0)
                    } else e = this.circle(e), this.animate(a * -this.slideWidth, e * -this.slideWidth, t);
                    this.onslide(e)
                }
            },
            getIndex: function () {
                return this.index
            },
            getNumber: function () {
                return this.num
            },
            prev: function () {
                (this.options.continuous || this.index) && this.slide(this.index - 1)
            },
            next: function () {
                (this.options.continuous || this.index < this.num - 1) && this.slide(this.index + 1)
            },
            play: function (e) {
                var t = this;
                window.clearTimeout(this.timeout), this.interval = e || this.options.slideshowInterval, this.elements[this.index] > 1 && (this.timeout = this.setTimeout(!this.requestAnimationFrame && this.slide || function (e, n) {
                        t.animationFrameId = t.requestAnimationFrame.call(window, function () {
                            t.slide(e, n)
                        })
                    }, [this.index + 1, this.options.slideshowTransitionSpeed], this.interval)), this.container.addClass(this.options.playingClass)
            },
            pause: function () {
                window.clearTimeout(this.timeout), this.interval = null, this.container.removeClass(this.options.playingClass)
            },
            add: function (e) {
                var t;
                for (e.concat || (e = Array.prototype.slice.call(e)), this.list.concat || (this.list = Array.prototype.slice.call(this.list)), this.list = this.list.concat(e), this.num = this.list.length, this.num > 2 && null === this.options.continuous && (this.options.continuous = !0, this.container.removeClass(this.options.leftEdgeClass)), this.container.removeClass(this.options.rightEdgeClass).removeClass(this.options.singleClass), t = this.num - e.length; t < this.num; t += 1)this.addSlide(t), this.positionSlide(t);
                this.positions.length = this.num, this.initSlides(!0)
            },
            resetSlides: function () {
                this.slidesContainer.empty(), this.unloadAllSlides(), this.slides = []
            },
            handleClose: function () {
                var e = this.options;
                this.destroyEventListeners(), this.pause(), this.container[0].style.display = "none", this.container.removeClass(e.displayClass).removeClass(e.singleClass).removeClass(e.leftEdgeClass).removeClass(e.rightEdgeClass), e.hidePageScrollbars && (document.body.style.overflow = this.bodyOverflowStyle), this.options.clearSlides && this.resetSlides(), this.options.onclosed && this.options.onclosed.call(this)
            },
            close: function () {
                var e = this, t = function n(t) {
                    t.target === e.container[0] && (e.container.off(e.support.transition.end, n), e.handleClose())
                };
                this.options.onclose && this.options.onclose.call(this), this.support.transition && this.options.displayTransition ? (this.container.on(this.support.transition.end, t), this.container.removeClass(this.options.displayClass)) : this.handleClose()
            },
            circle: function (e) {
                return (this.num + e % this.num) % this.num
            },
            move: function (e, t, n) {
                this.translateX(e, t, n), this.positions[e] = t
            },
            translate: function (e, t, n, i) {
                var o = this.slides[e].style, a = this.support.transition, r = this.support.transform;
                o[a.name + "Duration"] = i + "ms", o[r.name] = "translate(" + t + "px, " + n + "px)" + (r.translateZ ? " translateZ(0)" : "")
            },
            translateX: function (e, t, n) {
                this.translate(e, t, 0, n)
            },
            translateY: function (e, t, n) {
                this.translate(e, 0, t, n)
            },
            animate: function (e, t, n) {
                if (!n)return void(this.slidesContainer[0].style.left = t + "px");
                var i = this, o = (new Date).getTime(), a = window.setInterval(function () {
                    var r = (new Date).getTime() - o;
                    return r > n ? (i.slidesContainer[0].style.left = t + "px", i.ontransitionend(), void window.clearInterval(a)) : void(i.slidesContainer[0].style.left = (t - e) * (Math.floor(r / n * 100) / 100) + e + "px")
                }, 4)
            },
            preventDefault: function (e) {
                e.preventDefault ? e.preventDefault() : e.returnValue = !1
            },
            stopPropagation: function (e) {
                e.stopPropagation ? e.stopPropagation() : e.cancelBubble = !0
            },
            onresize: function () {
                this.initSlides(!0)
            },
            onmousedown: function (e) {
                e.which && 1 === e.which && "VIDEO" !== e.target.nodeName && (e.preventDefault(), (e.originalEvent || e).touches = [{
                    pageX: e.pageX,
                    pageY: e.pageY
                }], this.ontouchstart(e))
            },
            onmousemove: function (e) {
                this.touchStart && ((e.originalEvent || e).touches = [{
                    pageX: e.pageX,
                    pageY: e.pageY
                }], this.ontouchmove(e))
            },
            onmouseup: function (e) {
                this.touchStart && (this.ontouchend(e), delete this.touchStart)
            },
            onmouseout: function (t) {
                if (this.touchStart) {
                    var n = t.target, i = t.relatedTarget;
                    (!i || i !== n && !e.contains(n, i)) && this.onmouseup(t)
                }
            },
            ontouchstart: function (e) {
                this.options.stopTouchEventsPropagation && this.stopPropagation(e);
                var t = (e.originalEvent || e).touches[0];
                this.touchStart = {
                    x: t.pageX,
                    y: t.pageY,
                    time: Date.now()
                }, this.isScrolling = void 0, this.touchDelta = {}
            },
            ontouchmove: function (e) {
                this.options.stopTouchEventsPropagation && this.stopPropagation(e);
                var t, n, i = (e.originalEvent || e).touches[0], o = (e.originalEvent || e).scale, a = this.index;
                if (!(i.length > 1 || o && 1 !== o))if (this.options.disableScroll && e.preventDefault(), this.touchDelta = {
                        x: i.pageX - this.touchStart.x,
                        y: i.pageY - this.touchStart.y
                    }, t = this.touchDelta.x, void 0 === this.isScrolling && (this.isScrolling = this.isScrolling || Math.abs(t) < Math.abs(this.touchDelta.y)), this.isScrolling)this.options.closeOnSwipeUpOrDown && this.translateY(a, this.touchDelta.y + this.positions[a], 0); else for (e.preventDefault(), window.clearTimeout(this.timeout), this.options.continuous ? n = [this.circle(a + 1), a, this.circle(a - 1)] : (this.touchDelta.x = t /= !a && t > 0 || a === this.num - 1 && 0 > t ? Math.abs(t) / this.slideWidth + 1 : 1, n = [a], a && n.push(a - 1), a < this.num - 1 && n.unshift(a + 1)); n.length;)a = n.pop(), this.translateX(a, t + this.positions[a], 0)
            },
            ontouchend: function (e) {
                this.options.stopTouchEventsPropagation && this.stopPropagation(e);
                var t, n, i, o, a, r = this.index, s = this.options.transitionSpeed, l = this.slideWidth, d = Number(Date.now() - this.touchStart.time) < 250, p = d && Math.abs(this.touchDelta.x) > 20 || Math.abs(this.touchDelta.x) > l / 2, c = !r && this.touchDelta.x > 0 || r === this.num - 1 && this.touchDelta.x < 0, u = !p && this.options.closeOnSwipeUpOrDown && (d && Math.abs(this.touchDelta.y) > 20 || Math.abs(this.touchDelta.y) > this.slideHeight / 2);
                this.options.continuous && (c = !1), t = this.touchDelta.x < 0 ? -1 : 1, this.isScrolling ? u ? this.close() : this.translateY(r, 0, s) : p && !c ? (n = r + t, i = r - t, o = l * t, a = -l * t, this.options.continuous ? (this.move(this.circle(n), o, 0), this.move(this.circle(r - 2 * t), a, 0)) : n >= 0 && n < this.num && this.move(n, o, 0), this.move(r, this.positions[r] + o, s), this.move(this.circle(i), this.positions[this.circle(i)] + o, s), r = this.circle(i), this.onslide(r)) : this.options.continuous ? (this.move(this.circle(r - 1), -l, s), this.move(r, 0, s), this.move(this.circle(r + 1), l, s)) : (r && this.move(r - 1, -l, s), this.move(r, 0, s), r < this.num - 1 && this.move(r + 1, l, s))
            },
            ontouchcancel: function (e) {
                this.touchStart && (this.ontouchend(e), delete this.touchStart)
            },
            ontransitionend: function (e) {
                var t = this.slides[this.index];
                e && t !== e.target || (this.interval && this.play(), this.setTimeout(this.options.onslideend, [this.index, t]))
            },
            oncomplete: function (t) {
                var n, i = t.target || t.srcElement, o = i && i.parentNode;
                i && o && (n = this.getNodeIndex(o), e(o).removeClass(this.options.slideLoadingClass), "error" === t.type ? (e(o).addClass(this.options.slideErrorClass), this.elements[n] = 3) : this.elements[n] = 2, i.clientHeight > this.container[0].clientHeight && (i.style.maxHeight = this.container[0].clientHeight), this.interval && this.slides[this.index] === o && this.play(), this.setTimeout(this.options.onslidecomplete, [n, o]))
            },
            onload: function (e) {
                this.oncomplete(e)
            },
            onerror: function (e) {
                this.oncomplete(e)
            },
            onkeydown: function (e) {
                switch (e.which || e.keyCode) {
                    case 13:
                        this.options.toggleControlsOnReturn && (this.preventDefault(e), this.toggleControls());
                        break;
                    case 27:
                        this.options.closeOnEscape && (this.close(), e.stopImmediatePropagation());
                        break;
                    case 32:
                        this.options.toggleSlideshowOnSpace && (this.preventDefault(e), this.toggleSlideshow());
                        break;
                    case 37:
                        this.options.enableKeyboardNavigation && (this.preventDefault(e), this.prev());
                        break;
                    case 39:
                        this.options.enableKeyboardNavigation && (this.preventDefault(e), this.next())
                }
            },
            handleClick: function (t) {
                var n = this.options, i = t.target || t.srcElement, o = i.parentNode, a = function (t) {
                    return e(i).hasClass(t) || e(o).hasClass(t)
                };
                a(n.toggleClass) ? (this.preventDefault(t), this.toggleControls()) : a(n.prevClass) ? (this.preventDefault(t), this.prev()) : a(n.nextClass) ? (this.preventDefault(t), this.next()) : a(n.closeClass) ? (this.preventDefault(t), this.close()) : a(n.playPauseClass) ? (this.preventDefault(t), this.toggleSlideshow()) : o === this.slidesContainer[0] ? (this.preventDefault(t), n.closeOnSlideClick ? this.close() : this.toggleControls()) : o.parentNode && o.parentNode === this.slidesContainer[0] && (this.preventDefault(t), this.toggleControls())
            },
            onclick: function (e) {
                return this.options.emulateTouchEvents && this.touchDelta && (Math.abs(this.touchDelta.x) > 20 || Math.abs(this.touchDelta.y) > 20) ? void delete this.touchDelta : this.handleClick(e)
            },
            updateEdgeClasses: function (e) {
                e ? this.container.removeClass(this.options.leftEdgeClass) : this.container.addClass(this.options.leftEdgeClass), e === this.num - 1 ? this.container.addClass(this.options.rightEdgeClass) : this.container.removeClass(this.options.rightEdgeClass)
            },
            handleSlide: function (e) {
                this.options.continuous || this.updateEdgeClasses(e), this.loadElements(e), this.options.unloadElements && this.unloadElements(e), this.setTitle(e)
            },
            onslide: function (e) {
                this.index = e, this.handleSlide(e), this.setTimeout(this.options.onslide, [e, this.slides[e]])
            },
            setTitle: function (e) {
                var t = this.slides[e].firstChild.title, n = this.titleElement;
                n.length && (this.titleElement.empty(), t && n[0].appendChild(document.createTextNode(t)))
            },
            setTimeout: function (e, t, n) {
                var i = this;
                return e && window.setTimeout(function () {
                        e.apply(i, t || [])
                    }, n || 0)
            },
            imageFactory: function (t, n) {
                var i, o, a, r = this, s = this.imagePrototype.cloneNode(!1), l = t, d = this.options.stretchImages, p = function c(t) {
                    if (!i) {
                        if (t = {type: t.type, target: o}, !o.parentNode)return r.setTimeout(c, [t]);
                        i = !0, e(s).off("load error", c), d && "load" === t.type && (o.style.background = 'url("' + l + '") center no-repeat', o.style.backgroundSize = d), n(t)
                    }
                };
                return "string" != typeof l && (l = this.getItemProperty(t, this.options.urlProperty), a = this.getItemProperty(t, this.options.titleProperty)), d === !0 && (d = "contain"), d = this.support.backgroundSize && this.support.backgroundSize[d] && d, d ? o = this.elementPrototype.cloneNode(!1) : (o = s, s.draggable = !1), a && (o.title = a), e(s).on("load error", p), s.src = l, o
            },
            createElement: function (t, n) {
                var i = t && this.getItemProperty(t, this.options.typeProperty), o = i && this[i.split("/")[0] + "Factory"] || this.imageFactory, a = t && o.call(this, t, n);
                return a || (a = this.elementPrototype.cloneNode(!1), this.setTimeout(n, [{
                    type: "error",
                    target: a
                }])), e(a).addClass(this.options.slideContentClass), a
            },
            loadElement: function (t) {
                this.elements[t] || (this.slides[t].firstChild ? this.elements[t] = e(this.slides[t]).hasClass(this.options.slideErrorClass) ? 3 : 2 : (this.elements[t] = 1, e(this.slides[t]).addClass(this.options.slideLoadingClass), this.slides[t].appendChild(this.createElement(this.list[t], this.proxyListener))))
            },
            loadElements: function (e) {
                var t, n = Math.min(this.num, 2 * this.options.preloadRange + 1), i = e;
                for (t = 0; n > t; t += 1)i += t * (t % 2 === 0 ? -1 : 1), i = this.circle(i), this.loadElement(i)
            },
            unloadElements: function (e) {
                var t, n;
                for (t in this.elements)this.elements.hasOwnProperty(t) && (n = Math.abs(e - t), n > this.options.preloadRange && n + this.options.preloadRange < this.num && (this.unloadSlide(t), delete this.elements[t]))
            },
            addSlide: function (e) {
                var t = this.slidePrototype.cloneNode(!1);
                t.setAttribute("data-index", e), this.slidesContainer[0].appendChild(t), this.slides.push(t)
            },
            positionSlide: function (e) {
                var t = this.slides[e];
                t.style.width = this.slideWidth + "px", this.support.transform && (t.style.left = e * -this.slideWidth + "px", this.move(e, this.index > e ? -this.slideWidth : this.index < e ? this.slideWidth : 0, 0))
            },
            initSlides: function (t) {
                var n, i;
                for (t || (this.positions = [], this.positions.length = this.num, this.elements = {}, this.imagePrototype = document.createElement("img"), this.elementPrototype = document.createElement("div"), this.slidePrototype = document.createElement("div"), e(this.slidePrototype).addClass(this.options.slideClass), this.slides = this.slidesContainer[0].children, n = this.options.clearSlides || this.slides.length !== this.num), this.slideWidth = this.container[0].offsetWidth, this.slideHeight = this.container[0].offsetHeight, this.slidesContainer[0].style.width = this.num * this.slideWidth + "px", n && this.resetSlides(), i = 0; i < this.num; i += 1)n && this.addSlide(i), this.positionSlide(i);
                this.options.continuous && this.support.transform && (this.move(this.circle(this.index - 1), -this.slideWidth, 0), this.move(this.circle(this.index + 1), this.slideWidth, 0)), this.support.transform || (this.slidesContainer[0].style.left = this.index * -this.slideWidth + "px")
            },
            unloadSlide: function (e) {
                var t, n;
                t = this.slides[e], n = t.firstChild, null !== n && t.removeChild(n)
            },
            unloadAllSlides: function () {
                var e, t;
                for (e = 0, t = this.slides.length; t > e; e++)this.unloadSlide(e)
            },
            toggleControls: function () {
                var e = this.options.controlsClass;
                this.container.hasClass(e) ? this.container.removeClass(e) : this.container.addClass(e)
            },
            toggleSlideshow: function () {
                this.interval ? this.pause() : this.play()
            },
            getNodeIndex: function (e) {
                return parseInt(e.getAttribute("data-index"), 10)
            },
            getNestedProperty: function (e, t) {
                return t.replace(/\[(?:'([^']+)'|"([^"]+)"|(\d+))\]|(?:(?:^|\.)([^\.\[]+))/g, function (t, n, i, o, a) {
                    var r = a || n || i || o && parseInt(o, 10);
                    t && e && (e = e[r])
                }), e
            },
            getDataProperty: function (t, n) {
                if (t.getAttribute) {
                    var i = t.getAttribute("data-" + n.replace(/([A-Z])/g, "-$1").toLowerCase());
                    if ("string" == typeof i) {
                        if (/^(true|false|null|-?\d+(\.\d+)?|\{[\s\S]*\}|\[[\s\S]*\])$/.test(i))try {
                            return e.parseJSON(i)
                        } catch (o) {
                        }
                        return i
                    }
                }
            },
            getItemProperty: function (e, t) {
                var n = e[t];
                return void 0 === n && (n = this.getDataProperty(e, t), void 0 === n && (n = this.getNestedProperty(e, t))), n
            },
            initStartIndex: function () {
                var e, t = this.options.index, n = this.options.urlProperty;
                if (t && "number" != typeof t)for (e = 0; e < this.num; e += 1)if (this.list[e] === t || this.getItemProperty(this.list[e], n) === this.getItemProperty(t, n)) {
                    t = e;
                    break
                }
                this.index = this.circle(parseInt(t, 10) || 0)
            },
            initEventListeners: function () {
                var t = this, n = this.slidesContainer, i = function (e) {
                    var n = t.support.transition && t.support.transition.end === e.type ? "transitionend" : e.type;
                    t["on" + n](e)
                };
                e(window).on("resize", i), e(document.body).on("keydown", i), this.container.on("click", i), this.support.touch ? n.on("touchstart touchmove touchend touchcancel", i) : this.options.emulateTouchEvents && this.support.transition && n.on("mousedown mousemove mouseup mouseout", i), this.support.transition && n.on(this.support.transition.end, i), this.proxyListener = i
            },
            destroyEventListeners: function () {
                var t = this.slidesContainer, n = this.proxyListener;
                e(window).off("resize", n), e(document.body).off("keydown", n), this.container.off("click", n), this.support.touch ? t.off("touchstart touchmove touchend touchcancel", n) : this.options.emulateTouchEvents && this.support.transition && t.off("mousedown mousemove mouseup mouseout", n), this.support.transition && t.off(this.support.transition.end, n)
            },
            handleOpen: function () {
                this.options.onopened && this.options.onopened.call(this)
            },
            initWidget: function () {
                var t = this, n = function i(e) {
                    e.target === t.container[0] && (t.container.off(t.support.transition.end, i), t.handleOpen())
                };
                return this.container = e(this.options.container), this.container.length ? (this.slidesContainer = this.container.find(this.options.slidesContainer).first(), this.slidesContainer.length ? (this.titleElement = this.container.find(this.options.titleElement).first(), 1 === this.num && this.container.addClass(this.options.singleClass), this.options.onopen && this.options.onopen.call(this), this.support.transition && this.options.displayTransition ? this.container.on(this.support.transition.end, n) : this.handleOpen(), this.options.hidePageScrollbars && (this.bodyOverflowStyle = document.body.style.overflow, document.body.style.overflow = "hidden"), this.container[0].style.display = "block", this.initSlides(), void this.container.addClass(this.options.displayClass)) : (this.console.log("blueimp Gallery: Slides container not found.", this.options.slidesContainer), !1)) : (this.console.log("blueimp Gallery: Widget container not found.", this.options.container), !1)
            },
            initOptions: function (t) {
                this.options = e.extend({}, this.options), (t && t.carousel || this.options.carousel && (!t || t.carousel !== !1)) && e.extend(this.options, this.carouselOptions), e.extend(this.options, t), this.num < 3 && (this.options.continuous = this.options.continuous ? null : !1), this.support.transition || (this.options.emulateTouchEvents = !1), this.options.event && this.preventDefault(this.options.event)
            }
        }), t
    }), function (e) {
        "function" == typeof define && define.amd ? define(["./blueimp-helper", "./blueimp-gallery"], e) : e(window.blueimp.helper || window.jQuery, window.blueimp.Gallery)
    }(function (e, t) {
        e.extend(t.prototype.options, {fullScreen: !1});
        var n = t.prototype.initialize, i = t.prototype.close;
        return e.extend(t.prototype, {
            getFullScreenElement: function () {
                return document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement
            }, requestFullScreen: function (e) {
                e.requestFullscreen ? e.requestFullscreen() : e.webkitRequestFullscreen ? e.webkitRequestFullscreen() : e.mozRequestFullScreen ? e.mozRequestFullScreen() : e.msRequestFullscreen && e.msRequestFullscreen()
            }, exitFullScreen: function () {
                document.exitFullscreen ? document.exitFullscreen() : document.webkitCancelFullScreen ? document.webkitCancelFullScreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.msExitFullscreen && document.msExitFullscreen()
            }, initialize: function () {
                n.call(this), this.options.fullScreen && !this.getFullScreenElement() && this.requestFullScreen(this.container[0])
            }, close: function () {
                this.getFullScreenElement() === this.container[0] && this.exitFullScreen(), i.call(this)
            }
        }), t
    }), function (e) {
        "function" == typeof define && define.amd ? define(["./blueimp-helper", "./blueimp-gallery"], e) : e(window.blueimp.helper || window.jQuery, window.blueimp.Gallery)
    }(function (e, t) {
        e.extend(t.prototype.options, {
            indicatorContainer: "ol",
            activeIndicatorClass: "active",
            thumbnailProperty: "thumbnail",
            thumbnailIndicators: !0
        });
        var n = t.prototype.initSlides, i = t.prototype.addSlide, o = t.prototype.resetSlides, a = t.prototype.handleClick, r = t.prototype.handleSlide, s = t.prototype.handleClose;
        return e.extend(t.prototype, {
            createIndicator: function (t) {
                var n, i, o = this.indicatorPrototype.cloneNode(!1), a = this.getItemProperty(t, this.options.titleProperty), r = this.options.thumbnailProperty;
                return this.options.thumbnailIndicators && (r && (n = this.getItemProperty(t, r)), void 0 === n && (i = t.getElementsByTagName && e(t).find("img")[0], i && (n = i.src)), n && (o.style.backgroundImage = 'url("' + n + '")')), a && (o.title = a), o
            }, addIndicator: function (e) {
                if (this.indicatorContainer.length) {
                    var t = this.createIndicator(this.list[e]);
                    t.setAttribute("data-index", e), this.indicatorContainer[0].appendChild(t), this.indicators.push(t)
                }
            }, setActiveIndicator: function (t) {
                this.indicators && (this.activeIndicator && this.activeIndicator.removeClass(this.options.activeIndicatorClass), this.activeIndicator = e(this.indicators[t]), this.activeIndicator.addClass(this.options.activeIndicatorClass))
            }, initSlides: function (e) {
                e || (this.indicatorContainer = this.container.find(this.options.indicatorContainer), this.indicatorContainer.length && (this.indicatorPrototype = document.createElement("li"), this.indicators = this.indicatorContainer[0].children)), n.call(this, e)
            }, addSlide: function (e) {
                i.call(this, e), this.addIndicator(e)
            }, resetSlides: function () {
                o.call(this), this.indicatorContainer.empty(), this.indicators = []
            }, handleClick: function (e) {
                var t = e.target || e.srcElement, n = t.parentNode;
                if (n === this.indicatorContainer[0])this.preventDefault(e), this.slide(this.getNodeIndex(t)); else {
                    if (n.parentNode !== this.indicatorContainer[0])return a.call(this, e);
                    this.preventDefault(e), this.slide(this.getNodeIndex(n))
                }
            }, handleSlide: function (e) {
                r.call(this, e), this.setActiveIndicator(e)
            }, handleClose: function () {
                this.activeIndicator && this.activeIndicator.removeClass(this.options.activeIndicatorClass), s.call(this)
            }
        }), t
    }), function (e) {
        "function" == typeof define && define.amd ? define(["./blueimp-helper", "./blueimp-gallery"], e) : e(window.blueimp.helper || window.jQuery, window.blueimp.Gallery)
    }(function (e, t) {
        e.extend(t.prototype.options, {
            videoContentClass: "video-content",
            videoLoadingClass: "video-loading",
            videoPlayingClass: "video-playing",
            videoPosterProperty: "poster",
            videoSourcesProperty: "sources"
        });
        var n = t.prototype.handleSlide;
        return e.extend(t.prototype, {
            handleSlide: function (e) {
                n.call(this, e), this.playingVideo && this.playingVideo.pause()
            }, videoFactory: function (t, n, i) {
                var o, a, r, s, l, d = this, p = this.options, c = this.elementPrototype.cloneNode(!1), u = e(c), h = [{
                    type: "error",
                    target: c
                }], f = i || document.createElement("video"), m = this.getItemProperty(t, p.urlProperty), g = this.getItemProperty(t, p.typeProperty), v = this.getItemProperty(t, p.titleProperty), y = this.getItemProperty(t, p.videoPosterProperty), w = this.getItemProperty(t, p.videoSourcesProperty);
                if (u.addClass(p.videoContentClass), v && (c.title = v), f.canPlayType)if (m && g && f.canPlayType(g))f.src = m; else for (; w && w.length;)if (a = w.shift(), m = this.getItemProperty(a, p.urlProperty), g = this.getItemProperty(a, p.typeProperty), m && g && f.canPlayType(g)) {
                    f.src = m;
                    break
                }
                return y && (f.poster = y, o = this.imagePrototype.cloneNode(!1), e(o).addClass(p.toggleClass), o.src = y, o.draggable = !1, c.appendChild(o)), r = document.createElement("a"), r.setAttribute("target", "_blank"), i || r.setAttribute("download", v), r.href = m, f.src && (f.controls = !0, (i || e(f)).on("error", function () {
                    d.setTimeout(n, h)
                }).on("pause", function () {
                    s = !1, u.removeClass(d.options.videoLoadingClass).removeClass(d.options.videoPlayingClass), l && d.container.addClass(d.options.controlsClass), delete d.playingVideo, d.interval && d.play()
                }).on("playing", function () {
                    s = !1, u.removeClass(d.options.videoLoadingClass).addClass(d.options.videoPlayingClass), d.container.hasClass(d.options.controlsClass) ? (l = !0, d.container.removeClass(d.options.controlsClass)) : l = !1
                }).on("play", function () {
                    window.clearTimeout(d.timeout), s = !0, u.addClass(d.options.videoLoadingClass), d.playingVideo = f
                }), e(r).on("click", function (e) {
                    d.preventDefault(e), s ? f.pause() : f.play()
                }), c.appendChild(i && i.element || f)), c.appendChild(r), this.setTimeout(n, [{
                    type: "load",
                    target: c
                }]), c
            }
        }), t
    }), function (e) {
        "function" == typeof define && define.amd ? define(["./blueimp-helper", "./blueimp-gallery-video"], e) : e(window.blueimp.helper || window.jQuery, window.blueimp.Gallery)
    }(function (e, t) {
        if (!window.postMessage)return t;
        e.extend(t.prototype.options, {
            vimeoVideoIdProperty: "vimeo",
            vimeoPlayerUrl: "//player.vimeo.com/video/VIDEO_ID?api=1&player_id=PLAYER_ID",
            vimeoPlayerIdPrefix: "vimeo-player-",
            vimeoClickToPlay: !0
        });
        var n = t.prototype.textFactory || t.prototype.imageFactory, i = function (e, t, n, i) {
            this.url = e, this.videoId = t, this.playerId = n, this.clickToPlay = i, this.element = document.createElement("div"), this.listeners = {}
        }, o = 0;
        return e.extend(i.prototype, {
            canPlayType: function () {
                return !0
            }, on: function (e, t) {
                return this.listeners[e] = t, this
            }, loadAPI: function () {
                for (var t, n, i = this, o = "//" + ("https" === location.protocol ? "secure-" : "") + "a.vimeocdn.com/js/froogaloop2.min.js", a = document.getElementsByTagName("script"), r = a.length, s = function () {
                    !n && i.playOnReady && i.play(), n = !0
                }; r;)if (r -= 1, a[r].src === o) {
                    t = a[r];
                    break
                }
                t || (t = document.createElement("script"), t.src = o), e(t).on("load", s), a[0].parentNode.insertBefore(t, a[0]), /loaded|complete/.test(t.readyState) && s()
            }, onReady: function () {
                var e = this;
                this.ready = !0, this.player.addEvent("play", function () {
                    e.hasPlayed = !0, e.onPlaying()
                }), this.player.addEvent("pause", function () {
                    e.onPause()
                }), this.player.addEvent("finish", function () {
                    e.onPause()
                }), this.playOnReady && this.play()
            }, onPlaying: function () {
                this.playStatus < 2 && (this.listeners.playing(), this.playStatus = 2)
            }, onPause: function () {
                this.listeners.pause(), delete this.playStatus
            }, insertIframe: function () {
                var e = document.createElement("iframe");
                e.src = this.url.replace("VIDEO_ID", this.videoId).replace("PLAYER_ID", this.playerId), e.id = this.playerId, this.element.parentNode.replaceChild(e, this.element), this.element = e
            }, play: function () {
                var e = this;
                this.playStatus || (this.listeners.play(), this.playStatus = 1), this.ready ? !this.hasPlayed && (this.clickToPlay || window.navigator && /iP(hone|od|ad)/.test(window.navigator.platform)) ? this.onPlaying() : this.player.api("play") : (this.playOnReady = !0, window.$f ? this.player || (this.insertIframe(), this.player = $f(this.element), this.player.addEvent("ready", function () {
                    e.onReady()
                })) : this.loadAPI())
            }, pause: function () {
                this.ready ? this.player.api("pause") : this.playStatus && (delete this.playOnReady, this.listeners.pause(), delete this.playStatus)
            }
        }), e.extend(t.prototype, {
            VimeoPlayer: i, textFactory: function (e, t) {
                var a = this.options, r = this.getItemProperty(e, a.vimeoVideoIdProperty);
                return r ? (void 0 === this.getItemProperty(e, a.urlProperty) && (e[a.urlProperty] = "//vimeo.com/" + r), o += 1, this.videoFactory(e, t, new i(a.vimeoPlayerUrl, r, a.vimeoPlayerIdPrefix + o, a.vimeoClickToPlay))) : n.call(this, e, t)
            }
        }), t
    }), function (e) {
        "function" == typeof define && define.amd ? define(["./blueimp-helper", "./blueimp-gallery-video"], e) : e(window.blueimp.helper || window.jQuery, window.blueimp.Gallery)
    }(function (e, t) {
        if (!window.postMessage)return t;
        e.extend(t.prototype.options, {
            youTubeVideoIdProperty: "youtube",
            youTubePlayerVars: {wmode: "transparent"},
            youTubeClickToPlay: !0
        });
        var n = t.prototype.textFactory || t.prototype.imageFactory, i = function (e, t, n) {
            this.videoId = e, this.playerVars = t, this.clickToPlay = n, this.element = document.createElement("div"), this.listeners = {}
        };
        return e.extend(i.prototype, {
            canPlayType: function () {
                return !0
            }, on: function (e, t) {
                return this.listeners[e] = t, this
            }, loadAPI: function () {
                var e, t = this, n = window.onYouTubeIframeAPIReady, i = "//www.youtube.com/iframe_api", o = document.getElementsByTagName("script"), a = o.length;
                for (window.onYouTubeIframeAPIReady = function () {
                    n && n.apply(this), t.playOnReady && t.play()
                }; a;)if (a -= 1, o[a].src === i)return;
                e = document.createElement("script"), e.src = i, o[0].parentNode.insertBefore(e, o[0])
            }, onReady: function () {
                this.ready = !0, this.playOnReady && this.play()
            }, onPlaying: function () {
                this.playStatus < 2 && (this.listeners.playing(), this.playStatus = 2)
            }, onPause: function () {
                t.prototype.setTimeout.call(this, this.checkSeek, null, 2e3)
            }, checkSeek: function () {
                (this.stateChange === YT.PlayerState.PAUSED || this.stateChange === YT.PlayerState.ENDED) && (this.listeners.pause(), delete this.playStatus)
            }, onStateChange: function (e) {
                switch (e.data) {
                    case YT.PlayerState.PLAYING:
                        this.hasPlayed = !0, this.onPlaying();
                        break;
                    case YT.PlayerState.PAUSED:
                    case YT.PlayerState.ENDED:
                        this.onPause()
                }
                this.stateChange = e.data
            }, onError: function (e) {
                this.listeners.error(e)
            }, play: function () {
                var e = this;
                this.playStatus || (this.listeners.play(), this.playStatus = 1), this.ready ? !this.hasPlayed && (this.clickToPlay || window.navigator && /iP(hone|od|ad)/.test(window.navigator.platform)) ? this.onPlaying() : this.player.playVideo() : (this.playOnReady = !0, window.YT && YT.Player ? this.player || (this.player = new YT.Player(this.element, {
                    videoId: this.videoId,
                    playerVars: this.playerVars,
                    events: {
                        onReady: function () {
                            e.onReady()
                        }, onStateChange: function (t) {
                            e.onStateChange(t)
                        }, onError: function (t) {
                            e.onError(t)
                        }
                    }
                })) : this.loadAPI())
            }, pause: function () {
                this.ready ? this.player.pauseVideo() : this.playStatus && (delete this.playOnReady, this.listeners.pause(), delete this.playStatus)
            }
        }), e.extend(t.prototype, {
            YouTubePlayer: i, textFactory: function (e, t) {
                var o = this.options, a = this.getItemProperty(e, o.youTubeVideoIdProperty);
                return a ? (void 0 === this.getItemProperty(e, o.urlProperty) && (e[o.urlProperty] = "//www.youtube.com/watch?v=" + a), void 0 === this.getItemProperty(e, o.videoPosterProperty) && (e[o.videoPosterProperty] = "//img.youtube.com/vi/" + a + "/maxresdefault.jpg"), this.videoFactory(e, t, new i(a, o.youTubePlayerVars, o.youTubeClickToPlay))) : n.call(this, e, t)
            }
        }), t
    }), function (e) {
        "function" == typeof define && define.amd ? define(["jquery", "./blueimp-gallery"], e) : e(window.jQuery, window.blueimp.Gallery)
    }(function (e, t) {
        e(document).on("click", "[data-gallery]", function (n) {
            var i = e(this).data("gallery"), o = e(i), a = o.length && o || e(t.prototype.options.container), r = {
                onopen: function () {
                    a.data("gallery", this).trigger("open")
                }, onopened: function () {
                    a.trigger("opened")
                }, onslide: function () {
                    a.trigger("slide", arguments)
                }, onslideend: function () {
                    a.trigger("slideend", arguments)
                }, onslidecomplete: function () {
                    a.trigger("slidecomplete", arguments)
                }, onclose: function () {
                    a.trigger("close");
                }, onclosed: function () {
                    a.trigger("closed").removeData("gallery")
                }
            }, s = e.extend(a.data(), {container: a[0], index: this, event: n}, r), l = e('[data-gallery="' + i + '"]');
            return s.filter && (l = l.filter(s.filter)), new t(l, s)
        })
    }), !function (e) {
        "function" == typeof define && define.amd ? define(["jquery", "./blueimp-gallery"], e) : e(window.jQuery, window.blueimp.Gallery)
    }(function (e, t) {
        e.extend(t.prototype.options, {useBootstrapModal: !0});
        var n = t.prototype.close, i = t.prototype.imageFactory, o = t.prototype.videoFactory, a = t.prototype.textFactory;
        e.extend(t.prototype, {
            modalFactory: function (e, t, n, i) {
                if (!this.options.useBootstrapModal || n)return i.call(this, e, t, n);
                var o = this, a = this.container.children(".modal"), r = a.clone().show().on("click", function (e) {
                    (e.target === r[0] || e.target === r.children()[0]) && (e.preventDefault(), e.stopPropagation(), o.close())
                }), s = i.call(this, e, function (e) {
                    t({type: e.type, target: r[0]}), r.addClass("in")
                }, n);
                return r.find(".modal-title").text(s.title || String.fromCharCode(160)), r.find(".modal-body").append(s), r[0]
            }, imageFactory: function (e, t, n) {
                return this.modalFactory(e, t, n, i)
            }, videoFactory: function (e, t, n) {
                return this.modalFactory(e, t, n, o)
            }, textFactory: function (e, t, n) {
                return this.modalFactory(e, t, n, a)
            }, close: function () {
                this.container.find(".modal").removeClass("in"), n.call(this)
            }
        })
    }), $(document).ready(function () {
        new Swiper(".swiper-container", {
            pagination: ".swiper-pagination",
            paginationClickable: ".swiper-pagination",
            nextButton: ".swiper-button-next",
            prevButtin: ".swiper-button-prev"
        });
        $(".imgLiquidFill").imgLiquid(), $(".fancybox").fancybox({
            maxWidth: 800,
            maxHeight: 600,
            fitToView: !1,
            width: "70%",
            height: "70%",
            autoSize: !0,
            overlay: {css: {background: "#000"}}
        }), $(".header-auth-item.mod-login").on("click", function (e) {
            e.preventDefault(), $("#modalLogIn").fadeIn(), $("body").addClass("no-scroll")
        }), $("a.link-cond").on("click", function (e) {
            e.preventDefault(), $("#modalCondit").fadeIn(), $("body").addClass("no-scroll")
        }), $("a.link-reserv").on("click", function (e) {
            e.preventDefault(), $("#modalReserv").fadeIn(), $("body").addClass("no-scroll")
        }), $("button.mod-more").on("click", function (e) {
            e.preventDefault(), $("#modalOrder").fadeIn(), $("body").addClass("no-scroll")
        }),$("button.mod-more-franch").on("click", function (e) {
            e.preventDefault(), $("#modalOrderFranch").fadeIn(), $("body").addClass("no-scroll")
        }),$("button.mod-reserv").on("click", function (e) {
            e.preventDefault(), $("#modalReserv").fadeIn(), $("body").addClass("no-scroll")
        }), $("a.mod-more").on("click", function (e) {
            e.preventDefault(), $("#modalOrder").fadeIn(), $("body").addClass("no-scroll")
        }),  $(".modal-close, .modal-overlay").on("click", function (e) {
            e.preventDefault(), $(".modal").fadeOut(), $("body").removeClass("no-scroll")
        });

    }), "undefined" == typeof jQuery)throw new Error("Bootstrap's JavaScript requires jQuery");
+function (e) {
    var t = e.fn.jquery.split(" ")[0].split(".");
    if (t[0] < 2 && t[1] < 9 || 1 == t[0] && 9 == t[1] && t[2] < 1)throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher")
}(jQuery), +function (e) {
    function t() {
        var e = document.createElement("bootstrap"), t = {
            WebkitTransition: "webkitTransitionEnd",
            MozTransition: "transitionend",
            OTransition: "oTransitionEnd otransitionend",
            transition: "transitionend"
        };
        for (var n in t)if (void 0 !== e.style[n])return {end: t[n]};
        return !1
    }

    e.fn.emulateTransitionEnd = function (t) {
        var n = !1, i = this;
        e(this).one("bsTransitionEnd", function () {
            n = !0
        });
        var o = function () {
            n || e(i).trigger(e.support.transition.end)
        };
        return setTimeout(o, t), this
    }, e(function () {
        e.support.transition = t(), e.support.transition && (e.event.special.bsTransitionEnd = {
            bindType: e.support.transition.end,
            delegateType: e.support.transition.end,
            handle: function (t) {
                return e(t.target).is(this) ? t.handleObj.handler.apply(this, arguments) : void 0
            }
        })
    })
}(jQuery), +function (e) {
    function t(t) {
        return this.each(function () {
            var n = e(this), o = n.data("bs.alert");
            o || n.data("bs.alert", o = new i(this)), "string" == typeof t && o[t].call(n)
        })
    }

    var n = '[data-dismiss="alert"]', i = function (t) {
        e(t).on("click", n, this.close)
    };
    i.VERSION = "3.3.5", i.TRANSITION_DURATION = 150, i.prototype.close = function (t) {
        function n() {
            r.detach().trigger("closed.bs.alert").remove()
        }

        var o = e(this), a = o.attr("data-target");
        a || (a = o.attr("href"), a = a && a.replace(/.*(?=#[^\s]*$)/, ""));
        var r = e(a);
        t && t.preventDefault(), r.length || (r = o.closest(".alert")), r.trigger(t = e.Event("close.bs.alert")), t.isDefaultPrevented() || (r.removeClass("in"), e.support.transition && r.hasClass("fade") ? r.one("bsTransitionEnd", n).emulateTransitionEnd(i.TRANSITION_DURATION) : n())
    };
    var o = e.fn.alert;
    e.fn.alert = t, e.fn.alert.Constructor = i, e.fn.alert.noConflict = function () {
        return e.fn.alert = o, this
    }, e(document).on("click.bs.alert.data-api", n, i.prototype.close)
}(jQuery), +function (e) {
    function t(t) {
        return this.each(function () {
            var i = e(this), o = i.data("bs.button"), a = "object" == typeof t && t;
            o || i.data("bs.button", o = new n(this, a)), "toggle" == t ? o.toggle() : t && o.setState(t)
        })
    }

    var n = function o(t, n) {
        this.$element = e(t), this.options = e.extend({}, o.DEFAULTS, n), this.isLoading = !1
    };
    n.VERSION = "3.3.5", n.DEFAULTS = {loadingText: "loading..."}, n.prototype.setState = function (t) {
        var n = "disabled", i = this.$element, o = i.is("input") ? "val" : "html", a = i.data();
        t += "Text", null == a.resetText && i.data("resetText", i[o]()), setTimeout(e.proxy(function () {
            i[o](null == a[t] ? this.options[t] : a[t]), "loadingText" == t ? (this.isLoading = !0, i.addClass(n).attr(n, n)) : this.isLoading && (this.isLoading = !1, i.removeClass(n).removeAttr(n))
        }, this), 0)
    }, n.prototype.toggle = function () {
        var e = !0, t = this.$element.closest('[data-toggle="buttons"]');
        if (t.length) {
            var n = this.$element.find("input");
            "radio" == n.prop("type") ? (n.prop("checked") && (e = !1), t.find(".active").removeClass("active"), this.$element.addClass("active")) : "checkbox" == n.prop("type") && (n.prop("checked") !== this.$element.hasClass("active") && (e = !1), this.$element.toggleClass("active")), n.prop("checked", this.$element.hasClass("active")), e && n.trigger("change")
        } else this.$element.attr("aria-pressed", !this.$element.hasClass("active")), this.$element.toggleClass("active")
    };
    var i = e.fn.button;
    e.fn.button = t, e.fn.button.Constructor = n, e.fn.button.noConflict = function () {
        return e.fn.button = i, this
    }, e(document).on("click.bs.button.data-api", '[data-toggle^="button"]', function (n) {
        var i = e(n.target);
        i.hasClass("btn") || (i = i.closest(".btn")), t.call(i, "toggle"), e(n.target).is('input[type="radio"]') || e(n.target).is('input[type="checkbox"]') || n.preventDefault()
    }).on("focus.bs.button.data-api blur.bs.button.data-api", '[data-toggle^="button"]', function (t) {
        e(t.target).closest(".btn").toggleClass("focus", /^focus(in)?$/.test(t.type))
    })
}(jQuery), +function (e) {
    function t(t) {
        return this.each(function () {
            var i = e(this), o = i.data("bs.carousel"), a = e.extend({}, n.DEFAULTS, i.data(), "object" == typeof t && t), r = "string" == typeof t ? t : a.slide;
            o || i.data("bs.carousel", o = new n(this, a)), "number" == typeof t ? o.to(t) : r ? o[r]() : a.interval && o.pause().cycle()
        })
    }

    var n = function (t, n) {
        this.$element = e(t), this.$indicators = this.$element.find(".carousel-indicators"), this.options = n, this.paused = null, this.sliding = null, this.interval = null, this.$active = null, this.$items = null, this.options.keyboard && this.$element.on("keydown.bs.carousel", e.proxy(this.keydown, this)), "hover" == this.options.pause && !("ontouchstart" in document.documentElement) && this.$element.on("mouseenter.bs.carousel", e.proxy(this.pause, this)).on("mouseleave.bs.carousel", e.proxy(this.cycle, this))
    };
    n.VERSION = "3.3.5", n.TRANSITION_DURATION = 600, n.DEFAULTS = {
        interval: 5e3,
        pause: "hover",
        wrap: !0,
        keyboard: !0
    }, n.prototype.keydown = function (e) {
        if (!/input|textarea/i.test(e.target.tagName)) {
            switch (e.which) {
                case 37:
                    this.prev();
                    break;
                case 39:
                    this.next();
                    break;
                default:
                    return
            }
            e.preventDefault()
        }
    }, n.prototype.cycle = function (t) {
        return t || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval(e.proxy(this.next, this), this.options.interval)), this
    }, n.prototype.getItemIndex = function (e) {
        return this.$items = e.parent().children(".item"), this.$items.index(e || this.$active)
    }, n.prototype.getItemForDirection = function (e, t) {
        var n = this.getItemIndex(t), i = "prev" == e && 0 === n || "next" == e && n == this.$items.length - 1;
        if (i && !this.options.wrap)return t;
        var o = "prev" == e ? -1 : 1, a = (n + o) % this.$items.length;
        return this.$items.eq(a)
    }, n.prototype.to = function (e) {
        var t = this, n = this.getItemIndex(this.$active = this.$element.find(".item.active"));
        return e > this.$items.length - 1 || 0 > e ? void 0 : this.sliding ? this.$element.one("slid.bs.carousel", function () {
            t.to(e)
        }) : n == e ? this.pause().cycle() : this.slide(e > n ? "next" : "prev", this.$items.eq(e))
    }, n.prototype.pause = function (t) {
        return t || (this.paused = !0), this.$element.find(".next, .prev").length && e.support.transition && (this.$element.trigger(e.support.transition.end), this.cycle(!0)), this.interval = clearInterval(this.interval), this
    }, n.prototype.next = function () {
        return this.sliding ? void 0 : this.slide("next")
    }, n.prototype.prev = function () {
        return this.sliding ? void 0 : this.slide("prev")
    }, n.prototype.slide = function (t, i) {
        var o = this.$element.find(".item.active"), a = i || this.getItemForDirection(t, o), r = this.interval, s = "next" == t ? "left" : "right", l = this;
        if (a.hasClass("active"))return this.sliding = !1;
        var d = a[0], p = e.Event("slide.bs.carousel", {relatedTarget: d, direction: s});
        if (this.$element.trigger(p), !p.isDefaultPrevented()) {
            if (this.sliding = !0, r && this.pause(), this.$indicators.length) {
                this.$indicators.find(".active").removeClass("active");
                var c = e(this.$indicators.children()[this.getItemIndex(a)]);
                c && c.addClass("active")
            }
            var u = e.Event("slid.bs.carousel", {relatedTarget: d, direction: s});
            return e.support.transition && this.$element.hasClass("slide") ? (a.addClass(t), a[0].offsetWidth, o.addClass(s), a.addClass(s), o.one("bsTransitionEnd", function () {
                a.removeClass([t, s].join(" ")).addClass("active"), o.removeClass(["active", s].join(" ")), l.sliding = !1, setTimeout(function () {
                    l.$element.trigger(u)
                }, 0)
            }).emulateTransitionEnd(n.TRANSITION_DURATION)) : (o.removeClass("active"), a.addClass("active"), this.sliding = !1, this.$element.trigger(u)), r && this.cycle(), this
        }
    };
    var i = e.fn.carousel;
    e.fn.carousel = t, e.fn.carousel.Constructor = n, e.fn.carousel.noConflict = function () {
        return e.fn.carousel = i, this
    };
    var o = function (n) {
        var i, o = e(this), a = e(o.attr("data-target") || (i = o.attr("href")) && i.replace(/.*(?=#[^\s]+$)/, ""));
        if (a.hasClass("carousel")) {
            var r = e.extend({}, a.data(), o.data()), s = o.attr("data-slide-to");
            s && (r.interval = !1), t.call(a, r), s && a.data("bs.carousel").to(s), n.preventDefault()
        }
    };
    e(document).on("click.bs.carousel.data-api", "[data-slide]", o).on("click.bs.carousel.data-api", "[data-slide-to]", o), e(window).on("load", function () {
        e('[data-ride="carousel"]').each(function () {
            var n = e(this);
            t.call(n, n.data())
        })
    })
}(jQuery), +function (e) {
    function t(t) {
        var n, i = t.attr("data-target") || (n = t.attr("href")) && n.replace(/.*(?=#[^\s]+$)/, "");
        return e(i)
    }

    function n(t) {
        return this.each(function () {
            var n = e(this), o = n.data("bs.collapse"), a = e.extend({}, i.DEFAULTS, n.data(), "object" == typeof t && t);
            !o && a.toggle && /show|hide/.test(t) && (a.toggle = !1), o || n.data("bs.collapse", o = new i(this, a)), "string" == typeof t && o[t]()
        })
    }

    var i = function a(t, n) {
        this.$element = e(t), this.options = e.extend({}, a.DEFAULTS, n), this.$trigger = e('[data-toggle="collapse"][href="#' + t.id + '"],[data-toggle="collapse"][data-target="#' + t.id + '"]'), this.transitioning = null, this.options.parent ? this.$parent = this.getParent() : this.addAriaAndCollapsedClass(this.$element, this.$trigger), this.options.toggle && this.toggle()
    };
    i.VERSION = "3.3.5", i.TRANSITION_DURATION = 350, i.DEFAULTS = {toggle: !0}, i.prototype.dimension = function () {
        var e = this.$element.hasClass("width");
        return e ? "width" : "height"
    }, i.prototype.show = function () {
        if (!this.transitioning && !this.$element.hasClass("in")) {
            var t, o = this.$parent && this.$parent.children(".panel").children(".in, .collapsing");
            if (!(o && o.length && (t = o.data("bs.collapse"), t && t.transitioning))) {
                var a = e.Event("show.bs.collapse");
                if (this.$element.trigger(a), !a.isDefaultPrevented()) {
                    o && o.length && (n.call(o, "hide"), t || o.data("bs.collapse", null));
                    var r = this.dimension();
                    this.$element.removeClass("collapse").addClass("collapsing")[r](0).attr("aria-expanded", !0), this.$trigger.removeClass("collapsed").attr("aria-expanded", !0), this.transitioning = 1;
                    var s = function () {
                        this.$element.removeClass("collapsing").addClass("collapse in")[r](""), this.transitioning = 0, this.$element.trigger("shown.bs.collapse")
                    };
                    if (!e.support.transition)return s.call(this);
                    var l = e.camelCase(["scroll", r].join("-"));
                    this.$element.one("bsTransitionEnd", e.proxy(s, this)).emulateTransitionEnd(i.TRANSITION_DURATION)[r](this.$element[0][l])
                }
            }
        }
    }, i.prototype.hide = function () {
        if (!this.transitioning && this.$element.hasClass("in")) {
            var t = e.Event("hide.bs.collapse");
            if (this.$element.trigger(t), !t.isDefaultPrevented()) {
                var n = this.dimension();
                this.$element[n](this.$element[n]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded", !1), this.$trigger.addClass("collapsed").attr("aria-expanded", !1), this.transitioning = 1;
                var o = function () {
                    this.transitioning = 0, this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")
                };
                return e.support.transition ? void this.$element[n](0).one("bsTransitionEnd", e.proxy(o, this)).emulateTransitionEnd(i.TRANSITION_DURATION) : o.call(this)
            }
        }
    }, i.prototype.toggle = function () {
        this[this.$element.hasClass("in") ? "hide" : "show"]()
    }, i.prototype.getParent = function () {
        return e(this.options.parent).find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]').each(e.proxy(function (n, i) {
            var o = e(i);
            this.addAriaAndCollapsedClass(t(o), o)
        }, this)).end()
    }, i.prototype.addAriaAndCollapsedClass = function (e, t) {
        var n = e.hasClass("in");
        e.attr("aria-expanded", n), t.toggleClass("collapsed", !n).attr("aria-expanded", n)
    };
    var o = e.fn.collapse;
    e.fn.collapse = n, e.fn.collapse.Constructor = i, e.fn.collapse.noConflict = function () {
        return e.fn.collapse = o, this
    }, e(document).on("click.bs.collapse.data-api", '[data-toggle="collapse"]', function (i) {
        var o = e(this);
        o.attr("data-target") || i.preventDefault();
        var a = t(o), r = a.data("bs.collapse"), s = r ? "toggle" : o.data();
        n.call(a, s)
    })
}(jQuery), +function (e) {
    function t(t) {
        var n = t.attr("data-target");
        n || (n = t.attr("href"), n = n && /#[A-Za-z]/.test(n) && n.replace(/.*(?=#[^\s]*$)/, ""));
        var i = n && e(n);
        return i && i.length ? i : t.parent()
    }

    function n(n) {
        n && 3 === n.which || (e(o).remove(), e(a).each(function () {
            var i = e(this), o = t(i), a = {relatedTarget: this};
            o.hasClass("open") && (n && "click" == n.type && /input|textarea/i.test(n.target.tagName) && e.contains(o[0], n.target) || (o.trigger(n = e.Event("hide.bs.dropdown", a)), n.isDefaultPrevented() || (i.attr("aria-expanded", "false"), o.removeClass("open").trigger("hidden.bs.dropdown", a))))
        }))
    }

    function i(t) {
        return this.each(function () {
            var n = e(this), i = n.data("bs.dropdown");
            i || n.data("bs.dropdown", i = new r(this)), "string" == typeof t && i[t].call(n)
        })
    }

    var o = ".dropdown-backdrop", a = '[data-toggle="dropdown"]', r = function (t) {
        e(t).on("click.bs.dropdown", this.toggle)
    };
    r.VERSION = "3.3.5", r.prototype.toggle = function (i) {
        var o = e(this);
        if (!o.is(".disabled, :disabled")) {
            var a = t(o), r = a.hasClass("open");
            if (n(), !r) {
                "ontouchstart" in document.documentElement && !a.closest(".navbar-nav").length && e(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(e(this)).on("click", n);
                var s = {relatedTarget: this};
                if (a.trigger(i = e.Event("show.bs.dropdown", s)), i.isDefaultPrevented())return;
                o.trigger("focus").attr("aria-expanded", "true"), a.toggleClass("open").trigger("shown.bs.dropdown", s)
            }
            return !1
        }
    }, r.prototype.keydown = function (n) {
        if (/(38|40|27|32)/.test(n.which) && !/input|textarea/i.test(n.target.tagName)) {
            var i = e(this);
            if (n.preventDefault(), n.stopPropagation(), !i.is(".disabled, :disabled")) {
                var o = t(i), r = o.hasClass("open");
                if (!r && 27 != n.which || r && 27 == n.which)return 27 == n.which && o.find(a).trigger("focus"), i.trigger("click");
                var s = " li:not(.disabled):visible a", l = o.find(".dropdown-menu" + s);
                if (l.length) {
                    var d = l.index(n.target);
                    38 == n.which && d > 0 && d--, 40 == n.which && d < l.length - 1 && d++, ~d || (d = 0), l.eq(d).trigger("focus")
                }
            }
        }
    };
    var s = e.fn.dropdown;
    e.fn.dropdown = i, e.fn.dropdown.Constructor = r, e.fn.dropdown.noConflict = function () {
        return e.fn.dropdown = s, this
    }, e(document).on("click.bs.dropdown.data-api", n).on("click.bs.dropdown.data-api", ".dropdown form", function (e) {
        e.stopPropagation()
    }).on("click.bs.dropdown.data-api", a, r.prototype.toggle).on("keydown.bs.dropdown.data-api", a, r.prototype.keydown).on("keydown.bs.dropdown.data-api", ".dropdown-menu", r.prototype.keydown)
}(jQuery), +function (e) {
    function t(t, i) {
        return this.each(function () {
            var o = e(this), a = o.data("bs.modal"), r = e.extend({}, n.DEFAULTS, o.data(), "object" == typeof t && t);
            a || o.data("bs.modal", a = new n(this, r)), "string" == typeof t ? a[t](i) : r.show && a.show(i)
        })
    }

    var n = function (t, n) {
        this.options = n, this.$body = e(document.body), this.$element = e(t), this.$dialog = this.$element.find(".modal-dialog"), this.$backdrop = null, this.isShown = null, this.originalBodyPad = null, this.scrollbarWidth = 0, this.ignoreBackdropClick = !1, this.options.remote && this.$element.find(".modal-content").load(this.options.remote, e.proxy(function () {
            this.$element.trigger("loaded.bs.modal")
        }, this))
    };
    n.VERSION = "3.3.5", n.TRANSITION_DURATION = 300, n.BACKDROP_TRANSITION_DURATION = 150, n.DEFAULTS = {
        backdrop: !0,
        keyboard: !0,
        show: !0
    }, n.prototype.toggle = function (e) {
        return this.isShown ? this.hide() : this.show(e)
    }, n.prototype.show = function (t) {
        var i = this, o = e.Event("show.bs.modal", {relatedTarget: t});
        this.$element.trigger(o), this.isShown || o.isDefaultPrevented() || (this.isShown = !0, this.checkScrollbar(), this.setScrollbar(), this.$body.addClass("modal-open"), this.escape(), this.resize(), this.$element.on("click.dismiss.bs.modal", '[data-dismiss="modal"]', e.proxy(this.hide, this)), this.$dialog.on("mousedown.dismiss.bs.modal", function () {
            i.$element.one("mouseup.dismiss.bs.modal", function (t) {
                e(t.target).is(i.$element) && (i.ignoreBackdropClick = !0)
            })
        }), this.backdrop(function () {
            var o = e.support.transition && i.$element.hasClass("fade");
            i.$element.parent().length || i.$element.appendTo(i.$body), i.$element.show().scrollTop(0), i.adjustDialog(), o && i.$element[0].offsetWidth, i.$element.addClass("in"), i.enforceFocus();
            var a = e.Event("shown.bs.modal", {relatedTarget: t});
            o ? i.$dialog.one("bsTransitionEnd", function () {
                i.$element.trigger("focus").trigger(a)
            }).emulateTransitionEnd(n.TRANSITION_DURATION) : i.$element.trigger("focus").trigger(a)
        }))
    }, n.prototype.hide = function (t) {
        t && t.preventDefault(), t = e.Event("hide.bs.modal"), this.$element.trigger(t), this.isShown && !t.isDefaultPrevented() && (this.isShown = !1, this.escape(), this.resize(), e(document).off("focusin.bs.modal"), this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"), this.$dialog.off("mousedown.dismiss.bs.modal"), e.support.transition && this.$element.hasClass("fade") ? this.$element.one("bsTransitionEnd", e.proxy(this.hideModal, this)).emulateTransitionEnd(n.TRANSITION_DURATION) : this.hideModal())
    }, n.prototype.enforceFocus = function () {
        e(document).off("focusin.bs.modal").on("focusin.bs.modal", e.proxy(function (e) {
            this.$element[0] === e.target || this.$element.has(e.target).length || this.$element.trigger("focus")
        }, this))
    }, n.prototype.escape = function () {
        this.isShown && this.options.keyboard ? this.$element.on("keydown.dismiss.bs.modal", e.proxy(function (e) {
            27 == e.which && this.hide()
        }, this)) : this.isShown || this.$element.off("keydown.dismiss.bs.modal")
    }, n.prototype.resize = function () {
        this.isShown ? e(window).on("resize.bs.modal", e.proxy(this.handleUpdate, this)) : e(window).off("resize.bs.modal")
    }, n.prototype.hideModal = function () {
        var e = this;
        this.$element.hide(), this.backdrop(function () {
            e.$body.removeClass("modal-open"), e.resetAdjustments(), e.resetScrollbar(), e.$element.trigger("hidden.bs.modal")
        })
    }, n.prototype.removeBackdrop = function () {
        this.$backdrop && this.$backdrop.remove(), this.$backdrop = null
    }, n.prototype.backdrop = function (t) {
        var i = this, o = this.$element.hasClass("fade") ? "fade" : "";
        if (this.isShown && this.options.backdrop) {
            var a = e.support.transition && o;
            if (this.$backdrop = e(document.createElement("div")).addClass("modal-backdrop " + o).appendTo(this.$body), this.$element.on("click.dismiss.bs.modal", e.proxy(function (e) {
                    return this.ignoreBackdropClick ? void(this.ignoreBackdropClick = !1) : void(e.target === e.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus() : this.hide()))
                }, this)), a && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !t)return;
            a ? this.$backdrop.one("bsTransitionEnd", t).emulateTransitionEnd(n.BACKDROP_TRANSITION_DURATION) : t()
        } else if (!this.isShown && this.$backdrop) {
            this.$backdrop.removeClass("in");
            var r = function () {
                i.removeBackdrop(), t && t()
            };
            e.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one("bsTransitionEnd", r).emulateTransitionEnd(n.BACKDROP_TRANSITION_DURATION) : r()
        } else t && t()
    }, n.prototype.handleUpdate = function () {
        this.adjustDialog()
    }, n.prototype.adjustDialog = function () {
        var e = this.$element[0].scrollHeight > document.documentElement.clientHeight;
        this.$element.css({
            paddingLeft: !this.bodyIsOverflowing && e ? this.scrollbarWidth : "",
            paddingRight: this.bodyIsOverflowing && !e ? this.scrollbarWidth : ""
        })
    }, n.prototype.resetAdjustments = function () {
        this.$element.css({paddingLeft: "", paddingRight: ""})
    }, n.prototype.checkScrollbar = function () {
        var e = window.innerWidth;
        if (!e) {
            var t = document.documentElement.getBoundingClientRect();
            e = t.right - Math.abs(t.left)
        }
        this.bodyIsOverflowing = document.body.clientWidth < e, this.scrollbarWidth = this.measureScrollbar()
    }, n.prototype.setScrollbar = function () {
        var e = parseInt(this.$body.css("padding-right") || 0, 10);
        this.originalBodyPad = document.body.style.paddingRight || "", this.bodyIsOverflowing && this.$body.css("padding-right", e + this.scrollbarWidth)
    }, n.prototype.resetScrollbar = function () {
        this.$body.css("padding-right", this.originalBodyPad)
    }, n.prototype.measureScrollbar = function () {
        var e = document.createElement("div");
        e.className = "modal-scrollbar-measure", this.$body.append(e);
        var t = e.offsetWidth - e.clientWidth;
        return this.$body[0].removeChild(e), t
    };
    var i = e.fn.modal;
    e.fn.modal = t, e.fn.modal.Constructor = n, e.fn.modal.noConflict = function () {
        return e.fn.modal = i, this
    }, e(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function (n) {
        var i = e(this), o = i.attr("href"), a = e(i.attr("data-target") || o && o.replace(/.*(?=#[^\s]+$)/, "")), r = a.data("bs.modal") ? "toggle" : e.extend({remote: !/#/.test(o) && o}, a.data(), i.data());
        i.is("a") && n.preventDefault(), a.one("show.bs.modal", function (e) {
            e.isDefaultPrevented() || a.one("hidden.bs.modal", function () {
                i.is(":visible") && i.trigger("focus")
            })
        }), t.call(a, r, this)
    })
}(jQuery), +function (e) {
    function t(t) {
        return this.each(function () {
            var i = e(this), o = i.data("bs.tooltip"), a = "object" == typeof t && t;
            (o || !/destroy|hide/.test(t)) && (o || i.data("bs.tooltip", o = new n(this, a)), "string" == typeof t && o[t]())
        })
    }

    var n = function (e, t) {
        this.type = null, this.options = null, this.enabled = null, this.timeout = null, this.hoverState = null, this.$element = null, this.inState = null, this.init("tooltip", e, t)
    };
    n.VERSION = "3.3.5", n.TRANSITION_DURATION = 150, n.DEFAULTS = {
        animation: !0,
        placement: "top",
        selector: !1,
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: "hover focus",
        title: "",
        delay: 0,
        html: !1,
        container: !1,
        viewport: {selector: "body", padding: 0}
    }, n.prototype.init = function (t, n, i) {
        if (this.enabled = !0, this.type = t, this.$element = e(n), this.options = this.getOptions(i), this.$viewport = this.options.viewport && e(e.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : this.options.viewport.selector || this.options.viewport), this.inState = {
                click: !1,
                hover: !1,
                focus: !1
            }, this.$element[0] instanceof document.constructor && !this.options.selector)throw new Error("`selector` option must be specified when initializing " + this.type + " on the window.document object!");
        for (var o = this.options.trigger.split(" "), a = o.length; a--;) {
            var r = o[a];
            if ("click" == r)this.$element.on("click." + this.type, this.options.selector, e.proxy(this.toggle, this)); else if ("manual" != r) {
                var s = "hover" == r ? "mouseenter" : "focusin", l = "hover" == r ? "mouseleave" : "focusout";
                this.$element.on(s + "." + this.type, this.options.selector, e.proxy(this.enter, this)), this.$element.on(l + "." + this.type, this.options.selector, e.proxy(this.leave, this))
            }
        }
        this.options.selector ? this._options = e.extend({}, this.options, {
            trigger: "manual",
            selector: ""
        }) : this.fixTitle()
    }, n.prototype.getDefaults = function () {
        return n.DEFAULTS
    }, n.prototype.getOptions = function (t) {
        return t = e.extend({}, this.getDefaults(), this.$element.data(), t), t.delay && "number" == typeof t.delay && (t.delay = {
            show: t.delay,
            hide: t.delay
        }), t
    }, n.prototype.getDelegateOptions = function () {
        var t = {}, n = this.getDefaults();
        return this._options && e.each(this._options, function (e, i) {
            n[e] != i && (t[e] = i)
        }), t
    }, n.prototype.enter = function (t) {
        var n = t instanceof this.constructor ? t : e(t.currentTarget).data("bs." + this.type);
        return n || (n = new this.constructor(t.currentTarget, this.getDelegateOptions()), e(t.currentTarget).data("bs." + this.type, n)), t instanceof e.Event && (n.inState["focusin" == t.type ? "focus" : "hover"] = !0), n.tip().hasClass("in") || "in" == n.hoverState ? void(n.hoverState = "in") : (clearTimeout(n.timeout), n.hoverState = "in", n.options.delay && n.options.delay.show ? void(n.timeout = setTimeout(function () {
            "in" == n.hoverState && n.show()
        }, n.options.delay.show)) : n.show())
    }, n.prototype.isInStateTrue = function () {
        for (var e in this.inState)if (this.inState[e])return !0;
        return !1
    }, n.prototype.leave = function (t) {
        var n = t instanceof this.constructor ? t : e(t.currentTarget).data("bs." + this.type);
        return n || (n = new this.constructor(t.currentTarget, this.getDelegateOptions()), e(t.currentTarget).data("bs." + this.type, n)), t instanceof e.Event && (n.inState["focusout" == t.type ? "focus" : "hover"] = !1), n.isInStateTrue() ? void 0 : (clearTimeout(n.timeout), n.hoverState = "out", n.options.delay && n.options.delay.hide ? void(n.timeout = setTimeout(function () {
            "out" == n.hoverState && n.hide()
        }, n.options.delay.hide)) : n.hide())
    }, n.prototype.show = function () {
        var t = e.Event("show.bs." + this.type);
        if (this.hasContent() && this.enabled) {
            this.$element.trigger(t);
            var i = e.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);
            if (t.isDefaultPrevented() || !i)return;
            var o = this, a = this.tip(), r = this.getUID(this.type);
            this.setContent(), a.attr("id", r), this.$element.attr("aria-describedby", r), this.options.animation && a.addClass("fade");
            var s = "function" == typeof this.options.placement ? this.options.placement.call(this, a[0], this.$element[0]) : this.options.placement, l = /\s?auto?\s?/i, d = l.test(s);
            d && (s = s.replace(l, "") || "top"), a.detach().css({
                top: 0,
                left: 0,
                display: "block"
            }).addClass(s).data("bs." + this.type, this), this.options.container ? a.appendTo(this.options.container) : a.insertAfter(this.$element), this.$element.trigger("inserted.bs." + this.type);
            var p = this.getPosition(), c = a[0].offsetWidth, u = a[0].offsetHeight;
            if (d) {
                var h = s, f = this.getPosition(this.$viewport);
                s = "bottom" == s && p.bottom + u > f.bottom ? "top" : "top" == s && p.top - u < f.top ? "bottom" : "right" == s && p.right + c > f.width ? "left" : "left" == s && p.left - c < f.left ? "right" : s, a.removeClass(h).addClass(s)
            }
            var m = this.getCalculatedOffset(s, p, c, u);
            this.applyPlacement(m, s);
            var g = function () {
                var e = o.hoverState;
                o.$element.trigger("shown.bs." + o.type), o.hoverState = null, "out" == e && o.leave(o)
            };
            e.support.transition && this.$tip.hasClass("fade") ? a.one("bsTransitionEnd", g).emulateTransitionEnd(n.TRANSITION_DURATION) : g()
        }
    }, n.prototype.applyPlacement = function (t, n) {
        var i = this.tip(), o = i[0].offsetWidth, a = i[0].offsetHeight, r = parseInt(i.css("margin-top"), 10), s = parseInt(i.css("margin-left"), 10);
        isNaN(r) && (r = 0), isNaN(s) && (s = 0), t.top += r, t.left += s, e.offset.setOffset(i[0], e.extend({
            using: function (e) {
                i.css({top: Math.round(e.top), left: Math.round(e.left)})
            }
        }, t), 0), i.addClass("in");
        var l = i[0].offsetWidth, d = i[0].offsetHeight;
        "top" == n && d != a && (t.top = t.top + a - d);
        var p = this.getViewportAdjustedDelta(n, t, l, d);
        p.left ? t.left += p.left : t.top += p.top;
        var c = /top|bottom/.test(n), u = c ? 2 * p.left - o + l : 2 * p.top - a + d, h = c ? "offsetWidth" : "offsetHeight";
        i.offset(t), this.replaceArrow(u, i[0][h], c)
    }, n.prototype.replaceArrow = function (e, t, n) {
        this.arrow().css(n ? "left" : "top", 50 * (1 - e / t) + "%").css(n ? "top" : "left", "")
    }, n.prototype.setContent = function () {
        var e = this.tip(), t = this.getTitle();
        e.find(".tooltip-inner")[this.options.html ? "html" : "text"](t), e.removeClass("fade in top bottom left right")
    }, n.prototype.hide = function (t) {
        function i() {
            "in" != o.hoverState && a.detach(), o.$element.removeAttr("aria-describedby").trigger("hidden.bs." + o.type), t && t()
        }

        var o = this, a = e(this.$tip), r = e.Event("hide.bs." + this.type);
        return this.$element.trigger(r), r.isDefaultPrevented() ? void 0 : (a.removeClass("in"), e.support.transition && a.hasClass("fade") ? a.one("bsTransitionEnd", i).emulateTransitionEnd(n.TRANSITION_DURATION) : i(), this.hoverState = null, this)
    }, n.prototype.fixTitle = function () {
        var e = this.$element;
        (e.attr("title") || "string" != typeof e.attr("data-original-title")) && e.attr("data-original-title", e.attr("title") || "").attr("title", "")
    }, n.prototype.hasContent = function () {
        return this.getTitle()
    }, n.prototype.getPosition = function (t) {
        t = t || this.$element;
        var n = t[0], i = "BODY" == n.tagName, o = n.getBoundingClientRect();
        null == o.width && (o = e.extend({}, o, {width: o.right - o.left, height: o.bottom - o.top}));
        var a = i ? {
            top: 0,
            left: 0
        } : t.offset(), r = {scroll: i ? document.documentElement.scrollTop || document.body.scrollTop : t.scrollTop()}, s = i ? {
            width: e(window).width(),
            height: e(window).height()
        } : null;
        return e.extend({}, o, r, s, a)
    }, n.prototype.getCalculatedOffset = function (e, t, n, i) {
        return "bottom" == e ? {
            top: t.top + t.height,
            left: t.left + t.width / 2 - n / 2
        } : "top" == e ? {
            top: t.top - i,
            left: t.left + t.width / 2 - n / 2
        } : "left" == e ? {top: t.top + t.height / 2 - i / 2, left: t.left - n} : {
            top: t.top + t.height / 2 - i / 2,
            left: t.left + t.width
        }
    }, n.prototype.getViewportAdjustedDelta = function (e, t, n, i) {
        var o = {top: 0, left: 0};
        if (!this.$viewport)return o;
        var a = this.options.viewport && this.options.viewport.padding || 0, r = this.getPosition(this.$viewport);
        if (/right|left/.test(e)) {
            var s = t.top - a - r.scroll, l = t.top + a - r.scroll + i;
            s < r.top ? o.top = r.top - s : l > r.top + r.height && (o.top = r.top + r.height - l)
        } else {
            var d = t.left - a, p = t.left + a + n;
            d < r.left ? o.left = r.left - d : p > r.right && (o.left = r.left + r.width - p)
        }
        return o
    }, n.prototype.getTitle = function () {
        var e, t = this.$element, n = this.options;
        return e = t.attr("data-original-title") || ("function" == typeof n.title ? n.title.call(t[0]) : n.title)
    }, n.prototype.getUID = function (e) {
        do e += ~~(1e6 * Math.random()); while (document.getElementById(e));
        return e
    }, n.prototype.tip = function () {
        if (!this.$tip && (this.$tip = e(this.options.template), 1 != this.$tip.length))throw new Error(this.type + " `template` option must consist of exactly 1 top-level element!");
        return this.$tip
    }, n.prototype.arrow = function () {
        return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
    }, n.prototype.enable = function () {
        this.enabled = !0
    }, n.prototype.disable = function () {
        this.enabled = !1
    }, n.prototype.toggleEnabled = function () {
        this.enabled = !this.enabled
    }, n.prototype.toggle = function (t) {
        var n = this;
        t && (n = e(t.currentTarget).data("bs." + this.type), n || (n = new this.constructor(t.currentTarget, this.getDelegateOptions()), e(t.currentTarget).data("bs." + this.type, n))), t ? (n.inState.click = !n.inState.click, n.isInStateTrue() ? n.enter(n) : n.leave(n)) : n.tip().hasClass("in") ? n.leave(n) : n.enter(n)
    }, n.prototype.destroy = function () {
        var e = this;
        clearTimeout(this.timeout), this.hide(function () {
            e.$element.off("." + e.type).removeData("bs." + e.type), e.$tip && e.$tip.detach(), e.$tip = null, e.$arrow = null, e.$viewport = null
        })
    };
    var i = e.fn.tooltip;
    e.fn.tooltip = t, e.fn.tooltip.Constructor = n, e.fn.tooltip.noConflict = function () {
        return e.fn.tooltip = i, this
    }
}(jQuery), +function (e) {
    function t(t) {
        return this.each(function () {
            var i = e(this), o = i.data("bs.popover"), a = "object" == typeof t && t;
            (o || !/destroy|hide/.test(t)) && (o || i.data("bs.popover", o = new n(this, a)), "string" == typeof t && o[t]());
        })
    }

    var n = function (e, t) {
        this.init("popover", e, t)
    };
    if (!e.fn.tooltip)throw new Error("Popover requires tooltip.js");
    n.VERSION = "3.3.5", n.DEFAULTS = e.extend({}, e.fn.tooltip.Constructor.DEFAULTS, {
        placement: "right",
        trigger: "click",
        content: "",
        template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    }), n.prototype = e.extend({}, e.fn.tooltip.Constructor.prototype), n.prototype.constructor = n, n.prototype.getDefaults = function () {
        return n.DEFAULTS
    }, n.prototype.setContent = function () {
        var e = this.tip(), t = this.getTitle(), n = this.getContent();
        e.find(".popover-title")[this.options.html ? "html" : "text"](t), e.find(".popover-content").children().detach().end()[this.options.html ? "string" == typeof n ? "html" : "append" : "text"](n), e.removeClass("fade top bottom left right in"), e.find(".popover-title").html() || e.find(".popover-title").hide()
    }, n.prototype.hasContent = function () {
        return this.getTitle() || this.getContent()
    }, n.prototype.getContent = function () {
        var e = this.$element, t = this.options;
        return e.attr("data-content") || ("function" == typeof t.content ? t.content.call(e[0]) : t.content)
    }, n.prototype.arrow = function () {
        return this.$arrow = this.$arrow || this.tip().find(".arrow")
    };
    var i = e.fn.popover;
    e.fn.popover = t, e.fn.popover.Constructor = n, e.fn.popover.noConflict = function () {
        return e.fn.popover = i, this
    }
}(jQuery), +function (e) {
    function t(n, i) {
        this.$body = e(document.body), this.$scrollElement = e(e(n).is(document.body) ? window : n), this.options = e.extend({}, t.DEFAULTS, i), this.selector = (this.options.target || "") + " .nav li > a", this.offsets = [], this.targets = [], this.activeTarget = null, this.scrollHeight = 0, this.$scrollElement.on("scroll.bs.scrollspy", e.proxy(this.process, this)), this.refresh(), this.process()
    }

    function n(n) {
        return this.each(function () {
            var i = e(this), o = i.data("bs.scrollspy"), a = "object" == typeof n && n;
            o || i.data("bs.scrollspy", o = new t(this, a)), "string" == typeof n && o[n]()
        })
    }

    t.VERSION = "3.3.5", t.DEFAULTS = {offset: 10}, t.prototype.getScrollHeight = function () {
        return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
    }, t.prototype.refresh = function () {
        var t = this, n = "offset", i = 0;
        this.offsets = [], this.targets = [], this.scrollHeight = this.getScrollHeight(), e.isWindow(this.$scrollElement[0]) || (n = "position", i = this.$scrollElement.scrollTop()), this.$body.find(this.selector).map(function () {
            var t = e(this), o = t.data("target") || t.attr("href"), a = /^#./.test(o) && e(o);
            return a && a.length && a.is(":visible") && [[a[n]().top + i, o]] || null
        }).sort(function (e, t) {
            return e[0] - t[0]
        }).each(function () {
            t.offsets.push(this[0]), t.targets.push(this[1])
        })
    }, t.prototype.process = function () {
        var e, t = this.$scrollElement.scrollTop() + this.options.offset, n = this.getScrollHeight(), i = this.options.offset + n - this.$scrollElement.height(), o = this.offsets, a = this.targets, r = this.activeTarget;
        if (this.scrollHeight != n && this.refresh(), t >= i)return r != (e = a[a.length - 1]) && this.activate(e);
        if (r && t < o[0])return this.activeTarget = null, this.clear();
        for (e = o.length; e--;)r != a[e] && t >= o[e] && (void 0 === o[e + 1] || t < o[e + 1]) && this.activate(a[e])
    }, t.prototype.activate = function (t) {
        this.activeTarget = t, this.clear();
        var n = this.selector + '[data-target="' + t + '"],' + this.selector + '[href="' + t + '"]', i = e(n).parents("li").addClass("active");
        i.parent(".dropdown-menu").length && (i = i.closest("li.dropdown").addClass("active")), i.trigger("activate.bs.scrollspy")
    }, t.prototype.clear = function () {
        e(this.selector).parentsUntil(this.options.target, ".active").removeClass("active")
    };
    var i = e.fn.scrollspy;
    e.fn.scrollspy = n, e.fn.scrollspy.Constructor = t, e.fn.scrollspy.noConflict = function () {
        return e.fn.scrollspy = i, this
    }, e(window).on("load.bs.scrollspy.data-api", function () {
        e('[data-spy="scroll"]').each(function () {
            var t = e(this);
            n.call(t, t.data())
        })
    })
}(jQuery), +function (e) {
    function t(t) {
        return this.each(function () {
            var i = e(this), o = i.data("bs.tab");
            o || i.data("bs.tab", o = new n(this)), "string" == typeof t && o[t]()
        })
    }

    var n = function (t) {
        this.element = e(t)
    };
    n.VERSION = "3.3.5", n.TRANSITION_DURATION = 150, n.prototype.show = function () {
        var t = this.element, n = t.closest("ul:not(.dropdown-menu)"), i = t.data("target");
        if (i || (i = t.attr("href"), i = i && i.replace(/.*(?=#[^\s]*$)/, "")), !t.parent("li").hasClass("active")) {
            var o = n.find(".active:last a"), a = e.Event("hide.bs.tab", {relatedTarget: t[0]}), r = e.Event("show.bs.tab", {relatedTarget: o[0]});
            if (o.trigger(a), t.trigger(r), !r.isDefaultPrevented() && !a.isDefaultPrevented()) {
                var s = e(i);
                this.activate(t.closest("li"), n), this.activate(s, s.parent(), function () {
                    o.trigger({type: "hidden.bs.tab", relatedTarget: t[0]}), t.trigger({
                        type: "shown.bs.tab",
                        relatedTarget: o[0]
                    })
                })
            }
        }
    }, n.prototype.activate = function (t, i, o) {
        function a() {
            r.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !1), t.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded", !0), s ? (t[0].offsetWidth, t.addClass("in")) : t.removeClass("fade"), t.parent(".dropdown-menu").length && t.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !0), o && o()
        }

        var r = i.find("> .active"), s = o && e.support.transition && (r.length && r.hasClass("fade") || !!i.find("> .fade").length);
        r.length && s ? r.one("bsTransitionEnd", a).emulateTransitionEnd(n.TRANSITION_DURATION) : a(), r.removeClass("in")
    };
    var i = e.fn.tab;
    e.fn.tab = t, e.fn.tab.Constructor = n, e.fn.tab.noConflict = function () {
        return e.fn.tab = i, this
    };
    var o = function (n) {
        n.preventDefault(), t.call(e(this), "show")
    };
    e(document).on("click.bs.tab.data-api", '[data-toggle="tab"]', o).on("click.bs.tab.data-api", '[data-toggle="pill"]', o)
}(jQuery), +function (e) {
    function t(t) {
        return this.each(function () {
            var i = e(this), o = i.data("bs.affix"), a = "object" == typeof t && t;
            o || i.data("bs.affix", o = new n(this, a)), "string" == typeof t && o[t]()
        })
    }

    var n = function o(t, n) {
        this.options = e.extend({}, o.DEFAULTS, n), this.$target = e(this.options.target).on("scroll.bs.affix.data-api", e.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", e.proxy(this.checkPositionWithEventLoop, this)), this.$element = e(t), this.affixed = null, this.unpin = null, this.pinnedOffset = null, this.checkPosition()
    };
    n.VERSION = "3.3.5", n.RESET = "affix affix-top affix-bottom", n.DEFAULTS = {
        offset: 0,
        target: window
    }, n.prototype.getState = function (e, t, n, i) {
        var o = this.$target.scrollTop(), a = this.$element.offset(), r = this.$target.height();
        if (null != n && "top" == this.affixed)return n > o ? "top" : !1;
        if ("bottom" == this.affixed)return null != n ? o + this.unpin <= a.top ? !1 : "bottom" : e - i >= o + r ? !1 : "bottom";
        var s = null == this.affixed, l = s ? o : a.top, d = s ? r : t;
        return null != n && n >= o ? "top" : null != i && l + d >= e - i ? "bottom" : !1
    }, n.prototype.getPinnedOffset = function () {
        if (this.pinnedOffset)return this.pinnedOffset;
        this.$element.removeClass(n.RESET).addClass("affix");
        var e = this.$target.scrollTop(), t = this.$element.offset();
        return this.pinnedOffset = t.top - e
    }, n.prototype.checkPositionWithEventLoop = function () {
        setTimeout(e.proxy(this.checkPosition, this), 1)
    }, n.prototype.checkPosition = function () {
        if (this.$element.is(":visible")) {
            var t = this.$element.height(), i = this.options.offset, o = i.top, a = i.bottom, r = Math.max(e(document).height(), e(document.body).height());
            "object" != typeof i && (a = o = i), "function" == typeof o && (o = i.top(this.$element)), "function" == typeof a && (a = i.bottom(this.$element));
            var s = this.getState(r, t, o, a);
            if (this.affixed != s) {
                null != this.unpin && this.$element.css("top", "");
                var l = "affix" + (s ? "-" + s : ""), d = e.Event(l + ".bs.affix");
                if (this.$element.trigger(d), d.isDefaultPrevented())return;
                this.affixed = s, this.unpin = "bottom" == s ? this.getPinnedOffset() : null, this.$element.removeClass(n.RESET).addClass(l).trigger(l.replace("affix", "affixed") + ".bs.affix")
            }
            "bottom" == s && this.$element.offset({top: r - t - a})
        }
    };
    var i = e.fn.affix;
    e.fn.affix = t, e.fn.affix.Constructor = n, e.fn.affix.noConflict = function () {
        return e.fn.affix = i, this
    }, e(window).on("load", function () {
        e('[data-spy="affix"]').each(function () {
            var n = e(this), i = n.data();
            i.offset = i.offset || {}, null != i.offsetBottom && (i.offset.bottom = i.offsetBottom), null != i.offsetTop && (i.offset.top = i.offsetTop), t.call(n, i)
        })
    })
}(jQuery);
var _imgLiquid = _imgLiquid || {VER: "0.9.944"};
_imgLiquid.bgs_Available = !1, _imgLiquid.bgs_CheckRunned = !1, _imgLiquid.injectCss = ".imgLiquid img {visibility:hidden}", function (e) {
    function t() {
        if (!_imgLiquid.bgs_CheckRunned) {
            _imgLiquid.bgs_CheckRunned = !0;
            var t = e('<span style="background-size:cover" />');
            e("body").append(t), !function () {
                var e = t[0];
                if (e && window.getComputedStyle) {
                    var n = window.getComputedStyle(e, null);
                    n && n.backgroundSize && (_imgLiquid.bgs_Available = "cover" === n.backgroundSize)
                }
            }(), t.remove()
        }
    }

    e.fn.extend({
        imgLiquid: function (n) {
            this.defaults = {
                fill: !0,
                verticalAlign: "center",
                horizontalAlign: "center",
                useBackgroundSize: !0,
                useDataHtmlAttr: !0,
                responsive: !0,
                delay: 0,
                fadeInTime: 0,
                removeBoxBackground: !0,
                hardPixels: !0,
                responsiveCheckTime: 500,
                timecheckvisibility: 500,
                onStart: null,
                onFinish: null,
                onItemStart: null,
                onItemFinish: null,
                onItemError: null
            }, t();
            var i = this;
            return this.options = n, this.settings = e.extend({}, this.defaults, this.options), this.settings.onStart && this.settings.onStart(), this.each(function (t) {
                function n() {
                    -1 === c.css("background-image").indexOf(encodeURI(u.attr("src"))) && c.css({"background-image": 'url("' + encodeURI(u.attr("src")) + '")'}), c.css({
                        "background-size": p.fill ? "cover" : "contain",
                        "background-position": (p.horizontalAlign + " " + p.verticalAlign).toLowerCase(),
                        "background-repeat": "no-repeat"
                    }), e("a:first", c).css({
                        display: "block",
                        width: "100%",
                        height: "100%"
                    }), e("img", c).css({display: "none"}), p.onItemFinish && p.onItemFinish(t, c, u), c.addClass("imgLiquid_bgSize"), c.addClass("imgLiquid_ready"), d()
                }

                function o() {
                    function n() {
                        u.data("imgLiquid_error") || u.data("imgLiquid_loaded") || u.data("imgLiquid_oldProcessed") || (c.is(":visible") && u[0].complete && u[0].width > 0 && u[0].height > 0 ? (u.data("imgLiquid_loaded", !0), setTimeout(l, t * p.delay)) : setTimeout(n, p.timecheckvisibility))
                    }

                    if (u.data("oldSrc") && u.data("oldSrc") !== u.attr("src")) {
                        var i = u.clone().removeAttr("style");
                        return i.data("imgLiquid_settings", u.data("imgLiquid_settings")), u.parent().prepend(i), u.remove(), u = i, u[0].width = 0, void setTimeout(o, 10)
                    }
                    return u.data("imgLiquid_oldProcessed") ? void l() : (u.data("imgLiquid_oldProcessed", !1), u.data("oldSrc", u.attr("src")), e("img:not(:first)", c).css("display", "none"), c.css({overflow: "hidden"}), u.fadeTo(0, 0).removeAttr("width").removeAttr("height").css({
                        visibility: "visible",
                        "max-width": "none",
                        "max-height": "none",
                        width: "auto",
                        height: "auto",
                        display: "block"
                    }), u.on("error", r), u[0].onerror = r, n(), void a())
                }

                function a() {
                    (p.responsive || u.data("imgLiquid_oldProcessed")) && u.data("imgLiquid_settings") && (p = u.data("imgLiquid_settings"), c.actualSize = c.get(0).offsetWidth + c.get(0).offsetHeight / 1e4, c.sizeOld && c.actualSize !== c.sizeOld && l(), c.sizeOld = c.actualSize, setTimeout(a, p.responsiveCheckTime))
                }

                function r() {
                    u.data("imgLiquid_error", !0), c.addClass("imgLiquid_error"), p.onItemError && p.onItemError(t, c, u), d()
                }

                function s() {
                    var e = {};
                    if (i.settings.useDataHtmlAttr) {
                        var t = c.attr("data-imgLiquid-fill"), n = c.attr("data-imgLiquid-horizontalAlign"), o = c.attr("data-imgLiquid-verticalAlign");
                        ("true" === t || "false" === t) && (e.fill = Boolean("true" === t)), void 0 === n || "left" !== n && "center" !== n && "right" !== n && -1 === n.indexOf("%") || (e.horizontalAlign = n), void 0 === o || "top" !== o && "bottom" !== o && "center" !== o && -1 === o.indexOf("%") || (e.verticalAlign = o)
                    }
                    return _imgLiquid.isIE && i.settings.ieFadeInDisabled && (e.fadeInTime = 0), e
                }

                function l() {
                    var e, n, i, o, a, r, s, l, h = 0, f = 0, m = c.width(), g = c.height();
                    void 0 === u.data("owidth") && u.data("owidth", u[0].width), void 0 === u.data("oheight") && u.data("oheight", u[0].height), p.fill === m / g >= u.data("owidth") / u.data("oheight") ? (e = "100%", n = "auto", i = Math.floor(m), o = Math.floor(m * (u.data("oheight") / u.data("owidth")))) : (e = "auto", n = "100%", i = Math.floor(g * (u.data("owidth") / u.data("oheight"))), o = Math.floor(g)), a = p.horizontalAlign.toLowerCase(), s = m - i, "left" === a && (f = 0), "center" === a && (f = .5 * s), "right" === a && (f = s), -1 !== a.indexOf("%") && (a = parseInt(a.replace("%", ""), 10), a > 0 && (f = s * a * .01)), r = p.verticalAlign.toLowerCase(), l = g - o, "top" === r && (h = 0), "center" === r && (h = .5 * l), "bottom" === r && (h = l), -1 !== r.indexOf("%") && (r = parseInt(r.replace("%", ""), 10), r > 0 && (h = l * r * .01)), p.hardPixels && (e = i, n = o), u.css({
                        width: e,
                        height: n,
                        "margin-left": Math.floor(f),
                        "margin-top": Math.floor(h)
                    }), u.data("imgLiquid_oldProcessed") || (u.fadeTo(p.fadeInTime, 1), u.data("imgLiquid_oldProcessed", !0), p.removeBoxBackground && c.css("background-image", "none"), c.addClass("imgLiquid_nobgSize"), c.addClass("imgLiquid_ready")), p.onItemFinish && p.onItemFinish(t, c, u), d()
                }

                function d() {
                    t === i.length - 1 && i.settings.onFinish && i.settings.onFinish()
                }

                var p = i.settings, c = e(this), u = e("img:first", c);
                return u.length ? (u.data("imgLiquid_settings") ? (c.removeClass("imgLiquid_error").removeClass("imgLiquid_ready"), p = e.extend({}, u.data("imgLiquid_settings"), i.options)) : p = e.extend({}, i.settings, s()), u.data("imgLiquid_settings", p), p.onItemStart && p.onItemStart(t, c, u), void(_imgLiquid.bgs_Available && p.useBackgroundSize ? n() : o())) : void r()
            })
        }
    })
}(jQuery), !function () {
    var e = _imgLiquid.injectCss, t = document.getElementsByTagName("head")[0], n = document.createElement("style");
    n.type = "text/css", n.styleSheet ? n.styleSheet.cssText = e : n.appendChild(document.createTextNode(e)), t.appendChild(n)
}(), function (e, t) {
    function n(e, t, n) {
        var i = e.children(), o = !1;
        e.empty();
        for (var r = 0, s = i.length; s > r; r++) {
            var l = i.eq(r);
            if (e.append(l), n && e.append(n), a(e, t)) {
                l.remove(), o = !0;
                break
            }
            n && n.detach()
        }
        return o
    }

    function i(t, n, r, s, l) {
        var d = !1, p = "a, table, thead, tbody, tfoot, tr, col, colgroup, object, embed, param, ol, ul, dl, blockquote, select, optgroup, option, textarea, script, style", c = "script, .dotdotdot-keep";
        return t.contents().detach().each(function () {
            var u = this, h = e(u);
            if ("undefined" == typeof u)return !0;
            if (h.is(c))t.append(h); else {
                if (d)return !0;
                t.append(h), !l || h.is(s.after) || h.find(s.after).length || t[t.is(p) ? "after" : "append"](l), a(r, s) && (d = 3 == u.nodeType ? o(h, n, r, s, l) : i(h, n, r, s, l), d || (h.detach(), d = !0)), d || l && l.detach()
            }
        }), n.addClass("is-truncated"), d
    }

    function o(t, n, i, o, s) {
        var p = t[0];
        if (!p)return !1;
        var u = d(p), h = -1 !== u.indexOf(" ") ? " " : "　", f = "letter" == o.wrap ? "" : h, m = u.split(f), g = -1, v = -1, y = 0, w = m.length - 1;
        for (o.fallbackToLetter && 0 == y && 0 == w && (f = "", m = u.split(f), w = m.length - 1); w >= y && (0 != y || 0 != w);) {
            var b = Math.floor((y + w) / 2);
            if (b == v)break;
            v = b, l(p, m.slice(0, v + 1).join(f) + o.ellipsis), i.children().each(function () {
                e(this).toggle().toggle()
            }), a(i, o) ? (w = v, o.fallbackToLetter && 0 == y && 0 == w && (f = "", m = m[0].split(f), g = -1, v = -1, y = 0, w = m.length - 1)) : (g = v, y = v)
        }
        if (-1 == g || 1 == m.length && 0 == m[0].length) {
            var x = t.parent();
            t.detach();
            var T = s && s.closest(x).length ? s.length : 0;
            x.contents().length > T ? p = c(x.contents().eq(-1 - T), n) : (p = c(x, n, !0), T || x.detach()), p && (u = r(d(p), o), l(p, u), T && s && e(p).parent().append(s))
        } else u = r(m.slice(0, g + 1).join(f), o), l(p, u);
        return !0
    }

    function a(e, t) {
        return e.innerHeight() > t.maxHeight
    }

    function r(t, n) {
        for (; e.inArray(t.slice(-1), n.lastCharacter.remove) > -1;)t = t.slice(0, -1);
        return e.inArray(t.slice(-1), n.lastCharacter.noEllipsis) < 0 && (t += n.ellipsis), t
    }

    function s(e) {
        return {width: e.innerWidth(), height: e.innerHeight()}
    }

    function l(e, t) {
        e.innerText ? e.innerText = t : e.nodeValue ? e.nodeValue = t : e.textContent && (e.textContent = t)
    }

    function d(e) {
        return e.innerText ? e.innerText : e.nodeValue ? e.nodeValue : e.textContent ? e.textContent : ""
    }

    function p(e) {
        do e = e.previousSibling; while (e && 1 !== e.nodeType && 3 !== e.nodeType);
        return e
    }

    function c(t, n, i) {
        var o = !0;
        e:for (; o;) {
            var a = t, r = n, s = i;
            o = !1;
            var l, d = a && a[0];
            if (d) {
                if (!s) {
                    if (3 === d.nodeType)return d;
                    if (e.trim(a.text())) {
                        t = a.contents().last(), n = r, i = void 0, o = !0, d = l = void 0;
                        continue e
                    }
                }
                for (l = p(d); !l;) {
                    if (a = a.parent(), a.is(r) || !a.length)return !1;
                    l = p(a[0])
                }
                if (l) {
                    t = e(l), n = r, i = void 0, o = !0, d = l = void 0;
                    continue e
                }
            }
            return !1
        }
    }

    function u(t, n) {
        return t ? "string" == typeof t ? (t = e(t, n), t.length ? t : !1) : t.jquery ? t : !1 : !1
    }

    function h(e) {
        for (var t = e.innerHeight(), n = ["paddingTop", "paddingBottom"], i = 0, o = n.length; o > i; i++) {
            var a = parseInt(e.css(n[i]), 10);
            isNaN(a) && (a = 0), t -= a
        }
        return t
    }

    if (!e.fn.dotdotdot) {
        e.fn.dotdotdot = function (t) {
            if (0 == this.length)return e.fn.dotdotdot.debug('No element found for "' + this.selector + '".'), this;
            if (this.length > 1)return this.each(function () {
                e(this).dotdotdot(t)
            });
            var o = this;
            o.data("dotdotdot") && o.trigger("destroy.dot"), o.data("dotdotdot-style", o.attr("style") || ""), o.css("word-wrap", "break-word"), "nowrap" === o.css("white-space") && o.css("white-space", "normal"), o.bind_events = function () {
                return o.bind("update.dot", function (t, s) {
                    switch (o.removeClass("is-truncated"), t.preventDefault(), t.stopPropagation(), typeof l.height) {
                        case"number":
                            l.maxHeight = l.height;
                            break;
                        case"function":
                            l.maxHeight = l.height.call(o[0]);
                            break;
                        default:
                            l.maxHeight = h(o)
                    }
                    l.maxHeight += l.tolerance, "undefined" != typeof s && (("string" == typeof s || "nodeType" in s && 1 === s.nodeType) && (s = e("<div />").append(s).contents()), s instanceof e && (r = s)), m = o.wrapInner('<div class="dotdotdot" />').children(), m.contents().detach().end().append(r.clone(!0)).find("br").replaceWith("  <br />  ").end().css({
                        height: "auto",
                        width: "auto",
                        border: "none",
                        padding: 0,
                        margin: 0
                    });
                    var p = !1, c = !1;
                    return d.afterElement && (p = d.afterElement.clone(!0), p.show(), d.afterElement.detach()), a(m, l) && (c = "children" == l.wrap ? n(m, l, p) : i(m, o, m, l, p)), m.replaceWith(m.contents()), m = null, e.isFunction(l.callback) && l.callback.call(o[0], c, r), d.isTruncated = c, c
                }).bind("isTruncated.dot", function (e, t) {
                    return e.preventDefault(), e.stopPropagation(), "function" == typeof t && t.call(o[0], d.isTruncated), d.isTruncated
                }).bind("originalContent.dot", function (e, t) {
                    return e.preventDefault(), e.stopPropagation(), "function" == typeof t && t.call(o[0], r), r
                }).bind("destroy.dot", function (e) {
                    e.preventDefault(), e.stopPropagation(), o.unwatch().unbind_events().contents().detach().end().append(r).attr("style", o.data("dotdotdot-style") || "").data("dotdotdot", !1)
                }), o
            }, o.unbind_events = function () {
                return o.unbind(".dot"), o
            }, o.watch = function () {
                if (o.unwatch(), "window" == l.watch) {
                    var t = e(window), n = t.width(), i = t.height();
                    t.bind("resize.dot" + d.dotId, function () {
                        n == t.width() && i == t.height() && l.windowResizeFix || (n = t.width(), i = t.height(), c && clearInterval(c), c = setTimeout(function () {
                            o.trigger("update.dot")
                        }, 100))
                    })
                } else p = s(o), c = setInterval(function () {
                    if (o.is(":visible")) {
                        var e = s(o);
                        (p.width != e.width || p.height != e.height) && (o.trigger("update.dot"), p = e)
                    }
                }, 500);
                return o
            }, o.unwatch = function () {
                return e(window).unbind("resize.dot" + d.dotId), c && clearInterval(c), o
            };
            var r = o.contents(), l = e.extend(!0, {}, e.fn.dotdotdot.defaults, t), d = {}, p = {}, c = null, m = null;
            return l.lastCharacter.remove instanceof Array || (l.lastCharacter.remove = e.fn.dotdotdot.defaultArrays.lastCharacter.remove), l.lastCharacter.noEllipsis instanceof Array || (l.lastCharacter.noEllipsis = e.fn.dotdotdot.defaultArrays.lastCharacter.noEllipsis), d.afterElement = u(l.after, o), d.isTruncated = !1, d.dotId = f++, o.data("dotdotdot", !0).bind_events().trigger("update.dot"), l.watch && o.watch(), o
        }, e.fn.dotdotdot.defaults = {
            ellipsis: "... ",
            wrap: "word",
            fallbackToLetter: !0,
            lastCharacter: {},
            tolerance: 0,
            callback: null,
            after: null,
            height: null,
            watch: !1,
            windowResizeFix: !0
        }, e.fn.dotdotdot.defaultArrays = {
            lastCharacter: {
                remove: [" ", "　", ",", ";", ".", "!", "?"],
                noEllipsis: []
            }
        }, e.fn.dotdotdot.debug = function (e) {
        };
        var f = 1, m = e.fn.html;
        e.fn.html = function (n) {
            return n != t && !e.isFunction(n) && this.data("dotdotdot") ? this.trigger("update", [n]) : m.apply(this, arguments)
        };
        var g = e.fn.text;
        e.fn.text = function (n) {
            return n != t && !e.isFunction(n) && this.data("dotdotdot") ? (n = e("<div />").text(n).html(), this.trigger("update", [n])) : g.apply(this, arguments)
        }
    }
}(jQuery), function (e, t, n, i) {
    var o = n("html"), a = n(e), r = n(t), s = n.fancybox = function () {
        s.open.apply(this, arguments)
    }, l = navigator.userAgent.match(/msie/i), d = null, p = t.createTouch !== i, c = function (e) {
        return e && e.hasOwnProperty && e instanceof n
    }, u = function (e) {
        return e && "string" === n.type(e)
    }, h = function (e) {
        return u(e) && e.indexOf("%") > 0
    }, f = function (e) {
        return e && !(e.style.overflow && "hidden" === e.style.overflow) && (e.clientWidth && e.scrollWidth > e.clientWidth || e.clientHeight && e.scrollHeight > e.clientHeight)
    }, m = function (e, t) {
        var n = parseInt(e, 10) || 0;
        return t && h(e) && (n = s.getViewport()[t] / 100 * n), Math.ceil(n)
    }, g = function (e, t) {
        return m(e, t) + "px"
    };
    n.extend(s, {
        version: "2.1.5",
        defaults: {
            padding: 15,
            margin: 20,
            width: 800,
            height: 600,
            minWidth: 100,
            minHeight: 100,
            maxWidth: 9999,
            maxHeight: 9999,
            pixelRatio: 1,
            autoSize: !0,
            autoHeight: !1,
            autoWidth: !1,
            autoResize: !0,
            autoCenter: !p,
            fitToView: !0,
            aspectRatio: !1,
            topRatio: .5,
            leftRatio: .5,
            scrolling: "auto",
            wrapCSS: "",
            arrows: !0,
            closeBtn: !0,
            closeClick: !1,
            nextClick: !1,
            mouseWheel: !0,
            autoPlay: !1,
            playSpeed: 3e3,
            preload: 3,
            modal: !1,
            loop: !0,
            ajax: {dataType: "html", headers: {"X-fancyBox": !0}},
            iframe: {scrolling: "auto", preload: !0},
            swf: {wmode: "transparent", allowfullscreen: "true", allowscriptaccess: "always"},
            keys: {
                next: {13: "left", 34: "up", 39: "left", 40: "up"},
                prev: {8: "right", 33: "down", 37: "right", 38: "down"},
                close: [27],
                play: [32],
                toggle: [70]
            },
            direction: {next: "left", prev: "right"},
            scrollOutside: !0,
            index: 0,
            type: null,
            href: null,
            content: null,
            title: null,
            tpl: {
                wrap: '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
                image: '<img class="fancybox-image" src="{href}" alt="" />',
                iframe: '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen' + (l ? ' allowtransparency="true"' : "") + "></iframe>",
                error: '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
                closeBtn: '<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"></a>',
                next: '<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
                prev: '<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
            },
            openEffect: "fade",
            openSpeed: 250,
            openEasing: "swing",
            openOpacity: !0,
            openMethod: "zoomIn",
            closeEffect: "fade",
            closeSpeed: 250,
            closeEasing: "swing",
            closeOpacity: !0,
            closeMethod: "zoomOut",
            nextEffect: "elastic",
            nextSpeed: 250,
            nextEasing: "swing",
            nextMethod: "changeIn",
            prevEffect: "elastic",
            prevSpeed: 250,
            prevEasing: "swing",
            prevMethod: "changeOut",
            helpers: {overlay: !0, title: !0},
            onCancel: n.noop,
            beforeLoad: n.noop,
            afterLoad: n.noop,
            beforeShow: n.noop,
            afterShow: n.noop,
            beforeChange: n.noop,
            beforeClose: n.noop,
            afterClose: n.noop
        },
        group: {},
        opts: {},
        previous: null,
        coming: null,
        current: null,
        isActive: !1,
        isOpen: !1,
        isOpened: !1,
        wrap: null,
        skin: null,
        outer: null,
        inner: null,
        player: {timer: null, isActive: !1},
        ajaxLoad: null,
        imgPreload: null,
        transitions: {},
        helpers: {},
        open: function (e, t) {
            return e && (n.isPlainObject(t) || (t = {}), !1 !== s.close(!0)) ? (n.isArray(e) || (e = c(e) ? n(e).get() : [e]), n.each(e, function (o, a) {
                var r, l, d, p, h, f, m, g = {};
                "object" === n.type(a) && (a.nodeType && (a = n(a)), c(a) ? (g = {
                    href: a.data("fancybox-href") || a.attr("href"),
                    title: a.data("fancybox-title") || a.attr("title"),
                    isDom: !0,
                    element: a
                }, n.metadata && n.extend(!0, g, a.metadata())) : g = a), r = t.href || g.href || (u(a) ? a : null), l = t.title !== i ? t.title : g.title || "", d = t.content || g.content, p = d ? "html" : t.type || g.type, !p && g.isDom && (p = a.data("fancybox-type"), p || (h = a.prop("class").match(/fancybox\.(\w+)/), p = h ? h[1] : null)), u(r) && (p || (s.isImage(r) ? p = "image" : s.isSWF(r) ? p = "swf" : "#" === r.charAt(0) ? p = "inline" : u(a) && (p = "html", d = a)), "ajax" === p && (f = r.split(/\s+/, 2), r = f.shift(), m = f.shift())), d || ("inline" === p ? r ? d = n(u(r) ? r.replace(/.*(?=#[^\s]+$)/, "") : r) : g.isDom && (d = a) : "html" === p ? d = r : p || r || !g.isDom || (p = "inline", d = a)), n.extend(g, {
                    href: r,
                    type: p,
                    content: d,
                    title: l,
                    selector: m
                }), e[o] = g
            }), s.opts = n.extend(!0, {}, s.defaults, t), t.keys !== i && (s.opts.keys = t.keys ? n.extend({}, s.defaults.keys, t.keys) : !1), s.group = e, s._start(s.opts.index)) : void 0
        },
        cancel: function () {
            var e = s.coming;
            e && !1 !== s.trigger("onCancel") && (s.hideLoading(), s.ajaxLoad && s.ajaxLoad.abort(), s.ajaxLoad = null, s.imgPreload && (s.imgPreload.onload = s.imgPreload.onerror = null), e.wrap && e.wrap.stop(!0, !0).trigger("onReset").remove(), s.coming = null, s.current || s._afterZoomOut(e))
        },
        close: function (e) {
            s.cancel(), !1 !== s.trigger("beforeClose") && (s.unbindEvents(), s.isActive && (s.isOpen && e !== !0 ? (s.isOpen = s.isOpened = !1, s.isClosing = !0, n(".fancybox-item, .fancybox-nav").remove(), s.wrap.stop(!0, !0).removeClass("fancybox-opened"), s.transitions[s.current.closeMethod]()) : (n(".fancybox-wrap").stop(!0).trigger("onReset").remove(), s._afterZoomOut())))
        },
        play: function (e) {
            var t = function () {
                clearTimeout(s.player.timer)
            }, n = function () {
                t(), s.current && s.player.isActive && (s.player.timer = setTimeout(s.next, s.current.playSpeed))
            }, i = function () {
                t(), r.unbind(".player"), s.player.isActive = !1, s.trigger("onPlayEnd")
            }, o = function () {
                s.current && (s.current.loop || s.current.index < s.group.length - 1) && (s.player.isActive = !0, r.bind({
                    "onCancel.player beforeClose.player": i,
                    "onUpdate.player": n,
                    "beforeLoad.player": t
                }), n(), s.trigger("onPlayStart"))
            };
            e === !0 || !s.player.isActive && e !== !1 ? o() : i()
        },
        next: function (e) {
            var t = s.current;
            t && (u(e) || (e = t.direction.next), s.jumpto(t.index + 1, e, "next"))
        },
        prev: function (e) {
            var t = s.current;
            t && (u(e) || (e = t.direction.prev), s.jumpto(t.index - 1, e, "prev"))
        },
        jumpto: function (e, t, n) {
            var o = s.current;
            o && (e = m(e), s.direction = t || o.direction[e >= o.index ? "next" : "prev"], s.router = n || "jumpto", o.loop && (0 > e && (e = o.group.length + e % o.group.length), e %= o.group.length), o.group[e] !== i && (s.cancel(), s._start(e)))
        },
        reposition: function (e, t) {
            var i, o = s.current, a = o ? o.wrap : null;
            a && (i = s._getPosition(t), e && "scroll" === e.type ? (delete i.position, a.stop(!0, !0).animate(i, 200)) : (a.css(i), o.pos = n.extend({}, o.dim, i)))
        },
        update: function (e) {
            var t = e && e.type, n = !t || "orientationchange" === t;
            n && (clearTimeout(d), d = null), s.isOpen && !d && (d = setTimeout(function () {
                var i = s.current;
                i && !s.isClosing && (s.wrap.removeClass("fancybox-tmp"), (n || "load" === t || "resize" === t && i.autoResize) && s._setDimension(), "scroll" === t && i.canShrink || s.reposition(e), s.trigger("onUpdate"), d = null)
            }, n && !p ? 0 : 300))
        },
        toggle: function (e) {
            s.isOpen && (s.current.fitToView = "boolean" === n.type(e) ? e : !s.current.fitToView, p && (s.wrap.removeAttr("style").addClass("fancybox-tmp"), s.trigger("onUpdate")), s.update())
        },
        hideLoading: function () {
            r.unbind(".loading"), n("#fancybox-loading").remove()
        },
        showLoading: function () {
            var e, t;
            s.hideLoading(), e = n('<div id="fancybox-loading"><div></div></div>').click(s.cancel).appendTo("body"), r.bind("keydown.loading", function (e) {
                27 === (e.which || e.keyCode) && (e.preventDefault(), s.cancel())
            }), s.defaults.fixed || (t = s.getViewport(), e.css({
                position: "absolute",
                top: .5 * t.h + t.y,
                left: .5 * t.w + t.x
            }))
        },
        getViewport: function () {
            var t = s.current && s.current.locked || !1, n = {x: a.scrollLeft(), y: a.scrollTop()};
            return t ? (n.w = t[0].clientWidth, n.h = t[0].clientHeight) : (n.w = p && e.innerWidth ? e.innerWidth : a.width(), n.h = p && e.innerHeight ? e.innerHeight : a.height()), n
        },
        unbindEvents: function () {
            s.wrap && c(s.wrap) && s.wrap.unbind(".fb"), r.unbind(".fb"), a.unbind(".fb")
        },
        bindEvents: function () {
            var e, t = s.current;
            t && (a.bind("orientationchange.fb" + (p ? "" : " resize.fb") + (t.autoCenter && !t.locked ? " scroll.fb" : ""), s.update), e = t.keys, e && r.bind("keydown.fb", function (o) {
                var a = o.which || o.keyCode, r = o.target || o.srcElement;
                return 27 === a && s.coming ? !1 : void(o.ctrlKey || o.altKey || o.shiftKey || o.metaKey || r && (r.type || n(r).is("[contenteditable]")) || n.each(e, function (e, r) {
                    return t.group.length > 1 && r[a] !== i ? (s[e](r[a]), o.preventDefault(), !1) : n.inArray(a, r) > -1 ? (s[e](), o.preventDefault(), !1) : void 0
                }))
            }), n.fn.mousewheel && t.mouseWheel && s.wrap.bind("mousewheel.fb", function (e, i, o, a) {
                for (var r = e.target || null, l = n(r), d = !1; l.length && !(d || l.is(".fancybox-skin") || l.is(".fancybox-wrap"));)d = f(l[0]), l = n(l).parent();
                0 === i || d || s.group.length > 1 && !t.canShrink && (a > 0 || o > 0 ? s.prev(a > 0 ? "down" : "left") : (0 > a || 0 > o) && s.next(0 > a ? "up" : "right"), e.preventDefault())
            }))
        },
        trigger: function (e, t) {
            var i, o = t || s.coming || s.current;
            if (o) {
                if (n.isFunction(o[e]) && (i = o[e].apply(o, Array.prototype.slice.call(arguments, 1))), i === !1)return !1;
                o.helpers && n.each(o.helpers, function (t, i) {
                    i && s.helpers[t] && n.isFunction(s.helpers[t][e]) && s.helpers[t][e](n.extend(!0, {}, s.helpers[t].defaults, i), o)
                }), r.trigger(e)
            }
        },
        isImage: function (e) {
            return u(e) && e.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg)((\?|#).*)?$)/i)
        },
        isSWF: function (e) {
            return u(e) && e.match(/\.(swf)((\?|#).*)?$/i)
        },
        _start: function (e) {
            var t, i, o, a, r, l = {};
            if (e = m(e), t = s.group[e] || null, !t)return !1;
            if (l = n.extend(!0, {}, s.opts, t), a = l.margin, r = l.padding, "number" === n.type(a) && (l.margin = [a, a, a, a]), "number" === n.type(r) && (l.padding = [r, r, r, r]), l.modal && n.extend(!0, l, {
                    closeBtn: !1,
                    closeClick: !1,
                    nextClick: !1,
                    arrows: !1,
                    mouseWheel: !1,
                    keys: null,
                    helpers: {overlay: {closeClick: !1}}
                }), l.autoSize && (l.autoWidth = l.autoHeight = !0), "auto" === l.width && (l.autoWidth = !0), "auto" === l.height && (l.autoHeight = !0), l.group = s.group, l.index = e, s.coming = l, !1 === s.trigger("beforeLoad"))return void(s.coming = null);
            if (o = l.type, i = l.href, !o)return s.coming = null, s.current && s.router && "jumpto" !== s.router ? (s.current.index = e, s[s.router](s.direction)) : !1;
            if (s.isActive = !0, ("image" === o || "swf" === o) && (l.autoHeight = l.autoWidth = !1, l.scrolling = "visible"), "image" === o && (l.aspectRatio = !0), "iframe" === o && p && (l.scrolling = "scroll"), l.wrap = n(l.tpl.wrap).addClass("fancybox-" + (p ? "mobile" : "desktop") + " fancybox-type-" + o + " fancybox-tmp " + l.wrapCSS).appendTo(l.parent || "body"), n.extend(l, {
                    skin: n(".fancybox-skin", l.wrap),
                    outer: n(".fancybox-outer", l.wrap),
                    inner: n(".fancybox-inner", l.wrap)
                }), n.each(["Top", "Right", "Bottom", "Left"], function (e, t) {
                    l.skin.css("padding" + t, g(l.padding[e]))
                }), s.trigger("onReady"), "inline" === o || "html" === o) {
                if (!l.content || !l.content.length)return s._error("content")
            } else if (!i)return s._error("href");
            "image" === o ? s._loadImage() : "ajax" === o ? s._loadAjax() : "iframe" === o ? s._loadIframe() : s._afterLoad()
        },
        _error: function (e) {
            n.extend(s.coming, {
                type: "html",
                autoWidth: !0,
                autoHeight: !0,
                minWidth: 0,
                minHeight: 0,
                scrolling: "no",
                hasError: e,
                content: s.coming.tpl.error
            }), s._afterLoad()
        },
        _loadImage: function () {
            var e = s.imgPreload = new Image;
            e.onload = function () {
                this.onload = this.onerror = null, s.coming.width = this.width / s.opts.pixelRatio, s.coming.height = this.height / s.opts.pixelRatio, s._afterLoad()
            }, e.onerror = function () {
                this.onload = this.onerror = null, s._error("image")
            }, e.src = s.coming.href, e.complete !== !0 && s.showLoading()
        },
        _loadAjax: function () {
            var e = s.coming;
            s.showLoading(), s.ajaxLoad = n.ajax(n.extend({}, e.ajax, {
                url: e.href, error: function (e, t) {
                    s.coming && "abort" !== t ? s._error("ajax", e) : s.hideLoading()
                }, success: function (t, n) {
                    "success" === n && (e.content = t, s._afterLoad())
                }
            }))
        },
        _loadIframe: function () {
            var e = s.coming, t = n(e.tpl.iframe.replace(/\{rnd\}/g, (new Date).getTime())).attr("scrolling", p ? "auto" : e.iframe.scrolling).attr("src", e.href);
            n(e.wrap).bind("onReset", function () {
                try {
                    n(this).find("iframe").hide().attr("src", "//about:blank").end().empty()
                } catch (e) {
                }
            }), e.iframe.preload && (s.showLoading(), t.one("load", function () {
                n(this).data("ready", 1), p || n(this).bind("load.fb", s.update), n(this).parents(".fancybox-wrap").width("100%").removeClass("fancybox-tmp").show(), s._afterLoad()
            })), e.content = t.appendTo(e.inner), e.iframe.preload || s._afterLoad()
        },
        _preloadImages: function () {
            var e, t, n = s.group, i = s.current, o = n.length, a = i.preload ? Math.min(i.preload, o - 1) : 0;
            for (t = 1; a >= t; t += 1)e = n[(i.index + t) % o], "image" === e.type && e.href && ((new Image).src = e.href)
        },
        _afterLoad: function () {
            var e, t, i, o, a, r, l = s.coming, d = s.current, p = "fancybox-placeholder";
            if (s.hideLoading(), l && s.isActive !== !1) {
                if (!1 === s.trigger("afterLoad", l, d))return l.wrap.stop(!0).trigger("onReset").remove(), void(s.coming = null);
                switch (d && (s.trigger("beforeChange", d), d.wrap.stop(!0).removeClass("fancybox-opened").find(".fancybox-item, .fancybox-nav").remove()), s.unbindEvents(), e = l, t = l.content, i = l.type, o = l.scrolling, n.extend(s, {
                    wrap: e.wrap,
                    skin: e.skin,
                    outer: e.outer,
                    inner: e.inner,
                    current: e,
                    previous: d
                }), a = e.href, i) {
                    case"inline":
                    case"ajax":
                    case"html":
                        e.selector ? t = n("<div>").html(t).find(e.selector) : c(t) && (t.data(p) || t.data(p, n('<div class="' + p + '"></div>').insertAfter(t).hide()), t = t.show().detach(), e.wrap.bind("onReset", function () {
                            n(this).find(t).length && t.hide().replaceAll(t.data(p)).data(p, !1)
                        }));
                        break;
                    case"image":
                        t = e.tpl.image.replace("{href}", a);
                        break;
                    case"swf":
                        t = '<object id="fancybox-swf" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="movie" value="' + a + '"></param>', r = "", n.each(e.swf, function (e, n) {
                            t += '<param name="' + e + '" value="' + n + '"></param>', r += " " + e + '="' + n + '"'
                        }), t += '<embed src="' + a + '" type="application/x-shockwave-flash" width="100%" height="100%"' + r + "></embed></object>"
                }
                c(t) && t.parent().is(e.inner) || e.inner.append(t),
                    s.trigger("beforeShow"), e.inner.css("overflow", "yes" === o ? "scroll" : "no" === o ? "hidden" : o), s._setDimension(), s.reposition(), s.isOpen = !1, s.coming = null, s.bindEvents(), s.isOpened ? d.prevMethod && s.transitions[d.prevMethod]() : n(".fancybox-wrap").not(e.wrap).stop(!0).trigger("onReset").remove(), s.transitions[s.isOpened ? e.nextMethod : e.openMethod](), s._preloadImages()
            }
        },
        _setDimension: function () {
            var e, t, i, o, a, r, l, d, p, c, u, f, v, y, w, b = s.getViewport(), x = 0, T = !1, C = !1, S = s.wrap, k = s.skin, E = s.inner, P = s.current, I = P.width, D = P.height, N = P.minWidth, $ = P.minHeight, A = P.maxWidth, L = P.maxHeight, M = P.scrolling, O = P.scrollOutside ? P.scrollbarWidth : 0, z = P.margin, _ = m(z[1] + z[3]), j = m(z[0] + z[2]);
            if (S.add(k).add(E).width("auto").height("auto").removeClass("fancybox-tmp"), e = m(k.outerWidth(!0) - k.width()), t = m(k.outerHeight(!0) - k.height()), i = _ + e, o = j + t, a = h(I) ? (b.w - i) * m(I) / 100 : I, r = h(D) ? (b.h - o) * m(D) / 100 : D, "iframe" === P.type) {
                if (y = P.content, P.autoHeight && 1 === y.data("ready"))try {
                    y[0].contentWindow.document.location && (E.width(a).height(9999), w = y.contents().find("body"), O && w.css("overflow-x", "hidden"), r = w.outerHeight(!0))
                } catch (R) {
                }
            } else(P.autoWidth || P.autoHeight) && (E.addClass("fancybox-tmp"), P.autoWidth || E.width(a), P.autoHeight || E.height(r), P.autoWidth && (a = E.width()), P.autoHeight && (r = E.height()), E.removeClass("fancybox-tmp"));
            if (I = m(a), D = m(r), p = a / r, N = m(h(N) ? m(N, "w") - i : N), A = m(h(A) ? m(A, "w") - i : A), $ = m(h($) ? m($, "h") - o : $), L = m(h(L) ? m(L, "h") - o : L), l = A, d = L, P.fitToView && (A = Math.min(b.w - i, A), L = Math.min(b.h - o, L)), f = b.w - _, v = b.h - j, P.aspectRatio ? (I > A && (I = A, D = m(I / p)), D > L && (D = L, I = m(D * p)), N > I && (I = N, D = m(I / p)), $ > D && (D = $, I = m(D * p))) : (I = Math.max(N, Math.min(I, A)), P.autoHeight && "iframe" !== P.type && (E.width(I), D = E.height()), D = Math.max($, Math.min(D, L))), P.fitToView)if (E.width(I).height(D), S.width(I + e), c = S.width(), u = S.height(), P.aspectRatio)for (; (c > f || u > v) && I > N && D > $ && !(x++ > 19);)D = Math.max($, Math.min(L, D - 10)), I = m(D * p), N > I && (I = N, D = m(I / p)), I > A && (I = A, D = m(I / p)), E.width(I).height(D), S.width(I + e), c = S.width(), u = S.height(); else I = Math.max(N, Math.min(I, I - (c - f))), D = Math.max($, Math.min(D, D - (u - v)));
            O && "auto" === M && r > D && f > I + e + O && (I += O), E.width(I).height(D), S.width(I + e), c = S.width(), u = S.height(), T = (c > f || u > v) && I > N && D > $, C = P.aspectRatio ? l > I && d > D && a > I && r > D : (l > I || d > D) && (a > I || r > D), n.extend(P, {
                dim: {
                    width: g(c),
                    height: g(u)
                },
                origWidth: a,
                origHeight: r,
                canShrink: T,
                canExpand: C,
                wPadding: e,
                hPadding: t,
                wrapSpace: u - k.outerHeight(!0),
                skinSpace: k.height() - D
            }), !y && P.autoHeight && D > $ && L > D && !C && E.height("auto")
        },
        _getPosition: function (e) {
            var t = s.current, n = s.getViewport(), i = t.margin, o = s.wrap.width() + i[1] + i[3], a = s.wrap.height() + i[0] + i[2], r = {
                position: "absolute",
                top: i[0],
                left: i[3]
            };
            return t.autoCenter && t.fixed && !e && a <= n.h && o <= n.w ? r.position = "fixed" : t.locked || (r.top += n.y, r.left += n.x), r.top = g(Math.max(r.top, r.top + (n.h - a) * t.topRatio)), r.left = g(Math.max(r.left, r.left + (n.w - o) * t.leftRatio)), r
        },
        _afterZoomIn: function () {
            var e = s.current;
            e && (s.isOpen = s.isOpened = !0, s.wrap.css("overflow", "visible").addClass("fancybox-opened"), s.update(), (e.closeClick || e.nextClick && s.group.length > 1) && s.inner.css("cursor", "pointer").bind("click.fb", function (t) {
                n(t.target).is("a") || n(t.target).parent().is("a") || (t.preventDefault(), s[e.closeClick ? "close" : "next"]())
            }), e.closeBtn && n(e.tpl.closeBtn).appendTo(s.skin).bind("click.fb", function (e) {
                e.preventDefault(), s.close()
            }), e.arrows && s.group.length > 1 && ((e.loop || e.index > 0) && n(e.tpl.prev).appendTo(s.outer).bind("click.fb", s.prev), (e.loop || e.index < s.group.length - 1) && n(e.tpl.next).appendTo(s.outer).bind("click.fb", s.next)), s.trigger("afterShow"), e.loop || e.index !== e.group.length - 1 ? s.opts.autoPlay && !s.player.isActive && (s.opts.autoPlay = !1, s.play()) : s.play(!1))
        },
        _afterZoomOut: function (e) {
            e = e || s.current, n(".fancybox-wrap").trigger("onReset").remove(), n.extend(s, {
                group: {},
                opts: {},
                router: !1,
                current: null,
                isActive: !1,
                isOpened: !1,
                isOpen: !1,
                isClosing: !1,
                wrap: null,
                skin: null,
                outer: null,
                inner: null
            }), s.trigger("afterClose", e)
        }
    }), s.transitions = {
        getOrigPosition: function () {
            var e = s.current, t = e.element, n = e.orig, i = {}, o = 50, a = 50, r = e.hPadding, l = e.wPadding, d = s.getViewport();
            return !n && e.isDom && t.is(":visible") && (n = t.find("img:first"), n.length || (n = t)), c(n) ? (i = n.offset(), n.is("img") && (o = n.outerWidth(), a = n.outerHeight())) : (i.top = d.y + (d.h - a) * e.topRatio, i.left = d.x + (d.w - o) * e.leftRatio), ("fixed" === s.wrap.css("position") || e.locked) && (i.top -= d.y, i.left -= d.x), i = {
                top: g(i.top - r * e.topRatio),
                left: g(i.left - l * e.leftRatio),
                width: g(o + l),
                height: g(a + r)
            }
        }, step: function (e, t) {
            var n, i, o, a = t.prop, r = s.current, l = r.wrapSpace, d = r.skinSpace;
            ("width" === a || "height" === a) && (n = t.end === t.start ? 1 : (e - t.start) / (t.end - t.start), s.isClosing && (n = 1 - n), i = "width" === a ? r.wPadding : r.hPadding, o = e - i, s.skin[a](m("width" === a ? o : o - l * n)), s.inner[a](m("width" === a ? o : o - l * n - d * n)))
        }, zoomIn: function () {
            var e = s.current, t = e.pos, i = e.openEffect, o = "elastic" === i, a = n.extend({opacity: 1}, t);
            delete a.position, o ? (t = this.getOrigPosition(), e.openOpacity && (t.opacity = .1)) : "fade" === i && (t.opacity = .1), s.wrap.css(t).animate(a, {
                duration: "none" === i ? 0 : e.openSpeed,
                easing: e.openEasing,
                step: o ? this.step : null,
                complete: s._afterZoomIn
            })
        }, zoomOut: function () {
            var e = s.current, t = e.closeEffect, n = "elastic" === t, i = {opacity: .1};
            n && (i = this.getOrigPosition(), e.closeOpacity && (i.opacity = .1)), s.wrap.animate(i, {
                duration: "none" === t ? 0 : e.closeSpeed,
                easing: e.closeEasing,
                step: n ? this.step : null,
                complete: s._afterZoomOut
            })
        }, changeIn: function () {
            var e, t = s.current, n = t.nextEffect, i = t.pos, o = {opacity: 1}, a = s.direction, r = 200;
            i.opacity = .1, "elastic" === n && (e = "down" === a || "up" === a ? "top" : "left", "down" === a || "right" === a ? (i[e] = g(m(i[e]) - r), o[e] = "+=" + r + "px") : (i[e] = g(m(i[e]) + r), o[e] = "-=" + r + "px")), "none" === n ? s._afterZoomIn() : s.wrap.css(i).animate(o, {
                duration: t.nextSpeed,
                easing: t.nextEasing,
                complete: s._afterZoomIn
            })
        }, changeOut: function () {
            var e = s.previous, t = e.prevEffect, i = {opacity: .1}, o = s.direction, a = 200;
            "elastic" === t && (i["down" === o || "up" === o ? "top" : "left"] = ("up" === o || "left" === o ? "-" : "+") + "=" + a + "px"), e.wrap.animate(i, {
                duration: "none" === t ? 0 : e.prevSpeed,
                easing: e.prevEasing,
                complete: function () {
                    n(this).trigger("onReset").remove()
                }
            })
        }
    }, s.helpers.overlay = {
        defaults: {closeClick: !0, speedOut: 200, showEarly: !0, css: {}, locked: !p, fixed: !0},
        overlay: null,
        fixed: !1,
        el: n("html"),
        create: function (e) {
            e = n.extend({}, this.defaults, e), this.overlay && this.close(), this.overlay = n('<div class="fancybox-overlay"></div>').appendTo(s.coming ? s.coming.parent : e.parent), this.fixed = !1, e.fixed && s.defaults.fixed && (this.overlay.addClass("fancybox-overlay-fixed"), this.fixed = !0)
        },
        open: function (e) {
            var t = this;
            e = n.extend({}, this.defaults, e), this.overlay ? this.overlay.unbind(".overlay").width("auto").height("auto") : this.create(e), this.fixed || (a.bind("resize.overlay", n.proxy(this.update, this)), this.update()), e.closeClick && this.overlay.bind("click.overlay", function (e) {
                return n(e.target).hasClass("fancybox-overlay") ? (s.isActive ? s.close() : t.close(), !1) : void 0
            }), this.overlay.css(e.css).show()
        },
        close: function () {
            var e, t;
            a.unbind("resize.overlay"), this.el.hasClass("fancybox-lock") && (n(".fancybox-margin").removeClass("fancybox-margin"), e = a.scrollTop(), t = a.scrollLeft(), this.el.removeClass("fancybox-lock"), a.scrollTop(e).scrollLeft(t)), n(".fancybox-overlay").remove().hide(), n.extend(this, {
                overlay: null,
                fixed: !1
            })
        },
        update: function () {
            var e, n = "100%";
            this.overlay.width(n).height("100%"), l ? (e = Math.max(t.documentElement.offsetWidth, t.body.offsetWidth), r.width() > e && (n = r.width())) : r.width() > a.width() && (n = r.width()), this.overlay.width(n).height(r.height())
        },
        onReady: function (e, t) {
            var i = this.overlay;
            n(".fancybox-overlay").stop(!0, !0), i || this.create(e), e.locked && this.fixed && t.fixed && (i || (this.margin = r.height() > a.height() ? n("html").css("margin-right").replace("px", "") : !1), t.locked = this.overlay.append(t.wrap), t.fixed = !1), e.showEarly === !0 && this.beforeShow.apply(this, arguments)
        },
        beforeShow: function (e, t) {
            var i, o;
            t.locked && (this.margin !== !1 && (n("*").filter(function () {
                return "fixed" === n(this).css("position") && !n(this).hasClass("fancybox-overlay") && !n(this).hasClass("fancybox-wrap")
            }).addClass("fancybox-margin"), this.el.addClass("fancybox-margin")), i = a.scrollTop(), o = a.scrollLeft(), this.el.addClass("fancybox-lock"), a.scrollTop(i).scrollLeft(o)), this.open(e)
        },
        onUpdate: function () {
            this.fixed || this.update()
        },
        afterClose: function (e) {
            this.overlay && !s.coming && this.overlay.fadeOut(e.speedOut, n.proxy(this.close, this))
        }
    }, s.helpers.title = {
        defaults: {type: "float", position: "bottom"}, beforeShow: function (e) {
            var t, i, o = s.current, a = o.title, r = e.type;
            if (n.isFunction(a) && (a = a.call(o.element, o)), u(a) && "" !== n.trim(a)) {
                switch (t = n('<div class="fancybox-title fancybox-title-' + r + '-wrap">' + a + "</div>"), r) {
                    case"inside":
                        i = s.skin;
                        break;
                    case"outside":
                        i = s.wrap;
                        break;
                    case"over":
                        i = s.inner;
                        break;
                    default:
                        i = s.skin, t.appendTo("body"), l && t.width(t.width()), t.wrapInner('<span class="child"></span>'), s.current.margin[2] += Math.abs(m(t.css("margin-bottom")))
                }
                t["top" === e.position ? "prependTo" : "appendTo"](i)
            }
        }
    }, n.fn.fancybox = function (e) {
        var t, i = n(this), o = this.selector || "", a = function (a) {
            var r, l, d = n(this).blur(), p = t;
            a.ctrlKey || a.altKey || a.shiftKey || a.metaKey || d.is(".fancybox-wrap") || (r = e.groupAttr || "data-fancybox-group", l = d.attr(r), l || (r = "rel", l = d.get(0)[r]), l && "" !== l && "nofollow" !== l && (d = o.length ? n(o) : i, d = d.filter("[" + r + '="' + l + '"]'), p = d.index(this)), e.index = p, s.open(d, e) !== !1 && a.preventDefault())
        };
        return e = e || {}, t = e.index || 0, o && e.live !== !1 ? r.undelegate(o, "click.fb-start").delegate(o + ":not('.fancybox-item, .fancybox-nav')", "click.fb-start", a) : i.unbind("click.fb-start").bind("click.fb-start", a), this.filter("[data-fancybox-start=1]").trigger("click"), this
    }, r.ready(function () {
        var t, a;
        n.scrollbarWidth === i && (n.scrollbarWidth = function () {
            var e = n('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo("body"), t = e.children(), i = t.innerWidth() - t.height(99).innerWidth();
            return e.remove(), i
        }), n.support.fixedPosition === i && (n.support.fixedPosition = function () {
            var e = n('<div style="position:fixed;top:20px;"></div>').appendTo("body"), t = 20 === e[0].offsetTop || 15 === e[0].offsetTop;
            return e.remove(), t
        }()), n.extend(s.defaults, {
            scrollbarWidth: n.scrollbarWidth(),
            fixed: n.support.fixedPosition,
            parent: n("body")
        }), t = n(e).width(), o.addClass("fancybox-lock-test"), a = n(e).width(), o.removeClass("fancybox-lock-test"), n("<style type='text/css'>.fancybox-margin{margin-right:" + (a - t) + "px;}</style>").appendTo("head")
    })
}(window, document, jQuery), function (e, t, n) {
    function i(t, n) {
        this.bodyOverflowX, this.callbacks = {
            hide: [],
            show: []
        }, this.checkInterval = null, this.Content, this.$el = e(t), this.$elProxy, this.elProxyPosition, this.enabled = !0, this.options = e.extend({}, l, n), this.mouseIsOverProxy = !1, this.namespace = "tooltipster-" + Math.round(1e5 * Math.random()), this.Status = "hidden", this.timerHide = null, this.timerShow = null, this.$tooltip, this.options.iconTheme = this.options.iconTheme.replace(".", ""), this.options.theme = this.options.theme.replace(".", ""), this._init()
    }

    function o(t, n) {
        var i = !0;
        return e.each(t, function (e, o) {
            return "undefined" == typeof n[e] || t[e] !== n[e] ? (i = !1, !1) : void 0
        }), i
    }

    function a() {
        return !p && d
    }

    function r() {
        var e = n.body || n.documentElement, t = e.style, i = "transition";
        if ("string" == typeof t[i])return !0;
        v = ["Moz", "Webkit", "Khtml", "O", "ms"], i = i.charAt(0).toUpperCase() + i.substr(1);
        for (var o = 0; o < v.length; o++)if ("string" == typeof t[v[o] + i])return !0;
        return !1
    }

    var s = "tooltipster", l = {
        animation: "fade",
        arrow: !0,
        arrowColor: "",
        autoClose: !0,
        content: null,
        contentAsHTML: !1,
        contentCloning: !0,
        debug: !0,
        delay: 200,
        minWidth: 0,
        maxWidth: null,
        functionInit: function (e, t) {
        },
        functionBefore: function (e, t) {
            t()
        },
        functionReady: function (e, t) {
        },
        functionAfter: function (e) {
        },
        hideOnClick: !1,
        icon: "(?)",
        iconCloning: !0,
        iconDesktop: !1,
        iconTouch: !1,
        iconTheme: "tooltipster-icon",
        interactive: !1,
        interactiveTolerance: 350,
        multiple: !1,
        offsetX: 0,
        offsetY: 0,
        onlyOne: !1,
        position: "top",
        positionTracker: !1,
        positionTrackerCallback: function (e) {
            "hover" == this.option("trigger") && this.option("autoClose") && this.hide()
        },
        restoration: "current",
        speed: 350,
        timer: 0,
        theme: "tooltipster-default",
        touchDevices: !0,
        trigger: "hover",
        updateAnimation: !0
    };
    i.prototype = {
        _init: function () {
            var t = this;
            if (n.querySelector) {
                var i = null;
                void 0 === t.$el.data("tooltipster-initialTitle") && (i = t.$el.attr("title"), void 0 === i && (i = null), t.$el.data("tooltipster-initialTitle", i)), null !== t.options.content ? t._content_set(t.options.content) : t._content_set(i);
                var o = t.options.functionInit.call(t.$el, t.$el, t.Content);
                "undefined" != typeof o && t._content_set(o), t.$el.removeAttr("title").addClass("tooltipstered"), !d && t.options.iconDesktop || d && t.options.iconTouch ? ("string" == typeof t.options.icon ? (t.$elProxy = e('<span class="' + t.options.iconTheme + '"></span>'), t.$elProxy.text(t.options.icon)) : t.options.iconCloning ? t.$elProxy = t.options.icon.clone(!0) : t.$elProxy = t.options.icon, t.$elProxy.insertAfter(t.$el)) : t.$elProxy = t.$el, "hover" == t.options.trigger ? (t.$elProxy.on("mouseenter." + t.namespace, function () {
                    (!a() || t.options.touchDevices) && (t.mouseIsOverProxy = !0, t._show())
                }).on("mouseleave." + t.namespace, function () {
                    (!a() || t.options.touchDevices) && (t.mouseIsOverProxy = !1)
                }), d && t.options.touchDevices && t.$elProxy.on("touchstart." + t.namespace, function () {
                    t._showNow()
                })) : "click" == t.options.trigger && t.$elProxy.on("click." + t.namespace, function () {
                    (!a() || t.options.touchDevices) && t._show()
                })
            }
        }, _show: function () {
            var e = this;
            "shown" != e.Status && "appearing" != e.Status && (e.options.delay ? e.timerShow = setTimeout(function () {
                ("click" == e.options.trigger || "hover" == e.options.trigger && e.mouseIsOverProxy) && e._showNow()
            }, e.options.delay) : e._showNow())
        }, _showNow: function (n) {
            var i = this;
            i.options.functionBefore.call(i.$el, i.$el, function () {
                if (i.enabled && null !== i.Content) {
                    n && i.callbacks.show.push(n), i.callbacks.hide = [], clearTimeout(i.timerShow), i.timerShow = null, clearTimeout(i.timerHide), i.timerHide = null, i.options.onlyOne && e(".tooltipstered").not(i.$el).each(function (t, n) {
                        var i = e(n), o = i.data("tooltipster-ns");
                        e.each(o, function (e, t) {
                            var n = i.data(t), o = n.status(), a = n.option("autoClose");
                            "hidden" !== o && "disappearing" !== o && a && n.hide()
                        })
                    });
                    var o = function () {
                        i.Status = "shown", e.each(i.callbacks.show, function (e, t) {
                            t.call(i.$el)
                        }), i.callbacks.show = []
                    };
                    if ("hidden" !== i.Status) {
                        var a = 0;
                        "disappearing" === i.Status ? (i.Status = "appearing", r() ? (i.$tooltip.clearQueue().removeClass("tooltipster-dying").addClass("tooltipster-" + i.options.animation + "-show"), i.options.speed > 0 && i.$tooltip.delay(i.options.speed), i.$tooltip.queue(o)) : i.$tooltip.stop().fadeIn(o)) : "shown" === i.Status && o()
                    } else {
                        i.Status = "appearing";
                        var a = i.options.speed;
                        i.bodyOverflowX = e("body").css("overflow-x"), e("body").css("overflow-x", "hidden");
                        var s = "tooltipster-" + i.options.animation, l = "-webkit-transition-duration: " + i.options.speed + "ms; -webkit-animation-duration: " + i.options.speed + "ms; -moz-transition-duration: " + i.options.speed + "ms; -moz-animation-duration: " + i.options.speed + "ms; -o-transition-duration: " + i.options.speed + "ms; -o-animation-duration: " + i.options.speed + "ms; -ms-transition-duration: " + i.options.speed + "ms; -ms-animation-duration: " + i.options.speed + "ms; transition-duration: " + i.options.speed + "ms; animation-duration: " + i.options.speed + "ms;", p = i.options.minWidth ? "min-width:" + Math.round(i.options.minWidth) + "px;" : "", c = i.options.maxWidth ? "max-width:" + Math.round(i.options.maxWidth) + "px;" : "", u = i.options.interactive ? "pointer-events: auto;" : "";
                        if (i.$tooltip = e('<div class="tooltipster-base ' + i.options.theme + '" style="' + p + " " + c + " " + u + " " + l + '"><div class="tooltipster-content"></div></div>'), r() && i.$tooltip.addClass(s), i._content_insert(), i.$tooltip.appendTo("body"), i.reposition(), i.options.functionReady.call(i.$el, i.$el, i.$tooltip), r() ? (i.$tooltip.addClass(s + "-show"), i.options.speed > 0 && i.$tooltip.delay(i.options.speed), i.$tooltip.queue(o)) : i.$tooltip.css("display", "none").fadeIn(i.options.speed, o), i._interval_set(), e(t).on("scroll." + i.namespace + " resize." + i.namespace, function () {
                                i.reposition()
                            }), i.options.autoClose)if (e("body").off("." + i.namespace), "hover" == i.options.trigger) {
                            if (d && setTimeout(function () {
                                    e("body").on("touchstart." + i.namespace, function () {
                                        i.hide()
                                    })
                                }, 0), i.options.interactive) {
                                d && i.$tooltip.on("touchstart." + i.namespace, function (e) {
                                    e.stopPropagation()
                                });
                                var h = null;
                                i.$elProxy.add(i.$tooltip).on("mouseleave." + i.namespace + "-autoClose", function () {
                                    clearTimeout(h), h = setTimeout(function () {
                                        i.hide()
                                    }, i.options.interactiveTolerance)
                                }).on("mouseenter." + i.namespace + "-autoClose", function () {
                                    clearTimeout(h)
                                })
                            } else i.$elProxy.on("mouseleave." + i.namespace + "-autoClose", function () {
                                i.hide()
                            });
                            i.options.hideOnClick && i.$elProxy.on("click." + i.namespace + "-autoClose", function () {
                                i.hide()
                            })
                        } else"click" == i.options.trigger && (setTimeout(function () {
                            e("body").on("click." + i.namespace + " touchstart." + i.namespace, function () {
                                i.hide()
                            })
                        }, 0), i.options.interactive && i.$tooltip.on("click." + i.namespace + " touchstart." + i.namespace, function (e) {
                            e.stopPropagation()
                        }))
                    }
                    i.options.timer > 0 && (i.timerHide = setTimeout(function () {
                        i.timerHide = null, i.hide()
                    }, i.options.timer + a))
                }
            })
        }, _interval_set: function () {
            var t = this;
            t.checkInterval = setInterval(function () {
                if (0 === e("body").find(t.$el).length || 0 === e("body").find(t.$elProxy).length || "hidden" == t.Status || 0 === e("body").find(t.$tooltip).length)("shown" == t.Status || "appearing" == t.Status) && t.hide(), t._interval_cancel(); else if (t.options.positionTracker) {
                    var n = t._repositionInfo(t.$elProxy), i = !1;
                    o(n.dimension, t.elProxyPosition.dimension) && ("fixed" === t.$elProxy.css("position") ? o(n.position, t.elProxyPosition.position) && (i = !0) : o(n.offset, t.elProxyPosition.offset) && (i = !0)), i || (t.reposition(), t.options.positionTrackerCallback.call(t, t.$el))
                }
            }, 200)
        }, _interval_cancel: function () {
            clearInterval(this.checkInterval), this.checkInterval = null
        }, _content_set: function (e) {
            "object" == typeof e && null !== e && this.options.contentCloning && (e = e.clone(!0)), this.Content = e
        }, _content_insert: function () {
            var e = this, t = this.$tooltip.find(".tooltipster-content");
            "string" != typeof e.Content || e.options.contentAsHTML ? t.empty().append(e.Content) : t.text(e.Content)
        }, _update: function (e) {
            var t = this;
            t._content_set(e), null !== t.Content ? "hidden" !== t.Status && (t._content_insert(), t.reposition(), t.options.updateAnimation && (r() ? (t.$tooltip.css({
                width: "",
                "-webkit-transition": "all " + t.options.speed + "ms, width 0ms, height 0ms, left 0ms, top 0ms",
                "-moz-transition": "all " + t.options.speed + "ms, width 0ms, height 0ms, left 0ms, top 0ms",
                "-o-transition": "all " + t.options.speed + "ms, width 0ms, height 0ms, left 0ms, top 0ms",
                "-ms-transition": "all " + t.options.speed + "ms, width 0ms, height 0ms, left 0ms, top 0ms",
                transition: "all " + t.options.speed + "ms, width 0ms, height 0ms, left 0ms, top 0ms"
            }).addClass("tooltipster-content-changing"), setTimeout(function () {
                "hidden" != t.Status && (t.$tooltip.removeClass("tooltipster-content-changing"), setTimeout(function () {
                    "hidden" !== t.Status && t.$tooltip.css({
                        "-webkit-transition": t.options.speed + "ms",
                        "-moz-transition": t.options.speed + "ms",
                        "-o-transition": t.options.speed + "ms",
                        "-ms-transition": t.options.speed + "ms",
                        transition: t.options.speed + "ms"
                    })
                }, t.options.speed))
            }, t.options.speed)) : t.$tooltip.fadeTo(t.options.speed, .5, function () {
                "hidden" != t.Status && t.$tooltip.fadeTo(t.options.speed, 1)
            }))) : t.hide()
        }, _repositionInfo: function (e) {
            return {
                dimension: {height: e.outerHeight(!1), width: e.outerWidth(!1)},
                offset: e.offset(),
                position: {left: parseInt(e.css("left")), top: parseInt(e.css("top"))}
            }
        }, hide: function (n) {
            var i = this;
            n && i.callbacks.hide.push(n), i.callbacks.show = [], clearTimeout(i.timerShow), i.timerShow = null, clearTimeout(i.timerHide), i.timerHide = null;
            var o = function () {
                e.each(i.callbacks.hide, function (e, t) {
                    t.call(i.$el)
                }), i.callbacks.hide = []
            };
            if ("shown" == i.Status || "appearing" == i.Status) {
                i.Status = "disappearing";
                var a = function () {
                    i.Status = "hidden", "object" == typeof i.Content && null !== i.Content && i.Content.detach(), i.$tooltip.remove(), i.$tooltip = null, e(t).off("." + i.namespace), e("body").off("." + i.namespace).css("overflow-x", i.bodyOverflowX), e("body").off("." + i.namespace), i.$elProxy.off("." + i.namespace + "-autoClose"), i.options.functionAfter.call(i.$el, i.$el), o()
                };
                r() ? (i.$tooltip.clearQueue().removeClass("tooltipster-" + i.options.animation + "-show").addClass("tooltipster-dying"), i.options.speed > 0 && i.$tooltip.delay(i.options.speed), i.$tooltip.queue(a)) : i.$tooltip.stop().fadeOut(i.options.speed, a)
            } else"hidden" == i.Status && o();
            return i
        }, show: function (e) {
            return this._showNow(e), this
        }, update: function (e) {
            return this.content(e)
        }, content: function (e) {
            return "undefined" == typeof e ? this.Content : (this._update(e), this)
        }, reposition: function () {
            var n = this;
            if (0 !== e("body").find(n.$tooltip).length) {
                var i = function () {
                    var n = e(t).scrollLeft();
                    0 > I - n && (a = I - n, I = n), I + l - n > r && (a = I - (r + n - l), I = r + n - l)
                }, o = function (n, i) {
                    s.offset.top - e(t).scrollTop() - d - $ - 12 < 0 && i.indexOf("top") > -1 && (L = n), s.offset.top + s.dimension.height + d + 12 + $ > e(t).scrollTop() + e(t).height() && i.indexOf("bottom") > -1 && (L = n, N = s.offset.top - d - $ - 12)
                };
                n.$tooltip.css("width", ""), n.elProxyPosition = n._repositionInfo(n.$elProxy);
                var a = null, r = e(t).width(), s = n.elProxyPosition, l = n.$tooltip.outerWidth(!1), d = (n.$tooltip.innerWidth() + 1, n.$tooltip.outerHeight(!1));
                if (n.$elProxy.is("area")) {
                    var p = n.$elProxy.attr("shape"), c = n.$elProxy.parent().attr("name"), u = e('img[usemap="#' + c + '"]'), h = u.offset().left, f = u.offset().top, m = void 0 !== n.$elProxy.attr("coords") ? n.$elProxy.attr("coords").split(",") : void 0;
                    if ("circle" == p) {
                        var g = parseInt(m[0]), v = parseInt(m[1]), y = parseInt(m[2]);
                        s.dimension.height = 2 * y, s.dimension.width = 2 * y, s.offset.top = f + v - y, s.offset.left = h + g - y
                    } else if ("rect" == p) {
                        var g = parseInt(m[0]), v = parseInt(m[1]), w = parseInt(m[2]), b = parseInt(m[3]);
                        s.dimension.height = b - v, s.dimension.width = w - g, s.offset.top = f + v, s.offset.left = h + g
                    } else if ("poly" == p) {
                        for (var x = 0, T = 0, C = 0, S = 0, k = "even", E = 0; E < m.length; E++) {
                            var P = parseInt(m[E]);
                            "even" == k ? (P > C && (C = P, 0 === E && (x = C)), x > P && (x = P), k = "odd") : (P > S && (S = P, 1 == E && (T = S)), T > P && (T = P), k = "even")
                        }
                        s.dimension.height = S - T, s.dimension.width = C - x, s.offset.top = f + T, s.offset.left = h + x
                    } else s.dimension.height = u.outerHeight(!1), s.dimension.width = u.outerWidth(!1), s.offset.top = f, s.offset.left = h
                }
                var I = 0, D = 0, N = 0, $ = parseInt(n.options.offsetY), A = parseInt(n.options.offsetX), L = n.options.position;
                if ("top" == L) {
                    var M = s.offset.left + l - (s.offset.left + s.dimension.width);
                    I = s.offset.left + A - M / 2, N = s.offset.top - d - $ - 12, i(), o("bottom", "top")
                }
                if ("top-left" == L && (I = s.offset.left + A, N = s.offset.top - d - $ - 12, i(), o("bottom-left", "top-left")), "top-right" == L && (I = s.offset.left + s.dimension.width + A - l, N = s.offset.top - d - $ - 12, i(), o("bottom-right", "top-right")), "bottom" == L) {
                    var M = s.offset.left + l - (s.offset.left + s.dimension.width);
                    I = s.offset.left - M / 2 + A, N = s.offset.top + s.dimension.height + $ + 12, i(), o("top", "bottom")
                }
                if ("bottom-left" == L && (I = s.offset.left + A, N = s.offset.top + s.dimension.height + $ + 12, i(), o("top-left", "bottom-left")), "bottom-right" == L && (I = s.offset.left + s.dimension.width + A - l, N = s.offset.top + s.dimension.height + $ + 12, i(), o("top-right", "bottom-right")), "left" == L) {
                    I = s.offset.left - A - l - 12, D = s.offset.left + A + s.dimension.width + 12;
                    var O = s.offset.top + d - (s.offset.top + s.dimension.height);
                    if (N = s.offset.top - O / 2 - $, 0 > I && D + l > r) {
                        var z = 2 * parseFloat(n.$tooltip.css("border-width")), _ = l + I - z;
                        n.$tooltip.css("width", _ + "px"), d = n.$tooltip.outerHeight(!1), I = s.offset.left - A - _ - 12 - z, O = s.offset.top + d - (s.offset.top + s.dimension.height), N = s.offset.top - O / 2 - $
                    } else 0 > I && (I = s.offset.left + A + s.dimension.width + 12, a = "left")
                }
                if ("right" == L) {
                    I = s.offset.left + A + s.dimension.width + 12, D = s.offset.left - A - l - 12;
                    var O = s.offset.top + d - (s.offset.top + s.dimension.height);
                    if (N = s.offset.top - O / 2 - $, I + l > r && 0 > D) {
                        var z = 2 * parseFloat(n.$tooltip.css("border-width")), _ = r - I - z;
                        n.$tooltip.css("width", _ + "px"), d = n.$tooltip.outerHeight(!1), O = s.offset.top + d - (s.offset.top + s.dimension.height), N = s.offset.top - O / 2 - $
                    } else I + l > r && (I = s.offset.left - A - l - 12, a = "right")
                }
                if (n.options.arrow) {
                    var j = "tooltipster-arrow-" + L;
                    if (n.options.arrowColor.length < 1)var R = n.$tooltip.css("background-color"); else var R = n.options.arrowColor;
                    if (a ? "left" == a ? (j = "tooltipster-arrow-right", a = "") : "right" == a ? (j = "tooltipster-arrow-left", a = "") : a = "left:" + Math.round(a) + "px;" : a = "", "top" == L || "top-left" == L || "top-right" == L)var H = parseFloat(n.$tooltip.css("border-bottom-width")), F = n.$tooltip.css("border-bottom-color"); else if ("bottom" == L || "bottom-left" == L || "bottom-right" == L)var H = parseFloat(n.$tooltip.css("border-top-width")), F = n.$tooltip.css("border-top-color"); else if ("left" == L)var H = parseFloat(n.$tooltip.css("border-right-width")), F = n.$tooltip.css("border-right-color"); else if ("right" == L)var H = parseFloat(n.$tooltip.css("border-left-width")), F = n.$tooltip.css("border-left-color"); else var H = parseFloat(n.$tooltip.css("border-bottom-width")), F = n.$tooltip.css("border-bottom-color");
                    H > 1 && H++;
                    var B = "";
                    if (0 !== H) {
                        var W = "", q = "border-color: " + F + ";";
                        -1 !== j.indexOf("bottom") ? W = "margin-top: -" + Math.round(H) + "px;" : -1 !== j.indexOf("top") ? W = "margin-bottom: -" + Math.round(H) + "px;" : -1 !== j.indexOf("left") ? W = "margin-right: -" + Math.round(H) + "px;" : -1 !== j.indexOf("right") && (W = "margin-left: -" + Math.round(H) + "px;"), B = '<span class="tooltipster-arrow-border" style="' + W + " " + q + ';"></span>'
                    }
                    n.$tooltip.find(".tooltipster-arrow").remove();
                    var V = '<div class="' + j + ' tooltipster-arrow" style="' + a + '">' + B + '<span style="border-color:' + R + ';"></span></div>';
                    n.$tooltip.append(V)
                }
                n.$tooltip.css({top: Math.round(N) + "px", left: Math.round(I) + "px"})
            }
            return n
        }, enable: function () {
            return this.enabled = !0, this
        }, disable: function () {
            return this.hide(), this.enabled = !1, this
        }, destroy: function () {
            var t = this;
            t.hide(), t.$el[0] !== t.$elProxy[0] && t.$elProxy.remove(), t.$el.removeData(t.namespace).off("." + t.namespace);
            var n = t.$el.data("tooltipster-ns");
            if (1 === n.length) {
                var i = null;
                "previous" === t.options.restoration ? i = t.$el.data("tooltipster-initialTitle") : "current" === t.options.restoration && (i = "string" == typeof t.Content ? t.Content : e("<div></div>").append(t.Content).html()), i && t.$el.attr("title", i), t.$el.removeClass("tooltipstered").removeData("tooltipster-ns").removeData("tooltipster-initialTitle")
            } else n = e.grep(n, function (e, n) {
                return e !== t.namespace
            }), t.$el.data("tooltipster-ns", n);
            return t
        }, elementIcon: function () {
            return this.$el[0] !== this.$elProxy[0] ? this.$elProxy[0] : void 0
        }, elementTooltip: function () {
            return this.$tooltip ? this.$tooltip[0] : void 0
        }, option: function (e, t) {
            return "undefined" == typeof t ? this.options[e] : (this.options[e] = t, this)
        }, status: function () {
            return this.Status
        }
    }, e.fn[s] = function () {
        var t = arguments;
        if (0 === this.length) {
            if ("string" == typeof t[0]) {
                var n = !0;
                switch (t[0]) {
                    case"setDefaults":
                        e.extend(l, t[1]);
                        break;
                    default:
                        n = !1
                }
                return n ? !0 : this
            }
            return this
        }
        if ("string" == typeof t[0]) {
            var o = "#*$~&";
            return this.each(function () {
                var n = e(this).data("tooltipster-ns"), i = n ? e(this).data(n[0]) : null;
                if (!i)throw new Error("You called Tooltipster's \"" + t[0] + '" method on an uninitialized element');
                if ("function" != typeof i[t[0]])throw new Error('Unknown method .tooltipster("' + t[0] + '")');
                var a = i[t[0]](t[1], t[2]);
                return a !== i ? (o = a, !1) : void 0
            }), "#*$~&" !== o ? o : this
        }
        var a = [], r = t[0] && "undefined" != typeof t[0].multiple, s = r && t[0].multiple || !r && l.multiple, d = t[0] && "undefined" != typeof t[0].debug, p = d && t[0].debug || !d && l.debug;
        return this.each(function () {
            var n = !1, o = e(this).data("tooltipster-ns"), r = null;
            o ? s ? n = !0 : p && console.log('Tooltipster: one or more tooltips are already attached to this element: ignoring. Use the "multiple" option to attach more tooltips.') : n = !0, n && (r = new i(this, t[0]), o || (o = []), o.push(r.namespace), e(this).data("tooltipster-ns", o), e(this).data(r.namespace, r)), a.push(r)
        }), s ? a : this
    };
    var d = !!("ontouchstart" in t), p = !1;
    e("body").one("mousemove", function () {
        p = !0
    })
}(jQuery, window, document), !function () {
    function e(e) {
        e.fn.swiper = function (t) {
            var i;
            return e(this).each(function () {
                var e = new n(this, t);
                i || (i = e)
            }), i
        }
    }

    var t, n = function r(e, n) {
        function i() {
            return "horizontal" === b.params.direction
        }

        function o(e) {
            return Math.floor(e)
        }

        function a() {
            b.autoplayTimeoutId = setTimeout(function () {
                b.params.loop ? (b.fixLoop(), b._slideNext()) : b.isEnd ? n.autoplayStopOnLast ? b.stopAutoplay() : b._slideTo(0) : b._slideNext()
            }, b.params.autoplay)
        }

        function s(e, n) {
            var i = t(e.target);
            if (!i.is(n))if ("string" == typeof n)i = i.parents(n); else if (n.nodeType) {
                var o;
                return i.parents().each(function (e, t) {
                    t === n && (o = n)
                }), o ? n : void 0
            }
            return 0 !== i.length ? i[0] : void 0
        }

        function l(e, t) {
            t = t || {};
            var n = window.MutationObserver || window.WebkitMutationObserver, i = new n(function (e) {
                e.forEach(function (e) {
                    b.onResize(!0), b.emit("onObserverUpdate", b, e)
                })
            });
            i.observe(e, {
                attributes: "undefined" == typeof t.attributes ? !0 : t.attributes,
                childList: "undefined" == typeof t.childList ? !0 : t.childList,
                characterData: "undefined" == typeof t.characterData ? !0 : t.characterData
            }), b.observers.push(i)
        }

        function d(e) {
            e.originalEvent && (e = e.originalEvent);
            var t = e.keyCode || e.charCode;
            if (!b.params.allowSwipeToNext && (i() && 39 === t || !i() && 40 === t))return !1;
            if (!b.params.allowSwipeToPrev && (i() && 37 === t || !i() && 38 === t))return !1;
            if (!(e.shiftKey || e.altKey || e.ctrlKey || e.metaKey || document.activeElement && document.activeElement.nodeName && ("input" === document.activeElement.nodeName.toLowerCase() || "textarea" === document.activeElement.nodeName.toLowerCase()))) {
                if (37 === t || 39 === t || 38 === t || 40 === t) {
                    var n = !1;
                    if (b.container.parents(".swiper-slide").length > 0 && 0 === b.container.parents(".swiper-slide-active").length)return;
                    var o = {
                        left: window.pageXOffset,
                        top: window.pageYOffset
                    }, a = window.innerWidth, r = window.innerHeight, s = b.container.offset();
                    b.rtl && (s.left = s.left - b.container[0].scrollLeft);
                    for (var l = [[s.left, s.top], [s.left + b.width, s.top], [s.left, s.top + b.height], [s.left + b.width, s.top + b.height]], d = 0; d < l.length; d++) {
                        var p = l[d];
                        p[0] >= o.left && p[0] <= o.left + a && p[1] >= o.top && p[1] <= o.top + r && (n = !0)
                    }
                    if (!n)return
                }
                i() ? ((37 === t || 39 === t) && (e.preventDefault ? e.preventDefault() : e.returnValue = !1), (39 === t && !b.rtl || 37 === t && b.rtl) && b.slideNext(), (37 === t && !b.rtl || 39 === t && b.rtl) && b.slidePrev()) : ((38 === t || 40 === t) && (e.preventDefault ? e.preventDefault() : e.returnValue = !1), 40 === t && b.slideNext(), 38 === t && b.slidePrev())
            }
        }

        function p(e) {
            e.originalEvent && (e = e.originalEvent);
            var t = b.mousewheel.event, n = 0;
            if (e.detail)n = -e.detail; else if ("mousewheel" === t)if (b.params.mousewheelForceToAxis)if (i()) {
                if (!(Math.abs(e.wheelDeltaX) > Math.abs(e.wheelDeltaY)))return;
                n = e.wheelDeltaX
            } else {
                if (!(Math.abs(e.wheelDeltaY) > Math.abs(e.wheelDeltaX)))return;
                n = e.wheelDeltaY
            } else n = e.wheelDelta; else if ("DOMMouseScroll" === t)n = -e.detail; else if ("wheel" === t)if (b.params.mousewheelForceToAxis)if (i()) {
                if (!(Math.abs(e.deltaX) > Math.abs(e.deltaY)))return;
                n = -e.deltaX
            } else {
                if (!(Math.abs(e.deltaY) > Math.abs(e.deltaX)))return;
                n = -e.deltaY
            } else n = Math.abs(e.deltaX) > Math.abs(e.deltaY) ? -e.deltaX : -e.deltaY;
            if (0 !== n) {
                if (b.params.mousewheelInvert && (n = -n), b.params.freeMode) {
                    var o = b.getWrapperTranslate() + n * b.params.mousewheelSensitivity, a = b.isBeginning, r = b.isEnd;
                    if (o >= b.minTranslate() && (o = b.minTranslate()), o <= b.maxTranslate() && (o = b.maxTranslate()), b.setWrapperTransition(0), b.setWrapperTranslate(o), b.updateProgress(), b.updateActiveIndex(), (!a && b.isBeginning || !r && b.isEnd) && b.updateClasses(), b.params.freeModeSticky && (clearTimeout(b.mousewheel.timeout), b.mousewheel.timeout = setTimeout(function () {
                            b.slideReset()
                        }, 300)), 0 === o || o === b.maxTranslate())return
                } else {
                    if ((new window.Date).getTime() - b.mousewheel.lastScrollTime > 60)if (0 > n)if (b.isEnd && !b.params.loop || b.animating) {
                        if (b.params.mousewheelReleaseOnEdges)return !0
                    } else b.slideNext(); else if (b.isBeginning && !b.params.loop || b.animating) {
                        if (b.params.mousewheelReleaseOnEdges)return !0
                    } else b.slidePrev();
                    b.mousewheel.lastScrollTime = (new window.Date).getTime()
                }
                return b.params.autoplay && b.stopAutoplay(), e.preventDefault ? e.preventDefault() : e.returnValue = !1, !1
            }
        }

        function c(e, n) {
            e = t(e);
            var o, a, r;
            o = e.attr("data-swiper-parallax") || "0", a = e.attr("data-swiper-parallax-x"), r = e.attr("data-swiper-parallax-y"), a || r ? (a = a || "0", r = r || "0") : i() ? (a = o, r = "0") : (r = o, a = "0"), a = a.indexOf("%") >= 0 ? parseInt(a, 10) * n + "%" : a * n + "px", r = r.indexOf("%") >= 0 ? parseInt(r, 10) * n + "%" : r * n + "px", e.transform("translate3d(" + a + ", " + r + ",0px)")
        }

        function u(e) {
            return 0 !== e.indexOf("on") && (e = e[0] !== e[0].toUpperCase() ? "on" + e[0].toUpperCase() + e.substring(1) : "on" + e), e
        }

        if (!(this instanceof r))return new r(e, n);
        var h = {
            direction: "horizontal",
            touchEventsTarget: "container",
            initialSlide: 0,
            speed: 300,
            autoplay: !1,
            autoplayDisableOnInteraction: !0,
            iOSEdgeSwipeDetection: !1,
            iOSEdgeSwipeThreshold: 20,
            freeMode: !1,
            freeModeMomentum: !0,
            freeModeMomentumRatio: 1,
            freeModeMomentumBounce: !0,
            freeModeMomentumBounceRatio: 1,
            freeModeSticky: !1,
            freeModeMinimumVelocity: .02,
            autoHeight: !1,
            setWrapperSize: !1,
            virtualTranslate: !1,
            effect: "slide",
            coverflow: {
                rotate: 50, stretch: 0, depth: 100, modifier: 1, slideShadows: !0
            },
            cube: {slideShadows: !0, shadow: !0, shadowOffset: 20, shadowScale: .94},
            fade: {crossFade: !1},
            parallax: !1,
            scrollbar: null,
            scrollbarHide: !0,
            scrollbarDraggable: !1,
            scrollbarSnapOnRelease: !1,
            keyboardControl: !1,
            mousewheelControl: !1,
            mousewheelReleaseOnEdges: !1,
            mousewheelInvert: !1,
            mousewheelForceToAxis: !1,
            mousewheelSensitivity: 1,
            hashnav: !1,
            breakpoints: void 0,
            spaceBetween: 0,
            slidesPerView: 1,
            slidesPerColumn: 1,
            slidesPerColumnFill: "column",
            slidesPerGroup: 1,
            centeredSlides: !1,
            slidesOffsetBefore: 0,
            slidesOffsetAfter: 0,
            roundLengths: !1,
            touchRatio: 1,
            touchAngle: 45,
            simulateTouch: !0,
            shortSwipes: !0,
            longSwipes: !0,
            longSwipesRatio: .5,
            longSwipesMs: 300,
            followFinger: !0,
            onlyExternal: !1,
            threshold: 0,
            touchMoveStopPropagation: !0,
            pagination: null,
            paginationElement: "span",
            paginationClickable: !1,
            paginationHide: !1,
            paginationBulletRender: null,
            resistance: !0,
            resistanceRatio: .85,
            nextButton: null,
            prevButton: null,
            watchSlidesProgress: !1,
            watchSlidesVisibility: !1,
            grabCursor: !1,
            preventClicks: !0,
            preventClicksPropagation: !0,
            slideToClickedSlide: !1,
            lazyLoading: !1,
            lazyLoadingInPrevNext: !1,
            lazyLoadingOnTransitionStart: !1,
            preloadImages: !0,
            updateOnImagesReady: !0,
            loop: !1,
            loopAdditionalSlides: 0,
            loopedSlides: null,
            control: void 0,
            controlInverse: !1,
            controlBy: "slide",
            allowSwipeToPrev: !0,
            allowSwipeToNext: !0,
            swipeHandler: null,
            noSwiping: !0,
            noSwipingClass: "swiper-no-swiping",
            slideClass: "swiper-slide",
            slideActiveClass: "swiper-slide-active",
            slideVisibleClass: "swiper-slide-visible",
            slideDuplicateClass: "swiper-slide-duplicate",
            slideNextClass: "swiper-slide-next",
            slidePrevClass: "swiper-slide-prev",
            wrapperClass: "swiper-wrapper",
            bulletClass: "swiper-pagination-bullet",
            bulletActiveClass: "swiper-pagination-bullet-active",
            buttonDisabledClass: "swiper-button-disabled",
            paginationHiddenClass: "swiper-pagination-hidden",
            observer: !1,
            observeParents: !1,
            a11y: !1,
            prevSlideMessage: "Previous slide",
            nextSlideMessage: "Next slide",
            firstSlideMessage: "This is the first slide",
            lastSlideMessage: "This is the last slide",
            paginationBulletMessage: "Go to slide {{index}}",
            runCallbacksOnInit: !0
        }, f = n && n.virtualTranslate;
        n = n || {};
        var m = {};
        for (var g in n)if ("object" == typeof n[g]) {
            m[g] = {};
            for (var v in n[g])m[g][v] = n[g][v]
        } else m[g] = n[g];
        for (var y in h)if ("undefined" == typeof n[y])n[y] = h[y]; else if ("object" == typeof n[y])for (var w in h[y])"undefined" == typeof n[y][w] && (n[y][w] = h[y][w]);
        var b = this;
        if (b.params = n, b.originalParams = m, b.classNames = [], "undefined" != typeof t && "undefined" != typeof Dom7 && (t = Dom7), ("undefined" != typeof t || (t = "undefined" == typeof Dom7 ? window.Dom7 || window.Zepto || window.jQuery : Dom7)) && (b.$ = t, b.currentBreakpoint = void 0, b.getActiveBreakpoint = function () {
                if (!b.params.breakpoints)return !1;
                var e, t = !1, n = [];
                for (e in b.params.breakpoints)b.params.breakpoints.hasOwnProperty(e) && n.push(e);
                n.sort(function (e, t) {
                    return parseInt(e, 10) > parseInt(t, 10)
                });
                for (var i = 0; i < n.length; i++)e = n[i], e >= window.innerWidth && !t && (t = e);
                return t || "max"
            }, b.setBreakpoint = function () {
                var e = b.getActiveBreakpoint();
                if (e && b.currentBreakpoint !== e) {
                    var t = e in b.params.breakpoints ? b.params.breakpoints[e] : b.originalParams;
                    for (var n in t)b.params[n] = t[n];
                    b.currentBreakpoint = e
                }
            }, b.params.breakpoints && b.setBreakpoint(), b.container = t(e), 0 !== b.container.length)) {
            if (b.container.length > 1)return void b.container.each(function () {
                new r(this, n)
            });
            b.container[0].swiper = b, b.container.data("swiper", b), b.classNames.push("swiper-container-" + b.params.direction), b.params.freeMode && b.classNames.push("swiper-container-free-mode"), b.support.flexbox || (b.classNames.push("swiper-container-no-flexbox"), b.params.slidesPerColumn = 1), b.params.autoHeight && b.classNames.push("swiper-container-autoheight"), (b.params.parallax || b.params.watchSlidesVisibility) && (b.params.watchSlidesProgress = !0), ["cube", "coverflow"].indexOf(b.params.effect) >= 0 && (b.support.transforms3d ? (b.params.watchSlidesProgress = !0, b.classNames.push("swiper-container-3d")) : b.params.effect = "slide"), "slide" !== b.params.effect && b.classNames.push("swiper-container-" + b.params.effect), "cube" === b.params.effect && (b.params.resistanceRatio = 0, b.params.slidesPerView = 1, b.params.slidesPerColumn = 1, b.params.slidesPerGroup = 1, b.params.centeredSlides = !1, b.params.spaceBetween = 0, b.params.virtualTranslate = !0, b.params.setWrapperSize = !1), "fade" === b.params.effect && (b.params.slidesPerView = 1, b.params.slidesPerColumn = 1, b.params.slidesPerGroup = 1, b.params.watchSlidesProgress = !0, b.params.spaceBetween = 0, "undefined" == typeof f && (b.params.virtualTranslate = !0)), b.params.grabCursor && b.support.touch && (b.params.grabCursor = !1), b.wrapper = b.container.children("." + b.params.wrapperClass), b.params.pagination && (b.paginationContainer = t(b.params.pagination), b.params.paginationClickable && b.paginationContainer.addClass("swiper-pagination-clickable")), b.rtl = i() && ("rtl" === b.container[0].dir.toLowerCase() || "rtl" === b.container.css("direction")), b.rtl && b.classNames.push("swiper-container-rtl"), b.rtl && (b.wrongRTL = "-webkit-box" === b.wrapper.css("display")), b.params.slidesPerColumn > 1 && b.classNames.push("swiper-container-multirow"), b.device.android && b.classNames.push("swiper-container-android"), b.container.addClass(b.classNames.join(" ")), b.translate = 0, b.progress = 0, b.velocity = 0, b.lockSwipeToNext = function () {
                b.params.allowSwipeToNext = !1
            }, b.lockSwipeToPrev = function () {
                b.params.allowSwipeToPrev = !1
            }, b.lockSwipes = function () {
                b.params.allowSwipeToNext = b.params.allowSwipeToPrev = !1
            }, b.unlockSwipeToNext = function () {
                b.params.allowSwipeToNext = !0
            }, b.unlockSwipeToPrev = function () {
                b.params.allowSwipeToPrev = !0
            }, b.unlockSwipes = function () {
                b.params.allowSwipeToNext = b.params.allowSwipeToPrev = !0
            }, b.params.grabCursor && (b.container[0].style.cursor = "move", b.container[0].style.cursor = "-webkit-grab", b.container[0].style.cursor = "-moz-grab", b.container[0].style.cursor = "grab"), b.imagesToLoad = [], b.imagesLoaded = 0, b.loadImage = function (e, t, n, i, o) {
                function a() {
                    o && o()
                }

                var r;
                e.complete && i ? a() : t ? (r = new window.Image, r.onload = a, r.onerror = a, n && (r.srcset = n), t && (r.src = t)) : a()
            }, b.preloadImages = function () {
                function e() {
                    "undefined" != typeof b && null !== b && (void 0 !== b.imagesLoaded && b.imagesLoaded++, b.imagesLoaded === b.imagesToLoad.length && (b.params.updateOnImagesReady && b.update(), b.emit("onImagesReady", b)))
                }

                b.imagesToLoad = b.container.find("img");
                for (var t = 0; t < b.imagesToLoad.length; t++)b.loadImage(b.imagesToLoad[t], b.imagesToLoad[t].currentSrc || b.imagesToLoad[t].getAttribute("src"), b.imagesToLoad[t].srcset || b.imagesToLoad[t].getAttribute("srcset"), !0, e)
            }, b.autoplayTimeoutId = void 0, b.autoplaying = !1, b.autoplayPaused = !1, b.startAutoplay = function () {
                return "undefined" != typeof b.autoplayTimeoutId ? !1 : b.params.autoplay ? b.autoplaying ? !1 : (b.autoplaying = !0, b.emit("onAutoplayStart", b), void a()) : !1
            }, b.stopAutoplay = function (e) {
                b.autoplayTimeoutId && (b.autoplayTimeoutId && clearTimeout(b.autoplayTimeoutId), b.autoplaying = !1, b.autoplayTimeoutId = void 0, b.emit("onAutoplayStop", b))
            }, b.pauseAutoplay = function (e) {
                b.autoplayPaused || (b.autoplayTimeoutId && clearTimeout(b.autoplayTimeoutId), b.autoplayPaused = !0, 0 === e ? (b.autoplayPaused = !1, a()) : b.wrapper.transitionEnd(function () {
                    b && (b.autoplayPaused = !1, b.autoplaying ? a() : b.stopAutoplay())
                }))
            }, b.minTranslate = function () {
                return -b.snapGrid[0]
            }, b.maxTranslate = function () {
                return -b.snapGrid[b.snapGrid.length - 1]
            }, b.updateAutoHeight = function () {
                var e = b.slides.eq(b.activeIndex)[0].offsetHeight;
                e && b.wrapper.css("height", b.slides.eq(b.activeIndex)[0].offsetHeight + "px")
            }, b.updateContainerSize = function () {
                var e, t;
                e = "undefined" != typeof b.params.width ? b.params.width : b.container[0].clientWidth, t = "undefined" != typeof b.params.height ? b.params.height : b.container[0].clientHeight, 0 === e && i() || 0 === t && !i() || (e = e - parseInt(b.container.css("padding-left"), 10) - parseInt(b.container.css("padding-right"), 10), t = t - parseInt(b.container.css("padding-top"), 10) - parseInt(b.container.css("padding-bottom"), 10), b.width = e, b.height = t, b.size = i() ? b.width : b.height)
            }, b.updateSlidesSize = function () {
                b.slides = b.wrapper.children("." + b.params.slideClass), b.snapGrid = [], b.slidesGrid = [], b.slidesSizesGrid = [];
                var e, t = b.params.spaceBetween, n = -b.params.slidesOffsetBefore, a = 0, r = 0;
                "string" == typeof t && t.indexOf("%") >= 0 && (t = parseFloat(t.replace("%", "")) / 100 * b.size), b.virtualSize = -t, b.rtl ? b.slides.css({
                    marginLeft: "",
                    marginTop: ""
                }) : b.slides.css({marginRight: "", marginBottom: ""});
                var s;
                b.params.slidesPerColumn > 1 && (s = Math.floor(b.slides.length / b.params.slidesPerColumn) === b.slides.length / b.params.slidesPerColumn ? b.slides.length : Math.ceil(b.slides.length / b.params.slidesPerColumn) * b.params.slidesPerColumn, "auto" !== b.params.slidesPerView && "row" === b.params.slidesPerColumnFill && (s = Math.max(s, b.params.slidesPerView * b.params.slidesPerColumn)));
                var l, d = b.params.slidesPerColumn, p = s / d, c = p - (b.params.slidesPerColumn * p - b.slides.length);
                for (e = 0; e < b.slides.length; e++) {
                    l = 0;
                    var u = b.slides.eq(e);
                    if (b.params.slidesPerColumn > 1) {
                        var h, f, m;
                        "column" === b.params.slidesPerColumnFill ? (f = Math.floor(e / d), m = e - f * d, (f > c || f === c && m === d - 1) && ++m >= d && (m = 0, f++), h = f + m * s / d, u.css({
                            "-webkit-box-ordinal-group": h,
                            "-moz-box-ordinal-group": h,
                            "-ms-flex-order": h,
                            "-webkit-order": h,
                            order: h
                        })) : (m = Math.floor(e / p), f = e - m * p), u.css({"margin-top": 0 !== m && b.params.spaceBetween && b.params.spaceBetween + "px"}).attr("data-swiper-column", f).attr("data-swiper-row", m)
                    }
                    "none" !== u.css("display") && ("auto" === b.params.slidesPerView ? (l = i() ? u.outerWidth(!0) : u.outerHeight(!0), b.params.roundLengths && (l = o(l))) : (l = (b.size - (b.params.slidesPerView - 1) * t) / b.params.slidesPerView, b.params.roundLengths && (l = o(l)), i() ? b.slides[e].style.width = l + "px" : b.slides[e].style.height = l + "px"), b.slides[e].swiperSlideSize = l, b.slidesSizesGrid.push(l), b.params.centeredSlides ? (n = n + l / 2 + a / 2 + t, 0 === e && (n = n - b.size / 2 - t), Math.abs(n) < .001 && (n = 0), r % b.params.slidesPerGroup === 0 && b.snapGrid.push(n), b.slidesGrid.push(n)) : (r % b.params.slidesPerGroup === 0 && b.snapGrid.push(n), b.slidesGrid.push(n), n = n + l + t), b.virtualSize += l + t, a = l, r++)
                }
                b.virtualSize = Math.max(b.virtualSize, b.size) + b.params.slidesOffsetAfter;
                var g;
                if (b.rtl && b.wrongRTL && ("slide" === b.params.effect || "coverflow" === b.params.effect) && b.wrapper.css({width: b.virtualSize + b.params.spaceBetween + "px"}), (!b.support.flexbox || b.params.setWrapperSize) && (i() ? b.wrapper.css({width: b.virtualSize + b.params.spaceBetween + "px"}) : b.wrapper.css({height: b.virtualSize + b.params.spaceBetween + "px"})), b.params.slidesPerColumn > 1 && (b.virtualSize = (l + b.params.spaceBetween) * s, b.virtualSize = Math.ceil(b.virtualSize / b.params.slidesPerColumn) - b.params.spaceBetween, b.wrapper.css({width: b.virtualSize + b.params.spaceBetween + "px"}), b.params.centeredSlides)) {
                    for (g = [], e = 0; e < b.snapGrid.length; e++)b.snapGrid[e] < b.virtualSize + b.snapGrid[0] && g.push(b.snapGrid[e]);
                    b.snapGrid = g
                }
                if (!b.params.centeredSlides) {
                    for (g = [], e = 0; e < b.snapGrid.length; e++)b.snapGrid[e] <= b.virtualSize - b.size && g.push(b.snapGrid[e]);
                    b.snapGrid = g, Math.floor(b.virtualSize - b.size) > Math.floor(b.snapGrid[b.snapGrid.length - 1]) && b.snapGrid.push(b.virtualSize - b.size)
                }
                0 === b.snapGrid.length && (b.snapGrid = [0]), 0 !== b.params.spaceBetween && (i() ? b.rtl ? b.slides.css({marginLeft: t + "px"}) : b.slides.css({marginRight: t + "px"}) : b.slides.css({marginBottom: t + "px"})), b.params.watchSlidesProgress && b.updateSlidesOffset()
            }, b.updateSlidesOffset = function () {
                for (var e = 0; e < b.slides.length; e++)b.slides[e].swiperSlideOffset = i() ? b.slides[e].offsetLeft : b.slides[e].offsetTop
            }, b.updateSlidesProgress = function (e) {
                if ("undefined" == typeof e && (e = b.translate || 0), 0 !== b.slides.length) {
                    "undefined" == typeof b.slides[0].swiperSlideOffset && b.updateSlidesOffset();
                    var t = -e;
                    b.rtl && (t = e), b.slides.removeClass(b.params.slideVisibleClass);
                    for (var n = 0; n < b.slides.length; n++) {
                        var i = b.slides[n], o = (t - i.swiperSlideOffset) / (i.swiperSlideSize + b.params.spaceBetween);
                        if (b.params.watchSlidesVisibility) {
                            var a = -(t - i.swiperSlideOffset), r = a + b.slidesSizesGrid[n], s = a >= 0 && a < b.size || r > 0 && r <= b.size || 0 >= a && r >= b.size;
                            s && b.slides.eq(n).addClass(b.params.slideVisibleClass)
                        }
                        i.progress = b.rtl ? -o : o
                    }
                }
            }, b.updateProgress = function (e) {
                "undefined" == typeof e && (e = b.translate || 0);
                var t = b.maxTranslate() - b.minTranslate(), n = b.isBeginning, i = b.isEnd;
                0 === t ? (b.progress = 0, b.isBeginning = b.isEnd = !0) : (b.progress = (e - b.minTranslate()) / t, b.isBeginning = b.progress <= 0, b.isEnd = b.progress >= 1), b.isBeginning && !n && b.emit("onReachBeginning", b), b.isEnd && !i && b.emit("onReachEnd", b), b.params.watchSlidesProgress && b.updateSlidesProgress(e), b.emit("onProgress", b, b.progress)
            }, b.updateActiveIndex = function () {
                var e, t, n, i = b.rtl ? b.translate : -b.translate;
                for (t = 0; t < b.slidesGrid.length; t++)"undefined" != typeof b.slidesGrid[t + 1] ? i >= b.slidesGrid[t] && i < b.slidesGrid[t + 1] - (b.slidesGrid[t + 1] - b.slidesGrid[t]) / 2 ? e = t : i >= b.slidesGrid[t] && i < b.slidesGrid[t + 1] && (e = t + 1) : i >= b.slidesGrid[t] && (e = t);
                (0 > e || "undefined" == typeof e) && (e = 0), n = Math.floor(e / b.params.slidesPerGroup), n >= b.snapGrid.length && (n = b.snapGrid.length - 1), e !== b.activeIndex && (b.snapIndex = n, b.previousIndex = b.activeIndex, b.activeIndex = e, b.updateClasses())
            }, b.updateClasses = function () {
                b.slides.removeClass(b.params.slideActiveClass + " " + b.params.slideNextClass + " " + b.params.slidePrevClass);
                var e = b.slides.eq(b.activeIndex);
                if (e.addClass(b.params.slideActiveClass), e.next("." + b.params.slideClass).addClass(b.params.slideNextClass), e.prev("." + b.params.slideClass).addClass(b.params.slidePrevClass), b.bullets && b.bullets.length > 0) {
                    b.bullets.removeClass(b.params.bulletActiveClass);
                    var n;
                    b.params.loop ? (n = Math.ceil(b.activeIndex - b.loopedSlides) / b.params.slidesPerGroup, n > b.slides.length - 1 - 2 * b.loopedSlides && (n -= b.slides.length - 2 * b.loopedSlides), n > b.bullets.length - 1 && (n -= b.bullets.length)) : n = "undefined" != typeof b.snapIndex ? b.snapIndex : b.activeIndex || 0, b.paginationContainer.length > 1 ? b.bullets.each(function () {
                        t(this).index() === n && t(this).addClass(b.params.bulletActiveClass)
                    }) : b.bullets.eq(n).addClass(b.params.bulletActiveClass)
                }
                b.params.loop || (b.params.prevButton && (b.isBeginning ? (t(b.params.prevButton).addClass(b.params.buttonDisabledClass), b.params.a11y && b.a11y && b.a11y.disable(t(b.params.prevButton))) : (t(b.params.prevButton).removeClass(b.params.buttonDisabledClass), b.params.a11y && b.a11y && b.a11y.enable(t(b.params.prevButton)))), b.params.nextButton && (b.isEnd ? (t(b.params.nextButton).addClass(b.params.buttonDisabledClass), b.params.a11y && b.a11y && b.a11y.disable(t(b.params.nextButton))) : (t(b.params.nextButton).removeClass(b.params.buttonDisabledClass), b.params.a11y && b.a11y && b.a11y.enable(t(b.params.nextButton)))))
            }, b.updatePagination = function () {
                if (b.params.pagination && b.paginationContainer && b.paginationContainer.length > 0) {
                    for (var e = "", t = b.params.loop ? Math.ceil((b.slides.length - 2 * b.loopedSlides) / b.params.slidesPerGroup) : b.snapGrid.length, n = 0; t > n; n++)e += b.params.paginationBulletRender ? b.params.paginationBulletRender(n, b.params.bulletClass) : "<" + b.params.paginationElement + ' class="' + b.params.bulletClass + '"></' + b.params.paginationElement + ">";
                    b.paginationContainer.html(e), b.bullets = b.paginationContainer.find("." + b.params.bulletClass), b.params.paginationClickable && b.params.a11y && b.a11y && b.a11y.initPagination()
                }
            }, b.update = function (e) {
                function t() {
                    i = Math.min(Math.max(b.translate, b.maxTranslate()), b.minTranslate()), b.setWrapperTranslate(i), b.updateActiveIndex(), b.updateClasses()
                }

                if (b.updateContainerSize(), b.updateSlidesSize(), b.updateProgress(), b.updatePagination(), b.updateClasses(), b.params.scrollbar && b.scrollbar && b.scrollbar.set(), e) {
                    var n, i;
                    b.controller && b.controller.spline && (b.controller.spline = void 0), b.params.freeMode ? (t(), b.params.autoHeight && b.updateAutoHeight()) : (n = ("auto" === b.params.slidesPerView || b.params.slidesPerView > 1) && b.isEnd && !b.params.centeredSlides ? b.slideTo(b.slides.length - 1, 0, !1, !0) : b.slideTo(b.activeIndex, 0, !1, !0), n || t())
                } else b.params.autoHeight && b.updateAutoHeight()
            }, b.onResize = function (e) {
                b.params.breakpoints && b.setBreakpoint();
                var t = b.params.allowSwipeToPrev, n = b.params.allowSwipeToNext;
                if (b.params.allowSwipeToPrev = b.params.allowSwipeToNext = !0, b.updateContainerSize(), b.updateSlidesSize(), ("auto" === b.params.slidesPerView || b.params.freeMode || e) && b.updatePagination(), b.params.scrollbar && b.scrollbar && b.scrollbar.set(), b.controller && b.controller.spline && (b.controller.spline = void 0), b.params.freeMode) {
                    var i = Math.min(Math.max(b.translate, b.maxTranslate()), b.minTranslate());
                    b.setWrapperTranslate(i), b.updateActiveIndex(), b.updateClasses(), b.params.autoHeight && b.updateAutoHeight()
                } else b.updateClasses(), ("auto" === b.params.slidesPerView || b.params.slidesPerView > 1) && b.isEnd && !b.params.centeredSlides ? b.slideTo(b.slides.length - 1, 0, !1, !0) : b.slideTo(b.activeIndex, 0, !1, !0);
                b.params.allowSwipeToPrev = t, b.params.allowSwipeToNext = n
            };
            var x = ["mousedown", "mousemove", "mouseup"];
            window.navigator.pointerEnabled ? x = ["pointerdown", "pointermove", "pointerup"] : window.navigator.msPointerEnabled && (x = ["MSPointerDown", "MSPointerMove", "MSPointerUp"]), b.touchEvents = {
                start: b.support.touch || !b.params.simulateTouch ? "touchstart" : x[0],
                move: b.support.touch || !b.params.simulateTouch ? "touchmove" : x[1],
                end: b.support.touch || !b.params.simulateTouch ? "touchend" : x[2]
            }, (window.navigator.pointerEnabled || window.navigator.msPointerEnabled) && ("container" === b.params.touchEventsTarget ? b.container : b.wrapper).addClass("swiper-wp8-" + b.params.direction), b.initEvents = function (e) {
                var i = e ? "off" : "on", o = e ? "removeEventListener" : "addEventListener", a = "container" === b.params.touchEventsTarget ? b.container[0] : b.wrapper[0], r = b.support.touch ? a : document, s = b.params.nested ? !0 : !1;
                b.browser.ie ? (a[o](b.touchEvents.start, b.onTouchStart, !1), r[o](b.touchEvents.move, b.onTouchMove, s), r[o](b.touchEvents.end, b.onTouchEnd, !1)) : (b.support.touch && (a[o](b.touchEvents.start, b.onTouchStart, !1), a[o](b.touchEvents.move, b.onTouchMove, s), a[o](b.touchEvents.end, b.onTouchEnd, !1)), !n.simulateTouch || b.device.ios || b.device.android || (a[o]("mousedown", b.onTouchStart, !1), document[o]("mousemove", b.onTouchMove, s), document[o]("mouseup", b.onTouchEnd, !1))), window[o]("resize", b.onResize), b.params.nextButton && (t(b.params.nextButton)[i]("click", b.onClickNext), b.params.a11y && b.a11y && t(b.params.nextButton)[i]("keydown", b.a11y.onEnterKey)), b.params.prevButton && (t(b.params.prevButton)[i]("click", b.onClickPrev), b.params.a11y && b.a11y && t(b.params.prevButton)[i]("keydown", b.a11y.onEnterKey)), b.params.pagination && b.params.paginationClickable && (t(b.paginationContainer)[i]("click", "." + b.params.bulletClass, b.onClickIndex), b.params.a11y && b.a11y && t(b.paginationContainer)[i]("keydown", "." + b.params.bulletClass, b.a11y.onEnterKey)), (b.params.preventClicks || b.params.preventClicksPropagation) && a[o]("click", b.preventClicks, !0)
            }, b.attachEvents = function (e) {
                b.initEvents()
            }, b.detachEvents = function () {
                b.initEvents(!0)
            }, b.allowClick = !0, b.preventClicks = function (e) {
                b.allowClick || (b.params.preventClicks && e.preventDefault(), b.params.preventClicksPropagation && b.animating && (e.stopPropagation(), e.stopImmediatePropagation()))
            }, b.onClickNext = function (e) {
                e.preventDefault(), (!b.isEnd || b.params.loop) && b.slideNext()
            }, b.onClickPrev = function (e) {
                e.preventDefault(), (!b.isBeginning || b.params.loop) && b.slidePrev()
            }, b.onClickIndex = function (e) {
                e.preventDefault();
                var n = t(this).index() * b.params.slidesPerGroup;
                b.params.loop && (n += b.loopedSlides), b.slideTo(n)
            }, b.updateClickedSlide = function (e) {
                var n = s(e, "." + b.params.slideClass), i = !1;
                if (n)for (var o = 0; o < b.slides.length; o++)b.slides[o] === n && (i = !0);
                if (!n || !i)return b.clickedSlide = void 0, void(b.clickedIndex = void 0);
                if (b.clickedSlide = n, b.clickedIndex = t(n).index(), b.params.slideToClickedSlide && void 0 !== b.clickedIndex && b.clickedIndex !== b.activeIndex) {
                    var a, r = b.clickedIndex;
                    if (b.params.loop) {
                        if (b.animating)return;
                        a = t(b.clickedSlide).attr("data-swiper-slide-index"), b.params.centeredSlides ? r < b.loopedSlides - b.params.slidesPerView / 2 || r > b.slides.length - b.loopedSlides + b.params.slidesPerView / 2 ? (b.fixLoop(), r = b.wrapper.children("." + b.params.slideClass + '[data-swiper-slide-index="' + a + '"]:not(.swiper-slide-duplicate)').eq(0).index(), setTimeout(function () {
                            b.slideTo(r)
                        }, 0)) : b.slideTo(r) : r > b.slides.length - b.params.slidesPerView ? (b.fixLoop(), r = b.wrapper.children("." + b.params.slideClass + '[data-swiper-slide-index="' + a + '"]:not(.swiper-slide-duplicate)').eq(0).index(), setTimeout(function () {
                            b.slideTo(r)
                        }, 0)) : b.slideTo(r)
                    } else b.slideTo(r)
                }
            };
            var T, C, S, k, E, P, I, D, N, $, A = "input, select, textarea, button", L = Date.now(), M = [];
            b.animating = !1, b.touches = {startX: 0, startY: 0, currentX: 0, currentY: 0, diff: 0};
            var O, z;
            if (b.onTouchStart = function (e) {
                    if (e.originalEvent && (e = e.originalEvent), O = "touchstart" === e.type, O || !("which" in e) || 3 !== e.which) {
                        if (b.params.noSwiping && s(e, "." + b.params.noSwipingClass))return void(b.allowClick = !0);
                        if (!b.params.swipeHandler || s(e, b.params.swipeHandler)) {
                            var n = b.touches.currentX = "touchstart" === e.type ? e.targetTouches[0].pageX : e.pageX, i = b.touches.currentY = "touchstart" === e.type ? e.targetTouches[0].pageY : e.pageY;
                            if (!(b.device.ios && b.params.iOSEdgeSwipeDetection && n <= b.params.iOSEdgeSwipeThreshold)) {
                                if (T = !0, C = !1, S = !0, E = void 0, z = void 0, b.touches.startX = n, b.touches.startY = i, k = Date.now(), b.allowClick = !0, b.updateContainerSize(), b.swipeDirection = void 0, b.params.threshold > 0 && (D = !1), "touchstart" !== e.type) {
                                    var o = !0;
                                    t(e.target).is(A) && (o = !1), document.activeElement && t(document.activeElement).is(A) && document.activeElement.blur(), o && e.preventDefault()
                                }
                                b.emit("onTouchStart", b, e)
                            }
                        }
                    }
                }, b.onTouchMove = function (e) {
                    if (e.originalEvent && (e = e.originalEvent), !(O && "mousemove" === e.type || e.preventedByNestedSwiper)) {
                        if (b.params.onlyExternal)return b.allowClick = !1, void(T && (b.touches.startX = b.touches.currentX = "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX, b.touches.startY = b.touches.currentY = "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY, k = Date.now()));
                        if (O && document.activeElement && e.target === document.activeElement && t(e.target).is(A))return C = !0, void(b.allowClick = !1);
                        if (S && b.emit("onTouchMove", b, e), !(e.targetTouches && e.targetTouches.length > 1)) {
                            if (b.touches.currentX = "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX, b.touches.currentY = "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY, "undefined" == typeof E) {
                                var o = 180 * Math.atan2(Math.abs(b.touches.currentY - b.touches.startY), Math.abs(b.touches.currentX - b.touches.startX)) / Math.PI;
                                E = i() ? o > b.params.touchAngle : 90 - o > b.params.touchAngle
                            }
                            if (E && b.emit("onTouchMoveOpposite", b, e), "undefined" == typeof z && b.browser.ieTouch && (b.touches.currentX !== b.touches.startX || b.touches.currentY !== b.touches.startY) && (z = !0), T) {
                                if (E)return void(T = !1);
                                if (z || !b.browser.ieTouch) {
                                    b.allowClick = !1, b.emit("onSliderMove", b, e), e.preventDefault(), b.params.touchMoveStopPropagation && !b.params.nested && e.stopPropagation(), C || (n.loop && b.fixLoop(), I = b.getWrapperTranslate(), b.setWrapperTransition(0), b.animating && b.wrapper.trigger("webkitTransitionEnd transitionend oTransitionEnd MSTransitionEnd msTransitionEnd"), b.params.autoplay && b.autoplaying && (b.params.autoplayDisableOnInteraction ? b.stopAutoplay() : b.pauseAutoplay()), $ = !1, b.params.grabCursor && (b.container[0].style.cursor = "move", b.container[0].style.cursor = "-webkit-grabbing", b.container[0].style.cursor = "-moz-grabbin", b.container[0].style.cursor = "grabbing")), C = !0;
                                    var a = b.touches.diff = i() ? b.touches.currentX - b.touches.startX : b.touches.currentY - b.touches.startY;
                                    a *= b.params.touchRatio, b.rtl && (a = -a), b.swipeDirection = a > 0 ? "prev" : "next", P = a + I;
                                    var r = !0;
                                    if (a > 0 && P > b.minTranslate() ? (r = !1, b.params.resistance && (P = b.minTranslate() - 1 + Math.pow(-b.minTranslate() + I + a, b.params.resistanceRatio))) : 0 > a && P < b.maxTranslate() && (r = !1, b.params.resistance && (P = b.maxTranslate() + 1 - Math.pow(b.maxTranslate() - I - a, b.params.resistanceRatio))), r && (e.preventedByNestedSwiper = !0), !b.params.allowSwipeToNext && "next" === b.swipeDirection && I > P && (P = I), !b.params.allowSwipeToPrev && "prev" === b.swipeDirection && P > I && (P = I), b.params.followFinger) {
                                        if (b.params.threshold > 0) {
                                            if (!(Math.abs(a) > b.params.threshold || D))return void(P = I);
                                            if (!D)return D = !0, b.touches.startX = b.touches.currentX, b.touches.startY = b.touches.currentY, P = I, void(b.touches.diff = i() ? b.touches.currentX - b.touches.startX : b.touches.currentY - b.touches.startY)
                                        }
                                        (b.params.freeMode || b.params.watchSlidesProgress) && b.updateActiveIndex(), b.params.freeMode && (0 === M.length && M.push({
                                            position: b.touches[i() ? "startX" : "startY"],
                                            time: k
                                        }), M.push({
                                            position: b.touches[i() ? "currentX" : "currentY"],
                                            time: (new window.Date).getTime()
                                        })), b.updateProgress(P), b.setWrapperTranslate(P)
                                    }
                                }
                            }
                        }
                    }
                }, b.onTouchEnd = function (e) {
                    if (e.originalEvent && (e = e.originalEvent), S && b.emit("onTouchEnd", b, e), S = !1, T) {
                        b.params.grabCursor && C && T && (b.container[0].style.cursor = "move", b.container[0].style.cursor = "-webkit-grab", b.container[0].style.cursor = "-moz-grab", b.container[0].style.cursor = "grab");
                        var n = Date.now(), i = n - k;
                        if (b.allowClick && (b.updateClickedSlide(e), b.emit("onTap", b, e), 300 > i && n - L > 300 && (N && clearTimeout(N), N = setTimeout(function () {
                                b && (b.params.paginationHide && b.paginationContainer.length > 0 && !t(e.target).hasClass(b.params.bulletClass) && b.paginationContainer.toggleClass(b.params.paginationHiddenClass), b.emit("onClick", b, e))
                            }, 300)), 300 > i && 300 > n - L && (N && clearTimeout(N), b.emit("onDoubleTap", b, e))), L = Date.now(), setTimeout(function () {
                                b && (b.allowClick = !0)
                            }, 0), !T || !C || !b.swipeDirection || 0 === b.touches.diff || P === I)return void(T = C = !1);
                        T = C = !1;
                        var o;
                        if (o = b.params.followFinger ? b.rtl ? b.translate : -b.translate : -P, b.params.freeMode) {
                            if (o < -b.minTranslate())return void b.slideTo(b.activeIndex);
                            if (o > -b.maxTranslate())return void(b.slides.length < b.snapGrid.length ? b.slideTo(b.snapGrid.length - 1) : b.slideTo(b.slides.length - 1));
                            if (b.params.freeModeMomentum) {
                                if (M.length > 1) {
                                    var a = M.pop(), r = M.pop(), s = a.position - r.position, l = a.time - r.time;
                                    b.velocity = s / l, b.velocity = b.velocity / 2, Math.abs(b.velocity) < b.params.freeModeMinimumVelocity && (b.velocity = 0), (l > 150 || (new window.Date).getTime() - a.time > 300) && (b.velocity = 0)
                                } else b.velocity = 0;
                                M.length = 0;
                                var d = 1e3 * b.params.freeModeMomentumRatio, p = b.velocity * d, c = b.translate + p;
                                b.rtl && (c = -c);
                                var u, h = !1, f = 20 * Math.abs(b.velocity) * b.params.freeModeMomentumBounceRatio;
                                if (c < b.maxTranslate())b.params.freeModeMomentumBounce ? (c + b.maxTranslate() < -f && (c = b.maxTranslate() - f), u = b.maxTranslate(), h = !0, $ = !0) : c = b.maxTranslate(); else if (c > b.minTranslate())b.params.freeModeMomentumBounce ? (c - b.minTranslate() > f && (c = b.minTranslate() + f), u = b.minTranslate(), h = !0, $ = !0) : c = b.minTranslate(); else if (b.params.freeModeSticky) {
                                    var m, g = 0;
                                    for (g = 0; g < b.snapGrid.length; g += 1)if (b.snapGrid[g] > -c) {
                                        m = g;
                                        break
                                    }
                                    c = Math.abs(b.snapGrid[m] - c) < Math.abs(b.snapGrid[m - 1] - c) || "next" === b.swipeDirection ? b.snapGrid[m] : b.snapGrid[m - 1], b.rtl || (c = -c)
                                }
                                if (0 !== b.velocity)d = b.rtl ? Math.abs((-c - b.translate) / b.velocity) : Math.abs((c - b.translate) / b.velocity); else if (b.params.freeModeSticky)return void b.slideReset();
                                b.params.freeModeMomentumBounce && h ? (b.updateProgress(u), b.setWrapperTransition(d), b.setWrapperTranslate(c), b.onTransitionStart(), b.animating = !0, b.wrapper.transitionEnd(function () {
                                    b && $ && (b.emit("onMomentumBounce", b), b.setWrapperTransition(b.params.speed), b.setWrapperTranslate(u), b.wrapper.transitionEnd(function () {
                                        b && b.onTransitionEnd()
                                    }))
                                })) : b.velocity ? (b.updateProgress(c), b.setWrapperTransition(d), b.setWrapperTranslate(c), b.onTransitionStart(), b.animating || (b.animating = !0, b.wrapper.transitionEnd(function () {
                                    b && b.onTransitionEnd()
                                }))) : b.updateProgress(c), b.updateActiveIndex()
                            }
                            return void((!b.params.freeModeMomentum || i >= b.params.longSwipesMs) && (b.updateProgress(), b.updateActiveIndex()))
                        }
                        var v, y = 0, w = b.slidesSizesGrid[0];
                        for (v = 0; v < b.slidesGrid.length; v += b.params.slidesPerGroup)"undefined" != typeof b.slidesGrid[v + b.params.slidesPerGroup] ? o >= b.slidesGrid[v] && o < b.slidesGrid[v + b.params.slidesPerGroup] && (y = v, w = b.slidesGrid[v + b.params.slidesPerGroup] - b.slidesGrid[v]) : o >= b.slidesGrid[v] && (y = v, w = b.slidesGrid[b.slidesGrid.length - 1] - b.slidesGrid[b.slidesGrid.length - 2]);
                        var x = (o - b.slidesGrid[y]) / w;
                        if (i > b.params.longSwipesMs) {
                            if (!b.params.longSwipes)return void b.slideTo(b.activeIndex);
                            "next" === b.swipeDirection && (x >= b.params.longSwipesRatio ? b.slideTo(y + b.params.slidesPerGroup) : b.slideTo(y)), "prev" === b.swipeDirection && (x > 1 - b.params.longSwipesRatio ? b.slideTo(y + b.params.slidesPerGroup) : b.slideTo(y))
                        } else {
                            if (!b.params.shortSwipes)return void b.slideTo(b.activeIndex);
                            "next" === b.swipeDirection && b.slideTo(y + b.params.slidesPerGroup), "prev" === b.swipeDirection && b.slideTo(y)
                        }
                    }
                }, b._slideTo = function (e, t) {
                    return b.slideTo(e, t, !0, !0)
                }, b.slideTo = function (e, t, n, i) {
                    "undefined" == typeof n && (n = !0), "undefined" == typeof e && (e = 0), 0 > e && (e = 0), b.snapIndex = Math.floor(e / b.params.slidesPerGroup), b.snapIndex >= b.snapGrid.length && (b.snapIndex = b.snapGrid.length - 1);
                    var o = -b.snapGrid[b.snapIndex];
                    b.params.autoplay && b.autoplaying && (i || !b.params.autoplayDisableOnInteraction ? b.pauseAutoplay(t) : b.stopAutoplay()), b.updateProgress(o);
                    for (var a = 0; a < b.slidesGrid.length; a++)-Math.floor(100 * o) >= Math.floor(100 * b.slidesGrid[a]) && (e = a);
                    return !b.params.allowSwipeToNext && o < b.translate && o < b.minTranslate() ? !1 : !b.params.allowSwipeToPrev && o > b.translate && o > b.maxTranslate() && (b.activeIndex || 0) !== e ? !1 : ("undefined" == typeof t && (t = b.params.speed), b.previousIndex = b.activeIndex || 0, b.activeIndex = e, b.params.autoHeight && b.updateAutoHeight(), o === b.translate ? (b.updateClasses(), "slide" !== b.params.effect && b.setWrapperTranslate(o), !1) : (b.updateClasses(), b.onTransitionStart(n), 0 === t ? (b.setWrapperTransition(0), b.setWrapperTranslate(o), b.onTransitionEnd(n)) : (b.setWrapperTransition(t), b.setWrapperTranslate(o), b.animating || (b.animating = !0, b.wrapper.transitionEnd(function () {
                        b && b.onTransitionEnd(n)
                    }))), !0))
                }, b.onTransitionStart = function (e) {
                    "undefined" == typeof e && (e = !0), b.lazy && b.lazy.onTransitionStart(), e && (b.emit("onTransitionStart", b), b.activeIndex !== b.previousIndex && (b.emit("onSlideChangeStart", b), b.activeIndex > b.previousIndex ? b.emit("onSlideNextStart", b) : b.emit("onSlidePrevStart", b)))
                }, b.onTransitionEnd = function (e) {
                    b.animating = !1, b.setWrapperTransition(0), "undefined" == typeof e && (e = !0), b.lazy && b.lazy.onTransitionEnd(), e && (b.emit("onTransitionEnd", b), b.activeIndex !== b.previousIndex && (b.emit("onSlideChangeEnd", b), b.activeIndex > b.previousIndex ? b.emit("onSlideNextEnd", b) : b.emit("onSlidePrevEnd", b))), b.params.hashnav && b.hashnav && b.hashnav.setHash()
                }, b.slideNext = function (e, t, n) {
                    return b.params.loop ? b.animating ? !1 : (b.fixLoop(), b.container[0].clientLeft, b.slideTo(b.activeIndex + b.params.slidesPerGroup, t, e, n)) : b.slideTo(b.activeIndex + b.params.slidesPerGroup, t, e, n)
                }, b._slideNext = function (e) {
                    return b.slideNext(!0, e, !0)
                }, b.slidePrev = function (e, t, n) {
                    return b.params.loop ? b.animating ? !1 : (b.fixLoop(), b.container[0].clientLeft, b.slideTo(b.activeIndex - 1, t, e, n)) : b.slideTo(b.activeIndex - 1, t, e, n)
                }, b._slidePrev = function (e) {
                    return b.slidePrev(!0, e, !0)
                }, b.slideReset = function (e, t, n) {
                    return b.slideTo(b.activeIndex, t, e)
                }, b.setWrapperTransition = function (e, t) {
                    b.wrapper.transition(e), "slide" !== b.params.effect && b.effects[b.params.effect] && b.effects[b.params.effect].setTransition(e), b.params.parallax && b.parallax && b.parallax.setTransition(e), b.params.scrollbar && b.scrollbar && b.scrollbar.setTransition(e), b.params.control && b.controller && b.controller.setTransition(e, t), b.emit("onSetTransition", b, e)
                }, b.setWrapperTranslate = function (e, t, n) {
                    var a = 0, r = 0, s = 0;
                    i() ? a = b.rtl ? -e : e : r = e, b.params.roundLengths && (a = o(a), r = o(r)), b.params.virtualTranslate || (b.support.transforms3d ? b.wrapper.transform("translate3d(" + a + "px, " + r + "px, " + s + "px)") : b.wrapper.transform("translate(" + a + "px, " + r + "px)")), b.translate = i() ? a : r;
                    var l, d = b.maxTranslate() - b.minTranslate();
                    l = 0 === d ? 0 : (e - b.minTranslate()) / d, l !== b.progress && b.updateProgress(e), t && b.updateActiveIndex(), "slide" !== b.params.effect && b.effects[b.params.effect] && b.effects[b.params.effect].setTranslate(b.translate), b.params.parallax && b.parallax && b.parallax.setTranslate(b.translate), b.params.scrollbar && b.scrollbar && b.scrollbar.setTranslate(b.translate), b.params.control && b.controller && b.controller.setTranslate(b.translate, n), b.emit("onSetTranslate", b, b.translate)
                }, b.getTranslate = function (e, t) {
                    var n, i, o, a;
                    return "undefined" == typeof t && (t = "x"), b.params.virtualTranslate ? b.rtl ? -b.translate : b.translate : (o = window.getComputedStyle(e, null), window.WebKitCSSMatrix ? (i = o.transform || o.webkitTransform, i.split(",").length > 6 && (i = i.split(", ").map(function (e) {
                        return e.replace(",", ".")
                    }).join(", ")), a = new window.WebKitCSSMatrix("none" === i ? "" : i)) : (a = o.MozTransform || o.OTransform || o.MsTransform || o.msTransform || o.transform || o.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,"), n = a.toString().split(",")), "x" === t && (i = window.WebKitCSSMatrix ? a.m41 : 16 === n.length ? parseFloat(n[12]) : parseFloat(n[4])), "y" === t && (i = window.WebKitCSSMatrix ? a.m42 : 16 === n.length ? parseFloat(n[13]) : parseFloat(n[5])),
                    b.rtl && i && (i = -i), i || 0)
                }, b.getWrapperTranslate = function (e) {
                    return "undefined" == typeof e && (e = i() ? "x" : "y"), b.getTranslate(b.wrapper[0], e)
                }, b.observers = [], b.initObservers = function () {
                    if (b.params.observeParents)for (var e = b.container.parents(), t = 0; t < e.length; t++)l(e[t]);
                    l(b.container[0], {childList: !1}), l(b.wrapper[0], {attributes: !1})
                }, b.disconnectObservers = function () {
                    for (var e = 0; e < b.observers.length; e++)b.observers[e].disconnect();
                    b.observers = []
                }, b.createLoop = function () {
                    b.wrapper.children("." + b.params.slideClass + "." + b.params.slideDuplicateClass).remove();
                    var e = b.wrapper.children("." + b.params.slideClass);
                    "auto" !== b.params.slidesPerView || b.params.loopedSlides || (b.params.loopedSlides = e.length), b.loopedSlides = parseInt(b.params.loopedSlides || b.params.slidesPerView, 10), b.loopedSlides = b.loopedSlides + b.params.loopAdditionalSlides, b.loopedSlides > e.length && (b.loopedSlides = e.length);
                    var n, i = [], o = [];
                    for (e.each(function (n, a) {
                        var r = t(this);
                        n < b.loopedSlides && o.push(a), n < e.length && n >= e.length - b.loopedSlides && i.push(a), r.attr("data-swiper-slide-index", n)
                    }), n = 0; n < o.length; n++)b.wrapper.append(t(o[n].cloneNode(!0)).addClass(b.params.slideDuplicateClass));
                    for (n = i.length - 1; n >= 0; n--)b.wrapper.prepend(t(i[n].cloneNode(!0)).addClass(b.params.slideDuplicateClass))
                }, b.destroyLoop = function () {
                    b.wrapper.children("." + b.params.slideClass + "." + b.params.slideDuplicateClass).remove(), b.slides.removeAttr("data-swiper-slide-index")
                }, b.fixLoop = function () {
                    var e;
                    b.activeIndex < b.loopedSlides ? (e = b.slides.length - 3 * b.loopedSlides + b.activeIndex, e += b.loopedSlides, b.slideTo(e, 0, !1, !0)) : ("auto" === b.params.slidesPerView && b.activeIndex >= 2 * b.loopedSlides || b.activeIndex > b.slides.length - 2 * b.params.slidesPerView) && (e = -b.slides.length + b.activeIndex + b.loopedSlides, e += b.loopedSlides, b.slideTo(e, 0, !1, !0))
                }, b.appendSlide = function (e) {
                    if (b.params.loop && b.destroyLoop(), "object" == typeof e && e.length)for (var t = 0; t < e.length; t++)e[t] && b.wrapper.append(e[t]); else b.wrapper.append(e);
                    b.params.loop && b.createLoop(), b.params.observer && b.support.observer || b.update(!0)
                }, b.prependSlide = function (e) {
                    b.params.loop && b.destroyLoop();
                    var t = b.activeIndex + 1;
                    if ("object" == typeof e && e.length) {
                        for (var n = 0; n < e.length; n++)e[n] && b.wrapper.prepend(e[n]);
                        t = b.activeIndex + e.length
                    } else b.wrapper.prepend(e);
                    b.params.loop && b.createLoop(), b.params.observer && b.support.observer || b.update(!0), b.slideTo(t, 0, !1)
                }, b.removeSlide = function (e) {
                    b.params.loop && (b.destroyLoop(), b.slides = b.wrapper.children("." + b.params.slideClass));
                    var t, n = b.activeIndex;
                    if ("object" == typeof e && e.length) {
                        for (var i = 0; i < e.length; i++)t = e[i], b.slides[t] && b.slides.eq(t).remove(), n > t && n--;
                        n = Math.max(n, 0)
                    } else t = e, b.slides[t] && b.slides.eq(t).remove(), n > t && n--, n = Math.max(n, 0);
                    b.params.loop && b.createLoop(), b.params.observer && b.support.observer || b.update(!0), b.params.loop ? b.slideTo(n + b.loopedSlides, 0, !1) : b.slideTo(n, 0, !1)
                }, b.removeAllSlides = function () {
                    for (var e = [], t = 0; t < b.slides.length; t++)e.push(t);
                    b.removeSlide(e)
                }, b.effects = {
                    fade: {
                        setTranslate: function () {
                            for (var e = 0; e < b.slides.length; e++) {
                                var t = b.slides.eq(e), n = t[0].swiperSlideOffset, o = -n;
                                b.params.virtualTranslate || (o -= b.translate);
                                var a = 0;
                                i() || (a = o, o = 0);
                                var r = b.params.fade.crossFade ? Math.max(1 - Math.abs(t[0].progress), 0) : 1 + Math.min(Math.max(t[0].progress, -1), 0);
                                t.css({opacity: r}).transform("translate3d(" + o + "px, " + a + "px, 0px)")
                            }
                        }, setTransition: function (e) {
                            if (b.slides.transition(e), b.params.virtualTranslate && 0 !== e) {
                                var t = !1;
                                b.slides.transitionEnd(function () {
                                    if (!t && b) {
                                        t = !0, b.animating = !1;
                                        for (var e = ["webkitTransitionEnd", "transitionend", "oTransitionEnd", "MSTransitionEnd", "msTransitionEnd"], n = 0; n < e.length; n++)b.wrapper.trigger(e[n])
                                    }
                                })
                            }
                        }
                    }, cube: {
                        setTranslate: function () {
                            var e, n = 0;
                            b.params.cube.shadow && (i() ? (e = b.wrapper.find(".swiper-cube-shadow"), 0 === e.length && (e = t('<div class="swiper-cube-shadow"></div>'), b.wrapper.append(e)), e.css({height: b.width + "px"})) : (e = b.container.find(".swiper-cube-shadow"), 0 === e.length && (e = t('<div class="swiper-cube-shadow"></div>'), b.container.append(e))));
                            for (var o = 0; o < b.slides.length; o++) {
                                var a = b.slides.eq(o), r = 90 * o, s = Math.floor(r / 360);
                                b.rtl && (r = -r, s = Math.floor(-r / 360));
                                var l = Math.max(Math.min(a[0].progress, 1), -1), d = 0, p = 0, c = 0;
                                o % 4 === 0 ? (d = 4 * -s * b.size, c = 0) : (o - 1) % 4 === 0 ? (d = 0, c = 4 * -s * b.size) : (o - 2) % 4 === 0 ? (d = b.size + 4 * s * b.size, c = b.size) : (o - 3) % 4 === 0 && (d = -b.size, c = 3 * b.size + 4 * b.size * s), b.rtl && (d = -d), i() || (p = d, d = 0);
                                var u = "rotateX(" + (i() ? 0 : -r) + "deg) rotateY(" + (i() ? r : 0) + "deg) translate3d(" + d + "px, " + p + "px, " + c + "px)";
                                if (1 >= l && l > -1 && (n = 90 * o + 90 * l, b.rtl && (n = 90 * -o - 90 * l)), a.transform(u), b.params.cube.slideShadows) {
                                    var h = i() ? a.find(".swiper-slide-shadow-left") : a.find(".swiper-slide-shadow-top"), f = i() ? a.find(".swiper-slide-shadow-right") : a.find(".swiper-slide-shadow-bottom");
                                    0 === h.length && (h = t('<div class="swiper-slide-shadow-' + (i() ? "left" : "top") + '"></div>'), a.append(h)), 0 === f.length && (f = t('<div class="swiper-slide-shadow-' + (i() ? "right" : "bottom") + '"></div>'), a.append(f)), a[0].progress, h.length && (h[0].style.opacity = -a[0].progress), f.length && (f[0].style.opacity = a[0].progress)
                                }
                            }
                            if (b.wrapper.css({
                                    "-webkit-transform-origin": "50% 50% -" + b.size / 2 + "px",
                                    "-moz-transform-origin": "50% 50% -" + b.size / 2 + "px",
                                    "-ms-transform-origin": "50% 50% -" + b.size / 2 + "px",
                                    "transform-origin": "50% 50% -" + b.size / 2 + "px"
                                }), b.params.cube.shadow)if (i())e.transform("translate3d(0px, " + (b.width / 2 + b.params.cube.shadowOffset) + "px, " + -b.width / 2 + "px) rotateX(90deg) rotateZ(0deg) scale(" + b.params.cube.shadowScale + ")"); else {
                                var m = Math.abs(n) - 90 * Math.floor(Math.abs(n) / 90), g = 1.5 - (Math.sin(2 * m * Math.PI / 360) / 2 + Math.cos(2 * m * Math.PI / 360) / 2), v = b.params.cube.shadowScale, y = b.params.cube.shadowScale / g, w = b.params.cube.shadowOffset;
                                e.transform("scale3d(" + v + ", 1, " + y + ") translate3d(0px, " + (b.height / 2 + w) + "px, " + -b.height / 2 / y + "px) rotateX(-90deg)")
                            }
                            var x = b.isSafari || b.isUiWebView ? -b.size / 2 : 0;
                            b.wrapper.transform("translate3d(0px,0," + x + "px) rotateX(" + (i() ? 0 : n) + "deg) rotateY(" + (i() ? -n : 0) + "deg)")
                        }, setTransition: function (e) {
                            b.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), b.params.cube.shadow && !i() && b.container.find(".swiper-cube-shadow").transition(e)
                        }
                    }, coverflow: {
                        setTranslate: function () {
                            for (var e = b.translate, n = i() ? -e + b.width / 2 : -e + b.height / 2, o = i() ? b.params.coverflow.rotate : -b.params.coverflow.rotate, a = b.params.coverflow.depth, r = 0, s = b.slides.length; s > r; r++) {
                                var l = b.slides.eq(r), d = b.slidesSizesGrid[r], p = l[0].swiperSlideOffset, c = (n - p - d / 2) / d * b.params.coverflow.modifier, u = i() ? o * c : 0, h = i() ? 0 : o * c, f = -a * Math.abs(c), m = i() ? 0 : b.params.coverflow.stretch * c, g = i() ? b.params.coverflow.stretch * c : 0;
                                Math.abs(g) < .001 && (g = 0), Math.abs(m) < .001 && (m = 0), Math.abs(f) < .001 && (f = 0), Math.abs(u) < .001 && (u = 0), Math.abs(h) < .001 && (h = 0);
                                var v = "translate3d(" + g + "px," + m + "px," + f + "px)  rotateX(" + h + "deg) rotateY(" + u + "deg)";
                                if (l.transform(v), l[0].style.zIndex = -Math.abs(Math.round(c)) + 1, b.params.coverflow.slideShadows) {
                                    var y = i() ? l.find(".swiper-slide-shadow-left") : l.find(".swiper-slide-shadow-top"), w = i() ? l.find(".swiper-slide-shadow-right") : l.find(".swiper-slide-shadow-bottom");
                                    0 === y.length && (y = t('<div class="swiper-slide-shadow-' + (i() ? "left" : "top") + '"></div>'), l.append(y)), 0 === w.length && (w = t('<div class="swiper-slide-shadow-' + (i() ? "right" : "bottom") + '"></div>'), l.append(w)), y.length && (y[0].style.opacity = c > 0 ? c : 0), w.length && (w[0].style.opacity = -c > 0 ? -c : 0)
                                }
                            }
                            if (b.browser.ie) {
                                var x = b.wrapper[0].style;
                                x.perspectiveOrigin = n + "px 50%"
                            }
                        }, setTransition: function (e) {
                            b.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e)
                        }
                    }
                }, b.lazy = {
                    initialImageLoaded: !1, loadImageInSlide: function (e, n) {
                        if ("undefined" != typeof e && ("undefined" == typeof n && (n = !0), 0 !== b.slides.length)) {
                            var i = b.slides.eq(e), o = i.find(".swiper-lazy:not(.swiper-lazy-loaded):not(.swiper-lazy-loading)");
                            !i.hasClass("swiper-lazy") || i.hasClass("swiper-lazy-loaded") || i.hasClass("swiper-lazy-loading") || (o = o.add(i[0])), 0 !== o.length && o.each(function () {
                                var e = t(this);
                                e.addClass("swiper-lazy-loading");
                                var o = e.attr("data-background"), a = e.attr("data-src"), r = e.attr("data-srcset");
                                b.loadImage(e[0], a || o, r, !1, function () {
                                    if (o ? (e.css("background-image", "url(" + o + ")"), e.removeAttr("data-background")) : (r && (e.attr("srcset", r), e.removeAttr("data-srcset")), a && (e.attr("src", a), e.removeAttr("data-src"))), e.addClass("swiper-lazy-loaded").removeClass("swiper-lazy-loading"), i.find(".swiper-lazy-preloader, .preloader").remove(), b.params.loop && n) {
                                        var t = i.attr("data-swiper-slide-index");
                                        if (i.hasClass(b.params.slideDuplicateClass)) {
                                            var s = b.wrapper.children('[data-swiper-slide-index="' + t + '"]:not(.' + b.params.slideDuplicateClass + ")");
                                            b.lazy.loadImageInSlide(s.index(), !1)
                                        } else {
                                            var l = b.wrapper.children("." + b.params.slideDuplicateClass + '[data-swiper-slide-index="' + t + '"]');
                                            b.lazy.loadImageInSlide(l.index(), !1)
                                        }
                                    }
                                    b.emit("onLazyImageReady", b, i[0], e[0])
                                }), b.emit("onLazyImageLoad", b, i[0], e[0])
                            })
                        }
                    }, load: function () {
                        var e;
                        if (b.params.watchSlidesVisibility)b.wrapper.children("." + b.params.slideVisibleClass).each(function () {
                            b.lazy.loadImageInSlide(t(this).index())
                        }); else if (b.params.slidesPerView > 1)for (e = b.activeIndex; e < b.activeIndex + b.params.slidesPerView; e++)b.slides[e] && b.lazy.loadImageInSlide(e); else b.lazy.loadImageInSlide(b.activeIndex);
                        if (b.params.lazyLoadingInPrevNext)if (b.params.slidesPerView > 1) {
                            for (e = b.activeIndex + b.params.slidesPerView; e < b.activeIndex + b.params.slidesPerView + b.params.slidesPerView; e++)b.slides[e] && b.lazy.loadImageInSlide(e);
                            for (e = b.activeIndex - b.params.slidesPerView; e < b.activeIndex; e++)b.slides[e] && b.lazy.loadImageInSlide(e)
                        } else {
                            var n = b.wrapper.children("." + b.params.slideNextClass);
                            n.length > 0 && b.lazy.loadImageInSlide(n.index());
                            var i = b.wrapper.children("." + b.params.slidePrevClass);
                            i.length > 0 && b.lazy.loadImageInSlide(i.index())
                        }
                    }, onTransitionStart: function () {
                        b.params.lazyLoading && (b.params.lazyLoadingOnTransitionStart || !b.params.lazyLoadingOnTransitionStart && !b.lazy.initialImageLoaded) && b.lazy.load()
                    }, onTransitionEnd: function () {
                        b.params.lazyLoading && !b.params.lazyLoadingOnTransitionStart && b.lazy.load()
                    }
                }, b.scrollbar = {
                    isTouched: !1, setDragPosition: function (e) {
                        var t = b.scrollbar, n = i() ? "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX || e.clientX : "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY || e.clientY, o = n - t.track.offset()[i() ? "left" : "top"] - t.dragSize / 2, a = -b.minTranslate() * t.moveDivider, r = -b.maxTranslate() * t.moveDivider;
                        a > o ? o = a : o > r && (o = r), o = -o / t.moveDivider, b.updateProgress(o), b.setWrapperTranslate(o, !0)
                    }, dragStart: function (e) {
                        var t = b.scrollbar;
                        t.isTouched = !0, e.preventDefault(), e.stopPropagation(), t.setDragPosition(e), clearTimeout(t.dragTimeout), t.track.transition(0), b.params.scrollbarHide && t.track.css("opacity", 1), b.wrapper.transition(100), t.drag.transition(100), b.emit("onScrollbarDragStart", b)
                    }, dragMove: function (e) {
                        var t = b.scrollbar;
                        t.isTouched && (e.preventDefault ? e.preventDefault() : e.returnValue = !1, t.setDragPosition(e), b.wrapper.transition(0), t.track.transition(0), t.drag.transition(0), b.emit("onScrollbarDragMove", b))
                    }, dragEnd: function (e) {
                        var t = b.scrollbar;
                        t.isTouched && (t.isTouched = !1, b.params.scrollbarHide && (clearTimeout(t.dragTimeout), t.dragTimeout = setTimeout(function () {
                            t.track.css("opacity", 0), t.track.transition(400)
                        }, 1e3)), b.emit("onScrollbarDragEnd", b), b.params.scrollbarSnapOnRelease && b.slideReset())
                    }, enableDraggable: function () {
                        var e = b.scrollbar, n = b.support.touch ? e.track : document;
                        t(e.track).on(b.touchEvents.start, e.dragStart), t(n).on(b.touchEvents.move, e.dragMove), t(n).on(b.touchEvents.end, e.dragEnd)
                    }, disableDraggable: function () {
                        var e = b.scrollbar, n = b.support.touch ? e.track : document;
                        t(e.track).off(b.touchEvents.start, e.dragStart), t(n).off(b.touchEvents.move, e.dragMove), t(n).off(b.touchEvents.end, e.dragEnd)
                    }, set: function () {
                        if (b.params.scrollbar) {
                            var e = b.scrollbar;
                            e.track = t(b.params.scrollbar), e.drag = e.track.find(".swiper-scrollbar-drag"), 0 === e.drag.length && (e.drag = t('<div class="swiper-scrollbar-drag"></div>'), e.track.append(e.drag)), e.drag[0].style.width = "", e.drag[0].style.height = "", e.trackSize = i() ? e.track[0].offsetWidth : e.track[0].offsetHeight, e.divider = b.size / b.virtualSize, e.moveDivider = e.divider * (e.trackSize / b.size), e.dragSize = e.trackSize * e.divider, i() ? e.drag[0].style.width = e.dragSize + "px" : e.drag[0].style.height = e.dragSize + "px", e.divider >= 1 ? e.track[0].style.display = "none" : e.track[0].style.display = "", b.params.scrollbarHide && (e.track[0].style.opacity = 0)
                        }
                    }, setTranslate: function () {
                        if (b.params.scrollbar) {
                            var e, t = b.scrollbar, n = (b.translate || 0, t.dragSize);
                            e = (t.trackSize - t.dragSize) * b.progress, b.rtl && i() ? (e = -e, e > 0 ? (n = t.dragSize - e, e = 0) : -e + t.dragSize > t.trackSize && (n = t.trackSize + e)) : 0 > e ? (n = t.dragSize + e, e = 0) : e + t.dragSize > t.trackSize && (n = t.trackSize - e), i() ? (b.support.transforms3d ? t.drag.transform("translate3d(" + e + "px, 0, 0)") : t.drag.transform("translateX(" + e + "px)"), t.drag[0].style.width = n + "px") : (b.support.transforms3d ? t.drag.transform("translate3d(0px, " + e + "px, 0)") : t.drag.transform("translateY(" + e + "px)"), t.drag[0].style.height = n + "px"), b.params.scrollbarHide && (clearTimeout(t.timeout), t.track[0].style.opacity = 1, t.timeout = setTimeout(function () {
                                t.track[0].style.opacity = 0, t.track.transition(400)
                            }, 1e3))
                        }
                    }, setTransition: function (e) {
                        b.params.scrollbar && b.scrollbar.drag.transition(e)
                    }
                }, b.controller = {
                    LinearSpline: function (e, t) {
                        this.x = e, this.y = t, this.lastIndex = e.length - 1;
                        var n, i;
                        this.x.length, this.interpolate = function (e) {
                            return e ? (i = o(this.x, e), n = i - 1, (e - this.x[n]) * (this.y[i] - this.y[n]) / (this.x[i] - this.x[n]) + this.y[n]) : 0
                        };
                        var o = function () {
                            var e, t, n;
                            return function (i, o) {
                                for (t = -1, e = i.length; e - t > 1;)i[n = e + t >> 1] <= o ? t = n : e = n;
                                return e
                            }
                        }()
                    }, getInterpolateFunction: function (e) {
                        b.controller.spline || (b.controller.spline = b.params.loop ? new b.controller.LinearSpline(b.slidesGrid, e.slidesGrid) : new b.controller.LinearSpline(b.snapGrid, e.snapGrid))
                    }, setTranslate: function (e, t) {
                        function n(t) {
                            e = t.rtl && "horizontal" === t.params.direction ? -b.translate : b.translate, "slide" === b.params.controlBy && (b.controller.getInterpolateFunction(t), o = -b.controller.spline.interpolate(-e)), o && "container" !== b.params.controlBy || (i = (t.maxTranslate() - t.minTranslate()) / (b.maxTranslate() - b.minTranslate()), o = (e - b.minTranslate()) * i + t.minTranslate()), b.params.controlInverse && (o = t.maxTranslate() - o), t.updateProgress(o), t.setWrapperTranslate(o, !1, b), t.updateActiveIndex()
                        }

                        var i, o, a = b.params.control;
                        if (b.isArray(a))for (var s = 0; s < a.length; s++)a[s] !== t && a[s] instanceof r && n(a[s]); else a instanceof r && t !== a && n(a)
                    }, setTransition: function (e, t) {
                        function n(t) {
                            t.setWrapperTransition(e, b), 0 !== e && (t.onTransitionStart(), t.wrapper.transitionEnd(function () {
                                o && (t.params.loop && "slide" === b.params.controlBy && t.fixLoop(), t.onTransitionEnd())
                            }))
                        }

                        var i, o = b.params.control;
                        if (b.isArray(o))for (i = 0; i < o.length; i++)o[i] !== t && o[i] instanceof r && n(o[i]); else o instanceof r && t !== o && n(o)
                    }
                }, b.hashnav = {
                    init: function () {
                        if (b.params.hashnav) {
                            b.hashnav.initialized = !0;
                            var e = document.location.hash.replace("#", "");
                            if (e)for (var t = 0, n = 0, i = b.slides.length; i > n; n++) {
                                var o = b.slides.eq(n), a = o.attr("data-hash");
                                if (a === e && !o.hasClass(b.params.slideDuplicateClass)) {
                                    var r = o.index();
                                    b.slideTo(r, t, b.params.runCallbacksOnInit, !0)
                                }
                            }
                        }
                    }, setHash: function () {
                        b.hashnav.initialized && b.params.hashnav && (document.location.hash = b.slides.eq(b.activeIndex).attr("data-hash") || "")
                    }
                }, b.disableKeyboardControl = function () {
                    t(document).off("keydown", d)
                }, b.enableKeyboardControl = function () {
                    t(document).on("keydown", d)
                }, b.mousewheel = {
                    event: !1,
                    lastScrollTime: (new window.Date).getTime()
                }, b.params.mousewheelControl) {
                try {
                    new window.WheelEvent("wheel"), b.mousewheel.event = "wheel"
                } catch (_) {
                }
                b.mousewheel.event || void 0 === document.onmousewheel || (b.mousewheel.event = "mousewheel"), b.mousewheel.event || (b.mousewheel.event = "DOMMouseScroll")
            }
            b.disableMousewheelControl = function () {
                return b.mousewheel.event ? (b.container.off(b.mousewheel.event, p), !0) : !1
            }, b.enableMousewheelControl = function () {
                return b.mousewheel.event ? (b.container.on(b.mousewheel.event, p), !0) : !1
            }, b.parallax = {
                setTranslate: function () {
                    b.container.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function () {
                        c(this, b.progress)
                    }), b.slides.each(function () {
                        var e = t(this);
                        e.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function () {
                            var t = Math.min(Math.max(e[0].progress, -1), 1);
                            c(this, t)
                        })
                    })
                }, setTransition: function (e) {
                    "undefined" == typeof e && (e = b.params.speed), b.container.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function () {
                        var n = t(this), i = parseInt(n.attr("data-swiper-parallax-duration"), 10) || e;
                        0 === e && (i = 0), n.transition(i)
                    })
                }
            }, b._plugins = [];
            for (var j in b.plugins) {
                var R = b.plugins[j](b, b.params[j]);
                R && b._plugins.push(R)
            }
            return b.callPlugins = function (e) {
                for (var t = 0; t < b._plugins.length; t++)e in b._plugins[t] && b._plugins[t][e](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5])
            }, b.emitterEventListeners = {}, b.emit = function (e) {
                b.params[e] && b.params[e](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
                var t;
                if (b.emitterEventListeners[e])for (t = 0; t < b.emitterEventListeners[e].length; t++)b.emitterEventListeners[e][t](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
                b.callPlugins && b.callPlugins(e, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5])
            }, b.on = function (e, t) {
                return e = u(e), b.emitterEventListeners[e] || (b.emitterEventListeners[e] = []), b.emitterEventListeners[e].push(t), b
            }, b.off = function (e, t) {
                var n;
                if (e = u(e), "undefined" == typeof t)return b.emitterEventListeners[e] = [], b;
                if (b.emitterEventListeners[e] && 0 !== b.emitterEventListeners[e].length) {
                    for (n = 0; n < b.emitterEventListeners[e].length; n++)b.emitterEventListeners[e][n] === t && b.emitterEventListeners[e].splice(n, 1);
                    return b
                }
            }, b.once = function (e, t) {
                e = u(e);
                var n = function i() {
                    t(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]), b.off(e, i)
                };
                return b.on(e, n), b
            }, b.a11y = {
                makeFocusable: function (e) {
                    return e.attr("tabIndex", "0"), e
                },
                addRole: function (e, t) {
                    return e.attr("role", t), e
                },
                addLabel: function (e, t) {
                    return e.attr("aria-label", t), e
                },
                disable: function (e) {
                    return e.attr("aria-disabled", !0), e
                },
                enable: function (e) {
                    return e.attr("aria-disabled", !1), e
                },
                onEnterKey: function (e) {
                    13 === e.keyCode && (t(e.target).is(b.params.nextButton) ? (b.onClickNext(e), b.isEnd ? b.a11y.notify(b.params.lastSlideMessage) : b.a11y.notify(b.params.nextSlideMessage)) : t(e.target).is(b.params.prevButton) && (b.onClickPrev(e), b.isBeginning ? b.a11y.notify(b.params.firstSlideMessage) : b.a11y.notify(b.params.prevSlideMessage)), t(e.target).is("." + b.params.bulletClass) && t(e.target)[0].click())
                },
                liveRegion: t('<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>'),
                notify: function (e) {
                    var t = b.a11y.liveRegion;
                    0 !== t.length && (t.html(""), t.html(e))
                },
                init: function () {
                    if (b.params.nextButton) {
                        var e = t(b.params.nextButton);
                        b.a11y.makeFocusable(e), b.a11y.addRole(e, "button"), b.a11y.addLabel(e, b.params.nextSlideMessage)
                    }
                    if (b.params.prevButton) {
                        var n = t(b.params.prevButton);
                        b.a11y.makeFocusable(n), b.a11y.addRole(n, "button"), b.a11y.addLabel(n, b.params.prevSlideMessage)
                    }
                    t(b.container).append(b.a11y.liveRegion)
                },
                initPagination: function () {
                    b.params.pagination && b.params.paginationClickable && b.bullets && b.bullets.length && b.bullets.each(function () {
                        var e = t(this);
                        b.a11y.makeFocusable(e), b.a11y.addRole(e, "button"), b.a11y.addLabel(e, b.params.paginationBulletMessage.replace(/{{index}}/, e.index() + 1))
                    })
                },
                destroy: function () {
                    b.a11y.liveRegion && b.a11y.liveRegion.length > 0 && b.a11y.liveRegion.remove()
                }
            }, b.init = function () {
                b.params.loop && b.createLoop(), b.updateContainerSize(), b.updateSlidesSize(), b.updatePagination(), b.params.scrollbar && b.scrollbar && (b.scrollbar.set(), b.params.scrollbarDraggable && b.scrollbar.enableDraggable()), "slide" !== b.params.effect && b.effects[b.params.effect] && (b.params.loop || b.updateProgress(), b.effects[b.params.effect].setTranslate()), b.params.loop ? b.slideTo(b.params.initialSlide + b.loopedSlides, 0, b.params.runCallbacksOnInit) : (b.slideTo(b.params.initialSlide, 0, b.params.runCallbacksOnInit), 0 === b.params.initialSlide && (b.parallax && b.params.parallax && b.parallax.setTranslate(), b.lazy && b.params.lazyLoading && (b.lazy.load(), b.lazy.initialImageLoaded = !0))), b.attachEvents(), b.params.observer && b.support.observer && b.initObservers(), b.params.preloadImages && !b.params.lazyLoading && b.preloadImages(), b.params.autoplay && b.startAutoplay(), b.params.keyboardControl && b.enableKeyboardControl && b.enableKeyboardControl(), b.params.mousewheelControl && b.enableMousewheelControl && b.enableMousewheelControl(), b.params.hashnav && b.hashnav && b.hashnav.init(), b.params.a11y && b.a11y && b.a11y.init(), b.emit("onInit", b)
            }, b.cleanupStyles = function () {
                b.container.removeClass(b.classNames.join(" ")).removeAttr("style"), b.wrapper.removeAttr("style"), b.slides && b.slides.length && b.slides.removeClass([b.params.slideVisibleClass, b.params.slideActiveClass, b.params.slideNextClass, b.params.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-column").removeAttr("data-swiper-row"), b.paginationContainer && b.paginationContainer.length && b.paginationContainer.removeClass(b.params.paginationHiddenClass), b.bullets && b.bullets.length && b.bullets.removeClass(b.params.bulletActiveClass), b.params.prevButton && t(b.params.prevButton).removeClass(b.params.buttonDisabledClass), b.params.nextButton && t(b.params.nextButton).removeClass(b.params.buttonDisabledClass), b.params.scrollbar && b.scrollbar && (b.scrollbar.track && b.scrollbar.track.length && b.scrollbar.track.removeAttr("style"), b.scrollbar.drag && b.scrollbar.drag.length && b.scrollbar.drag.removeAttr("style"))
            }, b.destroy = function (e, t) {
                b.detachEvents(), b.stopAutoplay(), b.params.scrollbar && b.scrollbar && b.params.scrollbarDraggable && b.scrollbar.disableDraggable(), b.params.loop && b.destroyLoop(), t && b.cleanupStyles(), b.disconnectObservers(), b.params.keyboardControl && b.disableKeyboardControl && b.disableKeyboardControl(), b.params.mousewheelControl && b.disableMousewheelControl && b.disableMousewheelControl(), b.params.a11y && b.a11y && b.a11y.destroy(), b.emit("onDestroy"), e !== !1 && (b = null)
            }, b.init(), b
        }
    };
    n.prototype = {
        isSafari: function () {
            var e = navigator.userAgent.toLowerCase();
            return e.indexOf("safari") >= 0 && e.indexOf("chrome") < 0 && e.indexOf("android") < 0
        }(),
        isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(navigator.userAgent),
        isArray: function (e) {
            return "[object Array]" === Object.prototype.toString.apply(e)
        },
        browser: {
            ie: window.navigator.pointerEnabled || window.navigator.msPointerEnabled,
            ieTouch: window.navigator.msPointerEnabled && window.navigator.msMaxTouchPoints > 1 || window.navigator.pointerEnabled && window.navigator.maxTouchPoints > 1
        },
        device: function () {
            var e = navigator.userAgent, t = e.match(/(Android);?[\s\/]+([\d.]+)?/), n = e.match(/(iPad).*OS\s([\d_]+)/), i = e.match(/(iPod)(.*OS\s([\d_]+))?/), o = !n && e.match(/(iPhone\sOS)\s([\d_]+)/);
            return {ios: n || o || i, android: t}
        }(),
        support: {
            touch: window.Modernizr && Modernizr.touch === !0 || function () {
                return !!("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch)
            }(), transforms3d: window.Modernizr && Modernizr.csstransforms3d === !0 || function () {
                var e = document.createElement("div").style;
                return "webkitPerspective" in e || "MozPerspective" in e || "OPerspective" in e || "MsPerspective" in e || "perspective" in e
            }(), flexbox: function () {
                for (var e = document.createElement("div").style, t = "alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "), n = 0; n < t.length; n++)if (t[n] in e)return !0
            }(), observer: function () {
                return "MutationObserver" in window || "WebkitMutationObserver" in window
            }()
        },
        plugins: {}
    };
    for (var i = ["jQuery", "Zepto", "Dom7"], o = 0; o < i.length; o++)window[i[o]] && e(window[i[o]]);
    var a;
    a = "undefined" == typeof Dom7 ? window.Dom7 || window.Zepto || window.jQuery : Dom7, a && ("transitionEnd" in a.fn || (a.fn.transitionEnd = function (e) {
        function t(a) {
            if (a.target === this)for (e.call(this, a), n = 0; n < i.length; n++)o.off(i[n], t)
        }

        var n, i = ["webkitTransitionEnd", "transitionend", "oTransitionEnd", "MSTransitionEnd", "msTransitionEnd"], o = this;
        if (e)for (n = 0; n < i.length; n++)o.on(i[n], t);
        return this
    }), "transform" in a.fn || (a.fn.transform = function (e) {
        for (var t = 0; t < this.length; t++) {
            var n = this[t].style;
            n.webkitTransform = n.MsTransform = n.msTransform = n.MozTransform = n.OTransform = n.transform = e
        }
        return this
    }), "transition" in a.fn || (a.fn.transition = function (e) {
        "string" != typeof e && (e += "ms");
        for (var t = 0; t < this.length; t++) {
            var n = this[t].style;
            n.webkitTransitionDuration = n.MsTransitionDuration = n.msTransitionDuration = n.MozTransitionDuration = n.OTransitionDuration = n.transitionDuration = e
        }
        return this
    })), window.Swiper = n
}(), "undefined" != typeof module ? module.exports = window.Swiper : "function" == typeof define && define.amd && define([], function () {
    return window.Swiper
});