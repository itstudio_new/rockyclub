$(document).ready(function(){
    $(".openModal").on("click", function (e) {
        e.preventDefault();
        var modal = $(this).attr("href");
        $(modal).fadeIn();
        $("body").addClass("no-scroll")
    });
    if($("#modalSuccess").length > 0){
        $("#modalSuccess").fadeIn();
        $("body").addClass("no-scroll")
    }

    $(".filterTrainer").change(function(){
        var value = $(this).val();
        switch($(this).attr("name")){
            case "city": {
                $.ajax({
                    url:"/trenery/getClubs",
                    type:"GET",
                    dataType:"JSON",
                    data:{
                       city:value,
                    },
                    success:function(data){
                        $("#club.filterTrainer").html(data.message);
                    }
                })
                break;
            }
            case "club": {
                if(value){
                    location.href = "/trenery/?club="+value;
                }
                break;
            }
        }
    });

})

$(document).ready(function(){
    $(".filterClub").change(function(){
        var value = $(this).val();
        switch($(this).attr("name")){
            case "city": {
                $.ajax({
                    url:"/trenery/getClubs",
                    type:"GET",
                    dataType:"JSON",
                    data:{
                        city:value,
                    },
                    success:function(data){
                        $("#club.filterClub").html(data.message);
                    }
                })
                break;
            }
            case "club": {
                if(value){
                    location.href = "/vashClub/?club="+value;
                }
                break;
            }
        }
    });
})

var feValidation = function(){
    if($("form[id^='validate']").length > 0){

        // Validation prefix for custom form elements
        var prefix = "valPref_";

        //Add prefix to Bootstrap select plugin
        $("form[id^='validate'] .select").each(function(){
            $(this).next("div.bootstrap-select").attr("id", prefix + $(this).attr("id")).removeClass("validate[required]");
        });


        // Validation Engine init
        $("form[id^='validate']").validationEngine('attach', {
            promptPosition : "bottomLeft",
            scroll: false,
            autoHidePrompt: true,
            autoHideDelay: 3000,
            binded : false,

        });
    }
}//END Validation Engine

$(document).ready(function(){
    $(".filterShedule").change(function(){
        var value = $(this).val();
        switch($(this).attr("name")){
            case "city": {
                $.ajax({
                    url:"/raspisanie/getList",
                    type:"GET",
                    dataType:"JSON",
                    data:{
                        field:"city",
                        value:value,
                    },
                    success:function(data){
                        $("#club.filterShedule").html(data.message);
                        $("#hall.filterShedule").html("<option>Выберите зал</option>");
                    }
                })
                break;
            }
            case "club": {
                $.ajax({
                    url:"/raspisanie/getList",
                    type:"GET",
                    dataType:"JSON",
                    data:{
                        field:"club",
                        value:value,
                    },
                    success:function(data){
                        $("#hall.filterShedule").html(data.message);
                    }
                })
                break;
            }
            case "hall": {
                if(value){
                    location.href = "/raspisanie/?hall="+value;
                }
                break;
            }
        }
    });
    feValidation();
})