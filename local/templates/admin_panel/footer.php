			<div class="message-box message-box-danger animated fadeIn" id="dellbox"></div>
	</div>
</div>
</div>
        </div>

    </div>
</div>


<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title">
                <span class="fa fa-sign-out"></span> Выход ?</div>
            <div class="mb-content">
                <p>Вы уверены что хотите выйти?</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <a href="?logout=yes" class="btn btn-success btn-lg">Да</a>
                    <button class="btn btn-default btn-lg mb-control-close">Нет</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#frame_right').click(function(){
            $.ajax({
                url: '<?= $this->createUrl('admin/users/changeNavRight');?>',
                type: "POST",
                dataType: 'JSON'
            });
        });

        $('#nav_left').click(function(){
            $.ajax({
                url: '<?= $this->createUrl('admin/users/changeNavLeft');?>',
                type: "POST",
                dataType: 'JSON'
            });
        });
    });
</script>

</body>
</html>